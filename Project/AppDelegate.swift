//
//  AppDelegate.swift
//  Zazoo
//
//  Created by HTNaresh on 15/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//  com.Innovasolution.myzazoo  // com.innovasolutions.Zaazoo

import UIKit
import Fabric
import Crashlytics
import FBSDKCoreKit
import GoogleSignIn
import Firebase
import UserNotifications

//Zazoo Account:
//lama@myzazoo.com
//Zazooadmin1
//fxim-gymp-epub-odmy

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    var storyboard : UIStoryboard!
    var navigationController = UINavigationController()
    
    var UUIDValue = String()
    var TokenValue = String()
    var NotiTokenString = String()
    
    fileprivate var reach:Reachability!
    var networkStatus = Reachability()!
    var isInternetConnected: Bool!
    var isFromSharing: Bool = false
    
    var NoInternet : NoInternetVC! = nil
    
    let notificationCenter = UNUserNotificationCenter.current()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool
    {
        // Override point for customization after application launch.
        
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        
        isInternetConnected = true
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        UUIDValue = UIDevice.current.identifierForVendor!.uuidString
        
        
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        
        notificationCenter.requestAuthorization(options: [.alert, .sound]) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
        
        IQKeyboardManager.shared.enable = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reach)
        do
        {
            self.reach = Reachability()
            try reach.startNotifier()
        }
        catch
        {
            print("could not start reachability notifier")
        }
        
        if(isInternetConnected == true)
        {
            removesubview()
        }
        
        
        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisFirstTime)) == nil)
        {
            UserDefaults.standard.set(false, forKey: "arabic")
            UserDefaults.standard.set(false, forKey: GlobalConstants.kisLogin)
            UserDefaults.standard.synchronize()
            self.perform(#selector(AppDelegate.WSCallFordeviceRegistration), with: self, afterDelay: 0.4)
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = "175124988501-mhte2p7t1vtsv44phcdm0qlqo6v52slj.apps.googleusercontent.com"
        
        
        notificationCenter.delegate = self
        //        self.scheduleNotification(title: "Cooking Classes" , body:"You have classes in next 15 min" , strdate: "24/09/2019 01:25 PM")
        
        let storyboard1 : UIStoryboard!
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }
        
        let home = storyboard1.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        
        
        
        return true
    }
    
    func scheduleNotification(title: String,body:String,strdate:String)
    {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "notifi.mpeg"))
        let strdate = strdate
        let dateformater = DateFormatter()
        dateformater.dateFormat = "dd/MM/yyyy hh:mm a"
        let date1 = dateformater.date(from: strdate)
        let time = 300 //(Manager.sharedInstance.RemindNotificationTime * 60)
        let date = date1?.addingTimeInterval(TimeInterval(-time))
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date!)

        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: false)
        
        let identifier = "Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
    {
        if UserDefaults.standard.bool(forKey: "fb")
        {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[.annotation])
        }
        else if UserDefaults.standard.bool(forKey: "google")
        {
            return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[.annotation])
        }
        else
        {
            print("\(options)")
            
            let str = url.absoluteString
            if str.contains("ItemDetail.aspx")
            {
                isFromSharing = true
//                let str2 = (str as NSString).lastPathComponent
                
                let arrSeparate = str.components(separatedBy: "&")
                if arrSeparate.count == 3
                {
                    let ClassId = arrSeparate[0]
                    let itemId = arrSeparate[1]
                    let bundleItem = arrSeparate[2]
                    
                    let strItemId = itemId
                        .components(separatedBy:CharacterSet.decimalDigits.inverted)
                        .joined(separator: "")
                    
                    print("Decoded: \(strItemId)")
                    
                    let newString = ClassId
                        .components(separatedBy:CharacterSet.decimalDigits.inverted)
                        .joined(separator: "")
                    
                    print("Decoded: \(newString)")
                    
                    let  strBundleItem = bundleItem
                        .components(separatedBy:CharacterSet.decimalDigits.inverted)
                        .joined(separator: "")
                    
                    print("Decoded: \(strBundleItem)")
                    
                    
                    let storyboard1 : UIStoryboard!
                    
                    if UserDefaults.standard.bool(forKey: "arabic")
                    {
                        UIView.appearance().semanticContentAttribute = .forceRightToLeft
                        storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
                    }
                    else
                    {
                        UIView.appearance().semanticContentAttribute = .forceLeftToRight
                        storyboard1 = UIStoryboard(name: "Main", bundle: nil)
                    }
                    
                    if Int(strItemId)  == 5
                    {
                        let home = storyboard1.instantiateViewController(withIdentifier: "PartyDetailVC") as! PartyDetailVC
                        home.strcomeFrom = "push"
                        if let Id = Int(newString) {
                            let Catid = NSNumber(value:Id)
                            home.classId = Catid
                        }
                        let navigationController = UINavigationController()
                        navigationController.isNavigationBarHidden = true
                        navigationController.setViewControllers([home], animated: false)
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = navigationController
                    }
                    else if Int(strBundleItem)  == 1
                    {
                        let home = storyboard1.instantiateViewController(withIdentifier: "ClassDetail1VC") as! ClassDetail1VC
                        home.strcomeFromPush = "push"
                        if let Id = Int(newString) {
                            let Catid = NSNumber(value:Id)
                            home.classId = Catid
                        }
                        home.isBundleItem = true
                        let navigationController = UINavigationController()
                        navigationController.isNavigationBarHidden = true
                        navigationController.setViewControllers([home], animated: false)
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = navigationController
                    }
                    else
                    {
                        let home = storyboard1.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
                        home.strcomeFromPush = "push"
                        if let Id = Int(newString) {
                            let Catid = NSNumber(value:Id)
                            home.classId = Catid
                        }
                        let navigationController = UINavigationController()
                        navigationController.isNavigationBarHidden = true
                        navigationController.setViewControllers([home], animated: false)
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = navigationController
                    }
                }
            }
            return true
        }
        
    }
    
   
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var notif = String()
        for i in 0..<deviceToken.count {
            notif += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        NotiTokenString = notif
        print("NotiTokenString::\(NotiTokenString)")
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisFirstTime)) == nil)
        {
            self.perform(#selector(WSCallFordeviceRegistration), with: self, afterDelay:0.0)
        }
    }
    
   
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        if let aps = userInfo["aps"] as? NSDictionary
        {
            let dict = aps
            let storyboard1 : UIStoryboard!
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            }
            
            if let classId = dict["flex4"] as? NSNumber
            {
                let home = storyboard1.instantiateViewController(withIdentifier: "PartyDetailVC") as! PartyDetailVC
                home.strcomeFrom = "push"
                home.classId = classId
                let navigationController = UINavigationController()
                navigationController.isNavigationBarHidden = true
                navigationController.setViewControllers([home], animated: false)
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigationController
            }
            else if let classId = dict["flex3"] as? NSNumber
            {
                let home = storyboard1.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
                home.strcomeFromPush = "push"
                home.classId = classId
                let navigationController = UINavigationController()
                navigationController.isNavigationBarHidden = true
                navigationController.setViewControllers([home], animated: false)
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigationController
            }
                
            else if let vendorId = dict["flex2"] as? NSNumber
            {
                let home = storyboard1.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
                home.strcomeFrom = "push"
                home.VendorId = vendorId
                let navigationController = UINavigationController()
                navigationController.isNavigationBarHidden = true
                navigationController.setViewControllers([home], animated: false)
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigationController
            }
            else
            {
                let home = storyboard1.instantiateViewController(withIdentifier: "NotificationDetailVC") as! NotificationDetailVC
                home.strcomeFrom = "push"
                home.dictdata = [:]
                home.pushNotId = (dict["flex1"] as? NSNumber)!
                let navigationController = UINavigationController()
                navigationController.isNavigationBarHidden = true
                navigationController.setViewControllers([home], animated: false)
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigationController
            }
        }
    }
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])
//    {
//        if let aps = userInfo["aps"] as? NSDictionary
//        {
//            let dict = aps
//            let storyboard1 : UIStoryboard!
//            if UserDefaults.standard.bool(forKey: "arabic")
//            {
//                UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard,, bundle: nil)
//            }
//            else
//            {
//                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//                storyboard1 = UIStoryboard(name: "Main", bundle: nil)
//            }
//
//            if let classId = dict["flex3"] as? NSNumber
//            {
//                let home = storyboard1.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
//                home.strcomeFromPush = "push"
//                home.classId = classId
//                let navigationController = UINavigationController()
//                navigationController.isNavigationBarHidden = true
//                navigationController.setViewControllers([home], animated: false)
//                let window = UIApplication.shared.delegate!.window!!
//                window.rootViewController = navigationController
//            }
//
//            else if let vendorId = dict["flex2"] as? NSNumber
//            {
//                let home = storyboard1.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
//                home.strcomeFrom = "push"
//                home.VendorId = vendorId
//                let navigationController = UINavigationController()
//                navigationController.isNavigationBarHidden = true
//                navigationController.setViewControllers([home], animated: false)
//                let window = UIApplication.shared.delegate!.window!!
//                window.rootViewController = navigationController
//            }
//            else
//            {
//                let home = storyboard1.instantiateViewController(withIdentifier: "NotificationDetailVC") as! NotificationDetailVC
//                home.strcomeFrom = "push"
//                home.dictdata = [:]
//                home.pushNotId = (dict["flex1"] as? NSNumber)!
//                let navigationController = UINavigationController()
//                navigationController.isNavigationBarHidden = true
//                navigationController.setViewControllers([home], animated: false)
//                let window = UIApplication.shared.delegate!.window!!
//                window.rootViewController = navigationController
//            }
//        }
//    }
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        <#code#>
//    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        SwiftLoggor.writeToFile(message:SwiftLoggor.fileContent)

    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {

        return .portrait
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    @objc func reachabilityChanged(note: Notification)
    {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection
        {
        case .wifi:
            print("Reachable via WiFi")
            isInternetConnected = true
            removesubview()
            
        case .cellular:
            print("Reachable via Cellular")
            isInternetConnected = true
            removesubview()
            
            
        case .none:
            print("Network not reachable")
            self.isInternetConnected = false
            
            if UserDefaults.standard.bool(forKey: "arabic") == true
            {
                self.storyboard = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            }
            else
            {
                self.storyboard = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            }
            self.NoInternet = self.storyboard.instantiateViewController(withIdentifier: "NoInternetVC") as? NoInternetVC
            
            self.NoInternet.view.tag = 2304
            DispatchQueue.main.async(execute: { () -> Void in
                
                self.window?.addSubview(self.NoInternet.view)
                self.NoInternet.view.translatesAutoresizingMaskIntoConstraints = false
                
                self.window!.addConstraints(
                    [NSLayoutConstraint(item: self.NoInternet.view!, attribute: .top, relatedBy: .equal,toItem: self.window, attribute: .top, multiplier: 1.0, constant: 0.0),
                     NSLayoutConstraint(item:self.NoInternet.view!, attribute: .left, relatedBy: .equal, toItem: self.window, attribute: .left, multiplier: 1.0, constant: 0.0),
                     NSLayoutConstraint(item:self.NoInternet.view!, attribute: .bottom, relatedBy: .equal, toItem: self.window, attribute: .bottom, multiplier: 1.0, constant: 0.0),
                     NSLayoutConstraint(item:self.NoInternet.view!, attribute: .right, relatedBy: .equal, toItem: self.window, attribute: .right, multiplier: 1.0, constant: 0.0)])
            })
            
        }
    }
    
    func removesubview()
    {
        if( isInternetConnected == true)
        {
            let subviews: NSArray = self.window!.subviews as NSArray
            for id in subviews
            {
                let view:UIView   =  id as! UIView
                if(view.tag == 2304)
                {
                    DispatchQueue.main.async(execute: {
                        (id as AnyObject).removeFromSuperview()
                        print("Removed")

                    })
                }
            }
        }
    }
    
    @objc func WSCallFordeviceRegistration()
    {
        var base64Encoded:String = ""
        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey)) == nil)
        {
            UserDefaults.standard.set(false, forKey: "arabic")
            
            let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            let allowedCharsCount = UInt32(allowedChars.count)
            var randomString = ""
            
            for _ in (0..<16) {
                let randomNum = Int(arc4random_uniform(allowedCharsCount))
                let newCharacter = allowedChars[allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)]
                randomString += String(newCharacter)
            }
            let str = randomString
            let utf8str = str.data(using: String.Encoding.utf8)
            base64Encoded = (utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)))! as String
            UserDefaults.standard.set(base64Encoded, forKey: GlobalConstants.kStoreKey)
            UserDefaults.standard.synchronize()
        }
        else
        {
            base64Encoded = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String
        }
        UUIDValue = UIDevice.current.identifierForVendor!.uuidString
        
        let tokenstr = String(format: "%@",NotiTokenString)
        
        let dictPara:NSDictionary = ["DeviceType":"1","UniqueDeviceId":UUIDValue,"NotificationToken": (tokenstr != "") ? NotiTokenString : "1234","ClientKey":base64Encoded,"Preferred_Language":"E"]
        let strUrl:String = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRegisterDevice)
        //  print(strUrl)
        print(dictPara)
        WebServiceManager.sharedInstance.postWebServiceWithoutHeaderAlamofire(strUrl, parameter: dictPara as! Dictionary<String, String> as Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "")
        { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String

                    if strStatus == "Success"
                    {
                        if (tokenstr != "")
                        {
                            let splash1 = UIApplication.topViewController() as! UIViewController
                            if (splash1 is SplashVC)
                            {
                                let splash2 = splash1 as? SplashVC
                                splash2?.playVideo()
                            }
                        }
                    }
            }
        }
    }
}

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        if let aps = response.notification.request.content.userInfo["aps"] as? NSDictionary
        {
            let dict = aps
            let storyboard1 : UIStoryboard!
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                storyboard1 = UIStoryboard(name: "Main", bundle: nil)
            }
            
            if let classId = dict["flex3"] as? NSNumber
            {
                
                if  dict["flex4"] as? NSNumber == 5
                {
                    let home = storyboard1.instantiateViewController(withIdentifier: "PartyDetailVC") as! PartyDetailVC
                    home.strcomeFrom = "push"
                    home.classId = classId
                    let navigationController = UINavigationController()
                    navigationController.isNavigationBarHidden = true
                    navigationController.setViewControllers([home], animated: false)
                    let window = UIApplication.shared.delegate!.window!!
                    window.rootViewController = navigationController
                }
                else
                {
                    let home = storyboard1.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
                    home.strcomeFromPush = "push"
                    home.classId = classId
                    let navigationController = UINavigationController()
                    navigationController.isNavigationBarHidden = true
                    navigationController.setViewControllers([home], animated: false)
                    let window = UIApplication.shared.delegate!.window!!
                    window.rootViewController = navigationController
                }
                
            }
                
            else if let vendorId = dict["flex2"] as? NSNumber
            {
                let home = storyboard1.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
                home.strcomeFrom = "push"
                home.VendorId = vendorId
                let navigationController = UINavigationController()
                navigationController.isNavigationBarHidden = true
                navigationController.setViewControllers([home], animated: false)
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigationController
            }
            else
            {
                let home = storyboard1.instantiateViewController(withIdentifier: "NotificationDetailVC") as! NotificationDetailVC
                home.strcomeFrom = "push"
                home.dictdata = [:]
                home.pushNotId = dict["flex1"] as? NSNumber ?? 0
                let navigationController = UINavigationController()
                navigationController.isNavigationBarHidden = true
                navigationController.setViewControllers([home], animated: false)
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigationController
            }
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}

