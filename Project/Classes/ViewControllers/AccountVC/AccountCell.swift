//
//  AccountCell.swift
//  Project
//
//  Created by HTNaresh on 10/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import EventKit

class AccountCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArw: UIImageView!
    @IBOutlet weak var sgmt: UISegmentedControl!
    @IBOutlet weak var tblClassList: UITableView!
    
    @IBOutlet weak var conTblClassHeight: NSLayoutConstraint!
    
    
    var arrList = [[String : Any]]()
    
    var arrCalendars = [String]()


    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        view1.layer.cornerRadius = 3.0
        view1.clipsToBounds = true
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "childCell"
        
        let cell:childCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! childCell
        
        let dict = arrList[indexPath.row]
        cell.lblDetail.text = dict["Class_Detail"] as? String

        let strcode = dict["Color_Code"] as! String
        cell.viewBg.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: strcode)
        
        cell.btnCalendar.tag = indexPath.row
        cell.btnCalendar.addTarget(self, action: #selector(addtoEvent), for: .touchUpInside)

        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            conTblClassHeight.constant = CGFloat(arrList.count * 60)

        }
        else
        {
            conTblClassHeight.constant = CGFloat(arrList.count * 48)

        }
        tblClassList.updateConstraintsIfNeeded()
        tblClassList.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return 60
        }
        else
        {
            return 48
        }
    }
    
    @objc func addtoEvent(_ sender: Any)
    {
        let btn = sender as! UIButton
        let dict = arrList[btn.tag]
        
        let eventStore = EKEventStore()
        
        // 2
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            insertEvent(store: eventStore, dict: dict)
        case .denied:
            print("Access denied")
        case .notDetermined:
            // 3
            eventStore.requestAccess(to: .event, completion:
                {[weak self] (granted: Bool, error: Error?) -> Void in
                    if granted {
                        self!.insertEvent(store: eventStore, dict: dict)
                    } else {
                        print("Access denied")
                    }
            })
        default:
            print("Case default")
        }
    }
    
    func insertEvent(store: EKEventStore , dict:[String:Any])
    {
        arrCalendars.removeAll()
        
        let calendars = store.calendars(for: .event)

        for calendar:EKCalendar in calendars {
                
              arrCalendars.append(calendar.title)
            }
        
        
        var strMsg = ""
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            strMsg = "في أي التقويم تريد إضافة تذكير هذا الفصل؟"
        }
        else
        {
            strMsg = "In which Calendar do you want to add this class reminder ?"

        }
        
       
        SRPopview.show(withValues: arrCalendars, heading: strMsg) {
            (result) in
            switch result{
            case .notPicked:
                print("Didnt pick any")
            case .picked(let str, let index):
                print("Picked at \(index) \n Picked item : \(str)")
                
                for calendar:EKCalendar in calendars {
                    
                    if calendar.title == str {
                        
                        let info = dict["Class_Info"] as! [String:Any]
                        
                        let arrSelectedDates = info["SelectedDates"] as! [String]
                        for i in 0..<arrSelectedDates.count
                        {
                            var eventAlreadyExists = false
                            
                            let formattt = DateFormatter()
                            formattt.dateFormat = "dd/MM/yyyy hh:mm a"
                            
                            //            let date1 = formattt.date(from: String(format: "%@ %@",(info["Start_Date"] as? String)!, (info["Start_Time"] as? String)! ))
                            //            let date2 = formattt.date(from: String(format: "%@ %@",(info["Start_Date"] as? String)!, (info["End_Time"] as? String)! ))
                            
                            let date1 = formattt.date(from: String(format: "%@ %@",arrSelectedDates[i], (info["Start_Time"] as? String ?? "")! ))
                            let date2 = formattt.date(from: String(format: "%@ %@",arrSelectedDates[i], (info["End_Time"] as? String ?? "")! ))
                            
                            let startDate =  date1
                            let endDate =  date2
                            
                            let event = EKEvent(eventStore: store)
                            event.calendar = store.calendar(withIdentifier: calendar.calendarIdentifier)
                            
                            event.title = info["Class_Name"] as? String
                            event.startDate = startDate
                            event.endDate = endDate
                            
                            let predicate = store.predicateForEvents(withStart: startDate!, end: endDate!, calendars: [calendar])
                            let existingEvents = store.events(matching: predicate)
                            print(existingEvents)
                            
                            for singleEvent in existingEvents
                            {
                                if singleEvent.title == event.title && singleEvent.startDate == event.startDate
                                {
                                    eventAlreadyExists = true
                                    break
                                }
                            }
                            
                            if eventAlreadyExists
                            {
                                Manager.sharedInstance.showSmallAlert("Event already exist.")
                                return
                            }
                            
                            
                            do
                            {
                                try store.save(event, span: .thisEvent)
                            }
                            catch
                            {
                                Manager.sharedInstance.showSmallAlert("Error saving event in calendar")
                            }
                        }

                    }
                    
                }
                
                Manager.sharedInstance.showSmallAlert("Event added to device calender")
            }
            
        }
        
        

    }

}
