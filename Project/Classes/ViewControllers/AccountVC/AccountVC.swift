//
//  AccountVC.swift
//  Project
//
//  Created by HTNaresh on 4/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class AccountVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var tblAccount: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var conTblAccountHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewUserHeight: NSLayoutConstraint!
    @IBOutlet weak var lblVersion: UILabel!

    var arrlist = [[String:String]]()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.lblUserName.text = ""
        
        imgProfile.layer.cornerRadius = 37
        imgProfile.clipsToBounds = true
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        let infoDict = Bundle.main.infoDictionary
        lblVersion.text
            = String(format: "Version : %@", infoDict?["CFBundleShortVersionString"] as! CVarArg)

        
    }
    
    override func viewWillLayoutSubviews()
    {
        if Manager.sharedInstance.dictprofile.allKeys.count > 0
        {
            self.lblUserName.text =  Manager.sharedInstance.dictprofile.value(forKey: "FirstName") as? String
        }

    }
    
//    override func viewDidAppear(_ animated: Bool)
//    {
//        if Manager.sharedInstance.dictprofile.allKeys.count > 0
//        {
//            self.lblUserName.text =  Manager.sharedInstance.dictprofile.value(forKey: "FirstName") as? String
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "2")

        if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
        {
            arrlist = [["name":kpersonal,"image":""],["name":kChildinfo,"image":""],["name":kMyAddress,"image":""],["name":kMyOrder,"image":""],["name":kChangePW,"image":""],["name":kNotification,"image":""],["name":klang,"image":""],["name":klogout,"image":""]]
            
            conviewUserHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 200 : 120
            
        }
        else
        {
            arrlist = [["name":klang,"image":""],["name":kLogin,"image":""]]
            
            conviewUserHeight.constant = 0
        }
        
        self.tblAccount.reloadData()
    }
    
    
    
    //MARK:- Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "AccountCell"
        let cell:AccountCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! AccountCell
        
        let dict =  arrlist[indexPath.row] 
        
        cell.lblTitle.text = dict["name"]
        cell.sgmt.isHidden = true
        cell.imgArw.isHidden = false
        
        if dict["name"] == klang
        {
            cell.imgArw.isHidden = true
            cell.sgmt.isHidden = false
        }
        else if dict["name"] == klogout
        {
            cell.imgArw.isHidden = true
        }
        
        tblAccount.updateConstraints()
        tblAccount.layoutIfNeeded()
        conTblAccountHeight.constant = tblAccount.contentSize.height
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
        {
            let dict =  arrlist[indexPath.row]

            if dict["name"] == klogout
            {
                let alertController = UIAlertController(title:kWarningEn, message:kLogoutalert, preferredStyle: .alert)
        
                // Create the actions
                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
        
                    self.callWsLogout()
        
                }
        
                let CancelAction = UIAlertAction(title:kCancel, style: .default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(CancelAction)
        
                // Present the controller
                alertController.show()

            }
            else if dict["name"] == kpersonal
            {
                let login = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetailVC") as! ProfileDetailVC
                self.navigationController?.pushViewController(login, animated: true)
            }
            else if dict["name"] == kChildinfo
            {
                let login = self.storyboard?.instantiateViewController(withIdentifier: "AddChildVC") as! AddChildVC
                login.strComeFrom = ""
                self.navigationController?.pushViewController(login, animated: true)
            }
            else if dict["name"] == kMyAddress
            {
                let login = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
                self.navigationController?.pushViewController(login, animated: true)
            }
            else if dict["name"] == kMyOrder
            {
                let login = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
                self.navigationController?.pushViewController(login, animated: true)
            }
            else if dict["name"] == kNotification
            {
                let login = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                self.navigationController?.pushViewController(login, animated: true)
            }
            else if dict["name"] == kChangePW
            {
                let login = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                self.navigationController?.pushViewController(login, animated: true)
            }
            
        }
        else
        {
            if indexPath.row == 1
            {
                let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                login.strComeFrom = "account"
                self.navigationController?.pushViewController(login, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let dict =  arrlist[indexPath.row]

        if dict["name"] == klang
        {
            return 0
        }
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return 75
        }
        else
        {
            return 55

        }

    }
    
    
  
    //MARK:- button Methods
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    

    //MARK: -Logout Api

    func callWsLogout()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var strToken = appDelegate.UUIDValue
        if strToken == ""
        {
            strToken = "1234"
        }

        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wLogout,strToken)

        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {

                    print(result)
                    let strStatus = result["Message"] as? String

                    if strStatus == "Success"
                    {
                        Manager.sharedInstance.wscallforRemoveCart()
                        UserDefaults.standard.set(false, forKey: GlobalConstants.kisLogin)
                      UserDefaults.standard.removeObject(forKey: "userid")
                        UserDefaults.standard.synchronize()
                        
                        
                        let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        
                        home.strComeFrom = "account"
                        
                        let navigationController = UINavigationController()
                        navigationController.isNavigationBarHidden = true
                        
                        navigationController.setViewControllers([home], animated: false)
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = navigationController
                        

                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)

                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }

                        // Add the actions
                        alertController.addAction(okAction)

                        // Present the controller
                        alertController.show()
                    }

            }
        }
    }
    
}
