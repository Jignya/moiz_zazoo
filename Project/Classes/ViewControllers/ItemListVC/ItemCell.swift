//
//  ItemCell.swift
//  Zazoo
//
//  Created by HTNaresh on 31/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell
{
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var btnfav: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var btnBuyNow: UIButton!
    @IBOutlet weak var viewsold: UIView!

    
    override func awakeFromNib()
    {
        btnBuyNow.setTitle(kBuyNow, for: .normal)
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }
    
}
