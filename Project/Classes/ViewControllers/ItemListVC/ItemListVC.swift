//
//  SubcategoryVC.swift
//  Zazoo
//
//  Created by HTNaresh on 23/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class ItemListVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionItem: UICollectionView!
    @IBOutlet weak var lblNoRecord: UILabel!

    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var viewCart: UIView!
    
    
    @IBOutlet weak var btnCloseSearch: UIButton!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var view1: UIView!

    var arrItem : NSMutableArray = []
    var arrItemOriginal : NSMutableArray = []

    var CatId : NSNumber!
    var SubcatId : NSNumber!

    var strTitle : String!
    var search:String = ""

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblTitle.text = strTitle
        view1.layer.cornerRadius = 3.0
        self.viewSearch.isHidden = true
//        wscallforItemlist()

        txtSearch.placeholder = kSearch

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "0")


//        let strCount = Manager.sharedInstance.CartCount as! String
//        if strCount == "0"
//        {
//            viewCart.isHidden = true
//        }
//        else
//        {
//            viewCart.isHidden = false
//            lblCartCount.text = Manager.sharedInstance.CartCount
//        }

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 10
//        return arrItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
            let cellReuseIdentifier = "ItemCell"
            let cell:ItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! ItemCell
            
            
//            let dict = NSMutableDictionary(dictionary: arrItem[indexPath.row] as! Dictionary)
//
//
//            let availqty : Int = dict["Available_Qty"] as! Int
//            if availqty == 0
//            {
//                cell.viewsold.isHidden = false
//            }
//            else
//            {
//                cell.viewsold.isHidden = true
//            }
//
//
//            cell.lblItemName.text = dict["Item_Name"] as? String
//            var Stringimg = String()
//            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as! String)
//
//            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//            let url = URL(string: urlString!)
//            cell.imgItem.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
//
//        cell.lblBrand.text = dict["Vendor_Name"] as? String
//
//        cell.btnfav.isSelected = false
//        if (dict["Is_Favorite"] as? NSNumber)! == 1
//        {
//            cell.btnfav.isSelected = true
//        }
//
//
//
//        if let n = dict.value(forKey: "Special_Price") as? NSNumber
//        {
//            let f = n.floatValue
//            cell.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, f)
//
//        }
//        else
//        {
//            cell.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, (dict["Special_Price"] as? Float)!)
//        }
//
//        if (dict["Dis_Percent"] as? NSNumber)! == 0
//        {
//           cell.lblDiscount.text = ""
//           cell.lblDiscountPrice.text = ""
//
//        }
//        else
//        {
//            cell.lblDiscount.text = String(format: "%@ %@", (dict["Dis_Percent"] as? NSNumber)! , "% OFF")
//
//            let strike : String!
//
//            if let n = dict.value(forKey: "Primary_Price") as? NSNumber
//            {
//                let f = n.floatValue
//                strike = String(format: "%@ %.3f",kCurrency, f)
//
//
//            }
//            else
//            {
//                strike = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float)!)
//
//            }
//
//            cell.lblDiscountPrice.attributedText = Manager.sharedInstance.StrikeThroughString(str: strike)
//        }
        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        return cell
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
//    {
//        let dict = NSMutableDictionary(dictionary: arrItem[indexPath.row] as! Dictionary)
//
//        if dict["Deal_Id"] as! NSNumber == 0
//        {
//            let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
//
//            subCatvc.CatId = dict["Item_Id"] as! NSNumber
//            subCatvc.strTitle = strTitle
//            subCatvc.strfrom = "list"
//            subCatvc.wscallforItemDetail()
//
//            self.navigationController?.pushViewController(subCatvc, animated: true)
//            return
//        }
//
//        let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
//
//        subCatvc.CatId = dict["Deal_Id"] as! NSNumber
//        subCatvc.strTitle = strTitle
//        subCatvc.strfrom = "list"
//
//        subCatvc.wscallforsaleItemDetail()
//
//       self.navigationController?.pushViewController(subCatvc, animated: true)
//    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            let width:CGFloat = ((self.view.frame.size.width - 20) / 2) - 5
            
//            let height:CGFloat = ((self.view.frame.size.height - 100) / 2) - 10
        
            let cellSize = CGSize(width:width , height:310)
            return cellSize
    }
    
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool)
    {
        
    }
    

    
   
  
    
    //MARK:- Button Click
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController

    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnCartClick(_ sender: Any)
    {
        let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        
        self.navigationController?.pushViewController(subCatvc, animated: true)
    }
    
    @IBAction func btnFavClick(_ sender: Any)
    {
       
    }
    
    @IBAction func btnSearchClick(_ sender: Any)
    {
        self.viewSearch.isHidden = false
    }
    
    @IBAction func btnCloseSearchClick(_ sender: Any)
    {
        self.viewSearch.isHidden = true
        txtSearch.text = ""
        collectionItem.isHidden = false
        txtSearch.resignFirstResponder()
        arrItem = NSMutableArray(array: arrItemOriginal)
        collectionItem.reloadData()
    }
    
    
    // MARK:- textfield delegate
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField.text?.isEmpty)!
        {
            arrItem = NSMutableArray(array: arrItemOriginal)
            collectionItem.isHidden = false
            collectionItem.reloadData()
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if  string.isEmpty   //string
        {
            search = String(search.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        
        let arrSubCatTemp = arrItemOriginal.mutableCopy() as! [[String:Any]]
        
        let arrdata  = arrSubCatTemp.filter{
            let firstName = ($0["Item_Name"]! as AnyObject).lowercased
            
            return firstName?.range(of: search.lowercased()) != nil
            
        }
        
        arrItem = NSMutableArray(array: arrdata)
        
        if arrItem.count > 0
        {
            collectionItem.isHidden = false
            collectionItem.reloadData()
        }
            
        else
        {
            if(search.isEmpty)
            {
                txtSearch.text = ""
                collectionItem.isHidden = false
                textField.resignFirstResponder()
                arrItem = NSMutableArray(array: arrItemOriginal)
                collectionItem.reloadData()
            }
            else
            {
                collectionItem.isHidden = true
            }
            
        }
        return true
    }
    
    //MARK:- Webservice
    
    func wscallforItemlist()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetItems,CatId)
        
//        ,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E"

        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
//                    print(result)
                    let strStatus = result["Message"] as? String
                    if strStatus == "Success"
                    {
                        self.arrItem = NSMutableArray(array: result["Items"] as! Array)
                        self.arrItemOriginal = self.arrItem.mutableCopy() as! NSMutableArray
                        if self.arrItem.count > 0
                        {
                            
                            self.collectionItem.isHidden = false
                        }
                        else
                        {
                            self.collectionItem.isHidden = true
                        }
                        self.collectionItem.reloadData()
                    }
                    else
                    {
                        
                    }
            }
        }
    }
}





