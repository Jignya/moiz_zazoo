//
//  CartCell.swift
//  Project
//
//  Created by HTNaresh on 29/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell
{
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblvendor: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var viewsold: UIView!
    @IBOutlet weak var lblSoldout: UIView!

    @IBOutlet weak var btndelete: UIButton!

    @IBOutlet weak var btnCheckbox: UIButton!
    @IBOutlet weak var conViewCheckWidth: NSLayoutConstraint!

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblActivity: UILabel!

    @IBOutlet weak var lblDates: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if lblSoldout != nil
        {
            lblSoldout.tag = 0
        }
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
