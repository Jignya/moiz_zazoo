//
//  CartVC.swift
//  Project
//
//  Created by HTNaresh on 4/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class CartVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnSideMenu: UIButton!
    
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnGuest: UIButton!
    @IBOutlet weak var viewGuest: UIView!
    @IBOutlet weak var btnplaceOrder: UIButton!
    @IBOutlet weak var viewEmpty: UIView!


    var arrClasses : NSMutableArray = []
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
//        self.viewEmpty.isHidden = true


        wscallforcart()
        
        btnplaceOrder.setTitle(kPlaceOrder, for: .normal)
        btnSignIn.setTitle(kSignIn, for: .normal)
        btnGuest.setTitle(kGuest, for: .normal)

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "4")
        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisLogin)) != nil && UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin))
        {
            viewGuest.isHidden = true
        }
        else
        {
            viewGuest.isHidden = false
        }
    }
    
    //MARK:- button Methods
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btnSignInClick(_ sender: Any)
    {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        home.strComeFrom = "cart"
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    @IBAction func btnGuestClick(_ sender: Any)
    {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "GuestAddressVC") as! GuestAddressVC
        home.strComeFrom = "cart"
        self.navigationController?.pushViewController(home, animated: true)
        
    }

    @objc func btnDeleteClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        let dictdata = NSMutableDictionary(dictionary: self.arrClasses[btn.tag] as! Dictionary)
        
        var promotionId : NSNumber = 0
        var cartId : NSNumber = 0
        
        if dictdata["Cart_Item_Type"] as? NSNumber == 3
        {
            promotionId =  dictdata["Promotion_Id"] as? NSNumber ?? 0
        }
        else
        {
            cartId =  dictdata["Cart_Id"] as? NSNumber ?? 0
        }
        
        let alertController = UIAlertController(title:kWarningEn, message: kDeleteItem, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title:kOkEn, style: .default) {
            UIAlertAction in
            
            let strUrl:String = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveCartItem,cartId,promotionId)

            let dictPara = NSDictionary()
            WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
                DispatchQueue.main.async
                    {
                        let strTemp = result.object(forKey: "Message") as? String
                        if strTemp == "Success"
                        {
                            self.wscallforcart()
                        }
                }
            }
        }
        
        let CancelAction = UIAlertAction(title:kCancel, style: .default) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(CancelAction)
        
        // Present the controller
        alertController.show()
        
    }
    @IBAction func btnPlaceOrderClick(_ sender: Any)
    {
        var issold : Bool = false
        
        for i in 0..<arrClasses.count
        {
            let dict = arrClasses[i] as! [String:Any]
            if dict["Is_Currently_Available"] as? NSNumber == 0
            {
                issold = true
                break
            }
        }
        
        if issold
        {
            let alertController = UIAlertController(title:kWarningEn, message:kRemoveItemFromBag, preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            alertController.show()
            return
        }
        
//        let add =  self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
//        add.strcomeFrom = "cart"
//        self.navigationController?.pushViewController(add, animated: true)
        
        let pyment = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
//        pyment.dictAdd = dict.mutableCopy() as! NSMutableDictionary
//        pyment.addressId = dict["UserAddressId"] as! NSNumber
//        pyment.strArea = dict["AREA_NAME"] as? String
//        pyment.DelCharges = dict["DELIVERY_CHARGE"] as! Float
//        pyment.strMobile = (dict["Mobile"] as? String)!
        self.navigationController?.pushViewController(pyment, animated: true)

        
    }
    
    //MARK:- tableview Method

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  arrClasses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)

//        if dict["Qty"] is NSNumber  // inventory item
//        {
//            let cellReuseIdentifier = "CartCell1"
//            let cell:CartCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CartCell
//
//            let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
//
//            cell.lblItemName.text = dict["Item_Name"] as? String
//            cell.lblvendor.text = dict["Vendor_Name"] as? String
//            cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, (dict["Original_Price"] as? NSNumber)!,kCurrency)
//            cell.lblQty.text = String(format: "%@ - %@",kQty, (dict["Qty"] as? NSNumber)!)
//
//            cell.lblDiscountPrice.text = ""
//            cell.lblDiscount.text = ""
//
//            if dict["Image_Url"] as? String != ""
//            {
//                var Stringimg = String()
//                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as! String)
//                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                let url = URL(string: urlString!)
//                cell.imgItem.setImageWith(url, usingActivityIndicatorStyle: .gray)
//            }
//
//            cell.viewsold.isHidden = true
//            if dict["Is_Currently_Available"] as! NSNumber == 0
//            {
//                cell.viewsold.isHidden = false
//            }
//
//            return cell
//        }
        if  dict["Item_Type_Id"] as? NSNumber == 5
        {
            let cellReuseIdentifier = "CartCell"
            let cell:CartCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CartCell
            let arrAddons = dict["AddOns"] as? [[String:Any]]
            var  addon_price : Float = 0.00
            for i in 0..<arrAddons!.count
            {
                let dict = arrAddons![i]
                if addon_price == 0.00
                {
                    addon_price  =  (dict["Amount"]  as? NSNumber)?.floatValue ?? 0
                }
                else
                {
                    addon_price = addon_price + ((dict["Amount"]  as? NSNumber ?? 0)?.floatValue)!
                }
                
            }
            
            
            cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, ((dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue),kCurrency)
            
            if dict["Discount_Amount"] as? NSNumber != 0
            {
                cell.lblPrice.textColor = GlobalConstants.highLightClr

                cell.lblDiscountPrice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, ((dict["Original_Price"] as? NSNumber ?? 0)!.floatValue),kCurrency))
            }

            cell.lblItemName.text = dict["Item_Name"] as? String
            cell.lblTime.text = String(format: "%@ - %@", (dict["Start_Time"] as? String ?? "")!,(dict["End_Time"] as? String ?? "")!)
            cell.lblActivity.text = dict["Vendor_Name"] as? String
            
            let Classdays = String(format: "%@", (dict["No_of_Classes"] as? NSNumber ?? 0)!)
            if Classdays != "0"
            {
                cell.lblDates.text = String(format: "%@ %@ %@ (%@ %@)\n%@", (dict["Start_Date"] as? String ?? ""),kTo,(dict["End_Date"] as? String ?? ""),Classdays,kSession, (dict["Actvivity_Days"] as? String == nil) ? "" : (dict["Actvivity_Days"] as? String ?? ""))
                
                let attributedString = NSMutableAttributedString(string: cell.lblDates.text!)
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 8
                
                if UserDefaults.standard.bool(forKey: "arabic")
                {
                    paragraphStyle.alignment = .right
                }
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                cell.lblDates.attributedText = attributedString

            }
            else
            {
                if let Startdate =  dict["Start_Date"] as? String
                {
                    cell.lblDates.text = String(format: "%@ %@ %@", Startdate,kTo,(dict["End_Date"] as? String ?? ""))
                }
                else
                {
                    cell.lblDates.text = ""
                }
            }
            
            if dict["Image_Url"] as? String != ""
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                cell.imgItem?.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
            }
            else
            {
                cell.imgItem.image = UIImage(named: "logo.png")
            }
            
            
            cell.lblSoldout.tag = 0
            cell.viewsold.isHidden = true
            if dict["Is_Currently_Available"] as? NSNumber == 0
            {
                cell.viewsold.isHidden = false
            }
            
            if Manager.sharedInstance.isRemoveCart == 1
            {
                let date = Date()
                let strExpiryDate = dict["Expire_Datetime"] as? String
                let formattt = DateFormatter()
                formattt.timeZone = NSTimeZone(name:"UTC")! as TimeZone
                formattt.dateFormat = "dd/MM/yyyy hh:mm:ss a"
                
//                let expirydate : Date = formattt.date(from: strExpiryDate)!
                
                let strCurrentDate = formattt.string(from: date)
                
                let currentDate1 : Date = (formattt.date(from: strCurrentDate)?.addingTimeInterval(10800))!
                
                if let expirydate = formattt.date(from: strExpiryDate ?? "")
                {
                    if  currentDate1.compare((expirydate)) == .orderedDescending
                    {
                        deleteFromCart(Cart_id: dict["Cart_Id"] as! NSNumber)
                    }
                }
            }
            
            cell.btndelete.tag = indexPath.row
            cell.btndelete.addTarget(self, action: #selector(btnDeleteClick(_:)), for: .touchUpInside)

            
            
            return cell
        }
        else
        {
            let cellReuseIdentifier = "CartCell"
            let cell:CartCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CartCell
            

            cell.lblItemName.text = dict["Item_Name"] as? String
            cell.lblTime.text = String(format: "%@ - %@", (dict["Start_Time"] as? String)!,(dict["End_Time"] as? String)!)
            cell.lblActivity.text = dict["Type_of_Activity"] as? String
            cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, ((dict["Net_Amount"] as? NSNumber)!.floatValue),kCurrency)
            
            if dict["Discount_Amount"] as? NSNumber != 0
            {
                cell.lblPrice.textColor = GlobalConstants.highLightClr

                cell.lblDiscountPrice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, ((dict["Original_Price"] as? NSNumber)!.floatValue),kCurrency))
            }

//            let dateArray = NSMutableArray(array: dict["SelectedDates"] as! Array)
//            cell.lblDates.text = Manager.sharedInstance.getCommaSeparatedValuefromArray(dateArray, forkey: "Calender_Date")
            
            let Classdays = String(format: "%@", (dict["No_of_Classes"] as? NSNumber ?? 0)!)
            if Classdays != "0"
            {
                cell.lblDates.text = String(format: "%@ %@ %@ (%@ %@)\n%@", (dict["Start_Date"] as? String ?? "")!,kTo,(dict["End_Date"] as? String ?? "")!,Classdays,kSession, (dict["Actvivity_Days"] as? String == nil) ? "" : (dict["Actvivity_Days"] as? String ?? "")!)

                let attributedString = NSMutableAttributedString(string: cell.lblDates.text!)
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 8
                if UserDefaults.standard.bool(forKey: "arabic")
                {
                    paragraphStyle.alignment = .right
                }
                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                cell.lblDates.attributedText = attributedString
                
            }
            else
            {
                if let Startdate =  dict["Start_Date"] as? String
                {
                    cell.lblDates.text = String(format: "%@ %@ %@", Startdate,kTo,(dict["End_Date"] as? String ?? ""))
                }
                else
                {
                    cell.lblDates.text = ""
                }

            }



            if dict["Image_Url"] as? String != ""
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                cell.imgItem.setImageWith(url, usingActivityIndicatorStyle: .gray)
            }
            
            cell.lblSoldout.tag = 0

            cell.viewsold.isHidden = true
            if dict["Is_Currently_Available"] as? NSNumber == 0
            {
                cell.viewsold.isHidden = false
            }
            
            if Manager.sharedInstance.isRemoveCart == 1
            {
                let date = Date()
                let strExpiryDate = dict["Expire_Datetime"] as? String
                let formattt = DateFormatter()
                formattt.timeZone = NSTimeZone(name:"UTC")! as TimeZone
                formattt.dateFormat = "dd/MM/yyyy hh:mm:ss a"
                
                
                let strCurrentDate = formattt.string(from: date)
                
                let currentDate1 : Date = (formattt.date(from: strCurrentDate)?.addingTimeInterval(10800))!
                
                if let expirydate = formattt.date(from: strExpiryDate ?? "")
                {
                    if  currentDate1.compare((expirydate)) == .orderedDescending
                    {
                        deleteFromCart(Cart_id: dict["Cart_Id"] as! NSNumber)
                    }
                }
            }
            
            cell.btndelete.tag = indexPath.row
            cell.btndelete.addTarget(self, action: #selector(btnDeleteClick(_:)), for: .touchUpInside)

            return cell
        }
        
       
    }
    
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        
        let delete = UITableViewRowAction(style: .destructive, title: kDeleteEn) { (action, indexPath) in
            // delete item at indexPath
            
            let dictdata = NSMutableDictionary(dictionary: self.arrClasses[indexPath.row] as! Dictionary)
            
            var promotionId : NSNumber = 0
            var cartId : NSNumber = 0

            if dictdata["Cart_Item_Type"] as? NSNumber == 3
            {
               promotionId =  dictdata["Promotion_Id"] as? NSNumber ?? 0
            }
            else
            {
               cartId =  dictdata["Cart_Id"] as? NSNumber ?? 0
            }
            
            let alertController = UIAlertController(title:kWarningEn, message: kDeleteItem, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in

                let strUrl:String = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveCartItem,cartId,promotionId)

                let dictPara = NSDictionary()
                WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
                    DispatchQueue.main.async
                        {
                            let strTemp = result.object(forKey: "Message") as? String
                            if strTemp == "Success"
                            {
                                self.wscallforcart()
                            }
                    }
                }
            }
            
            let CancelAction = UIAlertAction(title:kCancel, style: .default) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(CancelAction)
            
            // Present the controller
            alertController.show()
            
        }
        return [delete]
        
    }
    
    func deleteFromCart(Cart_id:NSNumber)
    {
        let strUrl:String = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveCartItem,Cart_id)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            DispatchQueue.main.async
                {
                    // print(result)
                    let strTemp = result.object(forKey: "Message") as? String
                    if strTemp == "Success"
                    {
                        self.wscallforcart()
                    }
            }
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        
        if dict["Cart_Item_Type"] as? NSNumber == 2
        {
            return 0
        }
        
        var myString = ""
        
        let Classdays = String(format: "%@", (dict["No_of_Classes"] as? NSNumber ?? 0)!)
        if Classdays == "0"
        {
            if let Startdate =  dict["Start_Date"] as? String
            {
                myString = (dict["Actvivity_Days"] as? String) == "" ? String(format: "%@ to %@", Startdate,(dict["End_Date"] as? String ?? "")) : String(format: "%@ to %@\n%@", Startdate,(dict["End_Date"] as? String ?? ""),Classdays,(dict["Actvivity_Days"] as? String ?? ""))
                
            }
            else
            {
               myString = ""
            }

        }
        else
        {
            myString = (dict["Actvivity_Days"] as? String) == "" ? String(format: "%@ to %@ (%@)", (dict["Start_Date"] as? String ?? ""),(dict["End_Date"] as? String ?? ""),Classdays) : String(format: "%@ to %@ (%@)\n%@", (dict["Start_Date"] as? String ?? ""),(dict["End_Date"] as? String ?? ""),Classdays,(dict["Actvivity_Days"] as? String ?? ""))
        }
        
        
        

        let str2 = myString + (dict["Item_Name"] as? String ?? "")
        
        let str1 = str2 + "/n" + (dict["Type_of_Activity"] as? String ?? "")

        
        let attributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont.systemFont(ofSize: 17.0)]
        
        let attributedString: NSAttributedString = NSAttributedString(string:str1,attributes:attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: (self.tblList.frame.size.width - 190), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requiredSize:CGRect = rect
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return requiredSize.height + 150
        }
        else
        {
            return requiredSize.height + 70
        }
        
    }
    
    

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    
    //MARK:- webservice
    
    func wscallforcart()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetCart,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    if strStatus == "Success"
                    {
                        self.arrClasses =  NSMutableArray(array: result["CartItems"] as? Array ?? [])
                        Manager.sharedInstance.arrcart = self.arrClasses
                        if self.arrClasses.count > 0
                        {
                            let strCount =  String(format: "%d", self.arrClasses.count )
                            if strCount == "0"
                            {
                                Manager.sharedInstance.tabbarController.lblCount.isHidden = true
                            }
                            else
                            {
                                Manager.sharedInstance.tabbarController.lblCount.isHidden = false
                                Manager.sharedInstance.tabbarController.lblCount.text = strCount
                            }
                            
                            self.tblList.isHidden = false
                            self.viewEmpty.isHidden = true
                        }
                        else
                        {
                            Manager.sharedInstance.tabbarController.lblCount.isHidden = true

                            self.tblList.isHidden = true
                            self.viewEmpty.isHidden = false

                        }
                        self.tblList.reloadData()
                        Manager.sharedInstance.wscallforgetCart()
                       
                    }
                    else
                    {

                    }
            }
        }
    }
   
    
}
