//
//  AddressListVC.swift
//  Zazoo
//
//  Created by HTNaresh on 15/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class AddressListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblNorecord: UILabel!
    
    
    var arrList:NSMutableArray = []
    var strcomeFrom:String!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        lblNorecord.text = kNorecord
        
        
        wscallforGetAddress()
        
        
        let image = btnAdd.imageView?.image?.withRenderingMode(.alwaysTemplate)
        btnAdd.setImage(image, for: .normal)
        btnAdd.tintColor = UIColor(red: 230.0 / 255.0, green: 97.0 / 255.0, blue: 88.0 / 255.0, alpha: 1)
        
        
        
        lblTitle.text = kaddress


    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if arrList.count > 0
        {
            tblList.isHidden = false
        }
        else
        {
            tblList.isHidden = true
        }
        
        if strcomeFrom == "payment"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "4")

        }
        else if strcomeFrom == "payment1"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "4")
            
        }
        else if strcomeFrom == "cart"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "4")

        }
        else
        {
            Manager.sharedInstance.addTabBar2(self, tab: "2")

        }

    }
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    
    @IBAction func btnAddClick(_ sender: Any)
    {
        if strcomeFrom == "cart"
        {
            let address = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
            address.strComeFrom = strcomeFrom
            self.navigationController?.pushViewController(address, animated: true)
        }
        else  if strcomeFrom == "payment"
        {
            let address = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
            address.strComeFrom = "cart"
            self.navigationController?.pushViewController(address, animated: true)
        }
        else
        {
            let address = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
            address.strComeFrom = "new"
            self.navigationController?.pushViewController(address, animated: true)
        }

        
       
    }
    
    //MARK:- handle Back
    
    @objc func handleSwipebackscreen(_ gestureRecognizer: UIGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            self.btnBackClick(self)
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        if strcomeFrom == "cart"
        {
            if ((self.navigationController?.viewControllers) != nil)
            {
                let array = self.navigationController?.viewControllers ?? []
                for k in (0 ..< array.count)
                {
                    let controller: UIViewController? = (array[k] as! UIViewController)
                    if (controller is CartVC)
                    {
                        self.navigationController?.popToViewController(controller!, animated: true)
                        

                        
                        break
                    }
                }
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)

        }
    }
    
    //MARK:- tableview method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "AddressCell"
        let cell:AddressCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! AddressCell
        
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        cell.lblAddress.text = Manager.sharedInstance.displayAddress(dict)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
        
        
        if strcomeFrom == "payment"
        {
            if ((self.navigationController?.viewControllers) != nil)
            {
                let array = self.navigationController?.viewControllers ?? []
                for k in (0 ..< array.count)
                {
                    let controller: UIViewController? = (array[k] as! UIViewController)
                    if (controller is PaymentVC)
                    {
                        let pyment = controller as! PaymentVC
                        pyment.dictAdd = (dict.mutableCopy() as! NSMutableDictionary)
                        pyment.addressId = dict["UserAddressId"] as! NSNumber
                        pyment.strArea = dict["AREA_NAME"] as? String
                        pyment.DelCharges = dict["DELIVERY_CHARGE"] as! Float

                        pyment.strMobile = (dict["Mobile"] as? String ?? "")!
                    self.navigationController?.popToViewController(controller!, animated: true)

                        break
                    }
                }
            }
        }
        else if strcomeFrom == "cart"
        {
            let pyment = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            pyment.dictAdd = dict.mutableCopy() as! NSMutableDictionary
            pyment.addressId = dict["UserAddressId"] as! NSNumber
            pyment.strArea = dict["AREA_NAME"] as? String
            pyment.DelCharges = dict["DELIVERY_CHARGE"] as! Float
            pyment.strMobile = (dict["Mobile"] as? String)!
            self.navigationController?.pushViewController(pyment, animated: true)

        }
        else
        {
            let add = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
            add.strComeFrom = "edit"
            add.dictAdd = dict.mutableCopy() as! NSMutableDictionary
            add.addressId = dict["UserAddressId"] as! NSNumber
            self.navigationController?.pushViewController(add, animated: true)

        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
            
            let strData = Manager.sharedInstance.displayAddress(dict)
            let constraint = CGSize(width: self.view.frame.size.width - 30, height: 50000.0)
            let fontText: UIFont = (UIFont(name:GlobalConstants.kEnglishRegular, size:25))!
            let size: CGRect = strData.boundingRect(with: constraint, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: fontText], context: nil)
            var height1: CGFloat
            height1 = size.size.height
            return height1 + 40
        }
        else
        {
            let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
            
            let strData = Manager.sharedInstance.displayAddress(dict)
            let constraint = CGSize(width: self.view.frame.size.width - 30, height: 50000.0)
            let fontText: UIFont = (UIFont(name:GlobalConstants.kEnglishRegular, size:18))!
            let size: CGRect = strData.boundingRect(with: constraint, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: fontText], context: nil)
            var height1: CGFloat
            height1 = size.size.height
            return height1 + 40
        }
    }

    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            
            let dictdata = NSMutableDictionary(dictionary: self.arrList[indexPath.row] as! Dictionary)

            let alertController = UIAlertController(title:kWarningEn, message: kDeleteadd, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
                
                
                let postid = dictdata["UserAddressId"] as! NSNumber
                let strUrl:String = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wDeleteUserAddress,postid)
                
                let dictPara = NSDictionary()
                WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
                    DispatchQueue.main.async
                        {
                            // print(result)
                            let strTemp :String = result.object(forKey: "Message") as! String
                            if (strTemp.isEqual("Success"))
                            {
                                self.wscallforGetAddress()
                            }
                            else
                            {
                                
                            }
                    }
                }
                
                
            }
            
            let CancelAction = UIAlertAction(title:kCancel, style: .default) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(CancelAction)
            
            // Present the controller
            alertController.show()
        
        }
        return [delete]
        
    }
    
    
    //MARK:- Webservice
    
    func wscallforGetAddress()
    {
        let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as! CVarArg)

        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetUserAddress,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
//                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                       self.arrList = NSMutableArray(array: result["UserAddresses"] as! Array)
                        if self.arrList.count > 0
                        {
                            self.tblList.isHidden = false
                        }
                        else
                        {
                            self.tblList.isHidden = true
                        }
                        self.tblList.reloadData()
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }


}
