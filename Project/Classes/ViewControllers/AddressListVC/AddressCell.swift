//
//  AddressCell.swift
//  Zazoo
//
//  Created by HTNaresh on 17/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell
{

    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnUnread: ButtonExtender!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
