//
//  AddAddressVC.swift
//  Zazoo
//
//  Created by HTNaresh on 12/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtAddressName: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtMobile: UITextField!

    @IBOutlet weak var txtBlock: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtAvenue: UITextField!
    @IBOutlet weak var txtHouseName: UITextField!
    @IBOutlet weak var txtBuilding: UITextField!
    @IBOutlet weak var txtFloor: UITextField!
    @IBOutlet weak var txtAppartment: UITextField!
    @IBOutlet weak var txtExtraDirection: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnArea: UIButton!
    
    var arrArea  =  [[String:Any]]()
    var strComeFrom : String!
    var strIsNew : String!

    var delieveryCharge : Float!
    var areaId : NSNumber!
    var addressId : NSNumber = 0
    
    var dictAdd : NSMutableDictionary = [:]





    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        arrArea = Manager.sharedInstance.arrArea as? [[String : Any]] ?? []
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        setLabel()
    }
    
    func setLabel()
    {
        lblTitle.text = kaddress
        txtArea.placeholder = kArea
        txtAddressName.placeholder = kaddressname
        txtBlock.placeholder = kblock
        txtMobile.placeholder = kMobNum
        txtStreet.placeholder = kStreetname
        txtAvenue.placeholder = kAvenueoptional
        txtHouseName.placeholder = kHousNo
        txtBuilding.placeholder = kBuildingOptioanl
        txtFloor.placeholder = kFloorOptional
        txtAppartment.placeholder = kAppartmentOptional
        txtExtraDirection.placeholder = kExtraDir
        
        btnSubmit.setTitle(kSubmit, for: .normal)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
       
        if strComeFrom == "edit"
        {
            delieveryCharge = dictAdd["DELIVERY_CHARGE"] as? Float ?? 0
            self.txtAddressName.text = dictAdd["AddressName"] as? String
            self.txtArea.text = dictAdd["AREA_NAME"] as? String
            self.txtBlock.text = dictAdd["Block"] as? String
            self.txtStreet.text = dictAdd["Street"] as? String
            self.txtAvenue.text = dictAdd["Avenue"] as? String
            self.txtHouseName.text = dictAdd["HouseNo"] as? String
            self.txtBuilding.text = dictAdd["Building"] as? String
            self.txtFloor.text = dictAdd["Floor"] as? String
            self.txtAppartment.text = dictAdd["Apparment"] as? String
            self.txtExtraDirection.text = dictAdd["ExtraDirection"] as? String
            self.txtMobile.text = dictAdd["Mobile"] as? String
            areaId = dictAdd["AreaId"] as? NSNumber

            
        }
        
        if strComeFrom == "payment"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "4")
            
        }
        else if strComeFrom == "cart"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "4")
            
        }
        else
        {
            Manager.sharedInstance.addTabBar2(self, tab: "2")
            
        }
        

    }
    
    //MARK:- handle Back
    
    @objc func handleSwipeback(_ gestureRecognizer: UIGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            self.btnBackClick(self)
        }
    }
    
    
    //MARK:- Button Click
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnBackClick(_ sender: Any)
    {
        if strComeFrom == "cart"
        {
            if ((self.navigationController?.viewControllers) != nil)
            {
                let array = self.navigationController?.viewControllers ?? []
                for k in (0 ..< array.count)
                {
                    let controller: UIViewController? = (array[k] as! UIViewController)
                    if (controller is CartVC)
                    {
                        self.navigationController?.popToViewController(controller!, animated: true)

                        
                        break
                    }
                }
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)

        }
    }
    
    @IBAction func btnSubmitClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtAddressName.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterAddresssname, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if(txtMobile.text=="")
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourmobileEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if((txtMobile.text?.count)! < 8)
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseentervalidMobnum, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if txtArea.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectArea, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if txtBlock.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectblock, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if txtStreet.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterStreet, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if txtHouseName.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterhouse, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
//        else if txtAppartment.text == ""
//        {
//            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteraprtmnt, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
//            alert.show()
//
//        }
        else
        {
            
            wscallforAddAddress()
        }
        
    }
    
    @IBAction func btnAreaClick(_ sender: Any)
    {
        self.view.endEditing(true)

        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            print(dictSelect)
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                self.txtArea.text = dictSelect["Area_Name_A"] as? String
            }
            else
            {
                self.txtArea.text = dictSelect["Area_Name_E"] as? String
            }
            
            self.delieveryCharge = dictSelect["Delivery_Charge"] as? Float
            self.areaId = dictSelect["Area_Id"] as? NSNumber

        }, arrData: NSMutableArray(array: arrArea as Array), isLanguage: false, isNationality: true, isCategories: false, isProfession: false, isother: false,self)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobile
        {
            if string.isEmpty
            {
                return true
            }
            if validatePhone(string)
            {
                var strMaxLength = ""
                strMaxLength = "15"
                let newStr = textField.text as NSString?
                let currentString: String = newStr!.replacingCharacters(in: range, with: string)
                let j = Int(strMaxLength) ?? 0
                let length: Int = currentString.count
                if length >= j {
                    return false
                }
                else {
                    return true
                }
            }
            return false
        }
        return true
    }
    
    func validatePhone(_ phoneNumber: String) -> Bool {
        let phoneRegex = "[0-9]"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: phoneNumber) {
            return false
        }
        else {
            return true
        }
    }
    
    //MARK: webservice
    
    func wscallforAddAddress()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wUpdateUserAddress)
        
        let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        
        let dictPara:NSDictionary = ["AddressName":self.txtAddressName.text!,"Block":self.txtBlock.text!,"ExtraDirection":self.txtExtraDirection.text!,"HouseNo":self.txtHouseName.text!,"Street":self.txtStreet.text!,"UserId":userId,"Avenue":self.txtAvenue.text!,"Building":self.txtBuilding.text!,"Floor":self.txtFloor.text!,"Apparment":self.txtAppartment.text!,"CountryId":"1","AreaId":areaId,"UserAddressId":addressId,"Mobile":self.txtMobile.text!]
        
        print(dictPara)
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
//                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        if self.strComeFrom == "cart"
                        {
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC

                            self.addressId = result["UserAddressId"] as? NSNumber ?? 0

                            home.addressId = result["UserAddressId"] as? NSNumber ?? 0

                            home.strArea = self.txtArea.text!

                            home.dictAdd = NSMutableDictionary(dictionary: dictPara as! NSDictionary)

                            home.DelCharges = self.delieveryCharge
                            home.strMobile = self.txtMobile.text!

                            self.navigationController?.pushViewController(home, animated: true)
                        }
                        else
                        {
                            let array = self.navigationController?.viewControllers ?? []
                            
                            for k in (0 ..< array.count)
                            {
                                let controller: UIViewController? = (array[k] as! UIViewController)
                                if (controller is AddressListVC)
                                {
                                    let add = controller as! AddressListVC

                                    add.wscallforGetAddress()
                                    self.navigationController?.popToViewController(controller!, animated: true)

                                    break
                                }
                            }
                        }
                    }
                    else
                    {
                        
                    }
                    
            }
        }
        
    }
    
    
}
