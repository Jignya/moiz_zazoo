//
//  CMSVC.swift
//  Project
//
//  Created by HTNaresh on 10/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class CMSVC: UIViewController
{
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtContent: UITextView!
    
    var strTitle : String!
    var strRequest : String!
    var strdesc : String!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.txtContent.text = ""
        lblTitle.text = strTitle
        wscallforCMS()

//
//        if strTitle == "terms1"
//        {
//            lblTitle.text = kTerms
//
//            let htmlData = NSString(string: strdesc!).data(using: String.Encoding.unicode.rawValue)
//
//            let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
//
//
//            let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
//
//            let attritemutable =  NSMutableAttributedString(attributedString: attributedString)
//            attritemutable.addAttributes([NSAttributedStringKey.font : UIFont(name: GlobalConstants.kEnglishRegular , size: CGFloat(15.0)) as Any], range: NSMakeRange(0, attritemutable.length))
//
//
//            self.txtContent.attributedText = attritemutable
//        }

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "0")
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController

    }
    
    
    func wscallforCMS()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetCMSContent,strRequest,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.lblTitle.text = self.strTitle

                        let str = (result["Content"] as? String)!
                        
                        let attritemutable = NSMutableAttributedString(attributedString: str.html2AttributedString!)
                        let font = UIFont(name: GlobalConstants.kEnglishBold , size: (UIDevice.current.userInterfaceIdiom == .pad) ? CGFloat(25) : CGFloat(17))
                        attritemutable.setFontFace(font: font!, color: UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1))
                        
                        let paragraphStyle = NSMutableParagraphStyle()
                        paragraphStyle.alignment = UserDefaults.standard.bool(forKey: "arabic") ? .right : .left

                        attritemutable.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attritemutable.length))

                        self.txtContent.attributedText = attritemutable
                    }
                    else
                    {
                        self.txtContent.text = ""
                    }
            }
            
            
        }
        
    }
}

extension NSMutableAttributedString {
    func setFontFace(font: UIFont, color: UIColor?) {
        beginEditing()
        self.enumerateAttribute(.font, in: NSRange(location: 0, length: self.length)) { (value, range, stop) in
            if let f = value as? UIFont, let newFontDescriptor = f.fontDescriptor.withFamily(font.familyName).withSymbolicTraits(f.fontDescriptor.symbolicTraits) {
                let newFont = UIFont(descriptor: newFontDescriptor, size: font.pointSize)
                removeAttribute(.font, range: range)
                addAttribute(.font, value: newFont, range: range)
                if let color = color {
                    removeAttribute(.foregroundColor, range: range)
                    addAttribute(.foregroundColor, value: color, range: range)
                }
            }
        }
        endEditing()
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
