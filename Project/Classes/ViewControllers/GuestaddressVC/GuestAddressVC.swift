//
//  GuestAddressVC.swift
//  Zazoo
//
//  Created by HTNaresh on 25/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class GuestAddressVC: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtFullName: JVFloatLabeledTextField!
    @IBOutlet weak var txtMobile: JVFloatLabeledTextField!
    @IBOutlet weak var txtEmail: JVFloatLabeledTextField!
    @IBOutlet weak var txtArea: JVFloatLabeledTextField!
    @IBOutlet weak var txtBlock: JVFloatLabeledTextField!
    @IBOutlet weak var txtStreet: JVFloatLabeledTextField!
    @IBOutlet weak var txtAvenue: JVFloatLabeledTextField!
    @IBOutlet weak var txtHouseName: JVFloatLabeledTextField!
    @IBOutlet weak var txtBuilding: JVFloatLabeledTextField!
    @IBOutlet weak var txtFloor: JVFloatLabeledTextField!
    @IBOutlet weak var txtAppartment: JVFloatLabeledTextField!
    @IBOutlet weak var txtExtraDirection: JVFloatLabeledTextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnArea: UIButton!
    
    var arrArea  =  [[String:Any]]()
    var strComeFrom : String!
    var strIsNew : String!
    var delieveryCharge : Float!
    var areaId : NSNumber!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        arrArea = Manager.sharedInstance.arrArea as? [[String : Any]] ?? []

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        

        setLabel()
        
    }
    
    func setLabel()
    {
        lblTitle.text = kaddress
        txtArea.placeholder = kArea
        txtFullName.placeholder = kFullname
        txtEmail.placeholder = kEmail
        txtBlock.placeholder = kblock
        txtMobile.placeholder = kMobNum
        txtStreet.placeholder = kStreetname
        txtAvenue.placeholder = kAvenueoptional
        txtHouseName.placeholder = kHousNo
        txtBuilding.placeholder = kBuildingOptioanl
        txtFloor.placeholder = kFloorOptional
        txtAppartment.placeholder = kAppartmentOptional
        txtExtraDirection.placeholder = kExtraDir
        
        btnSubmit.setTitle(kSubmit, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "4")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Button Click
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobile
        {
            if string.isEmpty
            {
                return true
            }
            if validatePhone(string)
            {
                var strMaxLength = ""
                strMaxLength = "15"
                let newStr = textField.text as NSString?
                let currentString: String = newStr!.replacingCharacters(in: range, with: string)
                let j = Int(strMaxLength) ?? 0
                let length: Int = currentString.count
                if length >= j {
                    return false
                }
                else {
                    return true
                }
            }
            return false
        }
        return true
    }
    
    func validatePhone(_ phoneNumber: String) -> Bool {
        let phoneRegex = "[0-9]"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: phoneNumber) {
            return false
        }
        else {
            return true
        }
    }
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK:- handle Back
    
    @objc func handleSwipeback(_ gestureRecognizer: UIGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            self.btnBackClick(btnBack)
        }
    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnBackClick(_ sender: Any)
    {
        if strComeFrom == "cart"
        {
            if ((self.navigationController?.viewControllers) != nil)
            {
                let array = self.navigationController?.viewControllers as? Array ?? []
                for k in (0 ..< array.count)
                {
                    let controller: UIViewController? = (array[k] as! UIViewController)
                    if (controller is CartVC)
                    {
                        self.navigationController?.popToViewController(controller!, animated: true)

                        
                        break
                    }
                }
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btnSubmitClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtFullName.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterAddresssname, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if(txtMobile.text=="")
        {

            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourmobileEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if((txtMobile.text?.count)! < 8)
        {

            let alert = UIAlertController(title:kWarningEn, message:kPleaseentervalidMobnum, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if (txtEmail.text == "")
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryouremailaddressEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
            
        else if(!(isValidEmail(testStr: txtEmail.text!)))
        {
            let alert = UIAlertController(title:kWarningEn, message:kEmailValidationEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
            
        else if txtArea.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectArea, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if txtBlock.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectblock, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if txtStreet.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterStreet, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if txtHouseName.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterhouse, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
//        else if txtAppartment.text == ""
//        {
//            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteraprtmnt, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
//            alert.show()
//
//        }
        else
        {
            PassdataTocheckoutScreen()
        }
        
    }
    
    func PassdataTocheckoutScreen()
    {
        let dictPara:NSDictionary!
        
        dictPara = ["Block":self.txtBlock.text!,"ExtraDirection":self.txtExtraDirection.text!,"HouseNo":self.txtHouseName.text!,"Street":self.txtStreet.text!,"UserId":"0","Avenue":self.txtAvenue.text!,"Building":self.txtBuilding.text!,"Floor":self.txtFloor.text!,"Apparment":self.txtAppartment.text!,"CountryId":"1","AreaId":areaId,"UserAddressId":"0"]
        
        if self.strComeFrom == "cart"
        {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            
            home.addressId = 0
            
            home.strArea = self.txtArea.text!
            
            home.dictAdd = NSMutableDictionary(dictionary: dictPara as! NSDictionary)
            
            home.DelCharges = self.delieveryCharge!
            home.strName = self.txtFullName.text!
            home.strMobile = self.txtMobile.text!
            home.strEmail = self.txtEmail.text!
            
            self.navigationController?.pushViewController(home, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnAreaClick(_ sender: Any)
    {
        self.view.endEditing(true)
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
        
        print(dictSelect)
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            self.txtArea.text = dictSelect["Area_Name_A"] as? String
        }
        else
        {
            self.txtArea.text = dictSelect["Area_Name_E"] as? String
        }
        
        self.delieveryCharge = dictSelect["Delivery_Charge"] as! Float
        self.areaId = dictSelect["Area_Id"] as! NSNumber
        
        }, arrData: NSMutableArray(array: arrArea as Array), isLanguage: false, isNationality: true, isCategories: false, isProfession: false, isother: false,self)
    }
    
    //MARK: webservice
    
    func wscallforAddAddress()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wUpdateUserAddress)
        
        let dictPara:NSDictionary!
            
            dictPara = ["Block":self.txtBlock.text!,"ExtraDirection":self.txtExtraDirection.text!,"HouseNo":self.txtHouseName.text!,"Street":self.txtStreet.text!,"UserId":"0","Avenue":self.txtAvenue.text!,"Building":self.txtBuilding.text!,"Floor":self.txtFloor.text!,"Apartment":self.txtAppartment.text!,"CountryId":"1","AreaId":areaId,"UserAddressId":"0"]
        
        print(dictPara)
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        if self.strComeFrom == "cart"
                        {
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                            
                            home.addressId = result["UserAddressId"] as! NSNumber
                            
                            home.strArea = self.txtArea.text!
                            
                            home.dictAdd = NSMutableDictionary(dictionary: dictPara as! NSDictionary)
                            
                            home.DelCharges = self.delieveryCharge!
                            home.strName = self.txtFullName.text!
                            home.strMobile = self.txtMobile.text!
                            home.strEmail = self.txtEmail.text!
                            
                        self.navigationController?.pushViewController(home, animated: true)
                        }
                        else
                        {
                        self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else
                    {
                        
                    }
                    
            }
        }
        
    }
    
    
}
