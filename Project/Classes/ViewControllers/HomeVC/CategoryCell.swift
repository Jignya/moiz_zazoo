//
//  CategoryCell.swift
//  Project
//
//  Created by HTNaresh on 6/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell
{
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var imgPlay: UIImageView!


    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear

        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
        
    }
}
