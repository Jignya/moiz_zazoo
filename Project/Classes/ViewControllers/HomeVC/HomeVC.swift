//
//  HomeVC.swift
//  Project
//
//  Created by HTNaresh on 1/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class HomeVC: UIViewController,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,homeCellDelegate
{
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnLang: UIButton!

    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var imgtag: UIImageView!

    
    @IBOutlet weak var conviewSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var collectionAdv: UICollectionView!
    @IBOutlet weak var collectionType: UICollectionView!
    
    @IBOutlet weak var tblPopular: UITableView!
    @IBOutlet weak var ContblPopularHeight: NSLayoutConstraint!

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblAdventure: UILabel!

    
    @IBOutlet weak var conCollectionAdvHeight: NSLayoutConstraint!
    
    
    
    
    var arrayAdv : NSMutableArray = []
    var arrayType : NSMutableArray = []
    var arrPopular : NSMutableArray = []
    var arrVendors : NSMutableArray = []

    var timer = Timer()
    
    var type_id : NSNumber!


    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.btnSearch.isSelected = false
        view1.layer.cornerRadius = 4
        view1.clipsToBounds = true
    
        lblAdventure.text = kBookadventure.uppercased()
        
        self.conCollectionAdvHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 250 : 160
            //(3 * self.view.frame.size.width) / 4

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        if Manager.sharedInstance.dictHomePage.count > 0
        {
           SetHomepageData(result: Manager.sharedInstance.dictHomePage as! NSDictionary)
        }
        else
        {
            wscallforHomePage()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            btnLang.isSelected = true
            pageControl.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else
        {
            btnLang.isSelected = false
        }
        
        Manager.sharedInstance.addTabBar2(self, tab: "0")
        

    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        Manager.sharedInstance.wscallforgetCart()
    }
    
    //MARK:- textfiels delegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()  //if desired
        return true
    }
    
    //MARK:- tableview method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrPopular.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "HomeTableCell"
        let cell:HomeTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! HomeTableCell
        
        let dict = NSMutableDictionary(dictionary: arrPopular[indexPath.row] as! Dictionary)
        
        cell.delegate = self
        
        if dict["Popular_Name"] != nil
        {
            cell.lblTitle.text = dict["Popular_Name"] as? String
            cell.arrPopular = NSMutableArray(array: (dict["PopularItems"] as? Array)!)
        }
        else
        {
            cell.lblTitle.text = dict["Promotion_Name"] as? String
            cell.arrPopular = NSMutableArray(array: (dict["Promotions"] as? Array)!)

        }
        
        cell.collectionPopular.reloadData()
        
        ContblPopularHeight.constant = tblPopular.contentSize.height
        tblPopular.updateConstraintsIfNeeded()
        tblPopular.layoutIfNeeded()
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let dict = NSMutableDictionary(dictionary: arrPopular[indexPath.row] as! Dictionary)
        
        var arr1 = [[String:Any]]()
        
        if dict["PopularItems"] != nil
        {
            arr1 = dict["PopularItems"] as? [[String:Any]] ?? []
        }
        else
        {
            arr1 = dict["Promotions"] as? [[String:Any]] ?? []
        }
        
        if arr1.count > 0
        {
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                 return ((self.view.frame.size.width - 20) / 3 + 130)  //return 250
            }
            else
            {
                return ((self.view.frame.size.width - 20) / 3 + 90) //180
            }

        }
        
        return 0
    }
    
    func selectedDict(dict: [String : Any], index: NSInteger, type: String)
    {
        if dict["Promotion_Id"] != nil
        {
            print(dict["Promotion_Id"])
            if dict["Promotion_Type_Id"] as? NSNumber == 1 || dict["Promotion_Type_Id"] as? NSNumber == 2
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
                classdetail.VendorId = dict["Vendor_Id"] as? NSNumber
                self.navigationController?.pushViewController(classdetail, animated: true)
            }
            else if dict["Promotion_Type_Id"] as? NSNumber == 3
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
                classdetail.classId = dict["Parent_Linked_Item_Id"] as? NSNumber
                classdetail.strComeFrom = ""
                classdetail.isBundleItem = false
                self.navigationController?.pushViewController(classdetail, animated: true)
            }
            else if dict["Promotion_Type_Id"] as? NSNumber == 4
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetail1VC") as! ClassDetail1VC
                classdetail.classId = dict["Promotion_Id"] as? NSNumber
                classdetail.strComeFrom = ""
                classdetail.isBundleItem = true
                self.navigationController?.pushViewController(classdetail, animated: true)
            }
        }
        else if dict["Item_Type_Id"] as? NSNumber != 5
        {
            if dict["Is_Bundle_Item"] as? Bool == true
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetail1VC") as! ClassDetail1VC
                classdetail.classId = dict["Item_Id"] as? NSNumber
                classdetail.strComeFrom = ""
                classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
                self.navigationController?.pushViewController(classdetail, animated: true)

            }
            else
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
                classdetail.classId = dict["Item_Id"] as? NSNumber
                classdetail.strComeFrom = ""
                classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
                self.navigationController?.pushViewController(classdetail, animated: true)
            }
        }
        else
        {
            let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "PartyDetailVC") as! PartyDetailVC
            classdetail.classId = dict["Item_Id"] as? NSNumber
            classdetail.strDate = String(format: "%@ %@",(dict["Guests"] as? NSNumber ?? 0)! , kGuest)
            self.navigationController?.pushViewController(classdetail, animated: true)
        }
    }

    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionAdv
        {
            return arrayAdv.count
        }
        else
        {
            return arrayType.count
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == collectionAdv
        {
            let cellReuseIdentifier = "HomeCollectionCell"
            let cell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! HomeCollectionCell
            
            let dict = arrayAdv[indexPath.row] as! NSDictionary
            
            
//            var Stringimg = String()
//            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
//
//            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//            let url = URL(string: urlString!)
//            cell.imgAdv.setImageWith(url, usingActivityIndicatorStyle: .gray)
            
            cell.lblTitle.text = dict["Day_Name"] as? String
            cell.lblSubTitle.text = dict["Date_Name"] as? String
            let strcode = dict["Color_Code"] as? String
            cell.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: strcode ?? "")
            
            cell.layer.cornerRadius = 10.0

//            timer.invalidate()
//            timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats:true)
            return cell
        }
        else
        {
            let cellReuseIdentifier = "cell1"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            let dict = arrayType[indexPath.row] as! NSDictionary
            cell.lblTitle.text = (dict["Item_Type_Name"] as? String)?.uppercased()
//            let strcode = dict["Color_Code"] as? String
//            cell.viewBg.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: strcode ?? "")
            
            if dict["Item_Type_Id"] as? NSNumber == 2
            {
//                cell.lblTitle.textColor = UIColor.darkGray
                cell.imgLogo.image = UIImage(named: "Cls.png")
                cell.imgBg.image = UIImage(named: "cls_bg.png")
            }
            else if dict["Item_Type_Id"] as? NSNumber == 3
            {
                cell.imgLogo.image = UIImage(named: "Sem.png")
                cell.imgBg.image = UIImage(named: "Sem_bg.png")
            }
            else if dict["Item_Type_Id"] as? NSNumber == 4
            {
                cell.imgLogo.image = UIImage(named: "Cmp.png")
                cell.imgBg.image = UIImage(named: "Cmp_bg.png")
            }
            else if dict["Item_Type_Id"] as? NSNumber == 5
            {
                cell.imgLogo.image = UIImage(named: "Venu.png")
                cell.imgBg.image = UIImage(named: "Venu_bg.png")
            }
//            cell.viewBg.layer.cornerRadius = 32.0
//            cell.viewBg.clipsToBounds = true
            
            return cell
        }
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionType
        {
            let dict = arrayType[indexPath.row] as! NSDictionary

            if dict["Item_Type_Id"] as? NSNumber != 5
            {
                let venue = self.storyboard?.instantiateViewController(withIdentifier: "VenueListVC") as! VenueListVC
                venue.strTitle = dict["Item_Type_Name"] as? String
                venue.strcomeFrom = "vendor"
                venue.Id = dict["Item_Type_Id"] as? NSNumber
                venue.strcolor = dict["Color_Code"] as? String
                self.navigationController?.pushViewController(venue, animated: true)
            }
            else
            {
                let venue = self.storyboard?.instantiateViewController(withIdentifier: "VenueListVC") as! VenueListVC
                venue.strTitle = dict["Item_Type_Name"] as? String
                venue.strcomeFrom = "venue"
                venue.Id = 0
                venue.strcolor = dict["Color_Code"] as? String
                self.navigationController?.pushViewController(venue, animated: true)
            }
            
        }
        else if collectionView == collectionAdv
        {
            let dict = arrayAdv[indexPath.row] as! NSDictionary
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let strDate = formatter.date(from: dict["Date"] as? String ?? "" )
            
            var strPassedDate = ""
            if strDate != nil
            {
                let formatter1 = DateFormatter()
                formatter1.dateFormat = "EEEE, d MMM"
                strPassedDate = formatter1.string(from: strDate!)
            }
            

            var storyboard1 : UIStoryboard!

            
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                
                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            }
            
            let home = storyboard1.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            home.strcomeFrom = "date"
            home.InitDate = strDate ?? Date()
        
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController


        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionType
        {
            let cell = collectionType.cellForItem(at: indexPath) as! CategoryCell
//            cell.layer.borderColor = UIColor.clear.cgColor
//            cell.layer.borderWidth = 0
            cell.lblTitle.textColor = UIColor.darkGray
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if collectionView == collectionAdv
        {
            let cellSize = CGSize(width:(self.collectionAdv.frame.size.width / 2) - 10 , height:(self.collectionAdv.frame.size.height / 2) - 10)
            return cellSize

//            let cellSize = CGSize(width:self.view.frame.size.width , height:self.conCollectionAdvHeight.constant)
//            return cellSize
        }
        else
        {
//            let count = self.arrayType.count
//            let cellSize = CGSize(width:(self.collectionType.frame.size.width / CGFloat(count)) , height:collectionType.frame.size.height)
//            return cellSize
            
            let count = self.arrayType.count
            let cellSize = CGSize(width: (self.collectionType.frame.size.width / CGFloat(count)) , height: (collectionType.frame.size.height - 10))
            return cellSize

        }
       
        
    }
    
    @objc func timerAction()
    {
        
        if (collectionAdv.visibleCells.count>0)
        {
            let cell:UICollectionViewCell = (collectionAdv.visibleCells[0])
            
            let index:IndexPath = (collectionAdv!.indexPath(for: cell))!
            
            if (index.row+1 < arrayAdv.count) {
                
                collectionAdv.scrollToItem(at: IndexPath(item:index.row+1, section: 0), at:.centeredHorizontally, animated: true)
                
            }
            else
            {
                collectionAdv.scrollToItem(at: IndexPath(item:0, section: 0), at:.centeredHorizontally, animated: false)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let pageWidth = collectionAdv.frame.size.width
        pageControl.currentPage = Int(floor((collectionAdv.contentOffset.x / pageWidth)))
    }
    
    
    
    //MARK:- button Methods

    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btnLanguageClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        if btn.isSelected
        {
            //english
            
            wscallforLanguage(lang: "E") { (result) in
                
                Manager.sharedInstance.dictHomePage.removeAllObjects()
                btn.isSelected = false
                let storyboard1 : UIStoryboard!
                
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
                
                UserDefaults.standard.set(false, forKey: "arabic")
                UserDefaults.standard.synchronize()
                
                let table = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let navigation = UINavigationController()
                navigation.isNavigationBarHidden = true
                navigation.setViewControllers([table], animated: false)
                
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigation
                Manager.sharedInstance.wscallforSideMenu()
                Manager.sharedInstance.wscallforArea()

            }
            
            
        }
        else
        {
            wscallforLanguage(lang: "A") { (result) in
                
                Manager.sharedInstance.dictHomePage.removeAllObjects()

                let storyboard1 : UIStoryboard!
                btn.isSelected = true
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
                
                UserDefaults.standard.set(true, forKey: "arabic")
                UserDefaults.standard.synchronize()
                
                let table = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                let navigation = UINavigationController()
                navigation.isNavigationBarHidden = true
                navigation.setViewControllers([table], animated: false)
                
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigation
                Manager.sharedInstance.wscallforSideMenu()
                Manager.sharedInstance.wscallforArea()

            }

        }
        
    }
    
    @IBAction func btnSearchClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        
        if btn.isSelected
        {
            conviewSearchHeight.constant = self.view.frame.size.width - 99
            txtSearch.becomeFirstResponder()

        }
        else
        {
            conviewSearchHeight.constant = 0
            txtSearch.text = ""
            txtSearch.resignFirstResponder()

        }
    }
    
    
    
    
    //MARK:- webservice
    
    func SetHomepageData(result:NSDictionary)
    {
        self.arrayAdv = NSMutableArray(array: (result["Buttons"] as? Array)!)
        self.arrayType = NSMutableArray(array: (result["ItemTypes"] as? Array)!)
        self.arrPopular = NSMutableArray(array: (result["PopularItems"] as? Array)!)
        let arrPromotions = result["Promotions"] as? [String:Any] ?? [:]
        if arrPromotions.count > 0
        {
            self.arrPopular.add(arrPromotions)
        }
        
        
        self.arrVendors = NSMutableArray(array: (result["Vendors"] as? Array)!)
        
        Manager.sharedInstance.SaveDatainTextFile(fileName: "type", arrData: self.arrayType)
        self.pageControl.isHidden = true
        
        //                        self.pageControl.numberOfPages = self.arrayAdv.count
        
        self.tblPopular.reloadData()
        self.collectionAdv.reloadData()
        self.collectionType.reloadData()
        
        Manager.sharedInstance.Slidetime = result["Sliding_Time"] as? Int
        
        
        let Stringimg = String(format: "%@", result["StripImage"] as? String ?? "")
        
        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: urlString!)
        self.imgtag.setImageWith(url, usingActivityIndicatorStyle: .gray)
        
    }
    
    func wscallforHomePage()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNewHomePage,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
//                        self.arrayAdv = NSMutableArray(array: (result["Advertisements"] as? Array)!)
                        self.arrayAdv = NSMutableArray(array: (result["Buttons"] as? Array)!)
                        self.arrayType = NSMutableArray(array: (result["ItemTypes"] as? Array)!)
                        self.arrPopular = NSMutableArray(array: (result["PopularItems"] as? Array)!)
                        let arrPromotions = result["Promotions"] as? [String:Any] ?? [:]
                        if arrPromotions.count > 0
                        {
                          self.arrPopular.add(arrPromotions)
                        }
                        
                        
                        self.arrVendors = NSMutableArray(array: (result["Vendors"] as? Array)!)
                        
                        Manager.sharedInstance.SaveDatainTextFile(fileName: "type", arrData: self.arrayType)
                        self.pageControl.isHidden = true

//                        self.pageControl.numberOfPages = self.arrayAdv.count
                        
                        self.tblPopular.reloadData()
                        self.collectionAdv.reloadData()
                        self.collectionType.reloadData()

                        Manager.sharedInstance.Slidetime = result["Sliding_Time"] as? Int
                        
                       
                       let Stringimg = String(format: "%@", result["StripImage"] as? String ?? "")
            
                        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
                        let url = URL(string: urlString!)
                        self.imgtag.setImageWith(url, usingActivityIndicatorStyle: .gray)
                        
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
    func wscallforLanguage(lang:String, completion: @escaping (_ finishResult: String) -> Void)
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wUpdateLanguage,lang)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        completion("YES")
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    
            }
            
            
        }
        
    }
}
