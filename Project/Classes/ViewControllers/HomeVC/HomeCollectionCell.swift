//
//  HomeCollectionCell.swift
//  Zazoo
//
//  Created by HTNaresh on 16/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var imgAdv: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
        
        // Initialization code
    }
    
}
