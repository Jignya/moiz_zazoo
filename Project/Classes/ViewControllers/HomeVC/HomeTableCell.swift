//
//  HomeTableCell.swift
//  Project
//
//  Created by HTNaresh on 24/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

protocol homeCellDelegate: class
{
    func selectedDict(dict:[String:Any],index:NSInteger,type:String)
}


class HomeTableCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var collectionPopular: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var arrPopular : NSMutableArray = []

    weak var delegate: homeCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
       
        return  arrPopular.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
            let cellReuseIdentifier = "cell2"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            let dict = arrPopular[indexPath.row] as! NSDictionary
        
            if dict["Image_Url"] as? String != ""
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                cell.imgLogo?.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
            }
            else
            {
                cell.imgLogo.image = UIImage(named: "logo.png")
            }

            if dict["Title"] != nil
            {
                cell.lblTitle.text = (dict["Title"] as? String)

            }
            else
            {
                cell.lblTitle.text = (dict["Item_Name"] as? String)
            }
        
            cell.imgLogo.contentMode = .scaleToFill
        
            return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
          let dict = arrPopular[indexPath.row] as! [String:Any]
          delegate?.selectedDict(dict: dict, index: indexPath.row, type: "")

//            if indexPath.row == 0
//            {
//                let Id = "2"
//                let storyboard1 : UIStoryboard!
//                if UserDefaults.standard.bool(forKey: "arabic")
//                {
//                    storyboard1 = UIStoryboard(name: "Main_Ar", bundle: nil)
//                }
//                else
//                {
//                    storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
//                }
//
//                let home = storyboard1.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
//                home.dictFilterPara.setValue(Id, forKey: "Item_Type_Id")
//                home.strcomeFrom = "home"
//                home.strTitle = (dict["Item_Type_Name"] as? String)!
//
//                let navigationController = UINavigationController()
//                navigationController.isNavigationBarHidden = true
//
//                navigationController.setViewControllers([home], animated: false)
//                let window = UIApplication.shared.delegate!.window!!
//                window.rootViewController = navigationController
//            }
//            else if indexPath.row == 1
//            {
//                let venue = self.storyboard?.instantiateViewController(withIdentifier: "VenueListVC") as! VenueListVC
//
//                venue.strTitle = dict["Item_Type_Name"] as? String
//                venue.strcomeFrom = "venue"
//                venue.Id = 0
//
//                self.navigationController?.pushViewController(venue, animated: true)
//            }
//            else
//            {
//
//                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
//
//                classdetail.strTitle = dict["Item_Type_Name"] as? String
//                self.navigationController?.pushViewController(classdetail, animated: true)
//            }
    
    }
    

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
    
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
//            let cellSize = CGSize(width: 120, height:180)
            let cellSize = CGSize(width: (collectionPopular.frame.size.width / 3) - 5, height:(collectionPopular.frame.size.width / 3) + 90)

            return cellSize
        }
        else
        {
//            let cellSize = CGSize(width: 80, height:120)
            let cellSize = CGSize(width: (collectionPopular.frame.size.width / 3) - 5, height:(collectionPopular.frame.size.width / 3) + 55)
            return cellSize
        }
        
    }
    
   
}
