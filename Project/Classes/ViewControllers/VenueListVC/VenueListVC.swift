//
//  VenueListVC.swift
//  Project
//
//  Created by HTNaresh on 24/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class VenueListVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate
{
    
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    
    @IBOutlet weak var collectionType: UICollectionView!
        
    @IBOutlet weak var collectionPopular: UICollectionView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNorecord: UILabel!


    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var txtGuest: UITextField!
    @IBOutlet weak var btnCelebrate: UIButton!
    @IBOutlet weak var conViewFilterHeight: NSLayoutConstraint!
    @IBOutlet weak var viewFilter: UIView!
    
    var arrayType : NSMutableArray = []
    var arrPopular : NSMutableArray = []
    var strTitle : String!
    var strcomeFrom : String!
    var Id : NSNumber!
    var strcolor : String!

    let datePicker1 = UIDatePicker()



    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblNorecord.text = kNorecord
        
        txtDate.placeholder = kDate
        txtTime.placeholder = kTime
        txtGuest.placeholder = kGuest
        
        btnCelebrate.setTitle(kcelebrate.uppercased(), for: .normal)
        btnCelebrate.setTitle(kClear.uppercased(), for: .selected)

        self.btnSearch.isSelected = false
        self.conViewFilterHeight.constant = 0
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        self.arrayType = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "type")
        
        if strcomeFrom == "venue"
        {
            wscallforFilterVenue()
            self.btnSearchClick(btnSearch)
            btnSearch.isHidden = false
        }
        else
        {
            wscallforVendor()
            btnSearch.isHidden = true

        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.lblTitle.text = self.strTitle
        Manager.sharedInstance.addTabBar2(self, tab: "0")
    }
    

    
    //MARK:- textfiels delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == txtDate || textField == txtTime
        {
            return false
        }
        
        return true
    }
    
    //MARK:- Textfield delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtDate
        {
            SetdatePicker()
        }
        else if textField == txtTime
        {
            SetdatePicker1()
        }
       
    }
    
    //MARK:- Set datepicker
    func SetdatePicker()
    {
        datePicker1.datePickerMode = .date
        datePicker1.addTarget(self, action: #selector(self.updateTextField(_:)), for: .valueChanged)
        let formattt = DateFormatter()
        formattt.dateFormat = "dd/MMMM/yyyy"
        datePicker1.minimumDate = Date()
        txtDate.inputView = datePicker1
    }
    
    
    @objc func updateTextField(_ sender:UIDatePicker)
    {
        let dateForm1 = DateFormatter()
        dateForm1.dateFormat = "dd/MM/yyyy"
        let date1 = sender.date
        let strDate = dateForm1.string(from: date1)
        txtDate.text = strDate
    }
    
    func SetdatePicker1()
    {
        datePicker1.datePickerMode = .time
        datePicker1.addTarget(self, action: #selector(self.updateTextField1(_:)), for: .valueChanged)
        let formattt = DateFormatter()
        formattt.dateFormat = "hh:mm a"
        datePicker1.minimumDate = Date()
        txtTime.inputView = datePicker1
    }
    
    @objc func updateTextField1(_ sender:UIDatePicker)
    {
        let dateForm1 = DateFormatter()
        dateForm1.dateFormat = "hh:mm a"
        let date1 = sender.date
        let strDate = dateForm1.string(from: date1)
        txtTime.text = strDate
    }
    
    
   
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionType
        {
            return arrayType.count
        }
        else
        {
            return arrPopular.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == collectionType
        {
            let cellReuseIdentifier = "cell1"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            let dict = arrayType[indexPath.row] as! NSDictionary
            cell.lblTitle.text = (dict["Item_Type_Name"] as? String)?.uppercased()
            //            let strcode = dict["Color_Code"] as? String
            //            cell.viewBg.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: strcode ?? "")
            
            if dict["Item_Type_Id"] as? NSNumber == 2
            {
                cell.imgLogo.image = UIImage(named: "Cls.png")
                cell.imgBg.image = UIImage(named: "cls_bg.png")
            }
            else if dict["Item_Type_Id"] as? NSNumber == 3
            {
                cell.imgLogo.image = UIImage(named: "Sem.png")
                cell.imgBg.image = UIImage(named: "Sem_bg.png")
            }
            else if dict["Item_Type_Id"] as? NSNumber == 4
            {
                cell.imgLogo.image = UIImage(named: "Cmp.png")
                cell.imgBg.image = UIImage(named: "Cmp_bg.png")
            }
            else if dict["Item_Type_Id"] as? NSNumber == 5
            {
                cell.imgLogo.image = UIImage(named: "Venu.png")
                cell.imgBg.image = UIImage(named: "Venu_bg.png")
            }
            
            if dict["Item_Type_Id"] as? NSNumber == Id
            {

                let strcode = dict["Color_Code"] as? String
                cell.lblTitle.textColor = GlobalConstants.themeClr
            }
            else
            {
                cell.lblTitle.textColor = UIColor.darkGray

                if Id == 0 && dict["Item_Type_Id"] as? NSNumber == 5
                {
                    cell.lblTitle.textColor = GlobalConstants.themeClr
                }
            }

            
            return cell
        }
//        {
//            let cellReuseIdentifier = "cell1"
//            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
//
//            let dict = arrayType[indexPath.row] as! NSDictionary
//
//            cell.lblTitle.text = dict["Item_Type_Name"] as? String
//
//            if dict["Item_Type_Id"] as? NSNumber == Id
//            {
//
//                let strcode = dict["Color_Code"] as? String
//
//                cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: strcode ?? "")
//                cell.lblTitle.textColor = UIColor.white
//            }
//            else
//            {
//                cell.lblTitle.backgroundColor = UIColor(red: 244.0/255.0, green: 241.0/255.0, blue: 234.0/255.0, alpha: 1)
//                cell.lblTitle.textColor = UIColor.darkGray
//
//                if Id == 0 && dict["Item_Type_Id"] as? NSNumber == 5
//                {
//                    let strcode = dict["Color_Code"] as? String
//                    cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: strcode ?? "")
//                    cell.lblTitle.textColor = UIColor.white
//                }
//            }
//
//            return cell
//        }
        else
        {
            let cellReuseIdentifier = "cell2"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            cell.imgLogo.layer.cornerRadius = 3.0
            cell.imgLogo.clipsToBounds = true

            let dict = arrPopular[indexPath.row] as! NSDictionary
            
            if strcomeFrom == "venue"
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
                
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                
                let url = URL(string: urlString!)
                cell.imgLogo.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
                cell.lblTitle.text = dict["Class_Name"] as? String
                
                cell.viewShadow.backgroundColor = UIColor.clear
                cell.viewShadow.bounds = cell.bounds
                
                cell.setGradientBackground(view: cell.viewShadow, colorTop: UIColor.clear, colorBottom: UIColor.black.withAlphaComponent(0.65))

            }
            else
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Vendor_Image_Url"] as? String ?? "")
                
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                
                let url = URL(string: urlString!)
                cell.imgLogo.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
                cell.lblTitle.text = dict["Vendor_Name"] as? String
                
                cell.viewShadow.backgroundColor = UIColor.clear
                cell.viewShadow.bounds = cell.bounds
                
                cell.setGradientBackground(view: cell.viewShadow, colorTop: UIColor.clear, colorBottom: UIColor.black.withAlphaComponent(0.65))
                
            }
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionPopular
        {
            let dict = NSMutableDictionary(dictionary: arrPopular[indexPath.row] as! Dictionary)
            
            if strcomeFrom == "venue"
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "PartyDetailVC") as! PartyDetailVC
                classdetail.classId = dict["Class_Id"] as? NSNumber
                classdetail.strDate = String(format: "%@ %@",(dict["Guests"] as? NSNumber ?? 0)! , kGuest )
                self.navigationController?.pushViewController(classdetail, animated: true)
            }
            else
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
                classdetail.VendorId = dict["Vendor_Id"] as? NSNumber
                classdetail.typeId = Id
                self.navigationController?.pushViewController(classdetail, animated: true)
            }
        }
        else
        {
            let dict = arrayType[indexPath.row] as! NSDictionary
            
            lblTitle.text = dict["Item_Type_Name"] as? String

            
            if dict["Item_Type_Id"] as? NSNumber == 5
            {

                txtDate.text = ""
                txtTime.text = ""
                txtGuest.text = ""
                self.btnSearchClick(btnSearch)
                btnCelebrate.isSelected = false
                strcomeFrom = "venue"
                Id = dict["Item_Type_Id"] as? NSNumber
                collectionType.reloadData()

                wscallforFilterVenue()
                btnSearch.isHidden = false
                if btnSearch.isSelected
                {
                    btnSearch.isSelected = false
                }
            }
            else
            {
                strcomeFrom = "vendor"
                Id = dict["Item_Type_Id"] as? NSNumber
                collectionType.reloadData()
                wscallforVendor()
                btnSearch.isHidden = true
                conViewFilterHeight.constant = 0
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if collectionView == collectionType
        {
            
            let count = self.arrayType.count
            let cellSize = CGSize(width: (self.collectionType.frame.size.width / CGFloat(count)) , height: (collectionType.frame.size.height - 10))
            return cellSize
            
        }
        else
        {
            let height =  self.collectionPopular.frame.size.width / 2
            let height1 = (UIDevice.current.userInterfaceIdiom == .pad) ? (height + 70) : (height + 35)

            let cellSize = CGSize(width:(self.collectionPopular.frame.size.width), height:height1)
            return cellSize
        }
        
        
    }
    
    
    
    //MARK:- buttom methos
    
    @IBAction func btnbackClick(_ sender: Any)
    {
        let array = self.navigationController?.viewControllers ?? []
        for k in (0 ..< array.count)
        {
            let controller: UIViewController? = (array[k] )
            if (controller is HomeVC)
            {
                self.navigationController?.popToViewController(controller!, animated: true)
                return
            }
        }
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        
    }
    
    @IBAction func btnCelebrateClick(_ sender: Any)
    {
        self.view.endEditing(true)
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        
        if !btn.isSelected
        {
            txtDate.text = ""
            txtTime.text = ""
            txtGuest.text = ""
        }

        
        wscallforFilterVenue()
    }
    
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnSearchClick(_ sender: Any)
    {
        self.view.endEditing(true)
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        
        if btn.isSelected
        {
            self.conViewFilterHeight.constant = 0
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.conViewFilterHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 250 : 185
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }
        else
        {
            self.conViewFilterHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 250 : 185
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.conViewFilterHeight.constant = 0
                            self.view.layoutIfNeeded()
            }, completion: nil)
        }

        
       
        
//
//        if btn.isSelected
//        {
//            conviewSearchHeight.constant = self.view.frame.size.width - 99
//            txtSearch.becomeFirstResponder()
//
//        }
//        else
//        {
//            conviewSearchHeight.constant = 0
//            txtSearch.text = ""
//            txtSearch.resignFirstResponder()
//
//        }
    }
    
    //MARK:- webservice
    func wscallforVendor()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetClassesVendors,Id,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrPopular = NSMutableArray(array: (result["Vendors"] as? Array)!)
                        
                        if self.arrPopular.count > 0
                        {
                            self.collectionPopular.isHidden = false
                            self.lblNorecord.isHidden = true
                            self.collectionPopular.reloadData()
                        }
                        else
                        {
                            self.collectionPopular.isHidden = true
                            self.lblNorecord.isHidden = false
                        }
                        
                        
//                        self.lblTitle.text = self.strTitle
                    }
            }
        }
        
    }
    
    func wscallforVenue()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetParties,Id,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrPopular = NSMutableArray(array: (result["Classes"] as? Array)!)
                        self.collectionPopular.reloadData()
                        
//                        self.lblTitle.text = self.strTitle

                        
                    }
                    
            }
            
            
        }
        
    }
    
    func wscallforFilterVenue()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wGetParties)
        
        let dictPara:NSDictionary = ["Lang":UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E","Class_Date":self.txtDate.text!,"Time":self.txtTime.text!,"Guests":self.txtGuest.text!]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrPopular = NSMutableArray(array: (result["Classes"] as? Array)!)
                        
                        if self.arrPopular.count > 0
                        {
                            self.collectionPopular.isHidden = false
                            self.lblNorecord.isHidden = true
                            self.collectionPopular.reloadData()
                        }
                        else
                        {
                            self.collectionPopular.isHidden = true
                            self.lblNorecord.isHidden = false
                        }
                        
//                        self.lblTitle.text = self.strTitle
                        
                        
                    }
                    
            }
        }
        
    }

}
