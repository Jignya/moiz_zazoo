//
//  LoginVC.swift
//  Zazoo
//
//  Created by HTNaresh on 18/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class LoginVC: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate,UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblhappy: UILabel!
    @IBOutlet weak var lblLoginwith: UILabel!
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPW: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var btnFacebook: ButtonExtender!
    @IBOutlet weak var btnGplus: ButtonExtender!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var btnGuest: UIButton!
    @IBOutlet weak var lblsignInwith: UILabel!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    
    var strComeFrom : String!
    var strName : String!
    var strLastName : String!

    var strEmail : String!
    var strPw : String!
    var strMobile : String!

    var Fb_id : Int = 0
    var Gplus_id : Int = 0
    var login_type : Int = 1
    var UserImage : String = ""
    
    
    override func viewDidLoad()
    {
        UserDefaults.standard.set("YES", forKey: GlobalConstants.kisFirstTime)
        UserDefaults.standard.synchronize()
        

        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        img1.isHidden = true
        img2.isHidden = true
        
        txtUser.placeholder = kEmail
        txtPassword.placeholder = kPassword
        lblLogin.text = kLogin.uppercased()
        btnForgotPW.setTitle(kForgotPW, for: .normal)
        btnRegister.setTitle(ksignup.uppercased(), for: .normal)
        btnGuest.setTitle(kasGuest.uppercased(), for: .normal)
        btnback.setTitle(kback.uppercased(), for: .normal)
        
        btnFacebook.imageView?.contentMode = .scaleAspectFit
        btnGplus.imageView?.contentMode = .scaleAspectFit
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self

        if strComeFrom == "class"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "1")
            btnback.isHidden = false
        }
        else if strComeFrom == "account"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "2")
            btnback.isHidden = true
        }
        else if strComeFrom == "cart"
        {
            btnback.isHidden = false
            Manager.sharedInstance.addTabBar2(self, tab: "4")
        }
        else
        {
            btnback.isHidden = true
        }

        
    }
    
//    func shadowImage()
//    {
//        img1.layer.shadowColor = UIColor.black.cgColor
//        img1.layer.shadowOpacity = 1
//        img1.layer.shadowOffset = CGSize.zero
//        img1.layer.shadowRadius = 10
//        img1.layer.shadowPath = UIBezierPath(rect: img1.bounds).cgPath
//        img1.layer.shouldRasterize = true
//
//        img2.layer.shadowColor = UIColor.black.cgColor
//        img2.layer.shadowOpacity = 1
//        img2.layer.shadowOffset = CGSize.zero
//        img2.layer.shadowRadius = 10
//        img2.layer.shadowPath = UIBezierPath(rect: img2.bounds).cgPath
//        img2.layer.shouldRasterize = true
//    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if btnFacebook.isSelected || btnGplus.isSelected
        {
            Manager.sharedInstance.removeTabbar()
            btnFacebook.isSelected = false
            btnGplus.isSelected = false
        }
    }
    
    
    //MARK:- Button click
    
    @IBAction func btnClose1Click(_ sender: Any)
    {
        let btn = sender as! UIButton
        
        if btn.tag == 0
        {
            txtUser.text = ""
        }
        else
        {
            txtPassword.text = ""
        }
    }
    
    //MARK:- handle Back
    
    @objc func handleSwipeback(_ gestureRecognizer: UIGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            self.btnBackClick(btnback)
        }
    }
    @IBAction func btnGuestClick(_ sender: Any)
    {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLoginClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if (txtUser.text == "")
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryouremailaddressEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
            
        else if(!(isValidEmail(testStr: txtUser.text!)))
        {
            let alert = UIAlertController(title:kWarningEn, message:kEmailValidationEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
            
        else if(txtPassword.text=="")
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourpasswordEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
        else
        {
            wscallforLogin()
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func btnForgotPwClick(_ sender: Any)
    {
        self.view.endEditing(true)
        let home = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPWVC") as! ForgotPWVC
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    @IBAction func btnRegisterClick(_ sender: Any)
    {
        self.view.endEditing(true)
        let home = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        home.strComeFrom = strComeFrom
        self.navigationController?.pushViewController(home, animated: false)
        
    }
    
    @IBAction func btnFacebookClick(_ sender: Any)
    {
        self.view.endEditing(true)
        btnFacebook.isSelected = true

        UserDefaults.standard.set(false, forKey: "google")
        UserDefaults.standard.set(true, forKey: "fb")
        UserDefaults.standard.synchronize()
        
        if ((FBSDKAccessToken.current()) != nil)
        {
            // User is logged in, do work such as go to next view controller.
        }
        
        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.native
        let params = ["fields": "location, name, email, about, picture.type(large)"]
        
        
        login.logIn(withReadPermissions: ["email"], from: self, handler: {(_ result: FBSDKLoginManagerLoginResult?, _ error: Error?) -> Void in
            if error != nil
            {
                print("Process error")
                let alert = UIAlertController(title:kWarningEn, message:"Requested time out", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                alert.show()
            }
            else if result?.isCancelled == true
            {
                print("Cancelled")
                login.logOut()
            }
            else
            {
                
                print("Logged in")
                
                if (FBSDKAccessToken.current() != nil)
                {
                    FBSDKGraphRequest(graphPath: "/me", parameters: params).start(completionHandler: {(_ connection: FBSDKGraphRequestConnection?, _ result: Any?, _ error: Error?) -> Void in
                        
                        if error == nil
                        {
                            guard let dict = result as? Dictionary<AnyHashable, Any>else
                            {
                                SwiftLoggor.fileContent = SwiftLoggor.log(message:result.debugDescription,event:ErroTypes.e)
                                return
                            }
                            
                            let id = dict["id"] as! String
                            
                            self.login_type = 2
                            self.Gplus_id = 0
                            self.Fb_id = Int(id)!
                            self.strLastName = ""
                            self.strName = (dict["name"] as? String ?? "")!
                            self.strEmail = (dict["email"] as? String ?? "")!
                            self.strPw = "###"
                            self.strMobile = ""
                            
                            
                            let facebookProfileUrl = "http://graph.facebook.com/\(self.Fb_id)/picture?type=large"
                            self.UserImage = facebookProfileUrl
                            self.wscallforSignUp()
                            login.logOut()
                            
                        }
                        else
                        {
                            print(error.debugDescription)
                            let alert = UIAlertController(title:kWarningEn, message:"Cant connect with facebook", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                            alert.show()
                        }
                    })
                }
                else
                {
                    print(error.debugDescription)
                }
                
            }
        })
    }
    
    @IBAction func btnGPlusClick(_ sender: Any)
    {
        self.view.endEditing(true)
        btnGplus.isSelected = true

        
        UserDefaults.standard.set(true, forKey: "google")
        UserDefaults.standard.set(false, forKey: "fb")
        UserDefaults.standard.synchronize()
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    //MARK:- textfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
//        if textField == txtUser
//        {
//            img1.isHidden = false
//
//        }
//        else if textField == txtPassword
//        {
//            img2.isHidden = false
//        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
//        if textField == txtUser
//        {
//            img1.isHidden = false
//
//        }
//        else if textField == txtPassword
//        {
//            img2.isHidden = false
//        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
//        if textField == txtUser
//        {
//            img1.isHidden = true
//        }
//        else if textField == txtPassword
//        {
//            img2.isHidden = true
//        }
        
       
    }
    
    //MARK:- Google sign in
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,withError error: Error!)
    {
        
        print("sign In")
        if let error = error
        {
            print("\(error.localizedDescription)")
            let alert = UIAlertController(title:kWarningEn, message:"Requested time out", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else
        {
            // Perform any operations on signed in user here.
            let userId1 = (user.userID)
            // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            
            login_type = 3
            Fb_id = 0
            Gplus_id = (userId1! as NSString).integerValue
            strName = user.profile.name
            strEmail = user.profile.email
            strPw = "###"
            strMobile = ""
            strLastName = ""
            
            if user.profile.hasImage
            {
                let dimension = round(150 * UIScreen.main.scale);
                let pic = user.profile.imageURL(withDimension: UInt(dimension)) as NSURL
                self.UserImage = pic.path!
                //                print(self.UserImage)
            }
            else
            {
                self.UserImage = ""
            }
            
                wscallforSignUp()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,withError error: Error!)
    {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    func sign(_ signIn: GIDSignIn!,present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,dismiss viewController: UIViewController!)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- Webservice
    
    func wscallforLogin()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wLogin)
        
        let dictPara:NSDictionary = ["Email":self.txtUser.text!,"Password":self.txtPassword.text!]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let userId = result["App_User_Id"] as! NSNumber
                        UserDefaults.standard.set(userId, forKey: "userid")
                        UserDefaults.standard.set(true, forKey: GlobalConstants.kisLogin)
                        
                        Manager.sharedInstance.wscallforUserDetail()
                        
                        UserDefaults.standard.synchronize()
                        
                            if self.strComeFrom == "account"
                            {
                                let home = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
                                
                                let navigationController = UINavigationController()
                                navigationController.isNavigationBarHidden = true
                                navigationController.setViewControllers([home], animated: false)
                                let window = UIApplication.shared.delegate!.window!!
                                window.rootViewController = navigationController
                                
                        }
                        else if self.strComeFrom == "cart"
                        {
                        self.navigationController?.popViewController(animated: false)
                        }
                        else if self.strComeFrom == "class"
                        {
                          self.navigationController?.popViewController(animated: false)
                        }
                        else
                        {
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            
                            let navigationController = UINavigationController()
                            navigationController.isNavigationBarHidden = true
                            navigationController.setViewControllers([home], animated: false)
                            let window = UIApplication.shared.delegate!.window!!
                            window.rootViewController = navigationController
                            
                        }
                        
                        
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    
            }
        }
        
    }
    
    func wscallforSignUp()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRegister)
        
        let dictPara:NSDictionary = ["FirstName":strName,"LastName":strLastName,"Password":strPw,"Email_Id":strEmail,"Mobile":strMobile,"Address":"","Login_Type":login_type,"Facebook_Id":Fb_id,"Google_Id":Gplus_id,"Children":[]]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            print(result)
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {

                        let userId = result["App_User_Id"] as? NSNumber
                        UserDefaults.standard.set(userId, forKey: "userid")
                        UserDefaults.standard.set(true, forKey: GlobalConstants.kisLogin)
                        UserDefaults.standard.synchronize()
                        Manager.sharedInstance.wscallforUserDetail()
                        

                        
                        if self.strComeFrom == "account"
                        {
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
                            
                            let navigationController = UINavigationController()
                            navigationController.isNavigationBarHidden = true
                            navigationController.setViewControllers([home], animated: false)
                            let window = UIApplication.shared.delegate!.window!!
                            window.rootViewController = navigationController
                            
                        }
                        else if self.strComeFrom == "class"
                        {
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            
                            let navigationController = UINavigationController()
                            navigationController.isNavigationBarHidden = true
                            navigationController.setViewControllers([home], animated: false)
                            let window = UIApplication.shared.delegate!.window!!
                            window.rootViewController = navigationController
                            
                        }
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
            }
            
        }
        
    }
    
    
}
