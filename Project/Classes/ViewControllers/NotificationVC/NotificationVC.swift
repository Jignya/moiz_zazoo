//
//  NotificationVC.swift
//  Zazoo
//
//  Created by HTNaresh on 17/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblNorecord: UILabel!

    var arrList:NSMutableArray = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblNorecord.text = kNorecord

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        self.WSCallForNotificationList(load: true)
        
        lblTitle.text = kNotification
        

    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "2")
    }
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    //MARK:- tableview method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "AddressCell"
        let cell:AddressCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! AddressCell
        
        
        let objectData = arrList.object(at: indexPath.row) as! NSDictionary
        
        cell.btnUnread.setTitle(kUnread, for: .normal)

        if cell.isSelected
        {
            cell.lblDesc.textColor = UIColor.lightGray
            cell.lblTitle.textColor = UIColor.lightGray
            cell.lbldate.textColor = UIColor.lightGray
        }
        else
        {
            cell.lblDesc.textColor = UIColor.black
            cell.lblTitle.textColor = UIColor.black
            cell.lbldate.textColor = UIColor.black
        }

        
        cell.lblDesc.text = objectData["NOTIFICATION_TEXT"] as? String
        cell.lblTitle.text =  objectData["TITLE"] as? String
        cell.lbldate.text =  objectData["NOTIFICATION_DATE"] as? String
        
        cell.btnUnread.isHidden = true
        
        let readStatus = objectData["IS_READ"] as? Int
        
        if readStatus == 0
        {
            cell.btnUnread.isHidden = false
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 95
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrList.object(at: indexPath.row) as! Dictionary)
        let home = self.storyboard?.instantiateViewController(withIdentifier: "NotificationDetailVC") as! NotificationDetailVC
        home.dictdata = dict
        home.pushNotId = dict["PUSH_CAMPAIGN_ID"] as? NSNumber ?? 0
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let dictdata = self.arrList[indexPath.item] as? [String : Any]
        
        let delete = UITableViewRowAction(style: .destructive, title: kDeleteEn) { (action, indexPath) in
            
            
            let alertController = UIAlertController(title:kWarningEn, message: kDeleteNot, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
                
                
                let NotId = dictdata?["PUSH_CAMPAIGN_ID"] as? NSNumber ?? 0
                let strUrl:String = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wDeleteNotification,NotId)
                
                let dictPara = NSDictionary()
            WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
                
                    DispatchQueue.main.async
                        {
                            // print(result)
                            let strTemp = result.object(forKey: "Message") as? String
                            if strTemp == "Success"
                            {
                                self.WSCallForNotificationList(load: true)
                            }
                            else
                            {
                                
                            }
                    }
                }
                
                
            }
            
            let CancelAction = UIAlertAction(title:kCancel, style: .default) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(CancelAction)
            
            // Present the controller
            alertController.show()
        }
        
        delete.backgroundColor = UIColor(red:217/255,green:14/255,blue:44/255,alpha:1.0)
        return [delete]
    }
    
    func WSCallForNotificationList(load:Bool)
    {
        var strUrl:String = ""
        
        var userId : String = ""
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        else
        {
            userId = "0"
        }

        
        strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNotifications,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: load, LoadingText: "")
            { (result) in
                DispatchQueue.main.async
                {
                    // print(result)
                    let strTemp = result.object(forKey: "Message") as? String
                    if strTemp == "Success"
                    {
                        self.arrList = NSMutableArray(array: (result["Notifications"] as? Array)!)
                        
                        if self.arrList.count > 0
                        {
                            self.tblList.isHidden = false
                            self.tblList.reloadData()
                        }
                        else
                        {
                            self.tblList.isHidden = true
                        }
                    }
                    else
                    {
                        self.tblList.isHidden = true
                        
                        let alertController = UIAlertController(title:kWarningEn, message:strTemp, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default)
                        {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        // Add the actions
                        alertController.addAction(okAction)
                        // Present the controller
                        alertController.show()
                    }
            }
        }
    }
    
}


