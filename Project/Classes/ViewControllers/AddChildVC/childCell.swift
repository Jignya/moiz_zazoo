//
//  childCell.swift
//  Project
//
//  Created by HTNaresh on 2/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class childCell: UITableViewCell
{
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var btnBoy: UIButton!
    @IBOutlet weak var btnGirl: UIButton!
    
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnCalendar: UIButton!
    
//    @IBOutlet weak var txtGender: UITextField!
//    @IBOutlet weak var btnRemove: UIButton!
//    @IBOutlet weak var btnAdd: UIButton!
//    @IBOutlet weak var btnUpdate: UIButton!
//    @IBOutlet weak var viewAdd: UIView!
//    @IBOutlet weak var viewUpdate: UIView!
    
    
    

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

        
        if self.btnBoy != nil
        {
            self.btnBoy.setImage(UIImage(named: "boy.png"), for: .normal)
            self.btnGirl.setImage(UIImage(named: "girl.png"), for: .normal)
            
            self.btnBoy.setImage(UIImage(named: "boy1.png"), for: .selected)
            self.btnGirl.setImage(UIImage(named: "girl1.png"), for: .selected)
            
            
            txtAge.placeholder = kAge
            txtName.placeholder = kName
            txtDob.placeholder = kDOB
        }
        
       

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
