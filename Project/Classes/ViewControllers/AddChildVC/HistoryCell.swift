//
//  HistoryCell.swift
//  Project
//
//  Created by HTNaresh on 19/6/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell
{
    @IBOutlet weak var lblVendor: UILabel!
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
