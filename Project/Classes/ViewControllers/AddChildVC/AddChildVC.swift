//
//  RegisterVC.swift
//  Project
//
//  Created by HTNaresh on 1/4/19.
//  Copyright ©/Users/sumitcomputers/Documents/SVN Code/jignya/Innova app/Repo code/Zaazoo_Local/Zaazoo/Project/Classes 2019 HightecIT. All rights reserved.
//

import UIKit

class AddChildVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var lblAddChildren: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var tblChild: UITableView!
    @IBOutlet weak var conTblChildHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //popup outlet
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblGender: UILabel!


    @IBOutlet weak var btnBoy: UIButton!
    @IBOutlet weak var btnGirl: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnResave: UIButton!
    @IBOutlet weak var btnClose: UIButton!

    @IBOutlet weak var viewAdd: UIView!
    @IBOutlet weak var viewUpdate: UIView!
    
    @IBOutlet weak var viewChildPopup: UIView!
    @IBOutlet weak var viewChildMain: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var conbtnDoneHeight: NSLayoutConstraint!
    @IBOutlet weak var lblClassHistory: UILabel!
    
    @IBOutlet weak var tblClassHistory: UITableView!
    @IBOutlet weak var conTblClassHistoryHeight: NSLayoutConstraint!
    
    @IBOutlet weak var conViewChildPopupHeight: NSLayoutConstraint!
    @IBOutlet weak var lblVendor: UILabel!
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!

    
    var arrChild = [[String : Any]]()
    var arrChildSelected = [[String : Any]]()

    var arrHistory = [[String : Any]]()

    var dictChild : [String : Any] = [:]
    var dictChild1 : [String : Any] = [:]

    var strDob : String!
    
    var childrenId : NSNumber = 0

    var strComeFrom : String!

    var isSingleSelection : Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblClassHistory.text = kClassHistory
        lblTitle.text = kChildinfo
        lblAddChildren.text = kaddChildren
        lblName.text = kName
        lblDob.text = kDOB
        lblAge.text = kAge
        lblGender.text = kGender
        
        lblVendor.text = kVendorLbl
        lblActivity.text = kActivity
        lblDate.text = kDate
        lblStatus.text = kStatus

        btnSave.setTitle(kSave, for: .normal)
        btnResave.setTitle(kSave, for: .normal)
        btnRemove.setTitle(kDeleteEn, for: .normal)
        btnDone.setTitle(kDone, for: .normal)

        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        dictChild = ["Children_Name":"" , "Gender" : "" , "Date_of_Birth": "","Children_Id":0]
        
        viewChildPopup.layer.cornerRadius = 5.0
        viewChildPopup.clipsToBounds = true
        
        viewChildMain.isHidden = true
        
        self.txtDob.addTarget(self, action: #selector(textfieldBeginEdit(_:)), for: .editingDidBegin)
    
        
//        if Manager.sharedInstance.dictprofile.allKeys.count > 0
//        {
//            let array  = Manager.sharedInstance.dictprofile.value(forKey: "Children") as! Array<Any>
//            if array.count > 0
//            {
//              arrChild = array as! [[String : Any]]
//            }
//            tblChild.reloadData()
//        }
//
        conbtnDoneHeight.constant = 0
        
    
        if strComeFrom == "detail"
        {
            conbtnDoneHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 60:40
        }
       

        Manager.sharedInstance.removeTabbar()
        self.wscallforGetChild()


    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "2")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblClassHistory
        {
            return arrHistory.count
        }
        return arrChild.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if strComeFrom == "detail"
        {
            let cellReuseIdentifier = "AccountCell1"
            
            let cell:AccountCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! AccountCell
            
            let dict = arrChild[indexPath.row]
            cell.lblTitle.text = dict["Children_Name"] as? String
            cell.imgArw.isHidden = true
            
            let containsEntry = arrChildSelected.contains{ $0["Children_Name"] as? String == dict["Children_Name"] as? String }
            if containsEntry
            {
                cell.imgArw.isHidden = false
            }
            
            conTblChildHeight.constant = CGFloat(arrChild.count * 70)
            tblChild.updateConstraintsIfNeeded()
            tblChild.layoutIfNeeded()
            return cell
        }
        else
        {
            if tableView == tblClassHistory
            {
                
                let cellReuseIdentifier = "HistoryCell"
                
                let cell:HistoryCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! HistoryCell
                
                let dict = arrHistory[indexPath.row]
                cell.lblVendor.text = dict["Vendor_Name"] as? String
                cell.lblActivity.text = dict["Class_Name"] as? String
                cell.lblDate.text = String(format: "%@ - %@", (dict["Start_Date"] as? String ?? "")! , (dict["End_Date"] as? String ?? "")!)
                cell.lblStatus.text = dict["Status"] as? String

                return cell
            }
            else
            {
                
                let cellReuseIdentifier = "AccountCell"
                
                let cell:AccountCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! AccountCell
                
                let dict = arrChild[indexPath.row]
                cell.lblTitle.text = dict["Children_Name"] as? String
                
                let arr1 = dict["CurrentClasses"] as!
                    [[String:Any]]
                if arr1.count == 0
                {
                    cell.conTblClassHeight.constant = 0
                }
                else
                {
                    cell.arrList = arr1
                    cell.tblClassList.reloadData()
                    cell.conTblClassHeight.constant = CGFloat (arr1.count * 48)
                }
                
                conTblChildHeight.constant = tblChild.contentSize.height
                tblChild.updateConstraintsIfNeeded()
                tblChild.layoutIfNeeded()
                return cell
            }
           
        }
       
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = arrChild[indexPath.row]
        
        if strComeFrom == "detail"
        {
            if isSingleSelection == true
            {
                arrChildSelected.removeAll()
                arrChildSelected.append(dict)
                tblChild.reloadData()
            }
            else
            {
                let containsEntry = arrChildSelected.contains{ $0["Children_Name"] as? String == dict["Children_Name"] as? String }
                if containsEntry
                {
                    arrChildSelected.removeAll(where: { $0["Children_Name"] as? String == dict["Children_Name"] as? String })
                }
                else
                {
                    arrChildSelected.append(dict)
                }
                
                tblChild.reloadData()
            }
        }
        else
        {
            viewChildMain.isHidden = false
            self.view.bringSubviewToFront(viewChildMain)
            viewAdd.isHidden = true
            viewUpdate.isHidden = false
            
            childrenId = (dict["Children_Id"] as? NSNumber ?? 0)!
            
            self.txtName.text = dict["Children_Name"] as? String
            self.txtDob.text = dict["Date_of_Birth"] as? String
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let birthDate = dateFormatter.date(from: self.txtDob.text!)
            
            let now = Date()
            let calendar = Calendar.current
            
            let ageComponents = calendar.dateComponents([.year], from: birthDate!, to: now)
            let age = ageComponents.year!
            self.txtAge.text = String(format: "%d year", age)

            
            if dict["Gender"] as? String == "Female"
            {
                self.btnGirl.isSelected = true
                self.btnBoy.isSelected = false
            }
            else if dict["Gender"] as? String == "Male"
            {
                self.btnBoy.isSelected = true
                self.btnGirl.isSelected = false
            }
            else
            {
                self.btnGirl.isSelected = false
                self.btnBoy.isSelected = false
            }
            
            
            arrHistory = dict["HistoryClasses"] as! [[String:Any]]
            if arrHistory.count > 0
            {
                tblClassHistory.reloadData()
                conTblClassHistoryHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 160:130
                conViewChildPopupHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 465:350
            }
            else
            {
                conTblClassHistoryHeight.constant = 0
                conViewChildPopupHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 320:220

            }
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if strComeFrom == "detail"
        {
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                return 70

            }
            else
            {
                return 50
            }

        }
        else
        {
            if tableView == tblClassHistory
            {
                if (UIDevice.current.userInterfaceIdiom == .pad)
                {
                    return 25

                }
                else
                {
                    return 40
                }
            }
            else
            {
                let dict = arrChild[indexPath.row]
                let arr1 = dict["CurrentClasses"] as!
                    [[String:Any]]
                if arr1.count == 0
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return 83

                    }
                    else
                    {
                        return 63
                    }
                }
                else
                {
                    let arr1 = dict["CurrentClasses"] as! [[String:Any]]
                    
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return CGFloat(83 + (arr1.count * 60))
                    }
                    else
                    {
                        return CGFloat(63 + (arr1.count * 48))
                    }
                }
                
            }
            
        }
    }
    
    @IBAction func btnGenderClick(_ sender: Any)
    {
        self.view.endEditing(true)

        let btn = sender as! UIButton
        
//        let position:CGPoint = btn.convert(CGPoint.zero, to:self.tblChild)
//        let indexPath : IndexPath = self.tblChild.indexPathForRow(at: position)!
//
//        let cell = self.tblChild.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! childCell
//
//        var dict = arrChild[indexPath.row]
        if btn.tag == 1
        {
            dictChild["Gender"] = "Male"
            self.btnBoy.isSelected = true
            self.btnGirl.isSelected = false
        }
        else
        {
            dictChild["Gender"] = "Female"
            self.btnBoy.isSelected = false
            self.btnGirl.isSelected = true
            
        }
        
//        arrChild[indexPath.row] = dict
//        tblChild.reloadData()
        
    }
    
    
    @objc func textfieldBeginEdit(_ sender: Any)
    {
        let txt = sender as! UITextField
        if txt.tag == 1
        {
            //gender
        }
        else
        {
            //dob
            
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = .date

            if self.txtDob.text != ""
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy"
                if let birthDate = dateFormatter.date(from: self.txtDob.text!)
                {
                   datePickerView.setDate(birthDate, animated: false)
                }
            }
            
            datePickerView.maximumDate = Date()
            txt.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged1), for: .valueChanged)
            
        }
        
    }
    
    @objc func textfielddidend(_ sender: Any)
    {
        let txt = sender as! UITextField
        let position:CGPoint = txt.convert(CGPoint.zero, to:self.tblChild)
        let indexPath : IndexPath = self.tblChild.indexPathForRow(at: position)!
        
        let cell = self.tblChild.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! childCell
        
        var dict = arrChild[indexPath.row]
        if txt.tag == 0
        {
            dict["Children_Name"] = cell.txtName.text
        }
//        else if txt.tag == 1
//        {
//            dict["Gender"] = strGender
//        }
        else if txt.tag == 2
        {
            dict["Date_of_Birth"] = strDob
        }
        
        arrChild[indexPath.row] = dict
        tblChild.reloadData()
        
        print(arrChild)
        
    }
    
    @objc func datePickerValueChanged1(sender:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.txtDob.text = dateFormatter.string(from: sender.date)
        
        let now = Date()
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: sender.date, to: now)
        let age = ageComponents.year!
        self.txtAge.text = String(format: "%d year", age)
    }
    
    //MARK:- Button methods
    
    @IBAction func btnDoneClick(_ sender: Any)
    {
        let array = self.navigationController?.viewControllers ?? []
        let controller: UIViewController? = array[array.count - 2]

        if (controller is ClassDetailVC)
        {
            let classdetail = controller as! ClassDetailVC
            classdetail.arrChildSelected = NSMutableArray(array: arrChildSelected as Array)
            self.navigationController?.popToViewController(classdetail, animated: true)
        }
        else if (controller is ClassDetail1VC)
        {
            let classdetail = controller as! ClassDetail1VC
            classdetail.arrChildSelected = NSMutableArray(array: arrChildSelected as Array)
            self.navigationController?.popToViewController(classdetail, animated: true)
        }
        else
        {
            for k in (0 ..< array.count)
            {
                let controller: UIViewController? = (array[k] as! UIViewController)
                if (controller is ClassDetailVC)
                {
                    let classdetail = controller as! ClassDetailVC
                    classdetail.arrChildSelected = NSMutableArray(array: arrChildSelected as Array)
                    self.navigationController?.popToViewController(classdetail, animated: true)
                    
                    break
                }
            }
        }

        
    }

    
    @IBAction func btnClosePopupClick(_ sender: Any)
    {
        viewChildMain.isHidden = true
    }
    
    @IBAction func btnPLusClick(_ sender: Any)
    {
        
        childrenId = 0
        viewChildMain.isHidden = false
        self.view.bringSubviewToFront(viewChildMain)
        conTblClassHistoryHeight.constant = 0
        conViewChildPopupHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 320:220

        viewAdd.isHidden = false
        viewUpdate.isHidden = true
        
        self.txtName.text = ""
        self.txtDob.text = ""
        self.txtAge.text = ""
        
        self.txtName.isUserInteractionEnabled = true
        self.txtDob.isUserInteractionEnabled = true


        self.btnGirl.isSelected = false
        self.btnBoy.isSelected = false
        return
        
        
        let array1 = NSMutableArray(array: arrChild)
        
        if array1.contains(dictChild)
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseEnterdata, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if !array1.contains(dictChild)
        {
            for i in 0..<array1.count
            {
                let dict = array1[i] as! [String : Any]
                if dict["Children_Name"] as? String == ""
                {
                    let alert = UIAlertController(title:kWarningEn, message:kPleaseenterChildname, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                    alert.show()
                    return
                }
                else if dict["Gender"] as? String == ""
                {
                    let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectgender, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                    alert.show()
                    return
                }
                else if dict["Date_of_Birth"] as? String == ""
                {
                    let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectdob, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                    alert.show()
                    return
                }
            }
            
            arrChild.append(dictChild)
            tblChild.reloadData()
        }
        else
        {
            arrChild.append(dictChild)
            tblChild.reloadData()
        }
        
        
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
       
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnAddClick(_ sender: Any)
    {
        self.view.endEditing(true)
//        let btn = sender as! UIButton
//        var dict = arrChild[btn.tag]
//
//        let childrenId = dict["Children_Id"] as? NSNumber

        if !self.btnBoy.isSelected && !self.btnGirl.isSelected
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectgender, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            return
        }
        else if self.txtName.text  == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterChildname, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            return
        }
       
        else if self.txtDob.text  == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectdob, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            return
        }
        else
        {
            
            var strGender : String = ""
            
            if self.btnBoy.isSelected
            {
                strGender = "Male"
            }
            else
            {
                strGender = "Female"
            }
            
             let dictPara:NSDictionary = ["App_User_Id" : String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0),"Children_Name":self.txtName.text! , "Gender" : strGender , "Date_of_Birth" : self.txtDob.text!,"Children_Id":childrenId]

            
            let strUrl:String = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wAddChildren)
            WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

                DispatchQueue.main.async
                    {
                        print(result)
                        let strStatus = result["Message"] as? String
                        
                        if strStatus == "Success"
                        {
                            self.wscallforGetChild()
                            self.btnClosePopupClick(self.btnClose)
                        }
                }
            }
        }
    }
    
    @IBAction func btnRemoveClick(_ sender: Any)
    {
        self.view.endEditing(true)

//        let btn = sender as! UIButton
//        let dict = arrChild[btn.tag]
//
//        let childrenId = dict["Children_Id"] as? NSNumber
        
        let dictPara:NSDictionary = [:]
        let strUrl:String = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveChildren,childrenId)
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
            {
                print(result)
                let strStatus = result["Message"] as? String
                
                if strStatus == "Success"
                {
                    self.wscallforGetChild()
                    self.btnClosePopupClick(self.btnClose)

                }
            }
        }

    }
    
    func wscallforUserDetail1()
    {
        let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetUserProfile,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let dictprofile = NSMutableDictionary(dictionary: result as! Dictionary)
                    
                    let array  = dictprofile.value(forKey: "Children") as? Array ?? []
                    if array.count > 0
                    {
                        self.arrChild = array as! [[String : Any]]
                    }
                    self.tblChild.reloadData()
            }
            
            
        }
        
    }
    
    func wscallforGetChild()
    {
        let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetChildren,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let dictprofile = NSMutableDictionary(dictionary: result as! Dictionary)
                    
                    let array  = dictprofile.value(forKey: "Children") as? Array ?? []
                    if array.count > 0
                    {
                        self.arrChild = array as! [[String : Any]]
                    }
                    self.tblChild.reloadData()
            }
            
            
        }
        
    }
    

}


//{
//    let cellReuseIdentifier = "childCell"
//    let cell:childCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! childCell
//
//    let dict = arrChild[indexPath.row] as! [String : Any]
//    cell.txtName.text = dict["Children_Name"] as? String
//    cell.txtDob.text = dict["Date_of_Birth"] as? String
//
//    if dict["Gender"] as? String == "Female"
//    {
//        cell.btnGirl.isSelected = true
//        cell.btnBoy.isSelected = false
//    }
//    else if dict["Gender"] as? String == "Male"
//    {
//        cell.btnBoy.isSelected = true
//        cell.btnGirl.isSelected = false
//    }
//    else
//    {
//        cell.btnGirl.isSelected = false
//        cell.btnBoy.isSelected = false
//    }
//
//    cell.btnRemove.tag = indexPath.row
//    cell.btnAdd.tag = indexPath.row
//    cell.btnUpdate.tag = indexPath.row
//
//
//    if dict["Children_Id"]  as? NSNumber != 0
//    {
//        cell.viewAdd.isHidden = true
//        cell.viewUpdate.isHidden = false
//
//    }
//    else
//    {
//        cell.viewAdd.isHidden = false
//        cell.viewUpdate.isHidden = true
//
//    }
//
//    cell.btnAdd.addTarget(self, action: #selector(btnAddClick(_:)), for: .touchUpInside)
//
//    cell.btnUpdate.addTarget(self, action: #selector(btnAddClick(_:)), for: .touchUpInside)
//
//
//    cell.btnRemove.addTarget(self, action: #selector(btnRemoveClick(_:)), for: .touchUpInside)
//
//
//    cell.txtDob.addTarget(self, action: #selector(textfieldBeginEdit(_:)), for: .editingDidBegin)
//
//    cell.txtName.addTarget(self, action: #selector(textfielddidend(_:)), for: .editingDidEnd)
//
//    cell.txtDob.addTarget(self, action: #selector(textfielddidend(_:)), for: .editingDidEnd)
//
//    cell.btnBoy.addTarget(self, action: #selector(btnGenderClick(_:)), for: .touchUpInside)
//
//    cell.btnGirl.addTarget(self, action: #selector(btnGenderClick(_:)), for: .touchUpInside)
//
//
//    conTblChildHeight.constant = CGFloat(arrChild.count * 200)
//    tblChild.updateConstraintsIfNeeded()
//    tblChild.layoutIfNeeded()
//
//
//
//    return cell
//
//}
