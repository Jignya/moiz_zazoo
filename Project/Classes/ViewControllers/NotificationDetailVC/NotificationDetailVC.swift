//
//  NotificationDetailVC.swift
//  PetAnimal
//
//  Created by HTNaresh on 22/5/18.
//  Copyright © 2018 HTTarang. All rights reserved.
//

import UIKit

class NotificationDetailVC: UIViewController,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var txtNotText: UITextView!
    @IBOutlet weak var txtdesc: UITextView!
    
    @IBOutlet weak var contxtNotTextHeight: NSLayoutConstraint!
    @IBOutlet weak var conTxtDescHeight: NSLayoutConstraint!
    
    var pushNotId = NSNumber()
    
    var dictdata = NSMutableDictionary()
    var strcomeFrom : String = ""
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        lblTitle.text = kNotification

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        Manager.sharedInstance.addTabBar2(self, tab: "2")

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        if dictdata.allKeys.count > 0
        {
            self.txtNotText.text = dictdata["NOTIFICATION_TEXT"] as? String
            self.lblTitle.text =  dictdata["TITLE"] as? String
            self.lbldate.text =  dictdata["NOTIFICATION_DATE"] as? String
            self.txtdesc.text = dictdata["DESCRIPTION"] as? String
            
            let sizeThatFitsTextView = txtNotText.sizeThatFits(CGSize(width: txtNotText.frame.size.width, height: CGFloat(MAXFLOAT)))
            contxtNotTextHeight.constant = sizeThatFitsTextView.height
            
            let sizeThatFitsTextView1 = txtdesc.sizeThatFits(CGSize(width: txtdesc.frame.size.width, height: CGFloat(MAXFLOAT)))
            conTxtDescHeight.constant = sizeThatFitsTextView1.height
            
            let readStatus = dictdata["IS_READ"] as? Int
            
            if readStatus == 0
            {
                WSCallForGetReadPromoterNotification()
            }
        }
        else
        {
            WSCallForNotificationDetail(load: true)
        }
       
    }
    
    //MARK: - Webservice
    func WSCallForGetReadPromoterNotification()
    {
        var userId : String = ""
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        else
        {
            userId = "0"
        }

        let NotId = dictdata["PUSH_CAMPAIGN_ID"] as? NSNumber ?? 0
        let strUrl:String = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wReadNotification,NotId,userId)
        
        let dictPara = NSDictionary()
        
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "")
            { (result) in
            DispatchQueue.main.async
                {
                    // print(result)
                    let strTemp = result.object(forKey: "Message") as? String
                    if strTemp == "Success"
                    {
                        if ((self.navigationController?.viewControllers) != nil)
                        {
                            let array = self.navigationController?.viewControllers
                                as? Array ?? []
                            for k in (0 ..< array.count)
                            {
                                let controller: UIViewController? = (array[k] as! UIViewController)
                                if (controller is NotificationVC)
                                {
                                   
                                    let not = controller as! NotificationVC
                                    not.WSCallForNotificationList(load: false)
                                    
                                    
                                    break
                                }
                            }
                        }
                    }
            }
        }
    }
    
    func WSCallForNotificationDetail(load:Bool)
    {
        var strUrl:String = ""
       
        strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNotificationDetail,pushNotId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: load, LoadingText: "")
        { (result) in
            DispatchQueue.main.async
                {
                    // print(result)
                    let strTemp = result.object(forKey: "Message") as? String
                    if strTemp == "Success"
                    {
                        self.txtNotText.text = result["NOTIFICATION_TEXT"] as? String
                        self.lblTitle.text =  result["TITLE"] as? String
                        self.lbldate.text =  result["NOTIFICATION_DATE"] as? String
                        self.txtdesc.text = result["DESCRIPTION"] as? String
                        
                        let sizeThatFitsTextView = self.txtNotText.sizeThatFits(CGSize(width: self.txtNotText.frame.size.width, height: CGFloat(MAXFLOAT)))
                        self.contxtNotTextHeight.constant = sizeThatFitsTextView.height
                        
                        let sizeThatFitsTextView1 = self.txtdesc.sizeThatFits(CGSize(width: self.txtdesc.frame.size.width, height: CGFloat(MAXFLOAT)))
                        self.conTxtDescHeight.constant = sizeThatFitsTextView1.height

                    }
                    else
                    {
                        
                        let alertController = UIAlertController(title:kWarningEn, message:strTemp, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default)
                        {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        // Add the actions
                        alertController.addAction(okAction)
                        // Present the controller
                        alertController.show()
                    }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button methods
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        if strcomeFrom == "push"
        {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

}
