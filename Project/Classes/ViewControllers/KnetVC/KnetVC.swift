//  KnetVC.swift
//  Zazoo
//
//  Created by HTNaresh on 15/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.

import UIKit

class KnetVC: UIViewController,UIWebViewDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var web: UIWebView!
    
    var strUrl : String!
    var orderId : NSNumber!
    var responseUrl:NSURL!
    
    var strpayMode : String!
    var strAmount : String!
    var strcomeFrom : String!


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblTitle.text = kPayment
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        strUrl = String(format: "%@%@",GlobalConstants.kKNETUrl,orderId)
        let url = URL (string:strUrl!)
        let requestObj = URLRequest(url: url!)
        web.loadRequest(requestObj)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        Helper.sharedInstance.DismissProgressBar()

    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        Helper.sharedInstance.ShowProgressBarWithText("")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        Helper.sharedInstance.DismissProgressBar()
        responseUrl = webView.request?.url?.absoluteURL as! NSURL
        print("webView responce    Url:::\(responseUrl)")
        let string = "\(responseUrl)"
        let delimiter = "?"
        var token = string.components(separatedBy: delimiter)
        print (token[0])
        if token[0].contains(GlobalConstants.kPaymentSuccessUrl)
        {
            let PaymentReceipt = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouScreenVC") as! ThankYouScreenVC
            PaymentReceipt.orderId = orderId
            PaymentReceipt.strComeFrom = strcomeFrom

            
            PaymentReceipt.strAmount = strAmount
            PaymentReceipt.strPayMode = strpayMode

            self.navigationController?.pushViewController(PaymentReceipt, animated: true)
        }
        else if token[0].contains(GlobalConstants.kPaymentSuccessUrl1)
        {
            let PaymentReceipt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentReceiptVC") as! PaymentReceiptVC
            PaymentReceipt.orderId = orderId
            self.navigationController?.pushViewController(PaymentReceipt, animated: true)
        }
        else if token[0].contains(GlobalConstants.kPaymentFailureUrl)
        {
            Manager.sharedInstance.wscallforRemoveCart()

            let PaymentReceipt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentReceiptVC") as! PaymentReceiptVC
            PaymentReceipt.orderId = orderId
            self.navigationController?.pushViewController(PaymentReceipt, animated: true)

        }
        
    }
    
    //MARK:- handle Back
    
    @objc func handleSwipeback(_ gestureRecognizer: UIGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            self.btnBackClick(btnBack)
        }
    }
    

    @IBAction func btnBackClick(_ sender: Any)
    {
        let alertController = UIAlertController(title:kWarningEn, message:kcancelorder, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title:kYes, style: .default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.wscallforCancelOrder()
        }
        
        let noAction = UIAlertAction(title:kNo, style: .default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }

        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(noAction)

        // Present the controller
        alertController.show()
    }
    
    
    func wscallforCancelOrder()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wCancelOrder,orderId)
        
        let dictPara:NSDictionary = [:]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        if ((self.navigationController?.viewControllers) != nil)
                        {
                            let array = self.navigationController?.viewControllers
                                as! Array<Any>
                            for k in (0 ..< array.count)
                            {
                                let controller: UIViewController? = (array[k] as! UIViewController)
                                if (controller is CartVC)
                                {
                             self.navigationController?.popToViewController(controller!, animated: true)
                                    
                                    
                                    break
                                }
                                if (controller is PaymentGymVC)
                                {
                                    self.navigationController?.popToViewController(controller!, animated: true)
                                    
                                    
                                    break
                                }
                            }
                        }
//                    self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    
            }
        }
        
    }

}
