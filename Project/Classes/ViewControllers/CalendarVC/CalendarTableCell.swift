//
//  CalendarTableCell.swift
//  Project
//
//  Created by HTNaresh on 3/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class CalendarTableCell: UITableViewCell
{
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblClassname: UILabel!
    @IBOutlet weak var lblAgeGrp: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblwaiting: UILabel!
    @IBOutlet weak var lblActivity: UILabel!
    @IBOutlet weak var lblwaitList: UILabel!
    @IBOutlet weak var lbldays: UILabel!
    @IBOutlet weak var btnwish: UIButton!
    @IBOutlet weak var lblDiscountprice: UILabel!
    @IBOutlet weak var viewSold: UIView!
    @IBOutlet weak var lblSoldout: UILabel!

    @IBOutlet weak var lblAgeGrpArData: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
