//
//  ClassDetailVC.swift
//  Project
//
//  Created by HTNaresh on 12/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import MapKit
import AVKit


class ClassDetail1VC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CalenderDelegate,BundleCellDelegate,MKMapViewDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var lblProvider: UILabel!
    @IBOutlet weak var lblAgrGroup: UILabel!
    @IBOutlet weak var lblActivityType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnBook: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var txtClassDesc: UITextView!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var mapZoom: MKMapView!
    @IBOutlet weak var viewmapZoom: UIView!

    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lbladdres: UILabel!
    @IBOutlet weak var lblAboutProvider: UILabel!
    @IBOutlet weak var lblProvidername: UILabel!
    @IBOutlet weak var txtProviderDesc: UITextView!
    @IBOutlet weak var lblOtherClasses: UILabel!
    
    @IBOutlet weak var viewThankYou: UIView!

    @IBOutlet weak var colVendor: UICollectionView!
    @IBOutlet weak var conclassDescHeight: NSLayoutConstraint!
    @IBOutlet weak var convendorDescHeight: NSLayoutConstraint!
    
    @IBOutlet weak var conColSuggestedHeight: NSLayoutConstraint!
    @IBOutlet weak var conColTypeHeight: NSLayoutConstraint!

    @IBOutlet weak var conColItemInfoHeight: NSLayoutConstraint!

    @IBOutlet weak var conviewCalendarHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var colType: UICollectionView!
    
    @IBOutlet weak var colAddOn: UICollectionView!
    @IBOutlet weak var conAddOnHeight: NSLayoutConstraint!

    @IBOutlet weak var colItems: UICollectionView!
    @IBOutlet weak var colItemInfo: UICollectionView!

    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblday: UILabel!
    @IBOutlet weak var lblStartdates: UILabel!

    @IBOutlet weak var lblattending: UILabel!
    @IBOutlet weak var lbldates: UILabel!

    @IBOutlet weak var btnattending: UIButton!
    @IBOutlet weak var btndates: UIButton!
    @IBOutlet weak var btndateDone: UIButton!
    @IBOutlet weak var btndateClear: UIButton!

    @IBOutlet weak var scrl: UIScrollView!
    @IBOutlet weak var conViewdaysHeight: NSLayoutConstraint!
    @IBOutlet weak var conBtnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewChildHeight: NSLayoutConstraint!
    @IBOutlet weak var btnFavouriteItem: UIButton!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var pagecntrl : UIPageControl!

    
    var isPopupDisplay : Bool = false

    var isBundleItem : Bool = false

    var strcomeFromPush : String = ""

    var classId: NSNumber!
    var VendorId: NSNumber!

    var strDate: String!

    var arrQty  =  [[String:Any]]()

    var arrType = [[String:Any]]()
    var arrAddOn = [[String:Any]]()
    var arrSuggested = [[String:Any]]()
    var arrAvailDates = [[String:Any]]()
    var arrActivities = [[String:Any]]()
    var arrBundleItems = [[String:Any]]()
    var strGender : String = ""
    var arrImages : NSMutableArray = []

    
    var arrSelected = [String]()
    var arrBooked = [String]()
    var arrAvailable = [String]()
    var arrChildSelected : NSMutableArray = []
    var arrCartAvailibility : NSMutableArray = []

    var ChildrenQty : Int!

    var priceId : NSNumber!

    var itemTypeId : NSNumber!
    var childrenId : String!
    var calendarId = [NSNumber]()
    var strprice : String!
    var promotionId : NSNumber!

    var strDiscountprice : String!
    var strNetprice : String!
    var strTotalPrice : String!

    var strDiscountpriceTemp : String!
    var strNetpriceTemp : String!
    var childIndex : Int!
    var strAgeGrp : String = ""

    var strComeFrom : String = ""
    var timer = Timer()


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let layout = collectionImages.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        collectionImages.isPagingEnabled = true

        
        childrenId = ""
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        arrQty = [["name":"1"],["name":"2"],["name":"3"],["name":"4"],["name":"5"],["name":"6"],["name":"7"],["name":"8"],["name":"9"],["name":"10"]]

        conviewCalendarHeight.constant = 0
        conBtnViewHeight.constant = 0
        self.setLabel()

        if isBundleItem
        {
           self.wscallforClassBundleDetail()
        }
        else
        {
           self.wscallforClasseDetail()
        }
        
        btnFavouriteItem.isHidden = true

    }
    
    func setLabel()
    {
        lblTitle.text = kDetails
        lblLocation.text = klocation
        lblAboutProvider.text = kaboutProvider
        lblInfo.text = kinfo
        lbldates.text = kSeldates
        lblattending.text = kattending
        btnattending.setTitle(kselectchild, for: .normal)
        btndates.setTitle(kSeldates, for: .normal)
        btndateDone.setTitle(kDone, for: .normal)
        btndateClear.setTitle(kcleardates, for: .normal)
        btnBook.setTitle(kbook, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if strComeFrom == "wishlist"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "3")
        }
        else if strComeFrom == "calendar"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "1")
        }
        else
        {
            Manager.sharedInstance.addTabBar2(self, tab: "0")
        }
        
        if arrChildSelected.count > 0
        {
            for i in 0..<arrChildSelected.count
            {
                let dictChild = arrChildSelected[i] as! [String:Any]
                let isValidChild = self.isAgevalid(dict: dictChild)
                let isValidGender = self.isValidGender(dict: dictChild)
                if isValidGender == false
                {
                    let alertController = UIAlertController(title:kWarningEn, message:kGenderalert + " " + self.strGender, preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title:kOkEn, style: .default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        self.btnattending.setTitle(kselectchild, for: .normal)
                        self.arrChildSelected.removeAllObjects()
                        
                    }
                    // Add the actions
                    alertController.addAction(okAction)
                    // Present the controller
                    alertController.show()
                    return
                }
                else if isValidChild == false
                {
                    let msg = String(format:"%@ %@", dictChild["Children_Name"] as? String ?? "Your Child",kAgealert)
                    let alertController = UIAlertController(title:kWarningEn, message:msg, preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title:kOkEn, style: .default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        self.btnattending.setTitle(kselectchild, for: .normal)
                        self.arrChildSelected.removeAllObjects()

                    }
                    // Add the actions
                    alertController.addAction(okAction)
                    // Present the controller
                    alertController.show()
                    return

                }
                
            }
            
            var dictChild = arrBundleItems[childIndex]
            dictChild["arrChild"] = arrChildSelected as! [[String:Any]]
            
            childrenId = Manager.sharedInstance.getCommaSeparatedValuefromArray(arrChildSelected, forkey: "Children_Id")
            
            ChildrenQty = arrChildSelected.count
            dictChild["ChildrenIds"] = childrenId
            dictChild["Qty"] = ChildrenQty
            arrBundleItems[childIndex] = dictChild
            colItemInfo.reloadData()
        }
        
       
        

    }
    
    func isValidGender(dict:[String:Any]) -> Bool
    {
        let strChildGender = dict["Gender"] as? String ?? ""
        if strGender == ""
        {
            return true
        }
        else if strGender.contains(strChildGender)
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    func isAgevalid(dict:[String:Any]) -> Bool
    {
        var isvalid : Bool = false
        
        let dob = dict["Date_of_Birth"] as? String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
//        let birthDate = dateFormatter.date(from: dob)
        
        if let birthDate = dateFormatter.date(from: dob ?? "")
        {
            let now = Date()
            let calendar = Calendar.current
            
            let ageComponents = calendar.dateComponents([.year], from: birthDate, to: now)
            let age = ageComponents.year!
            
            if strAgeGrp != ""
            {
                let arr = strAgeGrp.components(separatedBy: "-")
                if arr.count == 2
                {
                    let first = Int(arr[0])
                    let last = Int(arr[1])
                    
                    if (age >= first! && age <= last!)
                    {
                        isvalid = true
                    }
                }
            }
            
        }

        

        
       
        return isvalid

    }
    
    // MARK:-  detail data set up
    
    func displayDetailData1(dict: NSMutableDictionary)
    {

        self.lblClassName.text = dict["Class_Name"] as? String
        
        arrActivities = dict["Activities"] as? [[String:Any]] ?? [].reversed()
        
//        arrActivities = arrActivities.reversed()
        
//        let str11 = Manager.sharedInstance.getCommaSeparatedValuefromArray(NSMutableArray(array: arrActivities), forkey: "Type_of_Activity")
        
        self.lblActivityType.text = dict["Type_of_Activity"] as? String ?? ""
        self.lblAgrGroup.text = ""

//        self.lblAgrGroup.text = String(format: "%@ %@",kAgeGrp, (arrActivities[0]["Age_Group"] as? String ?? "")!)
        strAgeGrp = arrActivities[0]["Age_Group"] as? String ?? ""
        strGender = dict["Genders"] as? String ?? ""
        strTotalPrice = dict["Price"] as? String ?? ""

        self.VendorId = dict["Vendor_Id"] as? NSNumber
        
        
        var Stringimg = String()
        Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: urlString!)
        imgLogo.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
        
        if dict["Is_Favorite"] as? NSNumber == 1
        {
            btnFavouriteItem.isSelected = true
        }
        else
        {
            btnFavouriteItem.isSelected = false
        }
        
        lblProvidername.text = dict["Vendor_Name"] as? String
        
        let text = (lblProvidername.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 : NSRange = (text as NSString).range(of: (dict["Vendor_Name"] as? String ?? "")!)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalConstants.themeClr , range: range1)
        
        lblProvidername.attributedText = underlineAttriString
        
        lblProvidername.isUserInteractionEnabled = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLabel1(gesture:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        lblProvidername.addGestureRecognizer(tapGestureRecognizer)
        
        lbladdres.text = dict["Vendor_Address"] as? String ?? ""

        txtClassDesc.text = (dict["Class_Desc"] as? String ?? "")!.html2String
        
        let sizeThatFitsTextView = txtClassDesc.sizeThatFits(CGSize(width: txtClassDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
        conclassDescHeight.constant = sizeThatFitsTextView.height
        txtClassDesc.updateConstraintsIfNeeded()
        txtClassDesc.updateFocusIfNeeded()
        
        txtProviderDesc.text = (dict["Vendor_Desc"] as? String ?? "")
        
        let sizeThatFitsTextView1 = txtProviderDesc.sizeThatFits(CGSize(width: txtProviderDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
        convendorDescHeight.constant = sizeThatFitsTextView1.height
        txtProviderDesc.updateConstraintsIfNeeded()
        txtProviderDesc.updateFocusIfNeeded()
        
        
        arrType = arrActivities[0]["PriceDetails"] as? Array ?? []

        if arrType.count == 0
        {
            self.conViewdaysHeight.constant = 0
            conviewCalendarHeight.constant = 0
            conBtnViewHeight.constant = 0
            conColTypeHeight.constant = 0
        }
        else
        {
            conColTypeHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 80 : 50
        }

        colType.reloadData()
        
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }

        arrBundleItems.removeAll()
        
        for j in 0..<arrActivities.count
        {
            let dict = arrActivities[j]
            
            let arrType = dict["PriceDetails"] as? Array ?? []
            
            if arrType.count > 0
            {
                let dictType = arrType[0] as? [String:Any] ?? [:]
                
                priceId = dictType["Price_Detail_Id"] as? NSNumber
                strprice = String(format: "%.3f", Float(dictType["Price"] as? String ?? "0")!)
                itemTypeId = dictType["Item_Type_Id"] as? NSNumber
                
                promotionId = classId  //dictType["Promotion_Id"] as? NSNumber
                strDiscountprice = String(format: "%.3f", (Float(dictType["Price"] as? String ?? "0"))! - (Float(dict["Price"] as? String ?? "0")!))
                strNetprice = dict["Price"] as? String ?? "0"
                    //String(format: "%.3f", (dictType["Offer_Price"] as? NSNumber ?? 0).floatValue)

            }
            
            

            
            let arrCalendar = setdateArray(arrDates: dict["AvailableClasses"] as? Array ?? [])
            
            calendarId.removeAll()
            for dict in arrCalendar
            {
                let dict1 = dict as [String:Any]
                let id = dict1["Calender_Id"] as? NSNumber
                calendarId.append(id!)
            }

            

            let dictPara = ["Price_Detail_Id":priceId,"ChildrenIds":"","Item_Id":dict["Class_Id"] as? NSNumber ?? 0,"Amount":strprice,"CalenderIds":calendarId,"Custom_Note":"","Rate":"","Qty":1,"App_User_Id":userId,"Cart_Id":"0","Item_Type_Id":itemTypeId,"Size_Id":"0","Deal_Id":"0","AddOns":[] ,"Promotion_Id":promotionId,"Discount_Amount":strDiscountprice,"Net_Amount":strNetprice,"arrChild":[]] as [String : Any]
            
            arrBundleItems.append(dictPara)

        }
      
        arrSuggested = dict["SuggestedClasses"] as? Array ?? []
        if arrSuggested.count  > 0
        {
            conColSuggestedHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 160 : 100
            lblOtherClasses.isHidden = false
            colVendor.reloadData()
            lblOtherClasses.text = String(format: "%@(%@)", kotherClass,(dict["Vendor_Name"] as? String ?? "")!)
            colVendor.isHidden = false
        }
        else
        {
            colVendor.isHidden = true
            conColSuggestedHeight.constant = 0
            lblOtherClasses.isHidden = true
            
        }
        var arrAnnotation : [MKAnnotation] = []
        for j in 0..<arrActivities.count
        {
            let dict1 = arrActivities[j]
            let strLat = dict1["Latitude"] as? String ?? "0"
            let strLong = dict1["Longitude"] as? String ?? "0"

            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: Double(strLat)!, longitude: Double(strLong)!)
            annotation.title = dict1["Class_Name"] as? String ?? ""
            map.addAnnotation(annotation)
            mapZoom.addAnnotation(annotation)
            arrAnnotation.append(annotation)
        }

        map.isScrollEnabled = false
        map.showAnnotations(arrAnnotation, animated: true)
        mapZoom.showAnnotations(arrAnnotation, animated: true)
        
//        var zoomRect = MKMapRectNull
//        for annotation in map.annotations {
//            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
//            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
//            if (MKMapRectIsNull(zoomRect)) {
//                zoomRect = pointRect
//            } else {
//                zoomRect = MKMapRectUnion(zoomRect, pointRect)
//            }
//        }
//        map.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(40, 40, 40, 40), animated: true)
//        mapZoom.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(40, 40, 40, 40), animated: true)


//        let span = MKCoordinateSpan(latitudeDelta:0.05 , longitudeDelta: 0.05)
//        let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
//        map.setRegion(region, animated: true)
//        mapZoom.setRegion(region, animated: true)
        
        if isBundleItem
        {
            self.conViewdaysHeight.constant = 0
            conviewCalendarHeight.constant = 0
            conBtnViewHeight.constant = 0
            conAddOnHeight.constant = 0
            conAddOnHeight.constant = 0
            
        }
        
        colItemInfo.reloadData()
        
        arrImages = NSMutableArray(array: (dict["Media"] as? [[String:Any]])!)
        if arrImages.count == 0
        {
            collectionImages.isHidden = true
        }
        else
        {
            collectionImages.backgroundColor = UIColor.clear
            imgLogo.isHidden = true
            collectionImages.reloadData()
        }
        
    }
    
    func isDataAvailablefordiscount(comparisonDict:[String:Any]) -> Bool
    {
        if comparisonDict["Promotion_Type_Id"] as? NSNumber == 3
        {
            let arrChildren = comparisonDict["Children_Linked_Ids"] as? NSArray

            let predicate = NSPredicate(format: "Item_Id = %@ AND Item_Type_Id = %@", (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber ?? 0)!, (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber ?? 0)!)
            var arr1 = [[String:Any]]()
            
            if let filtered = (Manager.sharedInstance.arrcart as? NSArray)?.filtered(using: predicate) {
                arr1 = (filtered as? [[String:Any]])!
            }
            
            if arr1.count > 0
            {
                let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", classId , (comparisonDict["Item_Type_Id"] as? NSNumber ?? 0)!)
                var arr2 = [[String:Any]]()
                
                if let filtered = arrChildren?.filtered(using: predicate) {
                    arr2 = (filtered as? [[String:Any]])!
                }
                
                if arr1.count > 0 && arr2.count > 0
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false

//                var arr1 = [[String:Any]]()
//
//                for i in 0..<Manager.sharedInstance.arrcart.count
//                {
//                    let dict1 = Manager.sharedInstance.arrcart[i] as! [String:Any]
//                    let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", (dict1["Item_Id"] as? NSNumber ?? 0)!, (dict1["Item_Type_Id"] as? NSNumber ?? 0)!)
//
//                    if let filtered = arrChildren?.filtered(using: predicate) {
//                        arr1 = (filtered as? [[String:Any]])!
//                    }
//
//                    if arr1.count > 0
//                    {
//                        break
//                    }
//
//                }
//
//                if arr1.count > 0 && (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber == classId) && (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber == comparisonDict["Item_Type_Id"] as? NSNumber)
//                {
//                   return true
//                }
//                else
//                {
//                    return false
//                }
                
            }
        }
        else if comparisonDict["Promotion_Type_Id"] as? NSNumber == 1
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    
    @objc func tapLabel1(gesture: UITapGestureRecognizer)
    {
        let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
        classdetail.VendorId = self.VendorId
        self.navigationController?.pushViewController(classdetail, animated: true)
    }

    
    func setdateArray(arrDates:Array<Any>) -> [[String:Any]]
    {
        arrAvailDates = arrDates as! [[String:Any]]
        let aa1 = arrDates as? [[String:Any]] ?? []
        for i in 0..<aa1.count
        {
            let dict = aa1[i]
            let aa2 = NSMutableArray(array: dict["Children"] as! Array)
            
            arrAvailable.append(dict["Calender_Date"] as! String)
            for j in 0..<aa2.count
            {
                let dict1 = aa2[j] as! [String:Any]
                if dict1["Is_Booked"] as? NSNumber == 1
                {
                    arrBooked.append(dict["Calender_Date"] as! String)
                    arrAvailable.removeLast()
                    break
                }
            }
            
        }
        
        return arrAvailDates
    }
    
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == colType
        {
            return arrType.count
        }
        else if collectionView == colAddOn
        {
            return arrAddOn.count
        }
        else if collectionView == colItems
        {
            return arrActivities.count
        }
        else if collectionView == colItemInfo
        {
            return arrActivities.count
        }
        else if collectionView == collectionImages
        {
            return arrImages.count
        }
        return  arrSuggested.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == colType
        {
            let cellReuseIdentifier = "cell2"
            let cell:SideCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! SideCollectionCell
            
            let dict = arrType[indexPath.row]
            
            if dict["Item_Type_Id"] as? NSNumber == 2
            {
                cell.lblTitle.text = kDropIn
            }
            else if dict["Item_Type_Id"] as? NSNumber == 3
            {
                cell.lblTitle.text = kSemester
            }
            else if dict["Item_Type_Id"] as? NSNumber == 4
            {
                cell.lblTitle.text = kCamp
            }
            else
            {
                cell.lblTitle.text = kVenue
            }
            
            cell.lblDiscount.text = ""
            cell.lblDiscountPrice.text = ""
            cell.lblSubTitle.text = strTotalPrice
            cell.btnCheck.isSelected = true


            
//            cell.lblSubTitle.text = String(format: "%.3f", (dict["Offer_Price"] as? NSNumber ?? 0).floatValue)
//
//            if dict["Discount_Percent"] as? NSNumber == 0
//            {
//                 cell.lblDiscount.text = ""
//                 cell.lblDiscountPrice.text = ""
//                 cell.lblSubTitle.text = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
//
//            }
//            else  if dict["Discount_Percent"] as? NSNumber != 0 && !isDataAvailablefordiscount(comparisonDict: dict)
//            {
//                cell.lblDiscount.text = ""
//                cell.lblDiscountPrice.text = ""
//                cell.lblSubTitle.text = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
//
//            }
//            else
//            {
//                cell.lblDiscount.text = String(format: "%.f%@ OFF ", (dict["Discount_Percent"] as? Float ?? 0.0)! , "%")
//                cell.lblDiscountPrice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: (dict["Price"] as? String ?? "0")!)
//
//            }
//
//
//            if dict["isSelect"] as? String == "1"
//            {
//                cell.btnCheck.isSelected = true
//                if dict["Item_Type_Id"] as? NSNumber == 2
//                {
//                    self.conViewdaysHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 90 : 70
//                }
//                else if dict["Item_Type_Id"] as? NSNumber == 3
//                {
//                    self.btndateClearClick(btndateClear)
//                    self.conViewdaysHeight.constant = 0
//                    conviewCalendarHeight.constant = 0
//                    conBtnViewHeight.constant = 0
//                }
//                else if dict["Item_Type_Id"] as? NSNumber == 4
//                {
//                    self.btndateClearClick(btndateClear)
//                    self.conViewdaysHeight.constant = 0
//                    conviewCalendarHeight.constant = 0
//                    conBtnViewHeight.constant = 0
//                }
//                else
//                {
//                    self.btndateClearClick(btndateClear)
//                    self.conViewdaysHeight.constant = 0
//                    conviewCalendarHeight.constant = 0
//                    conBtnViewHeight.constant = 0
//                }
//            }
//            else
//            {
//                cell.btnCheck.isSelected = false
//            }
//
            return cell
        }
        else if collectionView == colAddOn
        {
            
            let cellReuseIdentifier = "cell2"
            let cell:SideCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! SideCollectionCell
            
            let dict = arrAddOn[indexPath.row]
            
            cell.lblSubTitle.isHidden = false
            cell.lblTitle.text = dict["Add_On_Name"] as? String
            cell.lblSubTitle.text = String(format: "%.3f %@", (dict["Price"] as? NSNumber ?? 0)!.floatValue,kCurrency)
            
            if dict["SelectedQty"] as? String != "0"
            {
                cell.btnQty.setTitle(dict["SelectedQty"] as? String, for: .normal)
                cell.lblSubTitle.text = String(format: "%.3f %@", (dict["Amount"] as? Float ?? 0.0)!,kCurrency)
                
            }
            
            if dict["isSelect"] as? String == "1"
            {
                cell.btnCheck.isSelected = true
            }
            else
            {
                cell.btnCheck.isSelected = false
            }
            
            cell.btnQty.tag = indexPath.item
            cell.btnQty.addTarget(self, action: #selector(btnQtyClick(_:)), for: .touchUpInside)
            
            conAddOnHeight.constant = colAddOn.contentSize.height
            colAddOn.updateConstraintsIfNeeded()
            colAddOn.layoutIfNeeded()
            
            
            return cell
        }
        else if collectionView == colItems
        {
            let cellReuseIdentifier = "ClassBundleCell1"
            let cell:ClassBundleCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! ClassBundleCell
            let dict = arrActivities[indexPath.row]
            
            
            cell.lbltitle.text = String(format: "%@",(dict["Class_Name"] as? String) ?? "")

            return cell
        }
        else if collectionView == colItemInfo
        {
            let cellReuseIdentifier = "ClassBundleCell"
            let cell:ClassBundleCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! ClassBundleCell
            
            cell.delegate = self
            cell.index = indexPath.item
            
            let dict = arrActivities[indexPath.row]
            let arrPriceDetail = dict["PriceDetails"] as! [[String:Any]]
            
            if arrPriceDetail.count > 0
            {
                let dictPriceDetail = arrPriceDetail[0]
                
//                if dictPriceDetail["Discount_Percent"] as? NSNumber == 0
//                {
//                    cell.lblDiscount.text = ""
//                    cell.lblDiscountprice.text = String(format: "%.3f", Float(dictPriceDetail["Price"] as? String ?? "0")!)
//                    cell.lblOriginalPrice.text = ""
//
//                }
//                else
//                {
                    cell.lblDiscount.text = ""
                    cell.lblOriginalPrice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: (dictPriceDetail["Price"] as? String ?? "0")!)
                    cell.lblDiscountprice.text = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
                    cell.lblDiscountprice.textColor = GlobalConstants.highLightClr

                
//                }

            }
            
            
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            cell.imgitem.setImageWith(url, usingActivityIndicatorStyle: .gray)

            cell.lbltitle.text = String(format: "%@",(dict["Class_Name"] as? String) ?? "")
            cell.lblStartdates.text = String(format: "%@ to %@", (dict["Start_Date"] as? String) ?? "",(dict["End_Date"] as? String) ?? "")
            
            if dict["Start_Time"] is String
            {
                cell.lblday.text = String(format: "%@ - %@", dict["Start_Time"] as? String ?? "", dict["End_Time"] as? String ?? "")
            }
            else
            {
                cell.lblday.text = ""
            }
            
            cell.lblTime.text =  dict["Activity_Days"] as? String ?? ""
            
            var arrAddOn1 = dict["AddOns"] as? [[String:Any]] ?? []
    
            if arrAddOn1.count == 0
            {
                cell.conAddOnHeight.constant = 0
            }
            else
            {
                
                cell.conAddOnHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 80 : 50
    
                for i in 0..<arrAddOn1.count
                {
                    let dict = NSMutableDictionary(dictionary: (arrAddOn1[i] as? Dictionary)!)
                    if i == 0
                    {
                        dict.setValue("0", forKey: "isSelect")
                    }
                    else
                    {
                        dict.setValue("0", forKey: "isSelect")
    
                    }
    
                    dict.setValue("0", forKey: "SelectedQty")
    
                    arrAddOn1[i] = dict as! [String:Any]
                }
    
                cell.arrAddOn = arrAddOn1
                cell.colAddOn.reloadData()
            }
            
            cell.lblLocationData.text = dict["Vendor_Address"] as? String ?? ""
            
            let arrChild = NSMutableArray(array: arrBundleItems[indexPath.item]["arrChild"] as! [[String:Any]])
            
            if arrChild.count  > 0
            {
                let str11 = Manager.sharedInstance.getCommaSeparatedValuefromArray(arrChild, forkey: "Children_Name")
                cell.btnattending.setTitle(str11, for: .normal)
            }
            else
            {
                cell.btnattending.setTitle(kselectchild, for: .normal)
            }
    


            cell.btnattending.tag = indexPath.item
            cell.btnattending.addTarget(self, action: #selector(btnBundleItemchildrenClick(_:)), for: .touchUpInside)
            
            conColItemInfoHeight.constant = colItemInfo.contentSize.height
            colItemInfo.updateConstraintsIfNeeded()
            colItemInfo.layoutIfNeeded()
            
            return cell
        }
        else if collectionView == collectionImages
        {
            let cellReuseIdentifier = "cell2"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            let dict = arrImages[indexPath.row] as! [String:Any]
            
            let Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Thumbnail_URL"] as? String ?? "")
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            cell.imgLogo.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
            
            cell.imgPlay.isHidden = true
            
            if dict["Media_Type"] as? NSNumber == 2
            {
                cell.imgPlay.isHidden = false
            }
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction1), userInfo: nil, repeats:true)
            
            cell.imgLogo.backgroundColor = UIColor.clear

            
            return cell
            
        }
        else
        {
        
            let cellReuseIdentifier = "HomeCollectionCell"
            let cell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! HomeCollectionCell
            
            let dict = arrSuggested[indexPath.row]
        
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")

            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

            let url = URL(string: urlString!)
            cell.imgAdv?.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)

            cell.lblTitle.text = dict["Class_Name"] as? String

           
        
            return cell
        }
        
        
    }
    
   
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == colType
        {
            let cellSize = CGSize(width:(colType.frame.size.width / 2) - 2 , height:colType.frame.size.height)
            return cellSize
        }
        else  if collectionView == colAddOn
        {
            let cellSize = CGSize(width:(colAddOn.frame.size.width) , height:(UIDevice.current.userInterfaceIdiom == .pad) ? 80 : 50)
            return cellSize
        }
        else if collectionView == colItems
        {
            let cellSize = CGSize(width:(colItems.frame.size.width / 2) - 1 , height:colItems.frame.size.height)
            return cellSize

        }
        else if collectionView == collectionImages
        {
            let cellSize = CGSize(width:self.collectionImages.frame.size.width , height:self.collectionImages.frame.size.height)
            return cellSize
        }
        else if collectionView == colItemInfo
        {
            let dict = arrActivities[indexPath.row]
            let arrAddOn1 = dict["AddOns"] as? Array ?? []
            
            var Height:CGFloat = 0
            
            if arrAddOn1.count == 0
            {
                Height = 0
            }
            else
            {
                Height = CGFloat(((UIDevice.current.userInterfaceIdiom == .pad) ? 80 : 50) * arrAddOn1.count)
            }

            
            let cellSize = CGSize(width:colItemInfo.frame.size.width, height:Height + ((UIDevice.current.userInterfaceIdiom == .pad) ? 300 : 250))
            return cellSize

        }
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            let cellSize = CGSize(width:120 , height:200)
            return cellSize

        }
        else
        {
            let cellSize = CGSize(width:80 , height:100)
            return cellSize

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == colType
        {
            for i in 0..<arrType.count
            {
                var dict = arrType[i]
                
                if  indexPath.row == i
                {
                    dict["isSelect"] = "1"
                    
                    if dict["Discount_Percent"] as? NSNumber == 0
                    {
                        priceId = dict["Price_Detail_Id"] as? NSNumber
                        strprice = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
                        itemTypeId = dict["Item_Type_Id"] as? NSNumber
                        promotionId = 0
                        strDiscountprice = ""
                        strNetprice = String(format: "%.3f", Float(dict["Price"] as? String ?? "")!)

                    }
                    else if dict["Discount_Percent"] as? NSNumber != 0 && !isDataAvailablefordiscount(comparisonDict: dict)
                    {
                        priceId = dict["Price_Detail_Id"] as? NSNumber
                        strprice = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
                        itemTypeId = dict["Item_Type_Id"] as? NSNumber
                        promotionId = 0
                        strDiscountprice = ""
                        strNetprice = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
                        
                        strDiscountpriceTemp = String(format: "%.3f", (Float(dict["Price"] as? String ?? "0")! - (dict["Offer_Price"] as? NSNumber ?? 0).floatValue))
                        strNetpriceTemp = String(format: "%.3f", (dict["Offer_Price"] as? NSNumber ?? 0).floatValue)

                        
                    }
                    else
                    {
                        priceId = dict["Price_Detail_Id"] as? NSNumber
                        strprice = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
                        itemTypeId = dict["Item_Type_Id"] as? NSNumber
                        
                        promotionId = dict["Promotion_Id"] as? NSNumber
                        strDiscountprice = String(format: "%.3f", (Float(dict["Price"] as? String ?? "0")! - (dict["Offer_Price"] as? NSNumber ?? 0).floatValue))
                        strNetprice = String(format: "%.3f", (dict["Offer_Price"] as? NSNumber ?? 0).floatValue)

                    }
                    
                }
                else
                {
                   dict["isSelect"] = "0"
                }
                
                arrType[i] = dict

            }
            
            colType.reloadData()

        }
        else if collectionView == colAddOn
        {
            var dict = arrAddOn[indexPath.row]
            
            if dict["SelectedQty"] as? String == "0"
            {
                dict["SelectedQty"] = "1"
                let amt = Int((dict["SelectedQty"] as? String ?? "1")!)
                let price = (dict["Price"] as? NSNumber)?.floatValue
                let amunt = Float(amt!) * price!
                dict["Amount"] = amunt
                
            }
            
            if  dict["isSelect"] as? String == "1"
            {
                dict["isSelect"] = "0"
            }
            else
            {
                dict["isSelect"] = "1"
            }
            
            arrAddOn[indexPath.row] = dict
            colAddOn.reloadData()
        }
        else if collectionView == colVendor
        {
            let dict = NSMutableDictionary(dictionary: arrSuggested[indexPath.row] as Dictionary)
            
            let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
            classdetail.strDate = strDate
            classdetail.classId = dict["Class_Id"] as? NSNumber
            self.navigationController?.pushViewController(classdetail, animated: true)
        }
        else if collectionView == collectionImages
        {
            let dict = arrImages[indexPath.row] as! [String:Any]
            
            if dict["Media_Type"] as? NSNumber == 2
            {
                let StringUrl = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Media_URL"] as? String ?? "")
                let urlString = StringUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                
                let videoURL = URL(string: urlString)
                
                if videoURL != nil
                {
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                    
                }
            }
            
            
        }
        
    }
    
    @objc func timerAction1()
    {
        
        if (collectionImages.visibleCells.count>0)
        {
            let cell:UICollectionViewCell = (collectionImages.visibleCells[0])
            
            let index:IndexPath = (collectionImages!.indexPath(for: cell))!
            
            if (index.row+1 < arrImages.count) {
                
                collectionImages.scrollToItem(at: IndexPath(item:index.row+1, section: 0), at:.centeredHorizontally, animated: true)
                
            }
            else
            {
                collectionImages.scrollToItem(at: IndexPath(item:0, section: 0), at:.centeredHorizontally, animated: true)
            }
        }
    }
    
    //MARK:- addOn delegate Methods
    
    func selectedAddonDict(arrAdon: [[String : Any]], index: NSInteger, type: String)
    {
        var dict = arrBundleItems[index]
        dict["AddOns"] = arrAdon
        arrBundleItems[index] = dict
        
        
    }
    
    //MARK:- button Methods
    @objc func btnBundleItemchildrenClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        childIndex = btn.tag
        let arrChild = arrBundleItems[btn.tag]["arrChild"] as! [[String:Any]]
        if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
        {
            let addChildvc = self.storyboard?.instantiateViewController(withIdentifier: "AddChildVC") as! AddChildVC
            addChildvc.strComeFrom = "detail"
            addChildvc.isSingleSelection = true
            if arrChild.count > 0
            {
                addChildvc.arrChildSelected = arrChild
            }
            self.navigationController?.pushViewController(addChildvc, animated: true)
        }
        else
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            login.strComeFrom = "class"
            self.navigationController?.pushViewController(login, animated: false)
        }
        
    }
    
    @objc func btnQtyClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        var dict = arrAddOn[btn.tag]
        
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            print(dictSelect)
            
            dict["SelectedQty"] = dictSelect["name"]
            let amt = Int((dict["SelectedQty"] as? String ?? "1")!)
            let price = (dict["Price"] as? NSNumber)?.floatValue
            let amunt = Float(amt!) * price!
            dict["Amount"] = amunt
            
            self.arrAddOn[btn.tag] = dict
            self.colAddOn.reloadData()
            
        }, arrData: NSMutableArray(array: arrQty as Array), isLanguage: false, isNationality: false, isCategories: false, isProfession: false, isother: true,self)
    }
    
    @IBAction func btnShareClick(_ sender: Any)
    {
        let shareText = String(format:"%@%@&Item_Type_Id=%@&IsBundleItem=1",GlobalConstants.kShareUrl,classId,itemTypeId ?? 0)
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            if let popOver = vc.popoverPresentationController {
                popOver.sourceView = self.btnShare
                //popOver.sourceRect =
                //popOver.barButtonItem
            }
        }

    }

    
    @IBAction func btnLocationClick(_ sender: Any)
    {
        
    }
    
    @IBAction func btnSelectChildClick(_ sender: Any)
    {
        if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "AddChildVC") as! AddChildVC
            login.strComeFrom = "detail"
            if arrChildSelected.count > 0
            {
                login.arrChildSelected = arrChildSelected as! [[String : Any]]
            }
            self.navigationController?.pushViewController(login, animated: true)
        }

        else
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            login.strComeFrom = "class"
            self.navigationController?.pushViewController(login, animated: false)
        }
    }
    
    @IBAction func btnFavouriteItemClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        
        if btn.isSelected
        {
            self.wscallforRemoveFav()
        }
        else
        {
            self.wscallforAddFav()
        }
        
        
    }
    
    
    @IBAction func btnSelectDatesClick(_ sender: Any)
    {
        if arrChildSelected.count == 0
        {
            let alertController = UIAlertController(title:kWarningEn, message:kPleaseSelChild, preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            alertController.show()
            return
        }
        
        calenderView.frame = viewCalendar.bounds
        calenderView.availableDates = arrAvailable
        calenderView.bookedDate = arrBooked
        viewCalendar.addSubview(calenderView)
        calenderView.delegate = self
        calenderView.topAnchor.constraint(equalTo: viewCalendar.topAnchor, constant: 10).isActive=true
        calenderView.rightAnchor.constraint(equalTo: viewCalendar.rightAnchor, constant: -12).isActive=true
        calenderView.leftAnchor.constraint(equalTo: viewCalendar.leftAnchor, constant: 12).isActive=true
        calenderView.heightAnchor.constraint(equalToConstant: 330).isActive=true
        conviewCalendarHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 450 : 330
        conBtnViewHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 60 : 40
    }
    
    
    @IBAction func btndateDoneClick(_ sender: Any)
    {
        conviewCalendarHeight.constant = 0
        conBtnViewHeight.constant = 0

    }
    
    @IBAction func btndateClearClick(_ sender: Any)
    {
        calenderView.initializeView()
        calenderView.myCollectionView.reloadData()
        
        arrSelected.removeAll()
        btndates.setTitle(kSeldates, for: .normal)

    }

    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btnbackClick(_ sender: Any)
    {
        if strcomeFromPush == "push"
        {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func btnBookClick(_ sender: Any)
    {
        if arrType.count == 0
        {
            SwiftLoggor.fileContent = "\n \(SwiftLoggor.log(message:"priceDetails are coming null",event:ErroTypes.e))"
            
            let alertController = UIAlertController(title:kWarningEn, message:"Something went wrong", preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            alertController.show()
            return
        }
        
        for i in 0..<arrActivities.count
        {
            let dictItem = arrActivities[i]
            let seats  = dictItem["Seats_Available"] as? Int
            if seats == 0
            {
                let alertController = UIAlertController(title:kWarningEn, message:kSeatAvailable, preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                    UIAlertAction in
                }
                // Add the actions
                alertController.addAction(okAction)
                // Present the controller
                alertController.show()
                return
            }
        }
        
        for i in 0..<arrBundleItems.count
        {
            let dictItem = arrBundleItems[i]
            let childId  = dictItem["ChildrenIds"] as? String
            if childId == ""
            {
                let alertController = UIAlertController(title:kWarningEn, message:kPleaseSelChild, preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                // Add the actions
                alertController.addAction(okAction)
                // Present the controller
                alertController.show()
                return
            }
            
        }
        
        var count = 0
        for i in 0..<arrBundleItems.count
        {
            var dictItem = arrBundleItems[i]
            count = count + 1
            
            let ChildQty = dictItem["Qty"] as! Int
            let arAdon = dictItem["AddOns"] as! [[String:Any]]
            
            var amount : Float = Float(dictItem["Amount"] as? String ?? "0")! * Float(ChildQty)
            var Netamount : Float = Float(dictItem["Net_Amount"] as? String ?? "0")! * Float(ChildQty)
            var disAmount : Float = Float(dictItem["Discount_Amount"] as? String ?? "0")! * Float(ChildQty)
            
            for j in 0..<arAdon.count
            {
                let dict = arAdon[j]
                if dict["isSelect"] as? String == "1"
                {
                    amount =  amount + (dict["Amount"] as? Float ?? 0.0)!
                    Netamount =  Netamount + (dict["Amount"] as? Float ?? 0.0)!

                }
            }
            
            
            let StrAmount = String(format: "%.3f", amount)
            let StrNetAmount = String(format: "%.3f", Netamount)
            let StrdisAmount = String(format: "%.3f", disAmount)

            dictItem["Amount"] = StrAmount
            dictItem["Net_Amount"] = StrNetAmount
            dictItem["Discount_Amount"] = StrdisAmount

            wscallforBookClass(dict: dictItem) { (complete) in
                if complete
                {
                    if count == self.arrBundleItems.count
                    {
                        Manager.sharedInstance.wscallforgetCart()
                        let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                       self.navigationController?.pushViewController(subCatvc, animated: false)
                    }
                    
                }
            }


        }

        
        
        
//        if arrChildSelected.count == 0
//        {
//            let alertController = UIAlertController(title:kWarningEn, message:kPleaseSelChild, preferredStyle: .alert)
//            // Create the actions
//            let okAction = UIAlertAction(title:kOkEn, style: .default) {
//                UIAlertAction in
//                NSLog("OK Pressed")
//            }
//            // Add the actions
//            alertController.addAction(okAction)
//            // Present the controller
//            alertController.show()
//        }
//        else if conViewdaysHeight.constant != 0 && arrSelected.count == 0
//        {
//            let alertController = UIAlertController(title:kWarningEn, message:kPleaseSelDates, preferredStyle: .alert)
//            // Create the actions
//            let okAction = UIAlertAction(title:kOkEn, style: .default) {
//                UIAlertAction in
//                NSLog("OK Pressed")
//            }
//            // Add the actions
//            alertController.addAction(okAction)
//            // Present the controller
//            alertController.show()
//        }
//        else if conViewdaysHeight.constant == 0 && arrSelected.count == 0
//        {
//            calendarId.removeAll()
//            for dict in arrAvailDates
//            {
//                let dict1 = dict as [String:Any]
//                let id = dict1["Calender_Id"] as? NSNumber
//                calendarId.append(id!)
//            }
//
//   // Total Amount -------------------------------
//            let price : Float = Float(strprice ?? "0")! * Float(arrChildSelected.count)
//            strprice = String(format: "%.3f", price)
//
//            var addOnTotal : Float = Float(strprice ?? "0")!
//            for i in 0..<arrAddOn.count
//            {
//                let dict = arrAddOn[i]
//                if dict["isSelect"] as? String == "1"
//                {
//                    addOnTotal =  addOnTotal + (dict["Amount"] as? Float ?? 0.0)!
//                }
//            }
//            strprice = String(format: "%.3f", addOnTotal)
//
//
//   // Net Amount (after discount) -----------------------------------
//
//            let price1 : Float = Float(strNetprice)! * Float(arrChildSelected.count)
//            strNetprice = String(format: "%.3f", price1)
//
//
//            var addOnTotal1 : Float = Float(strNetprice ?? "0")!
//            for i in 0..<arrAddOn.count
//            {
//                let dict = arrAddOn[i]
//                if dict["isSelect"] as? String == "1"
//                {
//                    addOnTotal1 =  addOnTotal1 + (dict["Amount"] as? Float ?? 0.0)!
//                }
//            }
//            strNetprice = String(format: "%.3f", addOnTotal1)
//
// //---------------------------------------------------
//
////            for i in 0..<arrChildSelected.count
////            {
////                let dict = arrChildSelected[i] as! [String:Any]
////                childrenId = String(format: "%@", (dict["Children_Id"] as? NSNumber)!)
//                wscallforBookClass { (complete) in
//                    if complete
//                    {
//
//                    }
//                }
//
////            }
//
//
//
//        }
//        else
//        {
//            var finaldateArray = [[String:Any]]()
//            for strDates in arrSelected
//            {
//                let str1 = strDates
//
//                let foundItems = arrAvailDates.filter { $0["Calender_Date"] as! String == str1 }
//
//                if foundItems.count > 0
//                {
//                    finaldateArray.append(contentsOf:foundItems)
//                }
//            }
//
//            calendarId.removeAll()
//
//            for dict in finaldateArray
//            {
//                let dict1 = dict as [String:Any]
//                let id = dict1["Calender_Id"] as? NSNumber
//                calendarId.append(id!)
//            }
//            var TotalSelCount = Int()
//            if finaldateArray.count == 0
//            {
//                 TotalSelCount = arrChildSelected.count
//
//            }
//            else
//            {
//                 TotalSelCount = arrChildSelected.count * finaldateArray.count
//
//            }
//
//
//// Total Amount -------------------------------
//
//            let price : Float = Float(strprice ?? "0")! * Float(TotalSelCount)
//            strprice = String(format: "%.3f", price)
//
//
//            var addOnTotal : Float = Float(strprice)!
//            for i in 0..<arrAddOn.count
//            {
//                let dict = arrAddOn[i]
//                if dict["isSelect"] as? String == "1"
//                {
//                    addOnTotal =  addOnTotal + (dict["Amount"] as? Float ?? 0.0)!
//                }
//            }
//
//            strprice = String(format: "%.3f", addOnTotal)
//
//// Net Amount (after discount) -----------------------------------
//
//            let price1 : Float = Float(strNetprice  ?? "0")! * Float(TotalSelCount)
//            strNetprice = String(format: "%.3f", price1)
//
//
//            var addOnTotal1 : Float = Float(strNetprice ?? "0")!
//            for i in 0..<arrAddOn.count
//            {
//                let dict = arrAddOn[i]
//                if dict["isSelect"] as? String == "1"
//                {
//                    addOnTotal1 =  addOnTotal1 + (dict["Amount"] as? Float ?? 0.0)!
//                }
//            }
//
//            strNetprice = String(format: "%.3f", addOnTotal1)
//
//            //---------------------------------------------------
//
//
////            for i in 0..<arrChildSelected.count
////            {
////                let dict = arrChildSelected[i] as! [String:Any]
////                childrenId = String(format: "%@", (dict["Children_Id"] as? NSNumber)!)
//                wscallforBookClass { (complete) in
//                    if complete
//                    {
//
//                    }
//                }
//
////            }
//        }
        
        
    }
    
    @IBAction func btnmapZoomCloseClick(_ sender: Any)
    {
        viewmapZoom.isHidden = true
    }
    
    @IBAction func btnmapZoomviewClick(_ sender: Any)
    {
        viewmapZoom.isHidden = false
        self.view.bringSubviewToFront(viewmapZoom)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        let alertController = UIAlertController(title:kWarningEn, message: kOpenGoogleMap, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title:kOkEn, style: .default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
            {
                if let url = URL(string: "comgooglemaps://?saddr=&daddr=\( mapView.centerCoordinate.latitude),\( mapView.centerCoordinate.longitude)&directionsmode=driving") {
                    UIApplication.shared.open(url, options: [:])
                }
            }
            else
            {
                print("Can't use comgooglemaps://");
                
                if let url = URL(string: "http://maps.google.com/maps?q=\( mapView.centerCoordinate.latitude),\( mapView.centerCoordinate.longitude)") {
                    UIApplication.shared.open(url, options: [:])
                }
            }
            
            
        }
        
        let CancelAction = UIAlertAction(title:kCancel, style: .default) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(CancelAction)
        
        // Present the controller
        alertController.show()
    }
    
    //MARK:- Calendar delegate
    lazy var  calenderView: CalenderView = {
        let calenderView = CalenderView(theme: MyTheme.light)
        calenderView.translatesAutoresizingMaskIntoConstraints=false
        return calenderView
    }()
    
    func didTapDate(date: String, available: Bool)
    {
        if available == true
        {
            if !arrSelected.contains(date)
            {
                arrSelected.append(date)
            }
            
            btndates.setTitle(String(format: "%d %@", arrSelected.count,kdatesselected), for: .normal)
        }
        else
        {
        }
    }
    
    //MARK:- Webservice

    func wscallforClasseDetail()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        
      
        let strUrl = String(format: "%@%@/%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetClassDetail,classId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E",userId) //
        
        let dictPara = NSMutableDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let dictData = NSMutableDictionary(dictionary: result as Dictionary)
                        //self.displayDetailData(dict: dictData)

                    }
                    else
                    {
                        
                    }
              }
        }
        
    }
    
    func wscallforClassBundleDetail()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        
        
        let strUrl = String(format: "%@%@/%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetBundleClassDetail,classId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E",userId) //
        
        let dictPara = NSMutableDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let dictData = NSMutableDictionary(dictionary: result as Dictionary)
                        self.displayDetailData1(dict: dictData)
                        
                    }
                    else
                    {
                        
                    }
            }
        }
        
    }
    
    func wscallforBookClass(dict : [String:Any] , _ complete: @escaping (_ complete: Bool) -> Void)
    {
        let arrOrder : NSMutableArray = []
        let arrItemAddOn = dict["AddOns"] as? [[String:Any]] ?? []

        for i in 0..<arrItemAddOn.count
        {
            let dictorder : NSMutableDictionary = [:]
            let dict = arrItemAddOn[i]
            
            if dict["isSelect"] as? String == "1"
            {
                dictorder.setValue(dict["Add_On_Id"], forKey: "Add_On_Id")
                dictorder.setValue(dict["SelectedQty"], forKey: "Qty")
                dictorder.setValue(dict["Price"], forKey: "Price")
                dictorder.setValue(dict["Amount"], forKey: "Amount")
                arrOrder.add(dictorder)
            }
        }
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wAddToCart)
        
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        
//        strDiscountprice = String(format: "%.3f", ( Float(strprice  ?? "0")! - Float(strNetprice  ?? "0")!))

        let ChildId = dict["ChildrenIds"] as? String ?? ""
        let dictPara:NSDictionary = ["Price_Detail_Id":dict["Price_Detail_Id"] ,"ChildrenIds":ChildId.components(separatedBy: ","),"Item_Id":dict["Item_Id"] ,"Amount":dict["Amount"] ,"CalenderIds":dict["CalenderIds"] ,"Custom_Note":"","Rate":"","Qty":dict["Qty"] ,"App_User_Id":userId,"Cart_Id":"0","Item_Type_Id":dict["Item_Type_Id"],"Size_Id":"0","Deal_Id":"0","AddOns":arrOrder ,"Promotion_Id":dict["Promotion_Id"] ,"Discount_Amount":dict["Discount_Amount"],"Net_Amount":dict["Net_Amount"]]

        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    print(result)
                    let strStatus = result["Message"] as? String
                    if strStatus == "Success"
                    {
                        Manager.sharedInstance.wscallforgetCart()
                        let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                    self.navigationController?.pushViewController(subCatvc, animated: false)

//                        if self.arrChildSelected.count > 1
//                        {
//                            var isDiscount : Bool = false
//
//                            for i in 0..<self.arrType.count
//                            {
//                                let dict = self.arrType[i]
//                                isDiscount = self.isDataAvailablefordiscount(comparisonDict: dict)
//                                if isDiscount == true
//                                {
//                                    self.strNetprice = self.strNetpriceTemp
//                                    self.strDiscountprice = self.strDiscountpriceTemp
//
//                                    if self.isPopupDisplay == false
//                                    {
//                                        let alertController = UIAlertController(title:kWarningEn, message:"you will get discount for other child", preferredStyle: .alert)
//
//                                        // Create the actions
//                                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
//                                            UIAlertAction in
//                                            NSLog("OK Pressed")
//                                            complete(true)
//                                        }
//
//                                        alertController.addAction(okAction)
//                                        alertController.show()
//
//                                    }
//                                    else
//                                    {
//                                        complete(true)
//
//                                    }
//
//                                    self.isPopupDisplay = true
//                                    break
//                                }
//                            }
//                        }
                        
                        complete(true)
                        
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        alertController.addAction(okAction)
                        
                        alertController.show()
                    }
            }
        }
        
    }
    
    func wscallforAddFav()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wAddFavoriteItem,classId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.btnFavouriteItem.isSelected = true
                        self.refreshListView()
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    func wscallforRemoveFav()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveFavoriteItem,classId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.btnFavouriteItem.isSelected = false
                        self.refreshListView()
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    
    func refreshListView()
    {
        if self.navigationController?.viewControllers != nil
        {
            let array = self.navigationController?.viewControllers
                as! Array<Any>
            for k in (0 ..< array.count)
            {
                let controller: UIViewController? = (array[k] as! UIViewController)
                if (controller is CalendarVC)
                {
                    let item = controller as! CalendarVC
                    item.wscallforClasses()
                    break
                }
            }
        }
    }
    
    
}
