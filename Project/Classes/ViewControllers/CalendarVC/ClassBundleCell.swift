//
//  ClassBundleCell.swift
//  Project
//
//  Created by Hightech IT Solution Pvt Ltd on 02/10/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

protocol BundleCellDelegate: class
{
    func selectedAddonDict(arrAdon:[[String:Any]],index:NSInteger,type:String)
}



class ClassBundleCell: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{

    @IBOutlet weak var lbltitle: UILabel!

    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblday: UILabel!
    @IBOutlet weak var lblStartdates: UILabel!
    @IBOutlet weak var imgitem: UIImageView!
    @IBOutlet weak var lblattending: UILabel!
    @IBOutlet weak var btnattending: UIButton!
    
    @IBOutlet weak var colAddOn: UICollectionView!
    @IBOutlet weak var conAddOnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDiscountprice: UILabel!
    @IBOutlet weak var lblOriginalPrice: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblLocationData: UILabel!


    var arrAddOn = [[String:Any]]()
    var arrQty  =  [[String:Any]]()
    var index : Int!

    weak var delegate: BundleCellDelegate?


    override func awakeFromNib() {
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
        arrQty = [["name":"1"],["name":"2"],["name":"3"],["name":"4"],["name":"5"],["name":"6"],["name":"7"],["name":"8"],["name":"9"],["name":"10"]]
        lblLocation.text = klocation
    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrAddOn.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {            let cellReuseIdentifier = "cell2"
            let cell:SideCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! SideCollectionCell
            
            let dict = arrAddOn[indexPath.row]
            
            cell.lblSubTitle.isHidden = false
            cell.lblTitle.text = dict["Add_On_Name"] as? String
            cell.lblSubTitle.text = String(format: "%.3f %@", (dict["Price"] as? NSNumber ?? 0)!.floatValue,kCurrency)
            
            if dict["SelectedQty"] as? String != "0"
            {
                cell.btnQty.setTitle(dict["SelectedQty"] as? String, for: .normal)
                cell.lblSubTitle.text = String(format: "%.3f %@", (dict["Amount"] as? Float ?? 0.0)!,kCurrency)
                
            }
            
            if dict["isSelect"] as? String == "1"
            {
                cell.btnCheck.isSelected = true
            }
            else
            {
                cell.btnCheck.isSelected = false
            }
            
            cell.btnQty.tag = indexPath.item
            cell.btnQty.addTarget(self, action: #selector(btnQty1Click(_:)), for: .touchUpInside)
            
            conAddOnHeight.constant = colAddOn.contentSize.height
            colAddOn.updateConstraintsIfNeeded()
            colAddOn.layoutIfNeeded()
            
            
            return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width:(colAddOn.frame.size.width) , height:(UIDevice.current.userInterfaceIdiom == .pad) ? 80 : 50)
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
            var dict = arrAddOn[indexPath.row]
            
            if dict["SelectedQty"] as? String == "0"
            {
                dict["SelectedQty"] = "1"
                let amt = Int((dict["SelectedQty"] as? String)!)
                let price = (dict["Price"] as? NSNumber)?.floatValue
                let amunt = Float(amt!) * price!
                dict["Amount"] = amunt
                
            }
            
            if  dict["isSelect"] as? String == "1"
            {
                dict["isSelect"] = "0"
            }
            else
            {
                dict["isSelect"] = "1"
            }
            
            arrAddOn[indexPath.row] = dict
            colAddOn.reloadData()
        
            delegate?.selectedAddonDict(arrAdon: arrAddOn, index: index, type: "")
        
        
    }

    @objc func btnQty1Click(_ sender: Any)
    {
        let controller = UIApplication.topViewController() as! UIViewController

        let btn = sender as! UIButton
        var dict = arrAddOn[btn.tag]
        
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            print(dictSelect)
            
            dict["SelectedQty"] = dictSelect["name"]
            let amt = Int((dict["SelectedQty"] as? String)!)
            let price = (dict["Price"] as? NSNumber)?.floatValue
            let amunt = Float(amt!) * price!
            dict["Amount"] = amunt
            
            self.arrAddOn[btn.tag] = dict
            self.colAddOn.reloadData()
            self.delegate?.selectedAddonDict(arrAdon: self.arrAddOn, index: self.index, type: "")

            
        }, arrData: NSMutableArray(array: arrQty as Array), isLanguage: false, isNationality: false, isCategories: false, isProfession: false, isother: true,controller)
    }

}
