//
//  CalendarVC.swift
//  Project
//
//  Created by HTNaresh on 3/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class CalendarVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CGCalendarViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var ViewCalendar: UIView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblNorecord: UILabel!
    @IBOutlet weak var collectionType: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var conBackviewheight: NSLayoutConstraint!

    var calendarView = CGCalendarView()
    var Month = String()
    var Year =  String()
    
    var calendar:NSCalendar!
    var strDate = String()
    var InitDate = Date()

    var strSelectedDate = Date()
    var strPassedDate = String()
    var strcomeFrom = String()
    var strTitle = String()

    var dictFilterPara : NSMutableDictionary = [:]

    var arrClasses : NSMutableArray = []
    var arrayType : NSMutableArray = []



    override func viewDidLoad()
    {
        super.viewDidLoad()
        lblNorecord.text = kNorecord

        // Do any additional setup after loading the view.
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        if strcomeFrom == "date"
        {
            strDate = formatter.string(from: InitDate)
            wscallforClasses()

        }
        else
        {
            strDate = formatter.string(from: Date())
        }
        
        setupCalView()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        arrayType = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "calenderType")

        if strcomeFrom == "home"
        {
            conBackviewheight.constant = 40
            lblTitle.text = strTitle
        }
        else  if strcomeFrom == "date"
        {
            conBackviewheight.constant = 40
        }
        else
        {
            conBackviewheight.constant = 0
        }

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "1")
    }
    
    
    func setupCalView()
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            calendarView = CGCalendarView(frame: CGRect(x: 5, y: 0, width: view.frame.size.width - 10, height: 80))

        }
        else
        {
            calendarView = CGCalendarView(frame: CGRect(x: 5, y: 0, width: view.frame.size.width - 10, height: 60))

        }
        ViewCalendar.addSubview(calendarView)
        
        
        // use a global NSCalendar
        calendarView.calendar = calendar as Calendar?
        calendarView.isonlymonth = false
        // Set the background color
        calendarView.backgroundColor = .clear
        
        calendarView.rowCellClass = CGCalendarCell.self
        calendarView.firstDate = Date(timeIntervalSinceNow: 1)
        calendarView.lastDate = Date(timeIntervalSinceNow: 60 * 60 * 24 * 365)
        
        calendarView.delegate = self
        if strcomeFrom == "date"
        {
            calendarView.selectedDate = InitDate.addingTimeInterval(60 * 60 * 24)
        }
        else
        {
            calendarView.selectedDate = Date()
        }
        
        calendarView.tableView.isScrollEnabled = false
        calendarView.tableView.reloadData()
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            calendarView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        Manager.sharedInstance.setFontFamily("", for: calendarView, andSubViews: true)

    }
    func calendarView(_ calendarView: CGCalendarView!, didSelect date: Date!)
    {
          strSelectedDate = date
          let formatter = DateFormatter()
          formatter.dateFormat = "dd/MM/yyyy"
            if strcomeFrom == "date"
            {
                strDate = formatter.string(from: date.addingTimeInterval(-60 * 60 * 24))
                strSelectedDate = date.addingTimeInterval(-60 * 60 * 24)

            }
            else
            {
                strDate = formatter.string(from: date)

            }

        
          let formatter1 = DateFormatter()
          formatter1.dateFormat = "EEEE, d MMM"
          strPassedDate = formatter1.string(from: date)

          wscallforClasses()
        
    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cellReuseIdentifier = "cell1"
        let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
        
        let dict = arrayType[indexPath.row] as! NSDictionary
        
        cell.lblTitle.text = dict["Name"] as? String
        
        cell.lblTitle.textColor = UIColor.darkGray
        cell.lblTitle.backgroundColor = UIColor.groupTableViewBackground
        
        if dictFilterPara["Item_Type_Id"] is String && dictFilterPara["Item_Type_Id"] as? String != ""
        {
            let Id = String(format: "%@", (dict["Id"] as? NSNumber)!)
            
            let filterid = (dictFilterPara["Item_Type_Id"] as? NSString ?? "0").substring(to: 1)
            
            if filterid == Id
            {
                if indexPath.row == 0
                {
                    cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: "e77a4f")
                }
                else if indexPath.row == 1
                {
                    cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: "3f3967")
                }
                    
                else if indexPath.row == 2
                {
                    cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: "f392b3")
                }
                
                cell.lblTitle.textColor = UIColor.white
            }

        }
        else
        {
            if indexPath.row == 0
            {
                cell.lblTitle.textColor = UIColor.white
                collectionType.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
                
                cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: "e77a4f")
            }
            else if indexPath.row == 1
            {
                
            }
            else if indexPath.row == 2
            {
            }
        }


       
       
        return cell
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionType.cellForItem(at: indexPath) as! CategoryCell

        
        let dict = arrayType[indexPath.row] as! NSDictionary
        
        if dict["Id"] as? NSNumber != 5
        {
            let Id = String(format: "%@", (dict["Id"] as? NSNumber)!)
            
            dictFilterPara.setValue(Id, forKey: "Item_Type_Id")
            
            if indexPath.row == 0
            {
                cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: "e77a4f")
            }
            else if indexPath.row == 1
            {
                cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: "3f3967")
            }
                
            else if indexPath.row == 2
            {
                cell.lblTitle.backgroundColor = Manager.sharedInstance.hexStringToUIColor(hex: "f392b3")
            }
            
            cell.lblTitle.textColor = UIColor.white
            collectionType.reloadData()
            wscallforClasses()
        }
        else
        {
            let venue = self.storyboard?.instantiateViewController(withIdentifier: "VenueListVC") as! VenueListVC
            
            venue.strTitle = dict["Name"] as? String
            venue.strcomeFrom = "venue"
            venue.Id = 0
            
            self.navigationController?.pushViewController(venue, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionType
        {
            let cell = collectionType.cellForItem(at: indexPath) as! CategoryCell
            
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.borderWidth = 0
            cell.lblTitle.textColor = UIColor.darkGray
            cell.lblTitle.backgroundColor = UIColor.groupTableViewBackground
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let divide = self.arrayType.count
        
        let cellSize = CGSize(width:(self.collectionType.frame.size.width / CGFloat(divide)) , height:collectionType.frame.size.height)
        return cellSize
    }
    
    //MARK:- tableview Method
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return arrClasses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "CalendarTableCell"
        let cell:CalendarTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CalendarTableCell
        
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        
        cell.lblClassname.text = dict["Class_Name"] as? String
        cell.lblTime.text = String(format: "%@ - %@", (dict["Start_Time"] as? String ?? ""),(dict["End_Time"] as? String ?? ""))
        cell.lblActivity.text = dict["Type_of_Activity"] as? String ?? ""
        cell.lbldays.text = dict["Activity_Days"] as? String ?? ""

        let arrPrice = dict["PriceDetails"] as? [[String:Any]] ?? []
        
        cell.lblPrice.textColor = UIColor.black

        if arrPrice.count > 0
        {
            let strPrice = arrPrice[0]["Price"] as? String ?? "0"
            cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, strPrice,kCurrency)
            
            if dict["Discount_Percent"] as? NSNumber == 0
            {
                cell.lblDiscountprice.text = ""
                cell.lblPrice.text = String(format: "%.3f %@", Float(dict["Price"] as? String ?? "0")!,kCurrency)
                
            }
            else  if dict["Discount_Percent"] as? NSNumber != 0 && !isDataAvailablefordiscount(comparisonDict: arrPrice[0], classId: dict["Class_Id"] as? NSNumber ?? 0)
            {
                cell.lblDiscountprice.text = ""
                cell.lblPrice.text = String(format: "%.3f %@", Float(dict["Price"] as? String ?? "0")!,kCurrency)

            }
            else
            {
                cell.lblDiscountprice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, (Float(arrPrice[0]["Price"] as? String ?? "0")!),kCurrency))
                
                let strPrice = (arrPrice[0]["Offer_Price"] as? NSNumber ?? 0).floatValue
                cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, strPrice,kCurrency)
                
                cell.lblPrice.textColor = GlobalConstants.highLightClr

            }
            
//            if arrPrice[0]["Promotion_Type_Id"] as? NSNumber == 1
//            {
//                cell.lblDiscountprice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, (Float(arrPrice[0]["Price"] as? String ?? "0")!),kCurrency))
//
//                let strPrice = (arrPrice[0]["Offer_Price"] as? NSNumber ?? 0).floatValue
//                cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, strPrice,kCurrency)
//            }
//            else
//            {
//                cell.lblDiscountprice.text = ""
//            }
        }
        else
        {
            let strPrice = dict["Price"] as? String ?? "0"
            cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, strPrice,kCurrency)
            cell.lblDiscountprice.text = ""
        }
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            
//            let str = String((dict["Age_Group"] as? String ?? "").reversed())
            
            cell.lblAgeGrp.text = String(format: "%@ %@",(dict["Age_Group"] as? String ?? ""),kAgeGrp)
        }
        else
        {
            cell.lblAgeGrp.text = String(format: "%@ %@",kAgeGrp, (dict["Age_Group"] as? String ?? "")!)
        }
        
//        cell.lblAgeGrp.text = kAgeGrp + " " + (dict["Age_Group"] as? String ?? "")!
        
        
        if dict["Is_Favorite"] as? NSNumber == 1
        {
            cell.btnwish.isSelected = true
        }
        else
        {
            cell.btnwish.isSelected = false
        }
        
        
        if dict["Image_Url"] as? String != ""
        {
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            cell.imgIcon?.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
        }
        else
        {
            cell.imgIcon.image = UIImage(named: "logo.png")
        }
        
        cell.lblwaiting.isHidden = true
        cell.lblwaitList.isHidden = true
        
        cell.viewSold.isHidden = true
        if dict["Seats_Available"] as? Int == 0 && dict["Is_Bundle_Item"] as? Int == 0
        {
            cell.viewSold.isHidden = false
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        
        if dict["Is_Bundle_Item"] as? Bool == false
        {
            let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
            classdetail.strDate = strPassedDate
            classdetail.strComeFrom = "calendar"
            classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
            classdetail.classId = dict["Class_Id"] as? NSNumber
            self.navigationController?.pushViewController(classdetail, animated: true)

        }
        else
        {
            let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetail1VC") as! ClassDetail1VC
            classdetail.strDate = strPassedDate
            classdetail.strComeFrom = "calendar"
            classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
            classdetail.classId = dict["Class_Id"] as? NSNumber
            self.navigationController?.pushViewController(classdetail, animated: true)
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return 160

        }
        
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)

        var myString = ""
        
        if let strdays = dict["Activity_Days"] as? String
        {
            myString = String(format: "%@\n%@%@", dict["Class_Name"] as? String ?? "" , strdays,dict["Type_of_Activity"] as? String ?? "")
        }
        else
        {
            myString =  (dict["Class_Name"] as? String ?? "")  + (dict["Type_of_Activity"] as? String ?? "")
        }
        
        let attributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont.systemFont(ofSize: 14.0)]

        let attributedString: NSAttributedString = NSAttributedString(string:myString,attributes:attributes)

        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: self.tblList.frame.size.width - 200, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)

        let requiredSize:CGRect = rect
        return requiredSize.height + 90
        
//        return 95
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return 55
            
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            let view1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 55))
            let label = UILabel(frame: CGRect(x: 20, y: 0, width: view1.frame.size.width - 60, height: view1.frame.size.height))
            
            view1.backgroundColor = UIColor(red: 244/255, green: 241/255, blue: 234/255, alpha: 1)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE, MMM d"
            let str1 = formatter.string(from: strSelectedDate)
            
            label.text = String(format: "%@ (%d)", str1 , self.arrClasses.count)
            
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                view1.transform = CGAffineTransform(scaleX: -1, y: 1)
                label.textAlignment = NSTextAlignment.right
                label.font = UIFont(name: GlobalConstants.kArabicBold, size: 24)
                
                label.transform = CGAffineTransform(scaleX: -1, y: 1)
                
                
            }
            else
            {
                label.textAlignment = NSTextAlignment.left
                label.font = UIFont(name: GlobalConstants.kEnglishBold, size: 24)
                
                
            }
            
            
            label.textColor = UIColor.darkGray
            label.backgroundColor = UIColor.clear
            
            view1.addSubview(label)
            
            Manager.sharedInstance.setFontFamily("", for: view1, andSubViews: true)
            
            return view1
        }
        
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: view1.frame.size.width - 60, height: view1.frame.size.height))
        
        view1.backgroundColor = UIColor(red: 244/255, green: 241/255, blue: 234/255, alpha: 1)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d"
        let str1 = formatter.string(from: strSelectedDate)

        label.text = String(format: "%@ (%d)", str1 , self.arrClasses.count)
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            view1.transform = CGAffineTransform(scaleX: -1, y: 1)
            label.textAlignment = NSTextAlignment.right
            label.font = UIFont(name: GlobalConstants.kArabicBold, size: 18)

            label.transform = CGAffineTransform(scaleX: -1, y: 1)

            
        }
        else
        {
            label.textAlignment = NSTextAlignment.left
            label.font = UIFont(name: GlobalConstants.kEnglishBold, size: 18)

            
        }
        
       
        label.textColor = UIColor.darkGray
        label.backgroundColor = UIColor.clear
        
        view1.addSubview(label)
        
        Manager.sharedInstance.setFontFamily("", for: view1, andSubViews: true)
        
        return view1
    }
    
    func isDataAvailablefordiscount(comparisonDict:[String:Any],classId:NSNumber) -> Bool
    {
        
        if comparisonDict["Promotion_Type_Id"] as? NSNumber == 3
        {
            let arrChildren = comparisonDict["Children_Linked_Ids"] as? NSArray
            
            
            let predicate = NSPredicate(format: "Item_Id = %@ AND Item_Type_Id = %@", (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber ?? 0)!, (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber ?? 0)!)
            var arr1 = [[String:Any]]()
            
            if let filtered = (Manager.sharedInstance.arrcart as? NSArray)?.filtered(using: predicate) {
                arr1 = (filtered as? [[String:Any]])!
            }
            
            if arr1.count > 0
            {
                let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", classId , (comparisonDict["Item_Type_Id"] as? NSNumber ?? 0)!)
                var arr2 = [[String:Any]]()
                
                if let filtered = arrChildren?.filtered(using: predicate) {
                    arr2 = (filtered as? [[String:Any]])!
                }
                
                if arr1.count > 0 && arr2.count > 0
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false
                
                //                var arr1 = [[String:Any]]()
                //
                //                for i in 0..<Manager.sharedInstance.arrcart.count
                //                {
                //                    let dict1 = Manager.sharedInstance.arrcart[i] as! [String:Any]
                //                    let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", (dict1["Item_Id"] as? NSNumber ?? 0)!, (dict1["Item_Type_Id"] as? NSNumber ?? 0)!)
                //
                //                    if let filtered = arrChildren?.filtered(using: predicate) {
                //                        arr1 = (filtered as? [[String:Any]])!
                //                    }
                //
                //                    if arr1.count > 0
                //                    {
                //                        break
                //                    }
                //
                //                }
                //
                //                if arr1.count > 0 && (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber == classId) && (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber == comparisonDict["Item_Type_Id"] as? NSNumber)
                //                {
                //                   return true
                //                }
                //                else
                //                {
                //                    return false
                //                }
                
            }
        }
        else if comparisonDict["Promotion_Type_Id"] as? NSNumber == 1
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    @IBAction func btnPrevious2Click(_ sender: Any)
    {
        if calendarView.tableView.visibleCells.count > 0
        {
            var index: IndexPath? = calendarView.tableView.indexPathsForVisibleRows?.first
            if (index?.row ?? 0) > 0 && (index?.row ?? 0) < 7
            {
                
                calendarView.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .none, animated: true)
            }
            else if (index?.row ?? 0) != 0
            {
                calendarView.tableView.scrollToRow(at: IndexPath(row: (index?.row ?? 0) - 7, section: 0), at: .none, animated: true)

            }
        }
    }
    
    //MARK:- Button methods
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        
    }
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btnNext2Click(_ sender: Any)
    {
        if calendarView.tableView.visibleCells.count > 0
        {
            var index: IndexPath? = calendarView.tableView.indexPathsForVisibleRows?.last
            
            calendarView.tableView.scrollToRow(at: IndexPath(row: (index?.row ?? 0) + 7, section: 0), at: .none, animated: true)

        }
    }
    
    //MARK:- webservice
    func wscallforClasses()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wGetClaases)
        
        let dictPara = NSMutableDictionary()
        
        dictPara.setObject("", forKey: "Area_Id" as NSCopying)
        dictPara.setObject("", forKey: "Genders" as NSCopying)
        dictPara.setObject("", forKey: "Guests" as NSCopying)
        
        dictPara.addEntries(from: dictFilterPara as! [AnyHashable : Any])
        dictPara.setObject(strDate, forKey: "Class_Date" as NSCopying)

        if dictFilterPara["Item_Type_Id"] is String && dictFilterPara["Item_Type_Id"] as? String != ""
        {
        }
        else
        {
            dictPara.setObject("2", forKey: "Item_Type_Id" as NSCopying)
        }
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            dictPara.setObject("A", forKey: "Lang" as NSCopying)
        }
        else
        {
            dictPara.setObject("E", forKey: "Lang" as NSCopying)
        }
        
        
        
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {
//                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrClasses = NSMutableArray(array: result["Classes"] as? Array ?? [])
                        
                        if self.arrClasses.count > 0
                        {
                            self.tblList.isHidden = false
                            self.lblNorecord.isHidden = true
                            self.tblList.reloadData()

                        }
                        else
                        {
                            self.tblList.isHidden = true
                            self.lblNorecord.isHidden = false

                        }
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    

}
