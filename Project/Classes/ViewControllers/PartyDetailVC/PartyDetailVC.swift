//
//  PartyDetailVC.swift
//  Project
//
//  Created by HTNaresh on 7/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit
import MapKit
import AVKit


class PartyDetailVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CalenderDelegate,MKMapViewDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblClassName: UILabel!
    @IBOutlet weak var lblProvider: UILabel!
    @IBOutlet weak var lblAgrGroup: UILabel!
    @IBOutlet weak var lblActivityType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnBook: UIButton!
    @IBOutlet weak var txtClassDesc: UITextView!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var mapZoom: MKMapView!
    @IBOutlet weak var viewmapZoom: UIView!
    @IBOutlet weak var btnShare: UIButton!

    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lbladdres: UILabel!
    @IBOutlet weak var lblAboutProvider: UILabel!
    @IBOutlet weak var lblProvidername: UILabel!
    @IBOutlet weak var txtProviderDesc: UITextView!
    @IBOutlet weak var lblOtherClasses: UILabel!
    @IBOutlet weak var txtGuest: UITextField!
    
    @IBOutlet weak var viewThankYou: UIView!
    
    @IBOutlet weak var colVendor: UICollectionView!
    @IBOutlet weak var conclassDescHeight: NSLayoutConstraint!
    @IBOutlet weak var convendorDescHeight: NSLayoutConstraint!
    
    @IBOutlet weak var conColSuggestedHeight: NSLayoutConstraint!
    @IBOutlet weak var conColTypeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var conViewSpecialReqHeight: NSLayoutConstraint!
    @IBOutlet weak var txtSpecialReq: UITextView!
    @IBOutlet weak var lblSpecialReq: UILabel!

    
    @IBOutlet weak var conviewCalendarHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var colType: UICollectionView!
    
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblday: UILabel!
    @IBOutlet weak var lblstartDates: UILabel!

    
    @IBOutlet weak var lblattending: UILabel!
    @IBOutlet weak var lbldates: UILabel!
    
    @IBOutlet weak var btnattending: UIButton!
    @IBOutlet weak var btndates: UIButton!
    @IBOutlet weak var btndateDone: UIButton!
    @IBOutlet weak var btndateClear: UIButton!
    
    @IBOutlet weak var scrl: UIScrollView!
    @IBOutlet weak var conViewdaysHeight: NSLayoutConstraint!
    @IBOutlet weak var conBtnViewHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewChildHeight: NSLayoutConstraint!
    @IBOutlet weak var btnFavouriteItem: UIButton!
    
    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var pagecntrl : UIPageControl!

    var classId: NSNumber!
    var strDate: String!
    var VendorId: NSNumber!
    var strcomeFrom : String = ""

    
    var arrType = [[String:Any]]()
    var arrSuggested = [[String:Any]]()
    var arrAvailDates = [[String:Any]]()
    var arrImages : NSMutableArray = []

    var arrSelected = [String]()
    var arrBooked = [String]()
    var arrAvailable = [String]()
    var arrChildSelected : NSMutableArray = []
    
    var priceId : NSNumber!
    var itemTypeId : NSNumber!
    var childrenId : String!
    var calendarId = [NSNumber]()
    var strprice : String!
    var strAddOn : String!
    var arrQty  =  [[String:Any]]()

    var promotionId : NSNumber!
    
    var strDiscountprice : String!
    var strNetprice : String!
    let todate = Date()
    var strToday : String = ""
    var strTime : String = ""
    
    var dateFormatter = DateFormatter()
    var dateFormatter1 = DateFormatter()
    var timer = Timer()

    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let layout = collectionImages.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        collectionImages.isPagingEnabled = true

        
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter1.dateFormat = "dd/MM/yyyy"
        strToday = dateFormatter1.string(from: todate)
        strTime = dateFormatter.string(from: todate)

        childrenId = ""
        conviewChildHeight.constant = 0

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        conviewCalendarHeight.constant = 0
        conBtnViewHeight.constant = 0
        
        
        self.wscallforClasseDetail()
        
        arrQty = [["name":"1"],["name":"2"],["name":"3"],["name":"4"],["name":"5"],["name":"6"],["name":"7"],["name":"8"],["name":"9"],["name":"10"]]
        
        lblTitle.text = kDetails
        lblLocation.text = klocation
        lblAboutProvider.text = kaboutProvider
        lblInfo.text = kinfo
        lblattending.text = kattending
        btnattending.setTitle(kselectchild, for: .normal)
        lbldates.text = kSeldates
        btndates.setTitle(kSeldates, for: .normal)
        btndateDone.setTitle(kDone, for: .normal)
        btndateClear.setTitle(kcleardates, for: .normal)
        btnBook.setTitle(kbook, for: .normal)

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "0")
    }
    
    func displayDetailData(dict: NSMutableDictionary)
    {
        self.lblClassName.text = dict["Class_Name"] as? String
        self.lblActivityType.text = dict["Type_of_Activity"] as? String
        
        let attribute = NSMutableAttributedString(string:  String(format: "%@ %@ %@ ",kPrice, (dict["Price"] as? String ?? "")!,kCurrency))
        
        let result = NSMutableAttributedString()
        result.append(attribute)
        result.append(Manager.sharedInstance.StrikeThroughString(str: ""))
        
        self.lblAgrGroup.attributedText = result

        strprice = dict["Price"] as? String
        
        self.VendorId = dict["Vendor_Id"] as? NSNumber

        var Stringimg = String()
        Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: urlString!)
        imgLogo.setImageWith(url, usingActivityIndicatorStyle: .gray)
        
        lblProvidername.text = dict["Vendor_Name"] as? String
        let text = (lblProvidername.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 : NSRange = (text as NSString).range(of: (dict["Vendor_Name"] as? String ?? "")!)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalConstants.themeClr , range: range1)
        
        lblProvidername.attributedText = underlineAttriString
        
        lblProvidername.isUserInteractionEnabled = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLabel1(gesture:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        lblProvidername.addGestureRecognizer(tapGestureRecognizer)
        
        lbladdres.text = dict["Vendor_Address"] as? String ?? ""
        
        txtClassDesc.text = (dict["Class_Desc"] as? String ?? "").html2String
        let sizeThatFitsTextView = txtClassDesc.sizeThatFits(CGSize(width: txtClassDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
        conclassDescHeight.constant = sizeThatFitsTextView.height
        txtClassDesc.updateConstraintsIfNeeded()
        txtClassDesc.updateFocusIfNeeded()
        
        txtProviderDesc.text = (dict["Vendor_Desc"] as? String ?? "")
        let sizeThatFitsTextView1 = txtProviderDesc.sizeThatFits(CGSize(width: txtProviderDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
        convendorDescHeight.constant = sizeThatFitsTextView1.height
        txtProviderDesc.updateConstraintsIfNeeded()
        txtProviderDesc.updateFocusIfNeeded()
        
        lblstartDates.text = strDate
        
        lblstartDates.text = String(format: "%@ to %@  (%@ %@)", (dict["Start_Date"] as? String) ?? "",(dict["End_Date"] as? String) ?? "",(dict["Guests"] as? NSNumber ?? "") , kGuest)

        
        if dict["Is_Favorite"] as? NSNumber == 1
        {
            btnFavouriteItem.isSelected = true
        }
        else
        {
            btnFavouriteItem.isSelected = false
        }

        if dict["Start_Time"] is String
        {
            lblday.text = String(format: "%@ - %@", dict["Start_Time"] as? String ?? "", dict["End_Time"] as? String ?? "")
        }
        else
        {
            lblday.text = ""
        }
        
        lblTime.text = dict["Activity_Days"] as? String ?? ""

        
        arrType = dict["AddOns"] as? Array ?? []
       
        if arrType.count == 0
        {
//            self.conViewdaysHeight.constant = 0
            conviewCalendarHeight.constant = 0
            conBtnViewHeight.constant = 0
            conColTypeHeight.constant = 0
        }
        else
        {
            conColTypeHeight.constant = 50
            colType.reloadData()
        }
        
        
        
        for i in 0..<arrType.count
        {
            let dict = NSMutableDictionary(dictionary: (arrType[i] as? Dictionary)!)
            if i == 0
            {
                dict.setValue("0", forKey: "isSelect")
            }
            else
            {
                dict.setValue("0", forKey: "isSelect")
                
            }
            
            dict.setValue("0", forKey: "SelectedQty")
            
            arrType[i] = dict as! [String:Any]
        }
        
        colType.reloadData()
        
        let arrPriceDetail =  dict["PriceDetails"] as? [[String:Any]] ?? []
        for i in 0..<arrPriceDetail.count
        {
            let dict = NSMutableDictionary(dictionary: (arrPriceDetail[i] as? Dictionary)!)
            if i == 0
            {
                priceId = dict["Price_Detail_Id"] as? NSNumber
                strprice = String(format: "%.3f", Float(dict["Price"] as? String ?? "0")!)
                itemTypeId = dict["Item_Type_Id"] as? NSNumber

                promotionId = dict["Promotion_Id"] as? NSNumber
                strDiscountprice = String(format: "%.3f", (Float(dict["Price"] as? String ?? "0")! - (dict["Offer_Price"] as? Float ?? 0.0)!))
                strNetprice = String(format: "%.3f", (dict["Offer_Price"] as? Float ?? 0.0)!)
                
                let attribute = NSMutableAttributedString(string:  String(format: "%@ %@ %@ ",kPrice, strprice!,kCurrency))
                
                let result = NSMutableAttributedString()
                result.append(attribute)
                
                if dict["Discount_Percent"] as? NSNumber != 0
                {
                    let str2 = String(format: "%@ %@", strNetprice,kCurrency)
                    result.append(Manager.sharedInstance.StrikeThroughString(str: str2))
                }
                self.lblAgrGroup.attributedText = result
            }
           
        }
        
        arrSuggested = dict["SuggestedClasses"] as? Array ?? []
        if arrSuggested.count  > 0
        {
            conColSuggestedHeight.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 200 : 100
            lblOtherClasses.isHidden = false
            colVendor.reloadData()
            lblOtherClasses.text = String(format: "%@(%@)", kotherClass,(dict["Vendor_Name"] as? String ?? "")!)
        }
        else
        {
            conColSuggestedHeight.constant = 0
            lblOtherClasses.isHidden = true
            
        }
        
        
        let strLat = dict["Latitude"] as? String ?? "0"
        let strLong = dict["Longitude"] as?  String ?? "0"
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: Double(strLat)!, longitude: Double(strLong)!)
        map.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta:0.05 , longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
        map.setRegion(region, animated: true)
        map.isScrollEnabled = false
        
        mapZoom.addAnnotation(annotation)
        mapZoom.setRegion(region, animated: true)

        
        
        
        let arr1 = dict["PriceDetails"] as? [[String:Any]] ?? []
        if arr1.count > 0
        {
            priceId = arr1[0]["Price_Detail_Id"] as? NSNumber
        }
        
        setdateArray(arrDates: dict["AvailableClasses"] as! Array)
        
        arrImages = NSMutableArray(array: (dict["Media"] as? [[String:Any]])!)
        if arrImages.count == 0
        {
            collectionImages.isHidden = true
        }
        else
        {
            collectionImages.backgroundColor = UIColor.clear
            imgLogo.isHidden = true
            collectionImages.reloadData()
        }
        
    }
    
    @objc func tapLabel1(gesture: UITapGestureRecognizer)
    {
        let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
        classdetail.VendorId = self.VendorId
        self.navigationController?.pushViewController(classdetail, animated: true)
    }
    
    func setdateArray(arrDates:Array<Any>)
    {
        arrAvailDates = arrDates as! [[String:Any]]
        let aa1 = arrDates as? [[String:Any]] ?? []
        for i in 0..<aa1.count
        {
            let dict = aa1[i]
            
            let arr = lblday.text!.components(separatedBy: "-")
            if arr.count == 2 && dict["Calender_Date"] as? String == strToday
            {
                let dateF = DateFormatter()
                dateF.dateFormat = "dd/MM/yyyy hh:mm a"
                let strToday = dateF.string(from: Date())
                let Today = dateFormatter.date(from: (strToday as NSString).substring(from: 11))
                
                let date1 = dateFormatter.date(from: arr[0])
                let date2 = dateFormatter.date(from: arr[1])
                
                if (Today!.compare(date1!) == .orderedAscending) && Today!.compare(date2!) == .orderedAscending
                {
                    let aa2 = NSMutableArray(array: dict["Children"] as! Array)
                    
                    arrAvailable.append(dict["Calender_Date"] as? String ?? "")
                    for j in 0..<aa2.count
                    {
                        let dict1 = aa2[j] as! [String:Any]
                        if dict1["Is_Booked"] as? NSNumber == 1
                        {
                            arrBooked.append(dict["Calender_Date"] as! String)
                            arrAvailable.removeLast()
                            break
                        }
                    }
                }
            }
            else
            {
                let dateF = DateFormatter()
                dateF.dateFormat = "dd/MM/yyyy"
                let strToday = dateF.string(from: Date())
                let Today = dateF.date(from: (strToday))
                let otherDate = dateF.date(from: (dict["Calender_Date"] as? String ?? strToday))
                if (Today!.compare(otherDate!) == .orderedAscending)
                {
                    let aa2 = NSMutableArray(array: dict["Children"] as! Array)
                    arrAvailable.append(dict["Calender_Date"] as? String ?? "")
                    for j in 0..<aa2.count
                    {
                        let dict1 = aa2[j] as! [String:Any]
                        if dict1["Is_Booked"] as? NSNumber == 1
                        {
                            arrBooked.append(dict["Calender_Date"] as! String)
                            arrAvailable.removeLast()
                            break
                        }
                    }
                }
          
           }
        }
        
        print(arrAvailable.count)
        if arrAvailable.count == 0
        {
            btnBook.isEnabled = false
            btnBook.setTitle(kSoldOut, for: .normal)
            btnBook.backgroundColor = UIColor.lightGray
        }
    }

    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == colType
        {
            return arrType.count
        }
        else if collectionView == collectionImages
        {
            return arrImages.count
        }
        return  arrSuggested.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == colType
        {
            
            let cellReuseIdentifier = "cell2"
            let cell:SideCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! SideCollectionCell
            
            let dict = arrType[indexPath.row]
            
            cell.lblSubTitle.isHidden = false
            cell.lblTitle.text = dict["Add_On_Name"] as? String
            cell.lblSubTitle.text = String(format: "%.3f %@", (dict["Price"] as? NSNumber ?? 0)!.floatValue,kCurrency)
            
            if dict["SelectedQty"] as? String != "0"
            {
                cell.btnQty.setTitle(dict["SelectedQty"] as? String, for: .normal)
                cell.lblSubTitle.text = String(format: "%.3f %@", (dict["Amount"] as? Float ?? 0.0)!,kCurrency)

            }
            
            if dict["isSelect"] as? String == "1"
            {
                cell.btnCheck.isSelected = true
            }
            else
            {
                cell.btnCheck.isSelected = false
            }
            
            cell.btnQty.tag = indexPath.item
            cell.btnQty.addTarget(self, action: #selector(btnQtyClick(_:)), for: .touchUpInside)
            
            conColTypeHeight.constant = colType.contentSize.height
            colType.updateConstraintsIfNeeded()
            colType.layoutIfNeeded()

            
            return cell
        }
        else if collectionView == collectionImages
        {
            let cellReuseIdentifier = "cell2"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            let dict = arrImages[indexPath.row] as! [String:Any]
            
            let Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Thumbnail_URL"] as? String ?? "")
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            cell.imgLogo.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
            
            cell.imgPlay.isHidden = true
            
            if dict["Media_Type"] as? NSNumber == 2
            {
                cell.imgPlay.isHidden = false
            }

            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction1), userInfo: nil, repeats:true)
            
            cell.imgLogo.backgroundColor = UIColor.clear

            
            return cell
            
        }
        else
        {
            
            let cellReuseIdentifier = "HomeCollectionCell"
            let cell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! HomeCollectionCell
            
            let dict = arrSuggested[indexPath.row]
            
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
            
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            let url = URL(string: urlString!)
            cell.imgAdv.setImageWith(url, usingActivityIndicatorStyle: .gray)
            
            cell.lblTitle.text = dict["Class_Name"] as? String
            return cell
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == colType
        {
            let cellSize = CGSize(width:(colType.frame.size.width) , height:50)
            return cellSize
        }
        else if collectionView == collectionImages
        {
            let cellSize = CGSize(width:self.collectionImages.frame.size.width , height:self.collectionImages.frame.size.height)
            return cellSize
        }
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            let cellSize = CGSize(width:130 , height:200)
            return cellSize
        }
        else
        {
            let cellSize = CGSize(width:80 , height:100)
            return cellSize
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == colType
        {
            var dict = arrType[indexPath.row]
            
            if dict["SelectedQty"] as? String == "0"
            {
                dict["SelectedQty"] = "1"
                let amt = Int((dict["SelectedQty"] as? String ?? "1")!)
                let price = (dict["Price"] as? NSNumber ?? 0)?.floatValue
                let amunt = Float(amt!) * price!
                dict["Amount"] = amunt

            }
            
        
            if  dict["isSelect"] as? String == "1"
            {
                dict["isSelect"] = "0"
            }
            else
            {
                dict["isSelect"] = "1"
            }

            arrType[indexPath.row] = dict

            colType.reloadData()
            
        }
        else if collectionView == collectionImages
        {
            let dict = arrImages[indexPath.row] as! [String:Any]
            
            if dict["Media_Type"] as? NSNumber == 2
            {
                let StringUrl = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Media_URL"] as? String ?? "")
                let urlString = StringUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                
                let videoURL = URL(string: urlString)
                
                if videoURL != nil
                {
                    let player = AVPlayer(url: videoURL!)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }

                }
            }
            
            
        }
        
    }
    
    @objc func timerAction1()
    {
        
        if (collectionImages.visibleCells.count>0)
        {
            let cell:UICollectionViewCell = (collectionImages.visibleCells[0])
            
            let index:IndexPath = (collectionImages!.indexPath(for: cell))!
            
            if (index.row+1 < arrImages.count) {
                
                collectionImages.scrollToItem(at: IndexPath(item:index.row+1, section: 0), at:.centeredHorizontally, animated: true)
                
            }
            else
            {
                collectionImages.scrollToItem(at: IndexPath(item:0, section: 0), at:.centeredHorizontally, animated: true)
            }
        }
    }
    
    //MARK:- cart availibility check
    
    func isadded() -> Bool
    {

        var Cartdata : NSMutableArray = []

        Cartdata = Manager.sharedInstance.arrcart

        var isPresent : Bool = false
        if Cartdata.count > 0
        {
            for j in 0..<Cartdata.count
            {
                let dict1 = NSMutableDictionary(dictionary: Cartdata[j] as! Dictionary)
                
                let arrSelectDate = dict1["SelectedDates"] as? [[String:Any]]
                
                let arrfilter = arrSelectDate?.filter(){ calendarId.contains($0["Calender_Id"] as! NSNumber)}

                if dict1["Item_Id"] as? NSNumber == classId && arrfilter?.count ?? 0 > 0
                {
                    isPresent = true
                    break

                }

            }

            return isPresent
        }

        return false


    }
    
    //MARK:- button Methods
    
    @IBAction func btnShareClick(_ sender: Any)
    {
        let shareText = String(format:"%@%@&Item_Type_Id=%@&IsBundleItem=0",GlobalConstants.kShareUrl,classId,itemTypeId ?? 0)
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            if let popOver = vc.popoverPresentationController {
                popOver.sourceView = self.btnShare
                //popOver.sourceRect =
                //popOver.barButtonItem
            }
        }
        
    }
    @objc func btnQtyClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        var dict = arrType[btn.tag]
        
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            print(dictSelect)
            
            dict["SelectedQty"] = dictSelect["name"]
            let amt = Int((dict["SelectedQty"] as? String)!)
            let price = (dict["Price"] as? NSNumber)?.floatValue
            let amunt = Float(amt!) * price!
            dict["Amount"] = amunt

            self.arrType[btn.tag] = dict
            self.colType.reloadData()
            
        }, arrData: NSMutableArray(array: arrQty as Array), isLanguage: false, isNationality: false, isCategories: false, isProfession: false, isother: true,self)
        
        
    }
    
    @IBAction func btnLocationClick(_ sender: Any)
    {
        
    }
    
    @IBAction func btnSelectChildClick(_ sender: Any)
    {
        if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "AddChildVC") as! AddChildVC
            login.strComeFrom = "detail"
            if arrChildSelected.count > 0
            {
                login.arrChildSelected = arrChildSelected as! [[String : Any]]
            }
            self.navigationController?.pushViewController(login, animated: true)
        }
            
        else
        {
            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            login.strComeFrom = "class"
            self.navigationController?.pushViewController(login, animated: true)
        }
    }
    
    @IBAction func btnFavouriteItemClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        
        if btn.isSelected
        {
            self.wscallforRemoveFav()
        }
        else
        {
            self.wscallforAddFav()
        }
        
        
    }
    
    
    @IBAction func btnSelectDatesClick(_ sender: Any)
    {
        calenderView.frame = viewCalendar.bounds
        calenderView.availableDates = arrAvailable
        calenderView.bookedDate = arrBooked
        viewCalendar.addSubview(calenderView)
        calenderView.delegate = self
        calenderView.topAnchor.constraint(equalTo: viewCalendar.topAnchor, constant: 10).isActive=true
        calenderView.rightAnchor.constraint(equalTo: viewCalendar.rightAnchor, constant: -12).isActive=true
        calenderView.leftAnchor.constraint(equalTo: viewCalendar.leftAnchor, constant: 12).isActive=true
        calenderView.heightAnchor.constraint(equalToConstant: 330).isActive=true
        conviewCalendarHeight.constant = 330
        conBtnViewHeight.constant = 40
    }
    
    
    @IBAction func btndateDoneClick(_ sender: Any)
    {
        conviewCalendarHeight.constant = 0
        conBtnViewHeight.constant = 0
        
    }
    
    @IBAction func btndateClearClick(_ sender: Any)
    {
        calenderView.initializeView()
        calenderView.myCollectionView.reloadData()
        
        arrSelected.removeAll()
        btndates.setTitle(String(format: "Select dates"), for: .normal)
        
    }
    
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btnbackClick(_ sender: Any)
    {
        if strcomeFrom == "push"
        {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btnmapZoomCloseClick(_ sender: Any)
    {
       viewmapZoom.isHidden = true
    }
    
    @IBAction func btnmapZoomviewClick(_ sender: Any)
    {
        viewmapZoom.isHidden = false
        self.view.bringSubviewToFront(viewmapZoom)

    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        let alertController = UIAlertController(title:kWarningEn, message: kOpenGoogleMap, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title:kOkEn, style: .default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
            {
                if let url = URL(string: "comgooglemaps://?saddr=&daddr=\( mapView.centerCoordinate.latitude),\( mapView.centerCoordinate.longitude)&directionsmode=driving") {
                    UIApplication.shared.open(url, options: [:])
                }
            }
            else
            {
                print("Can't use comgooglemaps://");
                
                if let url = URL(string: "http://maps.google.com/maps?q=\( mapView.centerCoordinate.latitude),\( mapView.centerCoordinate.longitude)") {
                    UIApplication.shared.open(url, options: [:])
                }
            }
            
            
        }
        
        let CancelAction = UIAlertAction(title:kCancel, style: .default) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(CancelAction)
        
        // Present the controller
        alertController.show()
    }

    
    @IBAction func btnBookClick(_ sender: Any)
    {
        if strprice == nil || strprice == ""
        {
            SwiftLoggor.fileContent = "\n \(SwiftLoggor.log(message:"priceDetails are coming null",event:ErroTypes.e))"
            
            let alertController = UIAlertController(title:kWarningEn, message:"Something went wrong", preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            alertController.show()
            return
        }
        
        
//        if txtGuest.text?.count == 0
//        {
//            let alertController = UIAlertController(title:kWarningEn, message:"Please enter guest", preferredStyle: .alert)
//            // Create the actions
//            let okAction = UIAlertAction(title:kOkEn, style: .default) {
//                UIAlertAction in
//                NSLog("OK Pressed")
//            }
//            // Add the actions
//            alertController.addAction(okAction)
//            // Present the controller
//            alertController.show()
//        }
        
        if conViewdaysHeight.constant != 0 && arrSelected.count == 0
        {
            let alertController = UIAlertController(title:kWarningEn, message:kPleaseSelDates, preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            alertController.show()
        }
        else
        {
            var finaldateArray = [[String:Any]]()
            for strDates in arrSelected
            {
                let str1 = strDates
                
                let foundItems = arrAvailDates.filter { $0["Calender_Date"] as! String == str1 }
                
                if foundItems.count > 0
                {
                    finaldateArray.append(contentsOf:foundItems)
                }
            }
            
            for dict in finaldateArray
            {
                let dict1 = dict as [String:Any]
                let id = dict1["Calender_Id"] as? NSNumber
                calendarId.append(id!)
            }
 
// Total amount calculation-------------------
            var addOnTotal : Float = Float(strprice)!
            addOnTotal = addOnTotal * Float(calendarId.count)

            for i in 0..<arrType.count
            {
                let dict = arrType[i]
                if dict["isSelect"] as? String == "1"
                {
                    addOnTotal =  addOnTotal + (dict["Amount"] as? Float ?? 0.0)!
                }
            }
            
//           addOnTotal = addOnTotal * Float(calendarId.count)
            strprice = String(format: "%.3f", addOnTotal)
            
// Net amount calculation -------------------
            
            var addOnTotal1 : Float = Float(strNetprice)!
            addOnTotal1 = addOnTotal1 * Float(calendarId.count)
            
            for i in 0..<arrType.count
            {
                let dict = arrType[i]
                if dict["isSelect"] as? String == "1"
                {
                    addOnTotal1 =  addOnTotal1 + (dict["Amount"] as? Float ?? 0.0)!
                }
            }
            
//           addOnTotal1 = addOnTotal1 * Float(calendarId.count)
            strNetprice = String(format: "%.3f", addOnTotal1)
            

            if !isadded()
            {
                wscallforBookClass()
            }
            else
            {
                let alertController = UIAlertController(title:kWarningEn, message:kAlreadyAdded, preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                }
                // Add the actions
                alertController.addAction(okAction)
                // Present the controller
                alertController.show()
            }
            
        }
    }
    
    //MARK:- Calendar delegate
    lazy var  calenderView: CalenderView = {
        let calenderView = CalenderView(theme: MyTheme.light)
        calenderView.translatesAutoresizingMaskIntoConstraints=false
        return calenderView
    }()
    
    func didTapDate(date: String, available: Bool)
    {
        var isSeatAvail = true

        if available == true
        {
            var seat_avail = 0

            for dict in arrAvailDates
            {
                let dict1 = dict as [String:Any]
                seat_avail = dict1["Seats_Available"] as? Int ?? 0
                
                if date == dict1["Calender_Date"] as? String && seat_avail == 0
                {
                    isSeatAvail = false
                    break
                }
                
            }
            
            if isSeatAvail
            {
                if !arrSelected.contains(date)
                {
                    arrSelected.append(date)
                }
                
                btndates.setTitle(String(format: "%d %@", arrSelected.count,kdatesselected), for: .normal)
            }
            else
            {
                let msg = String(format: kVenueNotAvailable, seat_avail)
                let alertController = UIAlertController(title:kWarningEn, message:msg, preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                    UIAlertAction in
                    self.calenderView.initializeView()
                    self.calenderView.myCollectionView.reloadData()
                    
                    self.arrSelected.removeAll()
                    self.btndates.setTitle(kSeldates, for: .normal)
                }
                // Add the actions
                alertController.addAction(okAction)
                // Present the controller
                alertController.show()
            }
            
            
           
        }
        else
        {
        }
    }
    
    //MARK:- Webservice
    
    func wscallforClasseDetail()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        
        
        let strUrl = String(format: "%@%@/%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetClassDetail,classId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E",userId)
        
        let dictPara = NSMutableDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let dictData = NSMutableDictionary(dictionary: result as Dictionary)
                        self.displayDetailData(dict: dictData)
                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
    func wscallforBookClass()
    {
        let arrOrder : NSMutableArray = []

        for i in 0..<arrType.count
        {
            let dictorder : NSMutableDictionary = [:]
            let dict = arrType[i]
            
            if dict["isSelect"] as? String == "1"
            {
                dictorder.setValue(dict["Add_On_Id"], forKey: "Add_On_Id")
                dictorder.setValue(dict["SelectedQty"], forKey: "Qty")
                dictorder.setValue(dict["Price"], forKey: "Price")
                dictorder.setValue(dict["Amount"], forKey: "Amount")
                arrOrder.add(dictorder)
            }
        }
        
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wAddToCart)
        
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        
        strDiscountprice = String(format: "%.3f", ( Float(strprice ?? "")! - Float(strNetprice ?? "")!))

        
        let dictPara:NSDictionary = ["Price_Detail_Id":priceId,"ChildrenIds":[] ,"Item_Id":classId,"Amount":strprice,"CalenderIds":calendarId,"Custom_Note":self.txtSpecialReq.text,"Rate":"","Qty":"1","App_User_Id":userId,"Cart_Id":"0","Item_Type_Id":"5","Size_Id":"0","Deal_Id":"0","AddOns":arrOrder,"Promotion_Id":promotionId,"Discount_Amount":strDiscountprice,"Net_Amount":strNetprice]
        
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        Manager.sharedInstance.wscallforgetCart()
                        
                        let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                        self.navigationController?.pushViewController(subCatvc, animated: false)
                        
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:strStatus, preferredStyle: .alert)
                            // Create the actions
                            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                                UIAlertAction in
                                NSLog("OK Pressed")
                            }
                            // Add the actions
                            alertController.addAction(okAction)
                            alertController.show()
                    }
            }
        }
        
    }
    
    func wscallforAddFav()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wAddFavoriteItem,classId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.btnFavouriteItem.isSelected = true
                        self.refreshListView()
                        
                        
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    func wscallforRemoveFav()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveFavoriteItem,classId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.btnFavouriteItem.isSelected = false
                        self.refreshListView()
                        
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    
    func refreshListView()
    {
        let array = self.navigationController?.viewControllers
            as! Array<Any>
        for k in (0 ..< array.count)
        {
            let controller: UIViewController? = (array[k] as! UIViewController)
            
            //            if strfrom == "fav"
            //            {
            //                if (controller is FavouriteVC)
            //                {
            //                    let item = controller as! FavouriteVC
            //                    item.wscallforfavoritelist()
            //
            //                    break
            //                }
            //            }
            //            else
            //            {
            if (controller is CalendarVC)
            {
                let item = controller as! CalendarVC
                item.wscallforClasses()
                
                break
            }
            //            }
            
            
        }
    }
    
    
}

public extension UIAlertController {
    func show() {
        
        UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)

//        let win = UIWindow(frame: UIScreen.main.bounds)
//        let vc = UIViewController()
//        vc.view.backgroundColor = .clear
//        win.rootViewController = vc
//        win.windowLevel = UIWindow.Level.alert + 1
//        win.makeKeyAndVisible()
//        vc.present(self, animated: true, completion: nil)
    }
}
