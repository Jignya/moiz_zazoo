//
//  RegisterVC.swift
//  Zazoo
//
//  Created by HTNaresh on 18/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn



class RegisterVC: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,GIDSignInDelegate,GIDSignInUIDelegate,UIPickerViewDelegate,UIPickerViewDataSource
{
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastname: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPW: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!

    @IBOutlet weak var lblRegister: UILabel!

    
    @IBOutlet weak var imgZaazoo: UIImageView!
    
    @IBOutlet weak var lblAddChildren: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var tblChild: UITableView!
    @IBOutlet weak var conTblChildHeight: NSLayoutConstraint!
    @IBOutlet weak var btnFacebook: ButtonExtender!
    @IBOutlet weak var btnGplus: ButtonExtender!
    @IBOutlet weak var conviewSocialHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewNameHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewbottom: NSLayoutConstraint!

    @IBOutlet weak var lblSuccess: UILabel!


    
    var arrChild = [[String : String]]()
    var dictChild : [String : String] = [:]
    var pickOption = ["Male", "Female"]


    var strGender : String!
    var strDob : String!
    var strAge : String!

    var isSocial : Bool = false



    var strComeFrom : String!
    var strName : String!
    var strEmail : String!
    var strPw : String!
    var strMobile : String!
    var strLastName : String!

    
    var Fb_id : Int = 0
    var Gplus_id : Int = 0
    var login_type : Int = 1
    var UserImage : String = ""

    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        
        imgZaazoo.layer.cornerRadius = imgZaazoo.frame.size.height / 2
        imgZaazoo.clipsToBounds = true
        
        strDob = ""
        strAge = ""
        
        
        img1.isHidden = true
        img2.isHidden = true
        img3.isHidden = true
        img4.isHidden = true
        img5.isHidden = true
        img6.isHidden = true

        
        txtName.placeholder = kName
        txtMobile.placeholder = kMobNum
        txtLastname.placeholder = klastName
        txtEmail.placeholder = kEmail
        txtPassword.placeholder = kPassword
        txtConfirmPW.placeholder = kConfirmPw
        lblRegister.text = kregister
        lblAddChildren.text = kaddChildren

        
        btnLogin.setTitle(kLogin.uppercased(), for: .normal)
        
        btnFacebook.imageView?.contentMode = .scaleAspectFit
        btnGplus.imageView?.contentMode = .scaleAspectFit

        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self

        
        
        dictChild = ["Children_Name":"" , "Gender" : "" , "Date_of_Birth" : "","Age" : ""]
        arrChild.append(dictChild)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
//        if strComeFrom == "account"
//        {
//            Manager.sharedInstance.addTabBar2(self, tab: "2")
//            conviewbottom.constant = 60
//        }
                
        if strComeFrom == "class"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "1")
            conviewbottom.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? -80 : -60
        }
        else if strComeFrom == "account"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "2")
            conviewbottom.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? -80 : -60
        }
        else if strComeFrom == "cart"
        {
            Manager.sharedInstance.addTabBar2(self, tab: "4")
            conviewbottom.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? -80 : -60
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if btnFacebook.isSelected || btnGplus.isSelected
        {
            Manager.sharedInstance.removeTabbar()
            btnFacebook.isSelected = false
            btnGplus.isSelected = false
        }
        
        
    }
    
    
    //MARK:- Tableview methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrChild.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "childCell"
        let cell:childCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! childCell
        
        let dict = NSMutableDictionary(dictionary: arrChild[indexPath.row] as Dictionary)
        
        cell.txtName.text = dict["Children_Name"] as? String
        cell.txtDob.text = dict["Date_of_Birth"] as? String
        if dict["Age"] as? String != ""
        {
            cell.txtAge.text = (dict["Age"] as? String)!
        }

        cell.txtName.tag = 0
        
        cell.txtDob.addTarget(self, action: #selector(textfieldBeginEdit(_:)), for: .editingDidBegin)
        
        cell.txtName.addTarget(self, action: #selector(textfielddidend(_:)), for: .editingDidEnd)

        cell.txtDob.addTarget(self, action: #selector(textfielddidend(_:)), for: .editingDidEnd)
        
//        cell.txtGender.addTarget(self, action: #selector(textfieldBeginEdit(_:)), for: .editingDidBegin)

//        cell.txtGender.addTarget(self, action: #selector(textfielddidend(_:)), for: .editingDidEnd)
        
        
        cell.btnBoy.addTarget(self, action: #selector(btnGenderClick(_:)), for: .touchUpInside)
        
        cell.btnGirl.addTarget(self, action: #selector(btnGenderClick(_:)), for: .touchUpInside)

        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            conTblChildHeight.constant = CGFloat(arrChild.count * 320)

        }
        else
        {
            conTblChildHeight.constant = CGFloat(arrChild.count * 230)

        }
        tblChild.updateConstraintsIfNeeded()
        tblChild.layoutIfNeeded()
        
        return cell
    }
    

    
    @objc func btnGenderClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        
        let position:CGPoint = btn.convert(CGPoint.zero, to:self.tblChild)
        let indexPath : IndexPath = self.tblChild.indexPathForRow(at: position)!
        
        let cell = self.tblChild.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! childCell
        
        var dict = arrChild[indexPath.row]
        if btn.tag == 1
        {
            dict["Gender"] = "Male"
            cell.btnBoy.isSelected = true
            cell.btnGirl.isSelected = false
        }
        else
        {
            dict["Gender"] = "Female"
            cell.btnBoy.isSelected = false
            cell.btnGirl.isSelected = true

        }
        
        arrChild[indexPath.row] = dict
        tblChild.reloadData()

    }
    
    
    @objc func textfieldBeginEdit(_ sender: Any)
    {
        let txt = sender as! UITextField
        if txt.tag == 1
        {
            //gender
            
            let pickerView = UIPickerView()
            pickerView.delegate = self
            pickerView.dataSource = self
            txt.inputView = pickerView

        }
        else
        {
            //dob
            let datePickerView:UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = .date
            datePickerView.maximumDate = Date()
            txt.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
            
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        strGender = pickOption[row]
    }
    @objc func textfielddidend(_ sender: Any)
    {
        let txt = sender as! UITextField
        let position:CGPoint = txt.convert(CGPoint.zero, to:self.tblChild)
        let indexPath : IndexPath = self.tblChild.indexPathForRow(at: position)!
        
        let cell = self.tblChild.cellForRow(at: IndexPath(row: indexPath.row, section: 0)) as! childCell
        
        var dict = arrChild[indexPath.row]
        if txt.tag == 0
        {
            dict["Children_Name"] = cell.txtName.text
        }
//        else if txt.tag == 1
//        {
//            dict["Gender"] = strGender
//        }
        else if txt.tag == 2
        {
            dict["Date_of_Birth"] = strDob
            dict["Age"] = strAge

        }
        
        arrChild[indexPath.row] = dict
        tblChild.reloadData()

    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        strDob = dateFormatter.string(from: sender.date)
        
        let now = Date()
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: sender.date, to: now)
        let age = ageComponents.year!
        strAge = String(format: "%d year", age)

        if age == 0
        {
            let ageComponents = calendar.dateComponents([.month], from: sender.date, to: now)
            let age = ageComponents.month!
            strAge = String(format: "%d month", age)
        }
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return 320
        }
        return 230
    }
    
    //MARK:- Button methods
    
    @IBAction func btnPLusClick(_ sender: Any)
    {
        let array1 = NSMutableArray(array: arrChild)
        
        if array1.contains(dictChild)
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseEnterdata, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if !array1.contains(dictChild)
        {
            for i in 0..<array1.count
            {
                var dict = array1[i] as! [String : Any]
                if dict["Children_Name"] as? String == ""
                {
                    let alert = UIAlertController(title:kWarningEn, message:kPleaseenterChildname, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                    alert.show()
                    return
                }
                else if dict["Gender"] as? String == ""
                {
                    let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectgender, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                    alert.show()
                    return
                }
                else if dict["Date_of_Birth"] as? String == ""
                {
                    let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectdob, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                    alert.show()
                    return
                }
                
            }
            
            arrChild.append(dictChild)
            tblChild.reloadData()

            
        }
        else
        {
            arrChild.append(dictChild)
            tblChild.reloadData()
        }
        

    }
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnRegisterClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if isSocial
        {
            self.wscallforRegister()
            return
        }

        if (txtName.text=="")
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourNameEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if (txtLastname.text=="")
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterlastNameEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
        else if(txtMobile.text=="")
        {

            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourmobileEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if((txtMobile.text?.count)! < 8)
        {

            let alert = UIAlertController(title:kWarningEn, message:kPleaseentervalidMobnum, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()

        }
        else if (txtEmail.text == "")
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryouremailaddressEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
            
        else if(!(isValidEmail(testStr: txtEmail.text!)))
        {
            let alert = UIAlertController(title:kWarningEn, message:kEmailValidationEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
            
        else if(txtPassword.text=="")
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourpasswordEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if !isPwdLenth(txtPassword.text!)
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPasswordValidEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            
            alert.show()
            
        }
        else if(txtConfirmPW.text=="")
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourConfirmpasswordEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if(txtConfirmPW.text != txtPassword.text)
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kpasswordmismatchEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if arrChild.count == 0 || (arrChild.count == 1 && arrChild.contains(dictChild))
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenterChildInfo, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else
        {
            for i in 0..<arrChild.count
            {
                var dict = arrChild[i]
                if dict == dictChild
                {
                    arrChild.remove(at: i)
                }
                else
                {
                    if dict["Children_Name"] == ""
                    {
                        let alert = UIAlertController(title:kWarningEn, message:kPleaseenterChildname, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                        alert.show()
                        return
                    }
                    else if dict["Gender"] == ""
                    {
                        let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectgender, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                        alert.show()
                        return
                    }
                    else if dict["Date_of_Birth"] == ""
                    {
                        let alert = UIAlertController(title:kWarningEn, message:kPleaseSelectdob, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                        alert.show()
                        return
                    }
                    
                    dict.removeValue(forKey: "Age")
                    arrChild[i] = dict
                }
            }
            
            self.login_type = 1
            self.Gplus_id = 0
            self.Fb_id = 0
            self.strName = self.txtName.text!
            self.strEmail = self.txtEmail.text!
            self.strPw = self.txtConfirmPW.text!
            self.strMobile = self.txtMobile.text!
            self.strLastName = self.txtLastname.text!

            self.wscallforRegister()


        }
        
    }
    
    @IBAction func btnLoginClick(_ sender: Any)
    {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGPlusClick(_ sender: Any)
    {
        self.view.endEditing(true)
//        let btn = sender as! UIButton
//        btn.isSelected = !btn.isSelected
        btnGplus.isSelected = true

        UserDefaults.standard.set(true, forKey: "google")
        UserDefaults.standard.set(false, forKey: "fb")
        UserDefaults.standard.synchronize()
        GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK: -Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobile
        {
            if string.isEmpty
            {
                return true
            }
            if validatePhone(string)
            {
                var strMaxLength = ""
                strMaxLength = "9"
                let newStr = textField.text as NSString?
                let currentString: String = newStr!.replacingCharacters(in: range, with: string)
                let j = Int(strMaxLength) ?? 0
                let length: Int = currentString.count
                if length >= j {
                    return false
                }
                else {
                    return true
                }
            }
            return false
        }
        
//        if textField == txtName
//        {
//            img1.isHidden = false
//
//        }
//        else if textField == txtLastname
//        {
//            img6.isHidden = false
//        }
//        else if textField == txtMobile
//        {
//            img5.isHidden = false
//        }
//        else if textField == txtEmail
//        {
//            img2.isHidden = false
//        }
//        else if textField == txtPassword
//        {
//            img3.isHidden = false
//
//        }
//        else if textField == txtConfirmPW
//        {
//            img4.isHidden = false
//        }
//
        
        return true
    }
    
    func validatePhone(_ phoneNumber: String) -> Bool {
        let phoneRegex = "[0-9]"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: phoneNumber) {
            return false
        }
        else {
            return true
        }
    }
    
    
    // Password length validation
    func isPwdLenth(_ password: String) -> Bool {
        if (!(password.count < 5 || password.count > 20)){
            return true
        }
        else
        {
            return false
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK:- Google sign in
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,withError error: Error!)
    {
        
        print("sign In")
        if let error = error
        {
            print("\(error.localizedDescription)")
            let alert = UIAlertController(title:kWarningEn, message:"Requested time out", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else
        {
            // Perform any operations on signed in user here.
            let userId1 = (user.userID)
            // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            
            login_type = 3
            Fb_id = 0
            Gplus_id = (userId1! as NSString).integerValue
            strName = user.profile.name
            strEmail = user.profile.email
            strPw = "###"
            strMobile = ""
            strLastName = ""
            
            if user.profile.hasImage
            {
                let dimension = round(150 * UIScreen.main.scale);
                let pic = user.profile.imageURL(withDimension: UInt(dimension)) as NSURL
                self.UserImage = pic.path!
                //                print(self.UserImage)
            }
            else
            {
                self.UserImage = ""
            }
            
            isSocial = true
            conviewNameHeight.constant = 0
            conviewSocialHeight.constant = 0
            lblSuccess.text = kSuccessSocial + " Google. " + kTapToRegister
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,withError error: Error!)
    {
        
    }
    func sign(_ signIn: GIDSignIn!,present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,dismiss viewController: UIViewController!)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnFacebookClick(_ sender: Any)
    {
        self.view.endEditing(true)
//        let btn = sender as! UIButton
//        btn.isSelected = !btn.isSelected

        btnFacebook.isSelected = true

        UserDefaults.standard.set(false, forKey: "google")
        UserDefaults.standard.set(true, forKey: "fb")
        UserDefaults.standard.synchronize()
        
        if ((FBSDKAccessToken.current()) != nil) {
            // User is logged in, do work such as go to next view controller.
        }
        
        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.native
        let params = ["fields": "location, name, email, about, picture.type(large)"]
        
        //        Helper.sharedInstance.ShowProgressBarWithText("Loading..")
        
        login.logIn(withReadPermissions: ["email"], from: self, handler: {(_ result: FBSDKLoginManagerLoginResult?, _ error: Error?) -> Void in
            if error != nil
            {
                print("Process error")
                let alert = UIAlertController(title:kWarningEn, message:"Requested time out", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                alert.show()
            }
            else if result?.isCancelled == true
            {
                print("Cancelled")
                login.logOut()
            }
            else
            {
                self.isSocial = true

                print("Logged in")
                
                if (FBSDKAccessToken.current() != nil)
                {
                    //                    Helper.sharedInstance.DismissProgressBar()
                    FBSDKGraphRequest(graphPath: "/me", parameters: params).start(completionHandler: {(_ connection: FBSDKGraphRequestConnection?, _ result: Any?, _ error: Error?) -> Void in
                        
                        if error == nil
                        {
                            guard let dict = result as? Dictionary<AnyHashable, Any>else
                            {
                                SwiftLoggor.fileContent = SwiftLoggor.log(message:result.debugDescription,event:ErroTypes.e)
                                return
                            }

                            let id = dict["id"] as! String
                            self.login_type = 2
                            self.Gplus_id = 0
                            self.Fb_id = Int(id)!
                            self.strName = (dict["name"] as? String ?? "")!
                            self.strEmail = (dict["email"] as? String ?? "")!
                            self.strPw = "###"
                            self.strMobile = ""
                            self.strLastName = ""

                            
                            
                            let facebookProfileUrl = "http://graph.facebook.com/\(self.Fb_id)/picture?type=large"
                            self.UserImage = facebookProfileUrl
                            self.conviewNameHeight.constant = 0
                            self.conviewSocialHeight.constant = 0
                            self.lblSuccess.text = kSuccessSocial + " facebook. " + kTapToRegister
                            login.logOut()
                            
                        }
                        else
                        {
                            print(error.debugDescription)
                            let alert = UIAlertController(title:kWarningEn, message:"Cant connect with facebook", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
                            alert.show()
                        }
                    })
                }
                else
                {
                    print(error.debugDescription)
                }
                
            }
        })
    }
    //MARK:- textfield delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
//        if textField == txtName
//        {
//            img1.isHidden = false
//        }
//        else if textField == txtLastname
//        {
//            img6.isHidden = false
//        }
//        else if textField == txtMobile
//        {
//            img5.isHidden = false
//        }
//        else if textField == txtEmail
//        {
//            img2.isHidden = false
//        }
//        else if textField == txtPassword
//        {
//            img3.isHidden = false
//
//        }
//        else if textField == txtConfirmPW
//        {
//            img4.isHidden = false
//        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
//        if textField == txtName
//        {
//            img1.isHidden = true
//        }
//        else if textField == txtLastname
//        {
//            img6.isHidden = true
//        }
//        else if textField == txtMobile
//        {
//            img5.isHidden = true
//        }
//        else if textField == txtEmail
//        {
//            img2.isHidden = true
//        }
//        else if textField == txtPassword
//        {
//            img3.isHidden = true
//        }
//        else if textField == txtConfirmPW
//        {
//            img4.isHidden = true
//        }
    }
    
    
    
    
    
    //MARK:- Webservice
    
    func wscallforRegister()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRegister)

        let dictPara:NSDictionary = ["FirstName":strName,"LastName":strLastName,"Password":strPw,"Email_Id":strEmail,"Mobile":strMobile,"Address":"","Login_Type":login_type,"Facebook_Id":Fb_id,"Google_Id":Gplus_id,"Children":arrChild]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

            print(result)
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let userId = result["App_User_Id"] as? NSNumber
                        UserDefaults.standard.set(userId, forKey: "userid")
                        UserDefaults.standard.set(true, forKey: GlobalConstants.kisLogin)
                        UserDefaults.standard.synchronize()
                        
                        Manager.sharedInstance.wscallforUserDetail()
                        
    
                        if self.strComeFrom == "account"
                        {
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
                            
                            let navigationController = UINavigationController()
                            navigationController.isNavigationBarHidden = true
                            navigationController.setViewControllers([home], animated: false)
                            let window = UIApplication.shared.delegate!.window!!
                            window.rootViewController = navigationController
    
                        }
                        else if self.strComeFrom == "cart"
                        {
                            var storyboard1 : UIStoryboard!

                            if UserDefaults.standard.bool(forKey: "arabic")
                            {
                                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
                            }
                            else
                            {
                                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
                            }
                            
                            let home = storyboard1.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                            let navigationController = UINavigationController()
                            navigationController.isNavigationBarHidden = true
                            
                            navigationController.setViewControllers([home], animated: false)
                            let window = UIApplication.shared.delegate!.window!!
                            window.rootViewController = navigationController
                        }
                        else
                        {
                        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        
                        let navigationController = UINavigationController()
                        navigationController.isNavigationBarHidden = true
                        navigationController.setViewControllers([home], animated: false)
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = navigationController
                        
                        }
                        
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
            }

        }

    }
    
}
