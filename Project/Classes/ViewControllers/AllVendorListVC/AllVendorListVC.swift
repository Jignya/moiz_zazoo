//
//  AllVendorListVC.swift
//  Project
//
//  Created by HTNaresh on 25/6/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class AllVendorListVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate
{
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var collectionList: UICollectionView!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNorecord: UILabel!
    
    var arrList : NSMutableArray = []
    var strTitle : String!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblNorecord.text = kNorecord
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        self.lblTitle.text = self.strTitle.capitalized
        wscallforVendor()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "0")
    }
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellReuseIdentifier = "cell2"
        let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
        
        cell.imgLogo.layer.cornerRadius = 3.0
        cell.imgLogo.clipsToBounds = true
        
        let dict = arrList[indexPath.row] as! NSDictionary
        
        var Stringimg = String()
        Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Vendor_Image_Url"] as? String ?? "")
    
        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    
        let url = URL(string: urlString!)
        cell.imgLogo.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
        cell.lblTitle.text = dict["Vendor_Name"] as? String
        
        cell.viewShadow.backgroundColor = UIColor.clear
        
        cell.viewShadow.bounds = cell.bounds
        
        cell.setGradientBackground(view: cell.viewShadow, colorTop: UIColor.clear, colorBottom: UIColor.black.withAlphaComponent(0.65))
    
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrList[indexPath.row] as! Dictionary)
    
        let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "VendorDetailVC") as! VendorDetailVC
        classdetail.VendorId = dict["Vendor_Id"] as? NSNumber
        self.navigationController?.pushViewController(classdetail, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
//        let height = (3 * self.collectionList.frame.size.width) / 4

        let height = ((self.collectionList.frame.size.width / 2) + 35)

        let cellSize = CGSize(width:(self.collectionList.frame.size.width), height:height)
        return cellSize
    }
    
    
    
    //MARK:- buttom methos
    
    @IBAction func btnbackClick(_ sender: Any)
    {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
    }
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    
    //MARK:- webservice
    func wscallforVendor()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wGetVendors)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    if strStatus == "Success"
                    {
                        self.arrList = NSMutableArray(array: (result["Vendors"] as? Array ?? []))
                        
                        if self.arrList.count > 0
                        {
                            self.collectionList.isHidden = false
                            self.lblNorecord.isHidden = true
                            self.collectionList.reloadData()
                        }
                        else
                        {
                            self.collectionList.isHidden = true
                            self.lblNorecord.isHidden = false
                        }
                        
                        
                    }
            }
        }
        
    }
    
}

public extension UIView {
    func setGradientBackground(view:UIView, colorTop:UIColor, colorBottom:UIColor) {
        for layer in view.layer.sublayers ?? [] {
            if layer.name == "gradientLayer" {
                layer.removeFromSuperlayer()
            }
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradientLayer.locations = [0.8, 1.0]
        gradientLayer.frame = view.bounds
        gradientLayer.name = "gradientLayer"
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    
    func fadeIn()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
    
    func fadeOut()
    {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.7
        }, completion: nil)
    }
}
