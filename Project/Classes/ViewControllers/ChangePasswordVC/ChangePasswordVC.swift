//
//  ChangePasswordVC.swift
//  Zazoo
//
//  Created by HTNaresh on 24/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
//    @IBOutlet weak var txtoldPw: UITextField!
    @IBOutlet weak var txtNewPw: UITextField!
    @IBOutlet weak var txtConfirmPw: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnSubmit.setTitle(kSubmit, for: .normal)
        lblTitle.text = kChangePW
        
        txtNewPw.placeholder = kNewPw
        txtConfirmPw.placeholder = kConfirmPw

        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "2")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnSubmitClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
//        if txtoldPw.text == ""
//        {
//            let alert = UIAlertController(title:kWarningEn, message:"Please enter old password", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
//            alert.show()
//
//        }
        if txtNewPw.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kenterNewPw, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if txtConfirmPw.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kenterConfirmPw, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if(txtConfirmPw.text != txtNewPw.text)
        {
            let alert = UIAlertController(title:kWarningEn, message:kpasswordmismatchEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
        else
        {
            self.wscallforChangePW()
        }
        
    }
    
    //MARK:- webservice call
    func wscallforChangePW()
    {
        let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)

        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wChangePassword)
        
        let dictPara:NSDictionary = ["App_User_Id":userId,"Password":self.txtConfirmPw.text!]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                    self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    
            }
        }
        
    }
    
}
