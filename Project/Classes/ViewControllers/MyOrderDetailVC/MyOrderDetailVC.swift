//
//  MyOrderDetailVC.swift
//  Zazoo
//
//  Created by HTNaresh on 14/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class MyOrderDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblItemDetails: UILabel!
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var viewimgItem: UIView!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var btnQty: ButtonExtender!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    @IBOutlet weak var lblDeliveryData: UILabel!
    
    @IBOutlet weak var tblItem: UITableView!
    @IBOutlet weak var conTblHeight: NSLayoutConstraint!

    @IBOutlet weak var lblOrderSummary: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lbldateData: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTimeData: UILabel!
    @IBOutlet weak var lblPaidBy: UILabel!
    @IBOutlet weak var lblPaidByData: UILabel!
    @IBOutlet weak var lblTotalAmt: UILabel!
    @IBOutlet weak var lblTotalAmtData: UILabel!
    @IBOutlet weak var lblOrderNum: UILabel!
    @IBOutlet weak var lblOrderNumData: UILabel!
    @IBOutlet weak var lblTransNum: UILabel!
    @IBOutlet weak var lblTransNumData: UILabel!
    
    @IBOutlet weak var lblDelcharges: UILabel!
    @IBOutlet weak var lblDelchargesData: UILabel!
    
    @IBOutlet weak var btnRefund: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var conbtnConfirmHeight: NSLayoutConstraint!


    
    var OrderId : NSNumber!
    var dictdata : NSMutableDictionary!
    var arrItem : NSMutableArray = []

    override func viewDidLoad()
    {
        super.viewDidLoad()
        viewimgItem.layer.borderWidth = 1.0
        viewimgItem.layer.borderColor = UIColor.lightGray.cgColor
        viewimgItem.layer.cornerRadius = 1.0
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        setlabel()
        wscallforOrderDetail()
        
        btnRefund.isSelected = false
        conbtnConfirmHeight.constant = 0
        
        btnRefund.titleLabel?.font = UIFont(name:GlobalConstants.kEnglishRegular, size: ((UIDevice.current.userInterfaceIdiom == .pad) ? 20 : 15))
        btnConfirm.titleLabel?.font = UIFont(name:GlobalConstants.kEnglishRegular, size: ((UIDevice.current.userInterfaceIdiom == .pad) ? 20 : 15))
        
        btnRefund.isHidden = true


    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "2")
    }
    
    func setlabel()
    {
       lblTitle.text = kOrder
        lblOrderSummary.text = kOrderSummary
        lbldate.text = kDate
        lblTime.text = kTime
        lblOrderNum.text = kOrderNumber
        lblPaidBy.text = kPaidBy
//        lblDelcharges.text = kDelCharges
        lblTotalAmt.text = kTotalAmount
        lblItemDetails.text = kItemDetail
        lblDelcharges.text = kOrderStatus
        
        btnConfirm.setTitle(kConfirm, for: .normal)
        btnRefund.setTitle(kRefund, for: .normal)
        btnRefund.setTitle(kCancel, for: .selected)
        
        btnRefund.setTitleColor(GlobalConstants.themeClr, for: .normal)
        btnRefund.setTitleColor(UIColor.white, for: .selected)
        
        btnRefund.backgroundColor = UIColor.white


    }
    
    func displayData(dict:NSMutableDictionary)
    {
        let arrayType = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "type")

        arrItem = NSMutableArray(array: dict["OrderLines"] as! Array)
        var isRefundAvailable : Bool = false
        for i in 0..<arrItem.count
        {
            var dict1 = arrItem[i] as! [String:Any]
            if dict1["Is_Refund"] as? NSNumber == 0 && dict1["OrderDetail_Item_Type"] as? NSNumber != 3
            {
                let arrFiltered = Manager.sharedInstance.arrFilterDataforInt(dict1["Item_Type_Id"] as? Int, withPredicateKey: "Item_Type_Id", fromArray: arrayType)
                
                if arrFiltered.count >  0
                {
                    let dictType = arrFiltered[0] as! [String:Any]
                    let hours = dictType["Refund_Before_Hours"] as! Int
                    //                dict["refundHours"] = dictType["Refund_Before_Hours"]
                    
                    let strDate = dict1["Start_Date"] as! String
                    let dateF = DateFormatter()
                    dateF.dateFormat = "dd/MM/yyyy HH:mm a"
                    let strToday = dateF.string(from: Date())
                    let Today = dateF.date(from: strToday)
                    
                    let orderDate = dateF.date(from: strDate)?.addingTimeInterval(TimeInterval(-1*hours*60*60))
                    
                    if (Today!.compare(orderDate!) == .orderedAscending) //|| (Today!.compare(orderDate!) == .orderedSame)
                    {
                        dict1["isRefundAvailable"] = 1
                        isRefundAvailable = true
                    }
                    else
                    {
                        dict1["isRefundAvailable"] = 0
                    }
                    
                }
            }
            else
            {
                dict1["isRefundAvailable"] = 0
            }
            
            dict1["isSelect"] = "0"
            arrItem.replaceObject(at: i, with: dict1)
        }
        
        print(arrItem)
        
        tblItem.reloadData()
        
        if dict["Payment_Type"] as? String == "COD" || dict["Payment_Type"] as? String == "Cash"
        {
           isRefundAvailable = false
        }
        
        
        if isRefundAvailable
        {
            btnRefund.isHidden = false
        }
        else
        {
            btnRefund.isHidden = true
        }
        

        
        if let n = dict.value(forKey: "DeliveryCharges") as? NSNumber
        {
            let f = n.floatValue
            self.lblDelchargesData.text! = String(format: "%.3f %@", f , kCurrency)
        }
        else
        {
            self.lblDelchargesData.text! = String(format: "%.3f %@", (dict["PayAmount"] as? Float ?? 0.0)! , kCurrency)
        }
        
        self.lblOrderNumData.text = String(format: "%@", dict["OrderId"]  as! NSNumber)
        let strDate = dict["OrderDate"] as? String ?? ""
        let ar1 = strDate.components(separatedBy: " ")
        if ar1.count > 0
        {
            self.lbldateData.text = ar1[0]
            self.lblTimeData.text = ar1[1]
        }
        self.lblPaidByData.text = dict["Payment_Type"] as? String
        self.lblTotalAmtData.text = String(format: "%.3f %@", (dict["OrderAmount"] as? NSNumber ?? 0).floatValue,kCurrency)
        
        self.lblDelchargesData.text! = String(format: "%@", dict["Order_Status"] as? String ?? "")

        
//        if (dict["Amount"] as? NSNumber)! == (dict["Rate"] as? NSNumber)
//        {
//            self.lblPrice.text = ""
//            self.lblDiscountedPrice.text = String(format: "%@ %@",kCurrency, dict["Special_Price"] as! NSNumber)
//            self.lblDiscountedPrice.isHidden = true
//
//        }
//        else
//        {
//            self.lblDiscountedPrice.isHidden = false
//            let strike = String(format: "%@ %@",kCurrency, dict["Primary_Price"] as! NSNumber)
//            self.lblDiscountedPrice.attributedText = Manager.sharedInstance.StrikeThroughString(str: strike)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Table method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let dict = NSMutableDictionary(dictionary: arrItem[indexPath.row] as! Dictionary)
        
        if dict["Item_Type_Id"] as? NSNumber == 1  // inventory item
        {
            let cellReuseIdentifier = "CartCell1"
            let cell:CartCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CartCell
            
            let dict = NSMutableDictionary(dictionary: arrItem[indexPath.row] as! Dictionary)
            
            cell.lblItemName.text = dict["Prod_Name"] as? String
            cell.lblvendor.text = dict["Vendor_Name"] as? String
            cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, (dict["Amount"] as? NSNumber ?? 0)!,kCurrency)
            cell.lblQty.text = String(format: "%@ - %@",kQty, (dict["Qty"] as? NSNumber ?? 0)!)
            
            cell.lblDiscountPrice.text = ""
            cell.lblDiscount.text = ""
            
            if dict["Default_Image"] as? String != ""
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Default_Image"] as? String ?? "")
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                cell.imgItem.setImageWith(url, usingActivityIndicatorStyle: .gray)
            }
            
            conTblHeight.constant = tblItem.contentSize.height
            tblItem.updateConstraints()
            tblItem.layoutIfNeeded()

            
            return cell
        }
        else if  dict["Item_Type_Id"] as? NSNumber == 5
        {
            let cellReuseIdentifier = "CartCell"
            let cell:CartCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CartCell
            let arrAddons = dict["AddOns"] as? [[String:Any]]
            var  addon_price : Float = 0.00
            for i in 0..<arrAddons!.count
            {
                let dict = arrAddons![i]
                if addon_price == 0.00
                {
                    addon_price  =  (dict["Amount"]  as? NSNumber)?.floatValue ?? 0
                }
                else
                {
                    addon_price = addon_price + ((dict["Amount"]  as? NSNumber ?? 0)?.floatValue)!
                }
                
            }
            
            cell.lblTime.text = ""
            cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, ((dict["Amount"] as? NSNumber ?? 0)!.floatValue + addon_price),kCurrency)
            
            cell.lblItemName.text = dict["Prod_Name"] as? String
            cell.lblActivity.text = dict["Vendor_Name"] as? String
            
            let dateArray = NSMutableArray(array: dict["SelectedDates"] as! Array)
            
            cell.lblDates.text = Manager.sharedInstance.getCommaSeparatedValuefromArray(dateArray, forkey: "Calender_Date")
            
            if dict["Default_Image"] as? String != ""
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Default_Image"] as? String ?? "")
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                cell.imgItem.setImageWith(url, usingActivityIndicatorStyle: .gray)
            }
            if dict["isSelect"] as? String == "0"
            {
                cell.btnCheckbox.isSelected = false
            }
            else
            {
                cell.btnCheckbox.isSelected = true
            }
            
            if btnRefund.isSelected && dict["Is_Refund"] as? NSNumber == 0 && dict["isRefundAvailable"] as? Int == 1
            {
                cell.conViewCheckWidth.constant = 50
            }
            else
            {
                cell.conViewCheckWidth.constant = 0
            }
            
            cell.btnCheckbox.tag = indexPath.row
            cell.btnCheckbox.addTarget(self, action: #selector(btnCheckBoxClick(_:)), for: .touchUpInside)
            
            conTblHeight.constant = tblItem.contentSize.height
            tblItem.updateConstraints()
            tblItem.layoutIfNeeded()

            
            return cell
        }
        else
        {
            let cellReuseIdentifier = "CartCell"
            let cell:CartCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CartCell
            
            cell.lblTime.text = ""
            cell.lblActivity.text = dict["Vendor_Name"] as? String
            cell.lblItemName.text = dict["Prod_Name"] as? String
//            cell.lblTime.text = String(format: "%@ - %@", (dict["Start_Time"] as? String)!,(dict["End_Time"] as? String)!)
//            cell.lblActivity.text = dict["Type_of_Activity"] as? String
            cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, (dict["Amount"] as? NSNumber ?? 0)!.floatValue,kCurrency)

            let dateArray = NSMutableArray(array: dict["SelectedDates"] as! Array)
            cell.lblDates.text = Manager.sharedInstance.getCommaSeparatedValuefromArray(dateArray, forkey: "Calender_Date")
            
            if dict["Default_Image"] as? String != ""
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Default_Image"] as? String ?? "")
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                cell.imgItem.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
            }
            
            if dict["isSelect"] as? String == "0"
            {
                cell.btnCheckbox.isSelected = false
            }
            else
            {
                cell.btnCheckbox.isSelected = true
            }
            
            if dict["OrderDetail_Item_Type"] as? NSNumber == 3
            {
                cell.conViewCheckWidth.constant = 0
            }
            else
            {
                if btnRefund.isSelected && dict["Is_Refund"] as? NSNumber == 0
                {
                    cell.conViewCheckWidth.constant = 50
                }
                else
                {
                    cell.conViewCheckWidth.constant = 0
                }
            }
            
            
            
            cell.btnCheckbox.tag = indexPath.row
            cell.btnCheckbox.addTarget(self, action: #selector(btnCheckBoxClick(_:)), for: .touchUpInside)

            
            conTblHeight.constant = tblItem.contentSize.height
            tblItem.updateConstraints()
            tblItem.layoutIfNeeded()
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let dict = NSMutableDictionary(dictionary: arrItem[indexPath.row] as! Dictionary)
        
//        if dict["Qty"] is NSNumber
//        {
//            return 95
//        }
        
        let dateArray = NSMutableArray(array: dict["SelectedDates"] as! Array)
        
        var myString = ""
        
        if dateArray.count > 0
        {
            let strdays = Manager.sharedInstance.getCommaSeparatedValuefromArray(dateArray, forkey: "Calender_Date")
            myString = String(format: "%@\n%@", dict["Class_Name"] as? String ?? "" , strdays)
        }
        else
        {
            myString =  dict["Class_Name"] as? String ?? ""
        }
        
       
        
        let attributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont.systemFont(ofSize: 15.0)]
        
        let attributedString: NSAttributedString = NSAttributedString(string:myString,attributes:attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: (self.tblItem.frame.size.width - 20), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requiredSize:CGRect = rect
        return requiredSize.height + ((UIDevice.current.userInterfaceIdiom == .pad) ? 120 : 90)
        
        
    }
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @objc func btnCheckBoxClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        var dict = arrItem[btn.tag] as! [String:Any]
        if btn.isSelected
        {
            dict["isSelect"] = "1"
        }
        else
        {
            dict["isSelect"] = "0"
        }
         arrItem.replaceObject(at: btn.tag, with: dict)
        
    }


    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnRefundClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        if btn.isSelected
        {
            conbtnConfirmHeight.constant = 40
            btnRefund.backgroundColor = GlobalConstants.themeClr
        }
        else
        {
            btnRefund.backgroundColor = UIColor.white
            conbtnConfirmHeight.constant = 0
            for i in 0..<arrItem.count
            {
                var dict = arrItem[i] as! [String:Any]
                dict["isSelect"] = "0"
                arrItem.replaceObject(at: i, with: dict)
            }
            tblItem.reloadData()
        }
        
        tblItem.reloadData()
    }
    @IBAction func btnConfirmClick(_ sender: Any)
    {
        let arrIds = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: arrItem)
        
        if arrIds.count == 0
        {
            let alertController = UIAlertController(title:kWarningEn, message:kRefundAlert, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            alertController.addAction(okAction)
            alertController.show()
            return
        }
        
        let alertController = UIAlertController(title:"", message:kConfirmAlert, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title:kYes, style: .default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            self.wscallforRefundReq()
        }
        
        let cancel = UIAlertAction(title:kNo, style: .default) {
            UIAlertAction in
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancel)
        alertController.show()
    }
    
    func wscallforOrderDetail()
    {
        var strUrl : String = ""
        
        
        strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetOrderDetail,OrderId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.displayData(dict: NSMutableDictionary(dictionary: result as! Dictionary))
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
            }
        }
    }
    
    func wscallforRefundReq()
    {
        let arrIds = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: arrItem)
        
        var orderDetailId = [NSNumber]()
        if arrIds.count == 0
        {
            return
        }
        else
        {
            for dict in arrIds
            {
                let dict1 = dict as! [String:Any]
                let id = dict1["Order_Detail_Id"] as? NSNumber
                orderDetailId.append(id!)
            }
        }
        
        var strUrl : String = ""
        
        strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRefundRequest)
        
        let dictPara = NSMutableDictionary()
        
        dictPara.setObject(OrderId, forKey: "OrderId" as NSCopying)
        dictPara.setObject(orderDetailId, forKey: "OrderDetailIds" as NSCopying)
    
        
     WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let alertController = UIAlertController(title:"", message:krefundSuccess, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                            
                            self.btnRefundClick(self.btnRefund)
                            self.wscallforOrderDetail()
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
            }
        }
    }
    

}
