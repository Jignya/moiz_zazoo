//
//  HistoryDetailCell.swift
//  Zazoo
//
//  Created by HTNaresh on 1/10/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class HistoryDetailCell: UITableViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var viewimgItem: UIView!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var btnQty: ButtonExtender!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    @IBOutlet weak var lblDeliveryData: UILabel!


    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        lblQty.text = kQty
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
