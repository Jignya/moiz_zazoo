//
//  ForgotPWVC.swift
//  Zazoo
//
//  Created by HTNaresh on 29/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class ForgotPWVC: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)

        btnSubmit.setTitle(kSubmit, for: .normal)
        lblTitle.text = kForgotTitle
        txtEmail.placeholder = kEmail
        lblDesc.text = kForgotDesc
        
        btnLogin.setTitle(kLogin.uppercased(), for: .normal)
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        

    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func btnSubmitClick(_ sender: Any)
    {
        self.view.endEditing(true)
        if (txtEmail.text == "")
        {
            let alert = UIAlertController(title:kWarningEn, message:kfPWalert, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
        else if(!(isValidEmail(testStr: txtEmail.text!)))
        {
            let alert = UIAlertController(title:kWarningEn, message:kEmailValidationEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
        else
        {
            self.wscallforForgotPW()
        }
        
    }
    
    //MARK:- webservice call
    func wscallforForgotPW()
    {
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wForgotPassword)
        
        
        let dictPara:NSDictionary = ["Email_Id":self.txtEmail.text!]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:"New password has been sent to your registered email address", preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                            
                         self.navigationController?.popViewController(animated: true)

                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()

                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    
            }
        }
        
    }
    
    
}
