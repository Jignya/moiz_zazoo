//
//  ThankYouScreenVC.swift
//  Zazoo
//
//  Created by HTNaresh on 8/10/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import UserNotifications


class ThankYouScreenVC: UIViewController,AVAudioPlayerDelegate
{

    @IBOutlet weak var btnDone: ButtonExtender!
    @IBOutlet weak var lblViewPaymentHistory: UILabel!
    @IBOutlet weak var lblTotalPaid: UILabel!
    @IBOutlet weak var lblPaidBy: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblForpayment: UILabel!
    @IBOutlet weak var lblThank: UILabel!
    @IBOutlet weak var lbldesc: UILabel!

    @IBOutlet weak var viewHeaderData: UIView!

    var orderId : NSNumber!
    var strPayMode : String!
    var strAmount : String!
    var strComeFrom : String!
    var audioPlayer = AVAudioPlayer()

    let notificationCenter = UNUserNotificationCenter.current()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        viewHeaderData.layer.borderColor = UIColor(red: 230.0 / 255.0, green: 97.0 / 255.0, blue: 88.0 / 255.0, alpha: 1).cgColor
        viewHeaderData.layer.borderWidth  = 1.0
        viewHeaderData.layer.cornerRadius = 5.0

        
        lblPaidBy.text = String(format: "%@ %@", kPaidBy
            , strPayMode)
        lblPrice.text = strAmount
        
        btnDone.setTitle(kHome, for: .normal)
        lblViewPaymentHistory.text = kViewHistory
        lblTotalPaid.text = kTotalPaid
        lblForpayment.text = kForpayment
        lblThank.text = kThanku
        
//        if strComeFrom == "gym"
//        {
//            let app = UIApplication.shared.delegate as! AppDelegate
//            let strname : String = app.strVendorname
//
//            lbldesc.text = strname + " " + kGymConfirmation
//        }
//        else
//        {
//            lbldesc.text = kReceiveOrder
//        }
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        
        if self.strPayMode == "COD"
        {
            self.lblForpayment.text = kfororder
            Manager.sharedInstance.wscallforRemoveCart()
        }
        else
        {
            self.lblForpayment.text = kForpayment
        }
        
        playSound()
       
    }
    
    func playSound()
    {
        let pianoSound = URL(fileURLWithPath: Bundle.main.path(forResource: "thank", ofType: "mp4")!)
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: pianoSound)
            audioPlayer.delegate = self
            audioPlayer.play()
        } catch {
            // couldn't load file :(
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let arrNot = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "cart.txt")
        for i in 0..<arrNot.count
        {
            let dict = arrNot[i] as! [String:Any]
            let app = UIApplication.shared.delegate as! AppDelegate
            let date = String(format: "%@ %@",  dict["date"] as? String ?? "" , dict["time"] as? String ?? "")
            
            let strMsg = String(format: "You have classes in next %d min",Manager.sharedInstance.RemindNotificationTime)
            
            self.scheduleNotification1(title: dict["name"] as? String ?? "", body: strMsg, strdate: date)
            
//            app.scheduleNotification(title: dict["name"] as! String, body: strMsg, strdate: date)
        }
        
        Manager.sharedInstance.removeTabbar()


    }
    
    func scheduleNotification1(title: String,body:String,strdate:String)
    {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "notifi.mpeg"))
        let strdate = strdate
        let dateformater = DateFormatter()
        dateformater.dateFormat = "dd/MM/yyyy hh:mm a"
        let date1 = dateformater.date(from: strdate)
        let time = (Manager.sharedInstance.RemindNotificationTime * 60)
        let date = date1?.addingTimeInterval(TimeInterval(-time))
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date!)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
        //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: false)
        
        let identifier = "Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        
        print(request)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDoneClick(_ sender: Any)
    {
        Manager.sharedInstance.wscallforRemoveCart()
        Manager.sharedInstance.DeleteSavedDataFromTextFile(fileName: "cart.txt")
        
      
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        
    }
    
    //MARK:- handle Back
    
    @objc func handleSwipeback(_ gestureRecognizer: UIGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            self.btnDoneClick(btnDone)
        }
    }
    
    @IBAction func btnViewOrderClick(_ sender: Any)
    {
        Manager.sharedInstance.wscallforRemoveCart()
        Manager.sharedInstance.DeleteSavedDataFromTextFile(fileName: "cart.txt")
        
        if self.strPayMode == "COD"
        {
            return
        }
        
        

        let PaymentReceipt = self.storyboard?.instantiateViewController(withIdentifier: "PaymentReceiptVC") as! PaymentReceiptVC
        PaymentReceipt.orderId = orderId
        PaymentReceipt.strmode = strPayMode
        PaymentReceipt.strAmount = strAmount
        self.navigationController?.pushViewController(PaymentReceipt, animated: true)
    }

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        
    }
    
}
