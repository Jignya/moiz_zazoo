//
//  WishlistVC.swift
//  Project
//
//  Created by HTNaresh on 4/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class WishlistVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var tblList: UITableView!
    var arrClasses : NSMutableArray = []



    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "3")
        wscallforwishlist()

    }
    
    
    //MARK:- tableview Method
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrClasses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "CalendarTableCell1"
        let cell:CalendarTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CalendarTableCell
        
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        
        cell.lblClassname.text = dict["Item_Name"] as? String
        cell.lblTime.text = dict["Vendor_Name"] as? String
        cell.btnwish.isSelected = true
        if dict["Image_Url"] as? String != ""
        {
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            cell.imgIcon.setImageWith(url, usingActivityIndicatorStyle: .gray)
        }
        

        
        if dict["Item_Type_Id"] as? NSNumber == 1
        {
            cell.lblActivity.text = ""
            cell.lblPrice.text = ""
            
            if dict["Dis_Percent"] as? NSNumber != 0
            {
                cell.lblActivity.text = String(format: "%@ %@", (dict["Dis_Percent"] as? NSNumber ?? 0)! , "% OFF")
                
                let strike : String!
                
                if let n = dict.value(forKey: "Primary_Price") as? NSNumber
                {
                    let f = n.floatValue
                    strike = String(format: "%@ %.3f",kCurrency, f)
                    
                    
                }
                else
                {
                    strike = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float ?? 0.0)!)
                    
                }
                
                cell.lblPrice.attributedText = Manager.sharedInstance.StrikeThroughString(str: strike)
            }
            
            
            if let n = dict.value(forKey: "Special_Price") as? NSNumber
            {
                let f = n.floatValue
                cell.lblAgeGrp.text! =  String(format: "%@ %.3f",kCurrency, f)
            }
            else
            {
                cell.lblAgeGrp.text! =  String(format: "%@ %.3f",kCurrency, (dict["Special_Price"] as? Float ?? 0.0)!)
            }

        }
        else if dict["Item_Type_Id"] as? NSNumber == 2 || dict["Item_Type_Id"] as? NSNumber == 3
        {
            cell.lblAgeGrp.textColor = UIColor.black

            cell.lblActivity.text = dict["Type_of_Activity"] as? String
            
            let arrPrice = dict["PriceDetails"] as? [[String:Any]] ?? []
            if arrPrice.count > 0
            {
                let strPrice = arrPrice[0]["Price"] as? String ?? "0"
                cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, strPrice,kCurrency)
                
                if dict["Discount_Percent"] as? NSNumber == 0
                {
                    cell.lblAgeGrp.text = String(format: "%@ %.3f",kCurrency, Float(strPrice)!)

                }
                else  if dict["Discount_Percent"] as? NSNumber != 0 && !isDataAvailablefordiscount(comparisonDict: arrPrice[0], classId: dict["Class_Id"] as? NSNumber ?? 0)
                {
                    cell.lblAgeGrp.text = String(format: "%@ %.3f",kCurrency, Float(strPrice)!)

                }
                else
                {
//                    cell.lblDiscountprice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, (Float(arrPrice[0]["Price"] as? String ?? "0")!),kCurrency))
                    
                    let strPrice = (arrPrice[0]["Offer_Price"] as? NSNumber ?? 0).floatValue
                    cell.lblAgeGrp.text = String(format: "%@ %.3f %@",kPrice, strPrice,kCurrency)
                    
                    cell.lblAgeGrp.textColor = GlobalConstants.highLightClr
                    
                }
                
               
            }
            else
            {
                let strPrice = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float ?? 0.0)!)
                cell.lblAgeGrp.text = strPrice
            }

//            cell.lblAgeGrp.text = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float ?? 0.0)!)
            cell.lblPrice.text! =  String(format: "%@ - %@",(dict["Start_Time"] as? String ?? "")!,(dict["End_Time"] as? String ?? "")!)
            cell.lblPrice.textColor = UIColor.black
        }
        else if dict["Item_Type_Id"] as? NSNumber == 5
        {
            let arrPrice = dict["PriceDetails"] as? [[String:Any]] ?? []
            if arrPrice.count > 0
            {
                let strPrice = arrPrice[0]["Price"] as? String ?? "0"
                cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, strPrice,kCurrency)
                
                if dict["Discount_Percent"] as? NSNumber == 0
                {
                    cell.lblAgeGrp.text = String(format: "%@ %.3f",kCurrency, Float(strPrice)!)
                    
                }
                else  if dict["Discount_Percent"] as? NSNumber != 0 && !isDataAvailablefordiscount(comparisonDict: arrPrice[0], classId: dict["Class_Id"] as? NSNumber ?? 0)
                {
                    cell.lblAgeGrp.text = String(format: "%@ %.3f",kCurrency, Float(strPrice)!)
                    
                }
                else
                {
                    //                    cell.lblDiscountprice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, (Float(arrPrice[0]["Price"] as? String ?? "0")!),kCurrency))
                    
                    let strPrice = (arrPrice[0]["Offer_Price"] as? NSNumber ?? 0).floatValue
                    cell.lblAgeGrp.text = String(format: "%@ %.3f %@",kPrice, strPrice,kCurrency)
                    
                    cell.lblAgeGrp.textColor = GlobalConstants.highLightClr
                    
                }
                
                
            }
            else
            {
                let strPrice = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float ?? 0.0)!)
                cell.lblAgeGrp.text = strPrice
            }
            
//            cell.lblAgeGrp.text = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float ?? 0.0)!)

            cell.lblActivity.text = String(format: "%@ %@", kGuest , (dict["Guests"] as? NSNumber ?? 0)!)
            cell.lblPrice.text! =  String(format: "%@ - %@",(dict["Start_Time"] as? String ?? "")!,(dict["End_Time"] as? String ?? "")!)
            cell.lblPrice.textColor = UIColor.black
        }
        
        cell.viewSold.isHidden = true
        if dict["Seats_Available"] as? Int == 0
        {
            cell.viewSold.isHidden = false
        }

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        if dict["Item_Type_Id"] as? NSNumber == 1
        {
            if dict["Deal_Id"] as? NSNumber == 0
            {
                let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
                
                subCatvc.CatId = dict["Item_Id"] as! NSNumber
                subCatvc.strfrom = "list"
                subCatvc.wscallforItemDetail()
                
                self.navigationController?.pushViewController(subCatvc, animated: true)
                return
            }
            
            let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
            
            subCatvc.CatId = dict["Deal_Id"] as? NSNumber ?? 0
            subCatvc.strfrom = "list"
            
            subCatvc.wscallforsaleItemDetail()
            
            self.navigationController?.pushViewController(subCatvc, animated: true)
        }
        else if dict["Item_Type_Id"] as? NSNumber == 2 || dict["Item_Type_Id"] as? NSNumber == 3
        {
            if dict["Is_Bundle_Item"] as? Bool == false
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
                classdetail.strDate = ""
                classdetail.strComeFrom = "wishlist"
                classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
                classdetail.classId = dict["Item_Id"] as? NSNumber
                self.navigationController?.pushViewController(classdetail, animated: true)

            }
            else
            {
                let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetail1VC") as! ClassDetail1VC
                classdetail.strDate = ""
                classdetail.strComeFrom = "wishlist"
                classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
                classdetail.classId = dict["Item_Id"] as? NSNumber
                self.navigationController?.pushViewController(classdetail, animated: true)
            }
            
        }
        else if dict["Item_Type_Id"] as? NSNumber == 5
        {
            let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "PartyDetailVC") as! PartyDetailVC
            classdetail.classId = dict["Item_Id"] as? NSNumber
            classdetail.strDate = String(format: "%@ %@",(dict["Guests"] as? NSNumber ?? 0)! , kGuest )
            self.navigationController?.pushViewController(classdetail, animated: true)
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return 130
        }
        else
        {
            let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
            
            let myString = dict["Class_Name"] as? String ?? ""
            
            let attributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont.systemFont(ofSize: 14.0)]
            
            let attributedString: NSAttributedString = NSAttributedString(string:myString,attributes:attributes)
            
            let rect : CGRect = attributedString.boundingRect(with: CGSize(width: self.tblList.frame.size.width - 200, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
            
            let requiredSize:CGRect = rect
            return requiredSize.height + 90
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {

        let delete = UITableViewRowAction(style: .destructive, title: kDeleteEn) { (action, indexPath) in
            // delete item at indexPath

            let dictdata = NSMutableDictionary(dictionary: self.arrClasses[indexPath.row] as! Dictionary)

            let alertController = UIAlertController(title:kWarningEn, message: kremoveFav, preferredStyle: .alert)

            // Create the actions
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")


                let postid = dictdata["Item_Id"] as? NSNumber ?? 0
                let strUrl:String = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveFavoriteItem,postid)

                let dictPara = NSDictionary()
                WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
                    DispatchQueue.main.async
                        {
                            // print(result)
                            let strTemp :String = result.object(forKey: "Message") as? String ?? ""
                            if strTemp == "Success"
                            {
                                self.wscallforwishlist()
                            }
                            else
                            {

                            }
                    }
                }


            }

            let CancelAction = UIAlertAction(title:kCancel, style: .default) {
                UIAlertAction in
            }
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(CancelAction)

            // Present the controller
            alertController.show()

        }
        return [delete]

    }
    
    func isDataAvailablefordiscount(comparisonDict:[String:Any],classId:NSNumber) -> Bool
    {
        
        if comparisonDict["Promotion_Type_Id"] as? NSNumber == 3
        {
            let arrChildren = comparisonDict["Children_Linked_Ids"] as? NSArray
            
            
            let predicate = NSPredicate(format: "Item_Id = %@ AND Item_Type_Id = %@", (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber ?? 0)!, (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber ?? 0)!)
            var arr1 = [[String:Any]]()
            
            if let filtered = (Manager.sharedInstance.arrcart as? NSArray)?.filtered(using: predicate) {
                arr1 = (filtered as? [[String:Any]])!
            }
            
            if arr1.count > 0
            {
                let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", classId , (comparisonDict["Item_Type_Id"] as? NSNumber ?? 0)!)
                var arr2 = [[String:Any]]()
                
                if let filtered = arrChildren?.filtered(using: predicate) {
                    arr2 = (filtered as? [[String:Any]])!
                }
                
                if arr1.count > 0 && arr2.count > 0
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false
                
                //                var arr1 = [[String:Any]]()
                //
                //                for i in 0..<Manager.sharedInstance.arrcart.count
                //                {
                //                    let dict1 = Manager.sharedInstance.arrcart[i] as! [String:Any]
                //                    let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", (dict1["Item_Id"] as? NSNumber ?? 0)!, (dict1["Item_Type_Id"] as? NSNumber ?? 0)!)
                //
                //                    if let filtered = arrChildren?.filtered(using: predicate) {
                //                        arr1 = (filtered as? [[String:Any]])!
                //                    }
                //
                //                    if arr1.count > 0
                //                    {
                //                        break
                //                    }
                //
                //                }
                //
                //                if arr1.count > 0 && (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber == classId) && (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber == comparisonDict["Item_Type_Id"] as? NSNumber)
                //                {
                //                   return true
                //                }
                //                else
                //                {
                //                    return false
                //                }
                
            }
        }
        else if comparisonDict["Promotion_Type_Id"] as? NSNumber == 1
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    
    //MARK:- button Methods
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)

    }
    
    func wscallforwishlist()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetFavoriteItems,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    if strStatus == "Success"
                    {
                        self.arrClasses = NSMutableArray(array: result["Items"] as! Array)
                        if self.arrClasses.count > 0
                        {
                            self.tblList.isHidden = false
                        }
                        else
                        {
                            self.tblList.isHidden = true
                            
                        }
                        self.tblList.reloadData()
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
}
