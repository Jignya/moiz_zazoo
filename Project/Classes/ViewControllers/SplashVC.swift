//
//  SplashVC.swift
//  Zazoo
//
//  Created by HTNaresh on 15/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation


class SplashVC: UIViewController
{
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var Indicator: UIActivityIndicatorView!
    @IBOutlet weak var imgGif: UIImageView!
    @IBOutlet weak var viewPlayer: UIView!

    
    var arrayAdv : NSMutableArray = []
    var arrayCategory : NSMutableArray = []

    var player = AVPlayer()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        

        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisFirstTime)) != nil)
        {
            playVideo()
            Manager.sharedInstance.wscallforHomePageAlldata()
        }
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        Indicator.isHidden = true
        imgGif.isHidden = true
        
    }

     func playVideo()
     {
        guard let path = Bundle.main.path(forResource: "Zazoo_logo", ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        
        player = AVPlayer(url: URL(fileURLWithPath: path))
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)

        
        let playerController = AVPlayerViewController()
        playerController.player = player
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.frame = viewPlayer.bounds
//        layer.frame = CGRect(x: 0, y: 0, width: viewPlayer.frame.size.width, height: viewPlayer.frame.size.height)

        viewPlayer.layer.addSublayer(layer)
        player.play()
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        pushToHomeScreen()
        NotificationCenter.default.removeObserver(self)

    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func pushToHomeScreen()
    {
        wscallforAdvertisements()
        Manager.sharedInstance.wscallforSideMenu()
        Manager.sharedInstance.wscallforArea()
        
        let storyboard1 : UIStoryboard!
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }

        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisFirstTime)) != nil)
        {
            if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
            {
                Manager.sharedInstance.wscallforUserDetail()
            }
            
            let home = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(home, animated: false)
           
        }
        else
        {
            let login = storyboard1.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(login, animated: false)
        }
    }
    
    
   
    
//    //MARK:- webservice
//    func wscallforSideMenu()
//    {
//        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetSideMenu,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
//
//        let dictPara = NSDictionary()
//        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
//
//            DispatchQueue.main.async
//                {
////                    print(result)
//                    let strStatus = result["Message"] as? String
//
//                    if strStatus == "Success"
//                    {
//                        let filterArray = NSMutableArray(array: (result["Filters"] as? Array)!)
//
//                        let secondArray : NSMutableArray = []
//
//                        if result["Parties"] != nil
//                        {
//                            let parties = NSMutableDictionary(dictionary: result["Parties"] as! [AnyHashable : Any])
//                            secondArray.add(parties)
//                        }
//
//                        if result["Vendors"] != nil
//                        {
//                            let parties = NSMutableDictionary(dictionary: result["Vendors"] as! [AnyHashable : Any])
//                            secondArray.add(parties)
//                        }
//
//                        if result["Gifts"] != nil
//                        {
//                            let Gift = NSMutableDictionary(dictionary: result["Gifts"] as! [AnyHashable : Any])
//                            secondArray.add(Gift)
//
//
//                            let giftAr = NSMutableArray(array: (result.value(forKeyPath: "Gifts.Categories") as! Array))
//                            Manager.sharedInstance.SaveDatainTextFile(fileName: "gift", arrData: giftAr)
//
//                        }
//
//                        if result["Contents"] != nil
//                        {
//                            let content = NSMutableDictionary(dictionary: result["Contents"] as! [AnyHashable : Any])
//                            secondArray.add(content)
//                        }
//
//
//                        for i in 0..<filterArray.count
//                        {
//                            let dict = NSMutableDictionary(dictionary: filterArray[i] as! [AnyHashable:Any])
//
//                            if dict["Type"] as? String == "List"
//                            {
//                                let aa2 = NSMutableArray(array: dict["commonDTO"] as! Array)
//
//                            Manager.sharedInstance.SaveDatainTextFile(fileName: "calenderType", arrData: aa2)
//
//                            }
//                        }
//
//                        Manager.sharedInstance.SaveDatainTextFile(fileName: "filter", arrData: filterArray)
//
//                        Manager.sharedInstance.SaveDatainTextFile(fileName: "second", arrData: secondArray)
//
//                    }
//                    else
//                    {
//
//                    }
//            }
//        }
//    }
    
    
    //MARK:- webservice
    func wscallforAdvertisements()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wGetAdvertisements)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    if strStatus == "Success"
                    {
                        Manager.sharedInstance.isRemoveCart = result["Remove_Cart"] as! NSNumber
                    }
               }
            
            
        }
        
    }

    
}
