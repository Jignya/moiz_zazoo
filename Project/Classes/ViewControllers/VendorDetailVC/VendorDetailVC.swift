//
//  VendorDetailVC.swift
//  Project
//
//  Created by HTNaresh on 27/5/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class VendorDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CGCalendarViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblVenderName: UILabel!
    @IBOutlet weak var collectionType: UICollectionView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var conTblListHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var viewTerms: UIView!
    
    @IBOutlet weak var lblTermsUnderLine: UILabel!
    @IBOutlet weak var lblTermsTitle: UILabel!
    @IBOutlet weak var txtTermsDesc: UITextView!
    @IBOutlet weak var conviewTermsHeight: NSLayoutConstraint!

    @IBOutlet weak var conColImageHeight: NSLayoutConstraint!

    @IBOutlet weak var viewNoClass: UIView!
    @IBOutlet weak var lblNoClass: UILabel!


    var arrClasses : NSMutableArray = []
    var arrayType : NSMutableArray = []
    var VendorId : NSNumber!
    var typeId : NSNumber!
    var strcomeFrom : String = ""


    var strVendorImage : String = ""
    
    var isitemSelected : Bool = false


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
        tblList.isHidden = false
        txtDesc.isHidden = true
        
        tblList.isScrollEnabled = false
        
        collectionImages.layer.cornerRadius = 5.0
        collectionImages.clipsToBounds = true
        
        btnTerms.isHidden = true
        btnTerms.setTitle("", for: .normal)
        lblTermsTitle.text = kTerms
        


        wscallforVendorDetail()
        
        conColImageHeight.constant = (self.collectionImages.frame.size.width / 2) + 35

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "0")
    }
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionType
        {
            return  arrayType.count
        }
        else
        {
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionType
        {
            let cellReuseIdentifier = "CategoryCell"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            let dict = arrayType[indexPath.row] as! NSDictionary
    
            cell.lblTitle.text = dict["Class_Type_Name"] as? String
        
            if dict["isSelect"] as! String == "1"
            {
                cell.lblLine.isHidden = false
            }
            else
            {
                cell.lblLine.isHidden = true
            }
            if !isitemSelected
            {
                if dict["Type"] as? NSNumber == 2
                {
                    tblList.isHidden = true
                    txtDesc.isHidden = false
                    
                    let strTerms = dict["Terms"] as? String
                    if strTerms == ""
                    {
                       btnTerms.isHidden = true
                       lblTermsUnderLine.isHidden = true
                       btnTerms.setTitle("", for: .normal)
                    }
                    else
                    {
                        btnTerms.isHidden = false
                        lblTermsUnderLine.isHidden = false
                        btnTerms.setTitle(kVendorTerms, for: .normal)
                        
                        let attritemutable = NSMutableAttributedString(attributedString: (strTerms)!.html2AttributedString!)
                        let font = UIFont(name: GlobalConstants.kEnglishBold , size: CGFloat(17))
                        attritemutable.setFontFace(font: font!, color: UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1))
                        
                        let paragraphStyle = NSMutableParagraphStyle()
                        paragraphStyle.alignment = UserDefaults.standard.bool(forKey: "arabic") ? .right : .left
                        
                        attritemutable.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attritemutable.length))

                        self.txtTermsDesc.attributedText = attritemutable
                    }
                    
                    txtDesc.text = dict["Vender_Desc"] as? String
                    let sizeThatFitsTextView1 = txtDesc.sizeThatFits(CGSize(width: txtDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
                    conTblListHeight.constant = sizeThatFitsTextView1.height
                    tblList.updateConstraintsIfNeeded()
                    tblList.layoutIfNeeded()
                }
            }
            
            
            return cell

        }
        else
        {
            let cellReuseIdentifier = "cell2"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            if self.strVendorImage != ""
            {
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,self.strVendorImage)
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                cell.imgLogo.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)

            }
            else
            {
                cell.imgLogo.image = UIImage(named: "logo.png")
            }
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionType
        {
            isitemSelected = true
            
            let cell = collectionType.cellForItem(at: indexPath) as! CategoryCell
            
            cell.lblLine.isHidden = false
            
            let dict1 = self.arrayType[indexPath.row] as! [String:Any]

            for j in 0..<self.arrayType.count
            {
                let dic = NSMutableDictionary(dictionary: self.arrayType[j] as! [AnyHashable:Any])
                let dic1  = NSMutableDictionary(dictionary: self.arrayType[j] as! [AnyHashable:Any])
                
                if  j == indexPath.item
                {
                    dic1.setObject("1", forKey: "isSelect" as NSCopying)
                }
                else
                {
                    dic1.setObject("0", forKey: "isSelect" as NSCopying)
                }
                
                self.arrayType.replaceObject(at: j, with: dic1)
            }
            
            self.collectionType.reloadData()


            if dict1["Type"] as? NSNumber == 2
            {
                tblList.isHidden = true
                txtDesc.isHidden = false
                
                let strTerms = dict1["Terms"] as? String
                if strTerms == ""
                {
                    btnTerms.isHidden = true
                    lblTermsUnderLine.isHidden = true
                    btnTerms.setTitle("", for: .normal)
                }
                else
                {
                    btnTerms.isHidden = false
                    lblTermsUnderLine.isHidden = false

                }

                txtDesc.text = dict1["Vender_Desc"] as? String
                let sizeThatFitsTextView1 = txtDesc.sizeThatFits(CGSize(width: txtDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
                conTblListHeight.constant = sizeThatFitsTextView1.height
                tblList.updateConstraintsIfNeeded()
                tblList.layoutIfNeeded()
            }
            else
            {
                self.arrClasses = NSMutableArray(array: dict1["Classes"] as! Array)
                tblList.isHidden = false
                txtDesc.isHidden = true
                btnTerms.isHidden = true
                lblTermsUnderLine.isHidden = true
                
                if self.arrClasses.count > 0
                {
                    
                    tblList.isHidden = false
                    tblList.reloadData()
                    conTblListHeight.constant = tblList.contentSize.height

                }
                else
                {
                    lblNoClass.text = String(format:"%@ %@ %@",kNo.capitalized , (dict1["Class_Type_Name"] as? String ?? kClass).lowercased() , kavailable)
                    tblList.isHidden = true
                    tblList.reloadData()
                    conTblListHeight.constant = 150

                }



                tblList.updateConstraintsIfNeeded()
                tblList.layoutIfNeeded()
                
            }
        }
        
    }

    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        if collectionView == collectionType
        {
            let divide =  self.arrayType.count
            
            let cellSize = CGSize(width:(self.collectionType.frame.size.width / CGFloat(divide)) - 1 , height:collectionType.frame.size.height)
            return cellSize
        }
        else
        {
            let cellSize = CGSize(width:self.collectionImages.frame.size.width , height:self.collectionImages.frame.size.height)
            return cellSize

//            let cellSize = CGSize(width:self.collectionImages.frame.size.width  , height:self.collectionImages.frame.size.width)
//            return cellSize
        }
       
    }
    
    //MARK:- tableview Method

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrClasses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "CalendarTableCell"
        let cell:CalendarTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CalendarTableCell
        
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)

        cell.lblClassname.text = dict["Class_Name"] as? String ?? ""
        cell.lblTime.text = String(format: "%@ - %@", (dict["Start_Time"] as? String ?? ""),(dict["End_Time"] as? String ?? ""))
        cell.lblActivity.text = dict["Type_of_Activity"] as? String ?? ""
        cell.lbldays.text = dict["Activity_Days"] as? String ?? ""

        let arrPrice = dict["PriceDetails"] as? [[String:Any]] ?? []
        
        cell.lblPrice.textColor = UIColor.black


        if arrPrice.count > 0
        {
            let strPrice = arrPrice[0]["Price"] as? String ?? "0"
            cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, strPrice,kCurrency)
            
            if dict["Discount_Percent"] as? NSNumber == 0
            {
                cell.lblDiscountprice.text = ""
                cell.lblPrice.text = String(format: "%.3f %@", Float(dict["Price"] as? String ?? "0")!,kCurrency)
                
            }
            else  if dict["Discount_Percent"] as? NSNumber != 0 && !isDataAvailablefordiscount(comparisonDict: arrPrice[0], classId: dict["Class_Id"] as? NSNumber ?? 0)
            {
                cell.lblDiscountprice.text = ""
                cell.lblPrice.text = String(format: "%.3f %@", Float(dict["Price"] as? String ?? "0")!,kCurrency)
                
            }
            else
            {
                cell.lblDiscountprice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, (Float(arrPrice[0]["Price"] as? String ?? "0")!),kCurrency))
                
                let strPrice = (arrPrice[0]["Offer_Price"] as? NSNumber ?? 0).floatValue
                cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, strPrice,kCurrency)
                
                 cell.lblPrice.textColor = GlobalConstants.highLightClr
            }
            
//            if arrPrice[0]["Promotion_Type_Id"] as? NSNumber == 1
//            {
//                cell.lblDiscountprice.attributedText =  Manager.sharedInstance.StrikeThroughString(str: String(format: "%@ %.3f %@",kPrice, (Float(arrPrice[0]["Price"] as? String ?? "0")!),kCurrency))
//
//                let strPrice = (arrPrice[0]["Offer_Price"] as? NSNumber ?? 0).floatValue
//                cell.lblPrice.text = String(format: "%@ %.3f %@",kPrice, strPrice,kCurrency)
//            }
//            else
//            {
//                cell.lblDiscountprice.text = ""
//            }
        }
        else
        {
            let strPrice = dict["Price"] as? String ?? "0"
            cell.lblPrice.text = String(format: "%@ %@ %@",kPrice, strPrice,kCurrency)
            cell.lblDiscountprice.text = ""
        }
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            cell.lblAgeGrp.text = String(format: "%@ %@",(dict["Age_Group"] as? String ?? ""),kAgeGrp)
        }
        else
        {
            cell.lblAgeGrp.text = String(format: "%@ %@",kAgeGrp, (dict["Age_Group"] as? String ?? "")!)
        }
        
        if dict["Is_Favorite"] as? NSNumber == 1
        {
            cell.btnwish.isSelected = true
        }
        else
        {
            cell.btnwish.isSelected = false
        }


        if dict["Image_Url"] as? String != ""
        {
            var Stringimg = String()
            Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
            let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let url = URL(string: urlString!)
            cell.imgIcon?.setImageWith(url, placeholderImage: UIImage(named: "logo.png"), usingActivityIndicatorStyle: .gray)
        }
        else
        {
            cell.imgIcon.image = UIImage(named: "logo.png")
        }

        cell.lblwaiting.isHidden = true
        cell.lblwaitList.isHidden = true
        
        cell.viewSold.isHidden = true
        if dict["Seats_Available"] as? Int == 0 && dict["Is_Bundle_Item"] as? Int == 0
        {
            cell.viewSold.isHidden = false
        }
        
        tblList.updateConstraintsIfNeeded()
        tblList.layoutIfNeeded()
        conTblListHeight.constant = tblList.contentSize.height

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        
        if dict["Is_Bundle_Item"] as? Bool == false
        {
            let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetailVC") as! ClassDetailVC
            classdetail.strComeFrom = ""
            classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
            classdetail.classId = dict["Class_Id"] as? NSNumber
            self.navigationController?.pushViewController(classdetail, animated: true)

        }
        else
        {
            let classdetail = self.storyboard?.instantiateViewController(withIdentifier: "ClassDetail1VC") as! ClassDetail1VC
            classdetail.strComeFrom = ""
            classdetail.isBundleItem = dict["Is_Bundle_Item"] as? Bool ?? false
            classdetail.classId = dict["Class_Id"] as? NSNumber
            self.navigationController?.pushViewController(classdetail, animated: true)
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return 160
            
        }
        
       
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        
        var myString = ""
        
        if let strdays = dict["Activity_Days"] as? String
        {
            myString = String(format: "%@\n%@%@", dict["Class_Name"] as? String ?? "" , strdays,dict["Type_of_Activity"] as? String ?? "")
        }
        else
        {
            myString =  (dict["Class_Name"] as? String ?? "")  + (dict["Type_of_Activity"] as? String ?? "")
        }
        
        let attributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont.systemFont(ofSize: 14.0)]
        
        let attributedString: NSAttributedString = NSAttributedString(string:myString,attributes:attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: self.tblList.frame.size.width - 200, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requiredSize:CGRect = rect
        return requiredSize.height + 90
        
        //        return 95
    }
    
    @IBAction func btnTermsClick(_ sender: Any)
    {
        txtTermsDesc.setContentOffset(.zero, animated: false)
        
        self.view.bringSubviewToFront(viewTerms)
        
        var TopPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            TopPadding = (window?.safeAreaInsets.top)!
        }
        
        self.conviewTermsHeight.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conviewTermsHeight.constant = self.view.frame.size.height - TopPadding
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }

    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btncloseTermsClick(_ sender: Any)
    {
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conviewTermsHeight.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        if strcomeFrom == "push"
        {
            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    func isDataAvailablefordiscount(comparisonDict:[String:Any],classId:NSNumber) -> Bool
    {
        
        if comparisonDict["Promotion_Type_Id"] as? NSNumber == 3
        {
            let arrChildren = comparisonDict["Children_Linked_Ids"] as? NSArray
            
            
            let predicate = NSPredicate(format: "Item_Id = %@ AND Item_Type_Id = %@", (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber ?? 0)!, (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber ?? 0)!)
            var arr1 = [[String:Any]]()
            
            if let filtered = (Manager.sharedInstance.arrcart as? NSArray)?.filtered(using: predicate) {
                arr1 = (filtered as? [[String:Any]])!
            }
            
            if arr1.count > 0
            {
                let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", classId , (comparisonDict["Item_Type_Id"] as? NSNumber ?? 0)!)
                var arr2 = [[String:Any]]()
                
                if let filtered = arrChildren?.filtered(using: predicate) {
                    arr2 = (filtered as? [[String:Any]])!
                }
                
                if arr1.count > 0 && arr2.count > 0
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false
                
                //                var arr1 = [[String:Any]]()
                //
                //                for i in 0..<Manager.sharedInstance.arrcart.count
                //                {
                //                    let dict1 = Manager.sharedInstance.arrcart[i] as! [String:Any]
                //                    let predicate = NSPredicate(format: "Child_Linked_Item_Id = %@ AND Child_Linked_Item_Type_Id = %@", (dict1["Item_Id"] as? NSNumber ?? 0)!, (dict1["Item_Type_Id"] as? NSNumber ?? 0)!)
                //
                //                    if let filtered = arrChildren?.filtered(using: predicate) {
                //                        arr1 = (filtered as? [[String:Any]])!
                //                    }
                //
                //                    if arr1.count > 0
                //                    {
                //                        break
                //                    }
                //
                //                }
                //
                //                if arr1.count > 0 && (comparisonDict["Parent_Linked_Item_Id"] as? NSNumber == classId) && (comparisonDict["Parent_Linked_Item_Type_Id"] as? NSNumber == comparisonDict["Item_Type_Id"] as? NSNumber)
                //                {
                //                   return true
                //                }
                //                else
                //                {
                //                    return false
                //                }
                
            }
        }
        else if comparisonDict["Promotion_Type_Id"] as? NSNumber == 1
        {
            return true
        }
        else
        {
            return false
        }
        
    }
    
    //MARK:- webservice
    func wscallforVendorDetail()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetVendorClassDetail,VendorId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.lblTitle.text = result["Vendor_Name"] as? String
                        self.lblVenderName.text = result["Vendor_Name"] as? String

                        self.strVendorImage = (result["Vendor_Image_Url"] as? String ?? "")!
                        
                       self.collectionImages.reloadData()

                        self.arrayType = NSMutableArray(array: result["ClassTypes"] as! Array)
                        
                        self.collectionType.reloadData()
                        
                        
                        let dict = self.arrayType[0] as! [String:Any]
                        self.arrClasses = NSMutableArray(array: dict["Classes"] as! Array)
                        
                        for j in 0..<self.arrayType.count
                        {
                            let dic = NSMutableDictionary(dictionary: self.arrayType[j] as! [AnyHashable:Any])
                            let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
                            
                            if  j == 0
                            {
                                dic1.setObject("1", forKey: "isSelect" as NSCopying)
                            }
                            else
                            {
                                dic1.setObject("0", forKey: "isSelect" as NSCopying)
                            }
                            
                            self.arrayType.replaceObject(at: j, with: dic1)
                        }
                        
                        self.collectionType.reloadData()
                        self.tblList.reloadData()

                        
                    }
            }
        }
        
    }

}
