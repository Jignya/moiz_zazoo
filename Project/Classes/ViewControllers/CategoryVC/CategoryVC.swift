//
//  CategoryVC.swift
//  Project
//
//  Created by HTNaresh on 26/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit


class CategoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnFilter: UIButton!

    
    @IBOutlet weak var collectionType: UICollectionView!
    @IBOutlet weak var collectionSub: UICollectionView!
    @IBOutlet weak var collectionItem: UICollectionView!

    @IBOutlet weak var concollectionSubHeight: NSLayoutConstraint!


    var arrCategory : NSMutableArray = []
    var arrClasses : NSMutableArray = []
    var arrSubCategory : NSMutableArray = []
    var catId : String!
    var subCatId : String!
    var strTitle : String!
    
    var actionButton = JJFloatingActionButton()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        lblTitle.text = strTitle
   
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        arrCategory = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "gift")
        
        btnFilter.isHidden = true
//        wscallforSubCategory()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "0")
        
        var bottomPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }

        
        actionButton = JJFloatingActionButton.init(frame: CGRect(x: self.view.frame.size.width - 60, y: self.view.frame.size.height - 125 - bottomPadding, width: 50, height: 50))
        
        actionButton.addItem(title: "item 1", image: UIImage(named: "First")?.withRenderingMode(.alwaysTemplate)) { item in
            // do something
        }
        
        actionButton.addItem(title: "item 2", image: UIImage(named: "Second")?.withRenderingMode(.alwaysTemplate)) { item in
            // do something
        }
        
        actionButton.addItem(title: "item 3", image: nil) { item in
            // do something
        }
        
        actionButton.overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        actionButton.buttonImage = UIImage(named: "Filter")
        actionButton.buttonColor = GlobalConstants.themeClr
        
        let win = UIApplication.shared.delegate?.window as! UIWindow
        win.addSubview(actionButton)
        
//        self.view.addSubview(actionButton)
//        self.view.bringSubview(toFront: actionButton)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        actionButton.removeFromSuperview()
    }
    
    //MARK:- button Methods
    
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }
    
    @IBAction func btnFilterClick(_ sender: Any)
    {
        

    }
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        let array = self.navigationController?.viewControllers
            as? Array ?? []
        for k in (0 ..< array.count)
        {
            let controller: UIViewController? = (array[k] as! UIViewController)
            if (controller is HomeVC)
            {
                self.navigationController?.popToViewController(controller!, animated: true)
                return
            }
        }
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        
    }
    
    
    //MARK:- tableview Method
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrClasses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "CalendarTableCell"
        let cell:CalendarTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! CalendarTableCell
        
            let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
    
            cell.lblClassname.text = dict["Item_Name"] as? String
            cell.lblTime.text = dict["Vendor_Name"] as? String
    
            cell.lblActivity.text = ""
            cell.lblPrice.text = ""
    
            if dict["Dis_Percent"] as? NSNumber != 0
            {
                    cell.lblActivity.text = String(format: "%@ %@", (dict["Dis_Percent"] as? NSNumber)! , "% OFF")
        
                    let strike : String!
        
                    if let n = dict.value(forKey: "Primary_Price") as? NSNumber
                    {
                        let f = n.floatValue
                        strike = String(format: "%@ %.3f",kCurrency, f)
        
        
                    }
                    else
                    {
                        strike = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float)!)
        
                    }
        
                    cell.lblPrice.attributedText = Manager.sharedInstance.StrikeThroughString(str: strike)
                }
        
                cell.btnwish.isSelected = false
        
                if (dict["Is_Favorite"] as? NSNumber)! == 1
                {
                    cell.btnwish.isSelected = true
                }
        
                if let n = dict.value(forKey: "Special_Price") as? NSNumber
                {
                    let f = n.floatValue
                    cell.lblAgeGrp.text! =  String(format: "%@ %.3f",kCurrency, f)
        
                }
                else
                {
                    cell.lblAgeGrp.text! =  String(format: "%@ %.3f",kCurrency, (dict["Special_Price"] as? Float)!)
                }

                if dict["Image_Url"] as? String != ""
                {
                    var Stringimg = String()
                    Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
                    let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    let url = URL(string: urlString!)
                    cell.imgIcon.setImageWith(url, usingActivityIndicatorStyle: .gray)
                }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
        
        if dict["Deal_Id"] as? NSNumber == 0
        {
            let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
            
            subCatvc.CatId = dict["Item_Id"] as? NSNumber
            subCatvc.strTitle = strTitle
            subCatvc.strfrom = "list"
            subCatvc.wscallforItemDetail()
            
            self.navigationController?.pushViewController(subCatvc, animated: true)
            return
        }
        
        let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
        
        subCatvc.CatId = dict["Deal_Id"] as? NSNumber
        subCatvc.strTitle = strTitle
        subCatvc.strfrom = "list"
        
        subCatvc.wscallforsaleItemDetail()
        
        self.navigationController?.pushViewController(subCatvc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 95
    }
    
    
    //MARK:- Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == collectionSub
        {
            return arrSubCategory.count
        }
        else if collectionView == collectionType
        {
            return arrCategory.count
        }
        else
        {
            return arrClasses.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionSub
        {
            let cellReuseIdentifier = "cell2"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            let dict = arrSubCategory[indexPath.row] as! NSDictionary
            
            cell.lblTitle.backgroundColor = UIColor.clear

            cell.lblTitle.textColor = UIColor.darkGray

            if dict["isSelect"] as? String == "1"
            {
                cell.lblTitle.backgroundColor = UIColor(red: 231.0/255.0, green: 123.0/255.0, blue: 79.0/255.0, alpha: 1)
                
                cell.lblTitle.textColor = UIColor.white

            }
            
            cell.lblTitle.text = dict["Sub_Category_Name_E"] as? String
            
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.cornerRadius = 3.0
            
            
            return cell
        }
        else if collectionView == collectionType
        {
            let cellReuseIdentifier = "cell1"
            let cell:CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryCell
            
            let dict = arrCategory[indexPath.row] as! NSDictionary
            
            cell.lblTitle.text = dict["Category_Name_E"] as? String
            
            if catId == nil
            {
                catId = String(format: "%@", (dict["Cat_Id"] as? NSNumber)!)
                wscallforSubCategory()
            }
            
            if catId == String(format: "%@", (dict["Cat_Id"] as? NSNumber)!)
            {
                cell.layer.borderColor = GlobalConstants.selectedClr
                cell.layer.borderWidth = 2.0
                cell.lblTitle.textColor = UIColor.white
            }
            else
            {
                cell.layer.borderColor = UIColor.clear.cgColor
                cell.layer.borderWidth = 0
                cell.lblTitle.textColor = UIColor.groupTableViewBackground
            }
            
            if indexPath.row == 0
            {
                cell.lblTitle.backgroundColor = UIColor(red: 231.0/255.0, green: 123.0/255.0, blue: 79.0/255.0, alpha: 1)
            }
            if indexPath.row == 1
            {
                cell.lblTitle.backgroundColor = UIColor(red: 63.0/255.0, green: 57.0/255.0, blue: 100.0/255.0, alpha: 1)
            }
            else if indexPath.row == 2
            {
                
                cell.lblTitle.backgroundColor = UIColor(red: 243.0/255.0, green: 146.0/255.0, blue: 177.0/255.0, alpha: 1)
            }
            else if indexPath.row == 3
            {
                
                cell.lblTitle.backgroundColor = UIColor(red: 255.0/255.0, green: 214.0/255.0, blue: 120.0/255.0, alpha: 1)
            }
            
            return cell
        }
        else
        {
            let cellReuseIdentifier = "ItemCell"
            let cell:ItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! ItemCell
            
            
                let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
    
    
                let availqty = dict["Available_Qty"] as? Int
                if availqty == 0
                {
                    cell.viewsold.isHidden = false
                }
                else
                {
                    cell.viewsold.isHidden = true
                }
    
    
                cell.lblItemName.text = dict["Item_Name"] as? String
                var Stringimg = String()
                Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Image_Url"] as? String ?? "")
    
                let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    
                let url = URL(string: urlString!)
                cell.imgItem.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
    
            cell.lblBrand.text = dict["Vendor_Name"] as? String
    
            cell.btnfav.isSelected = false
            if (dict["Is_Favorite"] as? NSNumber)! == 1
            {
                cell.btnfav.isSelected = true
            }
    
    
    
            if let n = dict.value(forKey: "Special_Price") as? NSNumber
            {
                let f = n.floatValue
                cell.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, f)
    
            }
            else
            {
                cell.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, (dict["Special_Price"] as? Float)!)
            }
    
            if (dict["Dis_Percent"] as? NSNumber)! == 0
            {
               cell.lblDiscount.text = ""
               cell.lblDiscountPrice.text = ""
    
            }
            else
            {
                cell.lblDiscount.text = String(format: "%@ %@", (dict["Dis_Percent"] as? NSNumber)! , "% OFF")
    
                let strike : String!
    
                if let n = dict.value(forKey: "Primary_Price") as? NSNumber
                {
                    let f = n.floatValue
                    strike = String(format: "%@ %.3f",kCurrency, f)
    
    
                }
                else
                {
                    strike = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float)!)
    
                }
    
                cell.lblDiscountPrice.attributedText = Manager.sharedInstance.StrikeThroughString(str: strike)
            }
            
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.cornerRadius = 3.0
            
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionSub
        {
            let dict = arrSubCategory[indexPath.row] as! NSDictionary
            
            subCatId = String(format: "%@", (dict["Sub_Cat_Id"] as? NSNumber)!)
            
            for j in 0..<self.arrSubCategory.count
            {
                let dic = NSMutableDictionary(dictionary: (self.arrSubCategory[j] as? Dictionary)!)
                let dic1  = NSMutableDictionary(dictionary: (self.arrSubCategory[j] as? Dictionary)!)
                
                if j == indexPath.row
                {
                    dic1.setObject("1", forKey: "isSelect" as NSCopying)
                }
                else
                {
                    dic1.setObject("0", forKey: "isSelect" as NSCopying)
                }
                
                self.arrSubCategory.replaceObject(at: j, with: dic1)
            }
            
            
            collectionSub.reloadData()
            
            wscallforItemlist()
        }
        else if collectionView == collectionType
        {
            let dict = arrCategory[indexPath.row] as! NSDictionary
            
            catId = String(format: "%@", (dict["Cat_Id"] as? NSNumber)!)
            wscallforSubCategory()
            collectionType.reloadData()
            collectionSub.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: false)

        }
        else
        {
            let dict = NSMutableDictionary(dictionary: arrClasses[indexPath.row] as! Dictionary)
            
            if dict["Deal_Id"] as? NSNumber == 0
            {
                let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
                
                subCatvc.CatId = dict["Item_Id"] as? NSNumber
                subCatvc.strTitle = strTitle
                subCatvc.strfrom = "list"
                subCatvc.wscallforItemDetail()
                
                self.navigationController?.pushViewController(subCatvc, animated: true)
                return
            }
            
            let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailVC") as! ItemDetailVC
            
            subCatvc.CatId = dict["Deal_Id"] as? NSNumber
            subCatvc.strTitle = strTitle
            subCatvc.strfrom = "list"
            
            subCatvc.wscallforsaleItemDetail()
            
            self.navigationController?.pushViewController(subCatvc, animated: true)
        }
        
        
        
        //Cat_Id
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == collectionSub
        {
            let dict = arrSubCategory[indexPath.row] as! NSDictionary

            let nameString = dict["Sub_Category_Name_E"] as? String ?? ""
            let font : UIFont = UIFont.systemFont(ofSize: 16)
            let attributes = NSDictionary(object: font, forKey:NSAttributedString.Key.font as NSCopying)
            
            let sizeOfText = nameString.size(withAttributes: (attributes as! [NSAttributedString.Key : Any]))
            
            let cellSize = CGSize(width:(sizeOfText.width + 30) , height:collectionSub.frame.size.height)
            return cellSize
        }
        else if collectionView == collectionType
        {
            let divide = self.arrCategory.count
            
            let cellSize = CGSize(width:(self.collectionType.frame.size.width / CGFloat(divide)) , height:collectionType.frame.size.height)
            return cellSize
        }
        else
        {
            let width:CGFloat = ((self.view.frame.size.width - 20) / 2) - 5
            let cellSize = CGSize(width:width , height:250)
            return cellSize
        }
       
    }
    

    
    //    MARK:- Webservice
    
    func wscallforSubCategory()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetSubCategories,catId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrSubCategory = NSMutableArray(array: result["SubCategories"] as! Array)
                        
                        for j in 0..<self.arrSubCategory.count
                        {
                            let dic = NSMutableDictionary(dictionary: self.arrSubCategory[j] as! [AnyHashable:Any])
                            let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
                            
                            if j == 0
                            {
                                dic1.setObject("1", forKey: "isSelect" as NSCopying)
                                self.subCatId = String(format: "%@", (dic["Sub_Cat_Id"] as? NSNumber)!)
                            }
                            else
                            {
                                dic1.setObject("0", forKey: "isSelect" as NSCopying)
                            }
                            
                            self.arrSubCategory.replaceObject(at: j, with: dic1)
                        }
                        
                        
                        self.wscallforItemlist()
                        
                        if self.arrSubCategory.count > 0
                        {
                            self.collectionSub.isHidden = false
                            self.concollectionSubHeight.constant = 40
                        }
                        else
                        {
                            self.collectionSub.isHidden = true
                            self.concollectionSubHeight.constant = 0

                        }
                        
                        self.collectionSub.reloadData()
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    
    func wscallforItemlist()
    {
        let strUrl = String(format: "%@%@/%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetItems,catId,subCatId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    if strStatus == "Success"
                    {
                        self.arrClasses = NSMutableArray(array: result["Items"] as! Array)
                        if self.arrClasses.count > 0
                        {
                            self.collectionItem.isHidden = false
                        }
                        else
                        {
                            self.collectionItem.isHidden = true
                        }
                        self.collectionItem.reloadData()
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    

}
