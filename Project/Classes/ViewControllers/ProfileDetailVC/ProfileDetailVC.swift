//
//  ProfileDetailVC.swift
//  Zazoo
//
//  Created by HTNaresh on 14/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class ProfileDetailVC: UIViewController,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!

    @IBOutlet weak var btnSaveChange: UIButton!

    @IBOutlet weak var txtRemindingTime: UITextField!

    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        txtName.placeholder = kFullname
        txtMobile.placeholder = kMobNum
        txtEmail.placeholder = kEmail
        
        txtName.text = Manager.sharedInstance.dictprofile["FirstName"] as? String
        txtMobile.text = Manager.sharedInstance.dictprofile["Mobile"] as? String
        txtEmail.text = Manager.sharedInstance.dictprofile["Username"] as? String
        
        if Manager.sharedInstance.dictprofile["LastName"] is String
        {
            txtLastname.text = Manager.sharedInstance.dictprofile["LastName"] as? String
        }
        else
        {
            txtLastname.text = ""
        }
        
        btnSaveChange.setTitle(kSaveChanges, for: .normal)
        lblTitle.text = kprofile

        txtRemindingTime.text = String(format: "%d", Manager.sharedInstance.RemindNotificationTime)
        

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "2")

    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    @IBAction func btnSaveChangeClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if (txtName.text=="")
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourNameEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
        else if(txtMobile.text=="")
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryourmobileEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else if((txtMobile.text?.count)! < 8)
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseentervalidMobnum, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
            
        else if (txtEmail.text == "")
        {
            
            let alert = UIAlertController(title:kWarningEn, message:kPleaseenteryouremailaddressEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
        }
            
        else if(!(isValidEmail(testStr: txtEmail.text!)))
        {
            let alert = UIAlertController(title:kWarningEn, message:kEmailValidationEn, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            
        }
        else
        {
            wscallforupdate()
        }
        
    }
    
    // MARK: -Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobile
        {
            if string.isEmpty
            {
                return true
            }
            if validatePhone(string)
            {
                var strMaxLength = ""
                strMaxLength = "9"
                let newStr = textField.text as NSString?
                let currentString: String = newStr!.replacingCharacters(in: range, with: string)
                let j = Int(strMaxLength) ?? 0
                let length: Int = currentString.count
                if length >= j {
                    return false
                }
                else {
                    return true
                }
            }
            return false
        }
        
        else if textField == txtRemindingTime
        {
            if string.isEmpty
            {
                return true
            }
            if validatePhone(string)
            {
                return true
            }
            return false
        }
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == txtRemindingTime && txtRemindingTime.text != ""
        {
            Manager.sharedInstance.RemindNotificationTime = Int(txtRemindingTime.text!)
        }
        
        print(Manager.sharedInstance.RemindNotificationTime)
        
    }
    
    
    
    func validatePhone(_ phoneNumber: String) -> Bool {
        let phoneRegex = "[0-9]"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if !phoneTest.evaluate(with: phoneNumber) {
            return false
        }
        else {
            return true
        }
    }
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    @IBAction func btnbackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    
    func wscallforupdate()
    {
        let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)

        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wUpdateProfile)
        
        let dictPara:NSDictionary = ["FirstName":self.txtName.text!,"LastName":self.txtLastname.text!,"Mobile":self.txtMobile.text!,"Email":self.txtEmail.text!,"UserId":userId]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        Manager.sharedInstance.wscallforUserDetail()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        alertController.addAction(okAction)
                        alertController.show()
                    }
                    
            }
        }
        
    }

}
