//
//  MyOrderVC.swift
//  Zazoo
//
//  Created by HTNaresh on 14/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class MyOrderVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewRecentData: UIView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblRecentOrder: UILabel!
    @IBOutlet weak var lblRecentproductname: UILabel!
    @IBOutlet weak var lblTotalPaid: UILabel!
    @IBOutlet weak var lblProductRecentPrice: UILabel!
    @IBOutlet weak var viewNodata: UIView!


    @IBOutlet weak var conTblHeight: NSLayoutConstraint!
    
    var arrOrderList : NSMutableArray = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        self.tblList.isHidden = true
        self.viewRecentData.isHidden = true
        self.viewNodata.isHidden = true

        
        lblTitle.text = kOrders
        lblRecentOrder.text = kmostRecent
        lblTotalPaid.text = kTotalPaid

        
        wscallforOrderHistory()
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "2")
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellReuseIdentifier = "OrderCell"
        let cell:OrderCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! OrderCell
        
        let dict = NSMutableDictionary(dictionary: arrOrderList[indexPath.row] as! Dictionary)
                
        let strDate = dict["OrderDate"] as! String
        let ar1 = strDate.components(separatedBy: " ")
        if ar1.count > 0
        {
            cell.lblProductName.text = ar1[0]
        }
        
        if indexPath.row == 0
        {
            self.lblRecentproductname.text = ar1[0]
            self.lblProductRecentPrice.text = String(format: "%.3f %@", (dict["OrderAmount"] as? NSNumber ?? 0).floatValue , kCurrency)
        }
        
        cell.lblOrderNum.text = String(format: "%@", (dict["OrderId"] as? NSNumber ?? 0))
        
        if let n = dict.value(forKey: "OrderAmount") as? NSNumber
        {
            let f = n.floatValue
            cell.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, f)
            
        }
        else
        {
            cell.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, (dict["OrderAmount"] as? Float ?? 0.0)!)
        }
        
        
        conTblHeight.constant = tblList.contentSize.height
        tblList.updateConstraintsIfNeeded()
        tblList.layoutIfNeeded()

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dict = NSMutableDictionary(dictionary: arrOrderList[indexPath.row] as! Dictionary)

        let home = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderDetailVC") as! MyOrderDetailVC
        home.OrderId = dict["OrderId"] as! NSNumber
        home.dictdata = dict.mutableCopy() as! NSMutableDictionary
        self.navigationController?.pushViewController(home, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            return  80
        }
        else
        {
            return 65
        }

    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    func wscallforOrderHistory()
    {
        var strUrl : String = ""
        
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
           let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as! CVarArg)
            
            strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetUserOrderHistory,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        }
        else
        {
            strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetDeviceOrderHistory,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        }
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.arrOrderList = NSMutableArray(array: result["HistoryOrders"] as! Array)
                        
//                        self.arrOrderList =  NSMutableArray(array: self.arrOrderList.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray


                        if self.arrOrderList.count > 0
                        {
                            self.tblList.isHidden = false
                            self.viewRecentData.isHidden = false
                            self.viewNodata.isHidden = true

                            self.tblList.reloadData()
                        }
                        else
                        {
                            self.tblList.isHidden = true
                            self.viewRecentData.isHidden = true
                            self.viewNodata.isHidden = false

                        }
                    }
                    else
                    {
                        self.tblList.isHidden = true
                        self.viewRecentData.isHidden = true
                        self.viewNodata.isHidden = false

                    }
             }
            
            
        }
        
    }
    

    
}
