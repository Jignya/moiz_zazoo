//
//  OrderCell.swift
//  Zazoo
//
//  Created by HTNaresh on 14/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell
{
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOrderNum: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
