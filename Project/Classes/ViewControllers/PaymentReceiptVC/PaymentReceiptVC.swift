//
//  PaymentReceiptVC.swift
//  Zazoo
//
//  Created by HTNaresh on 15/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.

import UIKit

class PaymentReceiptVC: UIViewController,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblItemData: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lbldateData: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTimeData: UILabel!
    @IBOutlet weak var lblPaidBy: UILabel!
    @IBOutlet weak var lblPaidByData: UILabel!
    @IBOutlet weak var lblTotalAmt: UILabel!
    @IBOutlet weak var lblTotalAmtData: UILabel!
    @IBOutlet weak var lblOrderNum: UILabel!
    @IBOutlet weak var lblOrderNumData: UILabel!
    @IBOutlet weak var lblTransNum: UILabel!
    @IBOutlet weak var lblTransNumData: UILabel!
    
    @IBOutlet weak var btnSave: ButtonExtender!
    @IBOutlet weak var conViewTransactionHeight: NSLayoutConstraint!
    
    var orderId : NSNumber!
    var strmode : String!
    var strAmount : String!



    @IBOutlet weak var viewReceipt: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        
        Manager.sharedInstance.CartCount = 0

        if strmode == "COD"
        {
          conViewTransactionHeight.constant = 0
            self.lblOrderNumData.text = String(format: "%@", orderId )
            self.lblItemData.text = "Success"
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            let result = formatter.string(from: date)

            let ar1 = result.components(separatedBy: " ")
            if ar1.count > 0
            {
                self.lbldateData.text = ar1[0]
                self.lblTimeData.text = ar1[1]
            }
            
            self.lblPaidByData.text = strmode
            self.lblTotalAmtData.text! = strAmount
        }
        else
        {
            conViewTransactionHeight.constant = 50
            wscallforReceipt()
        }
        
        setlabel()
        
        
        

    }
    
    //MARK:- handle Back
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @objc func handleSwipeback(_ gestureRecognizer: UIGestureRecognizer)
    {
        if gestureRecognizer.state == .began
        {
            self.btnBackClick(btnBack)
        }
    }
    
    func setlabel()
    {
        lblTitle.text = kPaymentreceipt
        
        lblTransNum.text = kTransaction
        lblItem.text = kOrderStatus
        lbldate.text = kDate
        lblTime.text = kTime
        lblOrderNum.text = kOrderNumber
        lblPaidBy.text = kPaidBy
        lblTotalAmt.text = kTotalAmount
        btnSave.setTitle(kSave.uppercased(), for: .normal)
    }
    
    func wscallforReceipt()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetOrderPaymentDetail,orderId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    //KNetPayId
                    if result["KNetTransId"] is String
                    {
                    self.lblTransNumData.text = String(format: "%@", (result["KNetTransId"] as? String ?? "")!)
                    }
                    self.lblOrderNumData.text = String(format: "%@", result["OrderId"]  as? NSNumber ?? 0)
                    
                    if result["KNetResult"] as? String == "CAPTURED"
                    {
                        self.lblItemData.text = "Success"
                        
                    }
                    else
                    {
                        self.lblItemData.text = "Fail"
                    }
                    
//                    self.lblItemData.text = result["KNetResult"] as? String
                    let strDate = result["PayDate"] as? String ?? ""
                    let ar1 = strDate.components(separatedBy: " ")
                    if ar1.count > 1
                    {
                        self.lbldateData.text = ar1[0]
                        self.lblTimeData.text = ar1[1]
                    }
                    
                    self.lblPaidByData.text = result["PayType"] as? String
                    
                    if let n = result.value(forKey: "PayAmount") as? NSNumber
                    {
                        let f = n.floatValue
                        self.lblTotalAmtData.text! = String(format: "%.3f %@", f , kCurrency)

                    }
                    else
                    {
                        self.lblTotalAmtData.text! = String(format: "0.000 %@", kCurrency)
                    }
                    
                    
            }
            
            
        }
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnSaveClick(_ sender: Any)
    {
        UIGraphicsBeginImageContextWithOptions(viewReceipt.bounds.size, _: viewReceipt.isOpaque, _: 0.0)
        if let aContext = UIGraphicsGetCurrentContext() {
            viewReceipt.layer.render(in: aContext)
        }
        let screengrab: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(screengrab!, nil, nil, nil)
        
        Manager.sharedInstance.showSmallAlert("Saved to gallery")

    }
    
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        
    }
}
