//
//  Payment1VC.swift
//  Zazoo
//
//  Created by HTNaresh on 13/12/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class PaymentGymVC: UIViewController,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTotalPayable: UILabel!
    @IBOutlet weak var lblCOD: UILabel!
    @IBOutlet weak var btnPayNow: UIButton!
    
    @IBOutlet weak var lblPaymentOption: UILabel!
    @IBOutlet weak var lblKnet: UILabel!
    @IBOutlet weak var lblcredit: UILabel!
    
    
    @IBOutlet weak var btnKnet: UIButton!
    @IBOutlet weak var btnCod: UIButton!
    @IBOutlet weak var lblBottomPrice: UILabel!
    @IBOutlet weak var btnCreditCard: UIButton!
    
    
    @IBOutlet weak var conviewCCHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewCODHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewKnetHeight: NSLayoutConstraint!
    
    @IBOutlet weak var conPaymentHeight: NSLayoutConstraint!
    
    
    var addressId : NSNumber!
    var strTotal : String!
    var strmethod : String!
    var strArea : String!
    var arrParam : NSMutableArray = []
    var arrCartData : NSMutableArray = []
    var dictAdd : NSMutableDictionary!
    var DelCharges : Float!
    var totalSum1 : Float!
    
    
    var strName : String = ""
    var strEmail : String = ""
    var strMobile : String = ""
    
    var dictPara:NSDictionary!
    var strVendorname : String!


    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        strmethod = "KNET"
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        conviewKnetHeight.constant = 0
        conviewCODHeight.constant = 0
        conviewCCHeight.constant = 0
        conPaymentHeight.constant = conviewKnetHeight.constant + conviewCODHeight.constant + conviewCCHeight.constant + 50
        
        wscallforPaymentType()
        
        
        
        if let n = dictPara.value(forKey: "Total_Amount") as? NSNumber
        {
            let f = n.floatValue
            self.lblBottomPrice.text = String(format: "%.3f %@", f ,kCurrency)
            
        }
        else
        {
            self.lblBottomPrice.text = String(format: "%.3f %@", (dictPara["Total_Amount"] as? NSNumber ?? 0)!,kCurrency)
        }
        
        
        
        
        setLabel()
    }
    
    
    func setLabel()
    {
        lblCOD.text = kcod
        lblKnet.text = kKnetPayment
        lblcredit.text = kCredit
        lblPaymentOption.text = kPaymentoption
        lblTotalPayable.text = kTotalAmount
        lblTitle.text = kPayment
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    func setPaymentHeight(isOn1:Bool ,isOn2:Bool, isOn3:Bool)
    {
//        if isOn2 == true
//        {
//            strmethod = "COD"
//            btnCod.isSelected = true
//            btnPayNow.setTitle(kOrder, for: .normal)
//        }
//        else
//        {
            if isOn1 == true
            {
                strmethod = "KNET"
                btnKnet.isSelected = true
            }
            else
            {
                strmethod = "CREDITCARD"
                btnCreditCard.isSelected = true
            }
//        }
        
        if isOn1 == true
        {
            conviewKnetHeight.constant = 50
        }
        if isOn2 == true
        {
            conviewCODHeight.constant = 0
        }
        if isOn3 == true
        {
            conviewCCHeight.constant = 50
        }
        
        //        conviewCCHeight.constant = 50
        
        conPaymentHeight.constant = conviewKnetHeight.constant + conviewCODHeight.constant + conviewCCHeight.constant + 50
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Click
    
    @IBAction func btnBackClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnPayNowClick(_ sender: Any)
    {
        wscallforSaveOrder()
    }
    
 
    
    @IBAction func btnCodClick(_ sender: Any)
    {
        strmethod = "COD"
        btnPayNow.setTitle(kOrder, for: .normal)
        
        
        btnCod.isSelected = true
        btnKnet.isSelected = false
        btnCreditCard.isSelected = false
        
    }
    
    @IBAction func btnKnetClick(_ sender: Any)
    {
        strmethod = "KNET"
        
        btnPayNow.setTitle(kPaynow, for: .normal)
        btnKnet.isSelected = true
        btnCod.isSelected = false
        btnCreditCard.isSelected = false
        
    }
    
    
    @IBAction func btnCCClick(_ sender: Any)
    {
        strmethod = "CREDITCARD"
        btnPayNow.setTitle(kPaynow, for: .normal)
        
        btnCreditCard.isSelected = true
        btnCod.isSelected = false
        btnKnet.isSelected = false
    }
   
    
    //MARK: webservice
    
    
    func wscallforSaveOrder()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wSaveOrder)
        
        let dictpara1 : NSMutableDictionary = [:]
        dictpara1.addEntries(from: dictPara as! [AnyHashable : Any])
        dictpara1.setObject(strmethod, forKey:"Payment_Type" as NSCopying)
        print(dictpara1)
        
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictpara1 as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        if self.strmethod == "COD"
                        {
                            let PaymentReceipt = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouScreenVC") as! ThankYouScreenVC
                            
                            PaymentReceipt.orderId = result["OrderId"] as! NSNumber
                            
                            PaymentReceipt.strComeFrom = "gym"

                            PaymentReceipt.strAmount = self.lblBottomPrice.text
                            PaymentReceipt.strPayMode = self.strmethod
                            
                            self.navigationController?.pushViewController(PaymentReceipt, animated: true)
                            
                        }
                           
                        else
                        {
                            let knet =  self.storyboard?.instantiateViewController(withIdentifier: "KnetVC") as! KnetVC
                            knet.orderId = result["OrderId"] as! NSNumber
                            
                            knet.strAmount = self.lblBottomPrice.text
                            knet.strpayMode = self.strmethod
                            knet.strcomeFrom = "gym"

                            self.navigationController?.pushViewController(knet, animated: true)
                        }
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    
            }
        }
        
    }
    
    func wscallforPaymentType()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wGetPaymentTypes)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let arr1 = NSMutableArray(array: result["PaymentTypes"] as! Array)
                        
                        var on1 : Bool = false
                        var on2 : Bool = false
                        var on3 : Bool = false
                        
                        for i in 0..<arr1.count
                        {
                            let dict1 = NSMutableDictionary(dictionary: arr1[i] as! Dictionary)
                            
                            if i == 0
                            {
                                if dict1["Is_Enabled"] as! NSNumber == 1
                                {
                                    on2 = true
                                }
                            }
                            if i == 1
                            {
                                if dict1["Is_Enabled"] as! NSNumber == 1
                                {
                                    on1 = true
                                }
                            }
                            if i == 2
                            {
                                if dict1["Is_Enabled"] as! NSNumber == 1
                                {
                                    on3 = true
                                }
                            }
                        }
                        
                        self.setPaymentHeight(isOn1: on1, isOn2: on2, isOn3: on3)
                        
                        
                    }
                    else
                    {
                        
                    }
                    
                    
                    
                    
            }
            
            
        }
        
    }
    
    
    
    
}
