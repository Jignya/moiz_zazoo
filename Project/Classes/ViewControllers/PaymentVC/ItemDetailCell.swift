//
//  ItemDetailCell.swift
//  Zazoo
//
//  Created by HTNaresh on 2/10/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class ItemDetailCell: UITableViewCell
{
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTotal: UILabel!

    @IBOutlet weak var lblProductamount: UILabel!
    @IBOutlet weak var lblDelCharges: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var lblProductamountData: UILabel!
    @IBOutlet weak var lblDelChargesData: UILabel!
    @IBOutlet weak var lblTotalAmountData: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
        
        

    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
