//
//  PaymentVC.swift
//  Zazoo
//
//  Created by HTNaresh on 7/9/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblItemNumber: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var lblTotalPayable: UILabel!
    @IBOutlet weak var lblHeaderPrice1: UILabel!
    @IBOutlet weak var lblCOD: UILabel!
    @IBOutlet weak var btnPayNow: UIButton!
    
    @IBOutlet weak var lblPaymentOption: UILabel!
    @IBOutlet weak var lblKnet: UILabel!
    @IBOutlet weak var lblcredit: UILabel!

    @IBOutlet weak var lblDeliverTo: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var conViewAddHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnKnet: UIButton!
    @IBOutlet weak var btnCod: UIButton!
    @IBOutlet weak var lblBottomPrice: UILabel!
    @IBOutlet weak var btnViewDetail: UIButton!
    @IBOutlet weak var btnChange: ButtonExtender!
    @IBOutlet weak var btnCreditCard: UIButton!
    
    @IBOutlet weak var viewPaymentDetail: UIView!
    @IBOutlet weak var tblItemDetail: UITableView!
    @IBOutlet weak var contblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var conviewCCHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewCODHeight: NSLayoutConstraint!
    @IBOutlet weak var conviewKnetHeight: NSLayoutConstraint!
    
    @IBOutlet weak var conPaymentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewHeaderData: UIView!
    
    @IBOutlet weak var viewTerms: UIView!
    @IBOutlet weak var viewTotal: UIView!

    @IBOutlet weak var lblTermsTitle: UILabel!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var conviewTermsHeight: NSLayoutConstraint!

    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!

    @IBOutlet weak var txtCoupanCode: UITextField!
    @IBOutlet weak var btnAplyNow: UIButton!
    @IBOutlet weak var conViewCoupanCodeHeight: NSLayoutConstraint!
    @IBOutlet weak var conViewCoupanCodeTop: NSLayoutConstraint!

    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var lblNetTotalPrice: UILabel!
    
    @IBOutlet weak var conviewTotalHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblIagree: UILabel!
    @IBOutlet weak var lblterms1: UILabel!
    @IBOutlet weak var lbland: UILabel!
    @IBOutlet weak var btnzazoo: UIButton!
    @IBOutlet weak var btnvendor: UIButton!
    @IBOutlet weak var conTerms1LabelLeading: NSLayoutConstraint!
    @IBOutlet weak var viewZazooterms: UIView!
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!






    var promotionId : NSNumber = 0

    var addressId : NSNumber = 0
    var strmethod : String!
    var strCredit : String!
    var strKnet : String!
    var strCod : String!


    var strArea : String!
    var arrParam : NSMutableArray = []
    var arrCartData : NSMutableArray = []
    var arrCartTemp : NSArray = []

    var dictAdd : NSMutableDictionary!
    var DelCharges : Float = 0.0
    var totalSum1 : Float!
    var MinOrderAmount : Float!


    var strName : String = ""
    var strEmail : String = ""
    var strMobile : String = ""
    var isClass : Bool = false
    
    var range1 : NSRange!
    var range2 : NSRange!
    
    var ZazooTerms : String!
    var vendorTerms : String!

    var strTotal : String!
    var strDiscount : String!
    var strNet : String!




    override func viewDidLoad()
    {
        super.viewDidLoad()

        strmethod = "COD"
        MinOrderAmount = 0.0
        viewZazooterms.isHidden = true
        
        self.lblBottomPrice.tag = 30
        self.lblDiscountPrice.tag = 0
        self.lblNetTotalPrice.tag = 30

        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        viewPaymentDetail.isHidden = true
        btnPayNow.isEnabled = false
        btnPayNow.backgroundColor = UIColor.lightGray
        conviewTermsHeight.constant = 0
        lblTermsTitle.text = kTerms
        btnAplyNow.setTitle(kApplynow, for: .normal)
        btnPayNow.setTitle(kPaynow, for: .normal)

        lblTerms.text = ""
        

        arrCartData = Manager.sharedInstance.arrcart
        arrCartTemp = NSArray(array: Manager.sharedInstance.arrcart)

        TotalCalculation()
        
        
        
        
        conviewKnetHeight.constant = 0
        conviewCODHeight.constant = 0
        conviewCCHeight.constant = 0
        conPaymentHeight.constant = conviewKnetHeight.constant + conviewCODHeight.constant + conviewCCHeight.constant + 50
        
        viewHeaderData.layer.borderColor = UIColor(red: 230.0 / 255.0, green: 97.0 / 255.0, blue: 88.0 / 255.0, alpha: 1).cgColor
        viewHeaderData.layer.borderWidth  = 1.0
        viewHeaderData.layer.cornerRadius = 5.0

        wscallforPaymentType()
        
        txtDesc.isHidden = true
        self.txtDesc.text = ""
        self.activity.isHidden = false
        wscallforCMS(strReq: "")
        
        strName  = Manager.sharedInstance.dictprofile["FirstName"] as? String ?? ""
        strEmail = Manager.sharedInstance.dictprofile["Email"] as? String ?? ""
        strMobile = Manager.sharedInstance.dictprofile["Mobile"] as? String ?? ""
        setLabel()
        
    }
    
    func UpdateTerms(isvendor : Bool)
    {
        if isvendor
        {
            lbland.isHidden = false
            btnvendor.isHidden = false
            lbl2.isHidden = false
            lblIagree.text = kagree
            lbland.text = kand
            lblterms1.text = kTerms.lowercased()
            conTerms1LabelLeading.constant = (UIDevice.current.userInterfaceIdiom == .pad) ? 118 : 85
            
            lblTerms.text = kagree + " " + kZazoo +  " " + kand + " " + kVendor + " " + kTerms.lowercased()

//            lblTerms.text = "I agree to zazoo and vendor's terms and conditions"
            let text = (lblTerms.text)!
            let underlineAttriString = NSMutableAttributedString(string: text)
            range1 = (text as NSString).range(of: kZazoo)
            underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
            range2 = (text as NSString).range(of: kVendor)
            underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
            underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalConstants.themeClr , range: range1)
            underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalConstants.themeClr , range: range2)
            
            lblTerms.attributedText = underlineAttriString
        }
        else
        {
            
            lbland.isHidden = true
            btnvendor.isHidden = true
            lbl2.isHidden = true

            
            lblIagree.text = kagree
            lbland.text = kand
            lblterms1.text = kTerms.lowercased()
            conTerms1LabelLeading.constant = 5

            
            lblTerms.text = kagree + " " + "zazoo's" + " " + kTerms.lowercased()

            let text = (lblTerms.text)!
            let underlineAttriString = NSMutableAttributedString(string: text)
            range1 = (text as NSString).range(of: "zazoo's")
            underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
            range2 = (text as NSString).range(of: "vendor's")
            underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
            underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalConstants.themeClr , range: range1)
            underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalConstants.themeClr , range: range2)
            
            lblTerms.attributedText = underlineAttriString
        }

        
        
        lblTerms.isUserInteractionEnabled = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapLabel(gesture:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        lblTerms.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    
    @objc func tapLabel(gesture: UITapGestureRecognizer)
    {
        
        if gesture.didTapAttributedTextInLabel(label: lblTerms, inRange: range1)
        {
            print("Zazoo")
            openTermsPage()
            
            let attritemutable = NSMutableAttributedString(attributedString: ZazooTerms.html2AttributedString!)
            let font = UIFont(name: GlobalConstants.kEnglishBold , size: (UIDevice.current.userInterfaceIdiom == .pad) ? CGFloat(25) : CGFloat(17))
            attritemutable.setFontFace(font: font!, color: UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1))
            
            self.txtDesc.attributedText = attritemutable

        }
        else if gesture.didTapAttributedTextInLabel(label: lblTerms, inRange: range2)
        {
            print("Vendor")
            openTermsPage()
//            self.txtDesc.text = vendorTerms.html2String
            
            let attritemutable = NSMutableAttributedString(attributedString: vendorTerms.html2AttributedString!)
            let font = UIFont(name: GlobalConstants.kEnglishBold , size: (UIDevice.current.userInterfaceIdiom == .pad) ? CGFloat(25) : CGFloat(17))
            attritemutable.setFontFace(font: font!, color: UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1))
            
            self.txtDesc.attributedText = attritemutable
        }
        else
        {
            print("Tapped none")
        }
    }


    func setLabel()
    {
       lblCOD.text = kcod
       lblKnet.text = kKnetPayment
       lblcredit.text = kCredit
        lblDelivery.text = "" // kStandard
        lblDeliverTo.text = kDeliverTo
    
       btnChange.setTitle(kChange.uppercased(), for: .normal)
       btnViewDetail.setTitle(kviewDetails, for: .normal)
        lblPaymentOption.text = kPaymentoption
        lblTotalPayable.text = kTotalPayable
        lblTitle.text = kPayment
        

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.removeTabbar()

//        displayAddress()
        
        Manager.sharedInstance.addTabBar2(self, tab: "4")
        
        if UserDefaults.standard.value(forKey: "userid") == nil
        {
           conViewCoupanCodeTop.constant = 0
           conViewCoupanCodeHeight.constant = 0
        }
    }
    
    func displayAddress()
    {
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            let strAdd = Manager.sharedInstance.displayAddress(dictAdd)
            
            lblAddress.text =  strAdd

        }
        else
        {
            let strAdd = Manager.sharedInstance.displayAddress11(dictAdd)

            lblAddress.text = strArea + "\n" +  strAdd

        }
//        let strAdd = strArea + "\n" + "Block : \(dictAdd["Block"]!), Street : \(dictAdd["Street"]!), House No : \(dictAdd["HouseNo"]!), Floor : \(dictAdd["Floor"]!), Appartment : \(dictAdd["Apparment"]!)" + "\n" + "Mobile : " + strMobile
//
//        lblAddress.text = strAdd
        
        let sizeThatFitsTextView = lblAddress.sizeThatFits(CGSize(width: lblAddress.frame.size.width, height: CGFloat(MAXFLOAT)))
        conViewAddHeight.constant = sizeThatFitsTextView.height + 75
        self.lblAddress.superview?.updateConstraintsIfNeeded()
        self.lblAddress.superview?.updateFocusIfNeeded()
        
    }
    
    func setPaymentHeight(isOn1:Bool ,isOn2:Bool, isOn3:Bool)
    {
        var iscod = isOn2
        if isClass
        {
            iscod = false
        }
        
        if iscod == true
        {
            strmethod = "COD"
            btnCod.isSelected = true
            btnPayNow.setTitle(kOrder, for: .normal)
        }
        else
        {
            if isOn1 == true
            {
                strmethod = "KNET"
                btnKnet.isSelected = true
            }
            else
            {
                strmethod = "CREDITCARD"
                btnCreditCard.isSelected = true
            }
        }
        
        
        
        if isOn1 == true
        {
            conviewKnetHeight.constant = 50
        }
        if iscod == true
        {
            conviewCODHeight.constant = 50
        }
        if isOn3 == true
        {
            conviewCCHeight.constant = 50
        }
        

        conPaymentHeight.constant = conviewKnetHeight.constant + conviewCODHeight.constant + conviewCCHeight.constant + 50
    }

    
    func TotalCalculation()
    {
        arrParam = self.arrCartData
        let arrParam1 = arrParam as! [[String : Any]]
        
        var sumArray = [String]()
        var i : Int = 0
        
        for item  in arrParam1
        {
//            if item["Qty"] is NSNumber
//            {
//                if let n = item["Original_Price"] as? NSNumber
//                {
//                    let f = n.floatValue
//
//                    let qty = item["Qty"] as! NSNumber
//                    let rate = item["Original_Price"] as! NSNumber
//
//                    let total = Float(qty) * Float(rate)
//
//                    let amount = String(format: "%.3f",total)
//                    sumArray.insert(amount, at: i)
//                    i = i + 1
//                }
//            }
//            else
//            {
            
            if item["Cart_Item_Type"] as? NSNumber != 3
            {

                isClass = true

                if  item["Item_Type_Id"] as? NSNumber == 5
                {
                    let arrAddons = item["AddOns"] as? [[String:Any]]
                    var  addon_price : Float = 0.00
                    for i in 0..<arrAddons!.count
                    {
                        let dict = arrAddons![i]
                        if addon_price == 0.00
                        {
                            addon_price  =  (dict["Amount"]  as? NSNumber)?.floatValue ?? 0
                        }
                        else
                        {
                            addon_price = addon_price + ((dict["Amount"]  as? NSNumber ?? 0)?.floatValue)!
                        }
                        
                    }
                    
                    let total = (item["Net_Amount"] as? NSNumber ?? 0)!.floatValue
                    let amount = String(format: "%.3f",total)
                    sumArray.insert(amount, at: i)
                }
                else
                {
                    let rate = item["Net_Amount"] as? NSNumber ?? 0
                    let total = Float(rate)
                    let amount = String(format: "%.3f",total)
                    sumArray.insert(amount, at: i)
                }
            }
               
            i = i + 1
//            }
            
        }
        
        var force1 = [Float]()
        
        for item in sumArray
        {
            force1.append((item as NSString).floatValue)
        }
        
        var totalSum = force1.reduce(0, +)
        
        totalSum1 = totalSum
        
//        totalSum = totalSum + DelCharges
        
        strNet = String(format: "%.3f", totalSum)
        lblHeaderPrice1.text = String(format: "%.3f %@", totalSum , kCurrency)
        lblBottomPrice.text = String(format: "%@ : %.3f %@",kTotalAmount, totalSum , kCurrency)
        lblItemNumber.text = String(format: "%d %@ - %@", Manager.sharedInstance.CartCount,kItems,kviewDetails)
        
//        tblItemDetail.reloadData()
        
        self.TotalCalculationAfterPromoCode(isProductAvailable: false)
        
    }
    
    func TotalCalculationAfterPromoCode(isProductAvailable:Bool)
    {
        arrParam = NSMutableArray(array: self.arrCartData)
        let arrParam1 = arrParam as! [[String : Any]]
        
        var sumArray = [String]()
        var sumDiscountArray = [String]()

        var i : Int = 0
        
        for item  in arrParam1
        {
            if  item["Cart_Item_Type"] as? NSNumber != 2
            {
                if item["Cart_Item_Type"] as? NSNumber == 3
                {
                    let total =  (item["Net_Amount"] as? NSNumber ?? 0)!.floatValue
                    let amount = String(format: "%.3f",total)
                    sumArray.insert(amount, at: i)
                }
                else
                {
                    let total =  (item["Original_Price"] as? NSNumber ?? 0)!.floatValue
                    let amount = String(format: "%.3f",total)
                    sumArray.insert(amount, at: i)

                }
                
                let total1 = (item["Discount_Amount"] as? NSNumber ?? 0)!.floatValue
                let amount1 = String(format: "%.3f",total1)
                sumDiscountArray.insert(amount1, at: i)
            }
            else
            {
                sumArray.insert("0", at: i)
                sumDiscountArray.insert("0", at: i)
            }
            
            i = i + 1
        }
        
        var force1 = [Float]()
        
        for item in sumArray
        {
            force1.append((item as NSString).floatValue)
        }
        
        let totalSum = force1.reduce(0, +)
        
        var force2 = [Float]()

        for item1 in sumDiscountArray
        {
            force2.append((item1 as NSString).floatValue)
        }
        
        let DiscountSum = force2.reduce(0, +)
        
        totalSum1 = totalSum
        
        let netAmount = self.totalSum1 - DiscountSum
        
        
        self.strNet = String(format: "%.3f", netAmount)
        self.strTotal =  String(format: "%.3f", self.totalSum1)
        self.strDiscount =  String(format: "%.3f", DiscountSum)
        
        if isProductAvailable
        {
            self.conviewTotalHeight.constant = 80
            self.lblBottomPrice.text = String(format: "%@ : %.3f %@", kDiscountAmount , DiscountSum, kCurrency)
            self.lblDiscountPrice.text = String(format: "%@ : %.3f %@",kTotalAmount, self.totalSum1 , kCurrency)
            self.lblNetTotalPrice.text = String(format: "%@ : %.3f %@",kTotalPayable, netAmount , kCurrency)
            self.lblHeaderPrice1.text = String(format: "%.3f %@",netAmount , kCurrency)
        }
       
        tblItemDetail.reloadData()

        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Table method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrCartData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        var cell:ItemDetailCell!
        
        if indexPath.row == arrCartData.count
        {
            let cellReuseIdentifier = "ItemDetailCell1"
            cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as? ItemDetailCell
            
            cell.lblProductamountData.text = String(format: "%.3f %@", totalSum1 , kCurrency)
            
            cell.lblDelChargesData.text = String(format: "%.3f %@", DelCharges , kCurrency)

            cell.lblTotalAmountData.text = String(format: "%@ %@", strNet, kCurrency)
            
            cell.lblProductamount.text = kproductAmt
            cell.lblTotalAmount.text = kTotalAmount
            cell.lblDelCharges.text = kDelCharges

        }
        else
        {
            let cellReuseIdentifier = "ItemDetailCell"
            cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as? ItemDetailCell
            
            let dict = NSMutableDictionary(dictionary: arrCartData[indexPath.row] as! Dictionary)
            
            
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                cell.lblProductName.text = dict["Item_Name"] as? String
            }
            else
            {
                
                cell.lblProductName.text = dict["Item_Name"] as? String
            }
            
            
            
//            if dict["Qty"] is NSNumber
//            {
//                cell.lblQty.text = String(format: "%@",(dict["Qty"] as? NSNumber)!)
//                if let n = dict.value(forKey: "Original_Price") as? NSNumber
//                {
//                    let f = n.floatValue
//                    cell.lblPrice.text =  String(format: "%.3f %@",f , kCurrency)
//
//                    let qty = dict["Qty"] as! NSNumber
//                    let rate = dict["Original_Price"] as! NSNumber
//
//                    let total = Float(qty) * Float(rate)
//
//                    cell.lblTotal.text =  String(format: "%.3f %@",total , kCurrency)
//                }
//            }
//            else
//            {
            let arrChildren = NSMutableArray(array: (dict["Children"] as? Array)!)
            
                if  arrChildren.count > 0
                {
                    cell.lblPrice.text = Manager.sharedInstance.getCommaSeparatedValuefromArray(arrChildren, forkey: "Children_Name")
                }
                else
                {
                    cell.lblPrice.text = ""
                }
            
//                if  dict["Children_Name"] as? String != nil
//                {
//                    cell.lblPrice.text = dict["Children_Name"] as? String
//                }
//                else
//                {
//                    cell.lblPrice.text = ""
//                }
            
                
                if  dict["Item_Type_Id"] as? NSNumber == 5
                {
                    let arrAddons = dict["AddOns"] as? [[String:Any]]
                    var  addon_price : Float = 0.00
                    for i in 0..<arrAddons!.count
                    {
                        let dict = arrAddons![i]
                        if addon_price == 0.00
                        {
                            addon_price  =  (dict["Amount"]  as? NSNumber)?.floatValue ?? 0
                        }
                        else
                        {
                            addon_price = addon_price + ((dict["Amount"]  as? NSNumber ?? 0)?.floatValue)!
                        }
                        
                    }
                    
                    cell.lblTotal.numberOfLines = 0
                    
                    if dict["Discount_Percent"] as? NSNumber != nil
                    {
                        var discountedAmount : Float = 0.00
                        
                        let strOriginalPrice = String(format: "%@ %.3f %@ ",kPrice, (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue,kCurrency)
                        
                        let strVendorAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue
                        
                        if dict["Discount_Percent"] as? NSNumber == 0 && dict["Discount_Amount"] as? NSNumber == 0
                        {
                            cell.lblTotal.text = String(format: "%.3f %@", ((dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue),kCurrency)

                        }
                        else
                        {
                            if dict["Discount_Percent"] as? NSNumber != 0
                            {
                                discountedAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue - (strVendorAmount * (dict["Discount_Percent"] as! NSNumber).floatValue)/100
                            }
                                
                            else
                            {
                                discountedAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue - (strVendorAmount - (dict["Discount_Amount"] as? NSNumber ?? 0).floatValue)
                            }
                            
                            
                            let attribute = NSMutableAttributedString(string:  String(format: "%@ %.3f %@ \n",kPrice, discountedAmount,kCurrency))
                            
                            let result = NSMutableAttributedString()
                            result.append(attribute)
                            result.append(Manager.sharedInstance.StrikeThroughString(str: strOriginalPrice))
                            
                            cell.lblTotal.attributedText = result

                        }

                        
                    }
                    else if dict["Discount_Amount"] as? NSNumber != 0
                    {
                        let strOriginalPrice = String(format: "%.3f %@ ", (dict["Original_Price"] as? NSNumber ?? 0)!.floatValue,kCurrency)
                        
                        let strNetAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue
                        
                        let attribute = NSMutableAttributedString(string:  String(format: "%.3f %@ \n", strNetAmount,kCurrency))
                        
                        let result = NSMutableAttributedString()
                        result.append(attribute)
                        result.append(Manager.sharedInstance.StrikeThroughString(str: strOriginalPrice))
                        
                        cell.lblTotal.attributedText = result
                    }
                    else
                    {
                        cell.lblTotal.text = String(format: "%.3f %@", ((dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue),kCurrency)
                    }
                }
                else
                {
                    cell.lblTotal.numberOfLines = 0
                    
                    if dict["Discount_Percent"] as? NSNumber != nil
                    {
                        var discountedAmount : Float = 0.00
                        
                        let strOriginalPrice = String(format: "%.3f %@ ", (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue,kCurrency)

                        let strVendorAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue
                        
                        if dict["Discount_Percent"] as? NSNumber == 0 && dict["Discount_Amount"] as? NSNumber == 0
                        {
                            cell.lblTotal.text = String(format: "%.3f %@", ((dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue),kCurrency)
                            
                        }
                        else
                        {
                            if dict["Discount_Percent"] as? NSNumber != 0
                            {
                                discountedAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue - (strVendorAmount * (dict["Discount_Percent"] as? NSNumber ?? 0).floatValue)/100
                            }
                                
                            else
                            {
                                discountedAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue - (strVendorAmount - (dict["Discount_Amount"] as? NSNumber ?? 0).floatValue)
                            }
                            
                            
                            let attribute = NSMutableAttributedString(string:  String(format: "%.3f %@ \n", discountedAmount,kCurrency))
                            
                            let result = NSMutableAttributedString()
                            result.append(attribute)
                            result.append(Manager.sharedInstance.StrikeThroughString(str: strOriginalPrice))
                            
                            cell.lblTotal.attributedText = result
                            
                        }
                    }
                    else if dict["Discount_Amount"] as? NSNumber != 0
                    {
                        let strOriginalPrice = String(format: "%.3f %@ ", (dict["Original_Price"] as? NSNumber ?? 0)!.floatValue,kCurrency)
                        
                        let strNetAmount = (dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue
                        
                        let attribute = NSMutableAttributedString(string:  String(format: "%.3f %@ \n", strNetAmount,kCurrency))
                        
                        let result = NSMutableAttributedString()
                        result.append(attribute)
                        result.append(Manager.sharedInstance.StrikeThroughString(str: strOriginalPrice))
                        
                        cell.lblTotal.attributedText = result
                    }
                    else
                    {
                        cell.lblTotal.text = String(format: "%.3f %@", ((dict["Net_Amount"] as? NSNumber ?? 0)!.floatValue),kCurrency)
                    }
                }
                

//            }
            
           
            
            
        }
        
        
        let Height = tblItemDetail.contentSize.height
        
        if Height > self.view.frame.size.height - 100
        {
            contblHeight.constant = self.view.frame.size.height - 40
            self.tblItemDetail.isScrollEnabled = true
        }
        else
        {
            contblHeight.constant = Height
            self.tblItemDetail.isScrollEnabled = false

        }
        
        tblItemDetail.updateConstraintsIfNeeded()
        tblItemDetail.layoutIfNeeded()
        
        return cell

       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == arrCartData.count
        {
            return 60
        }
        else
        {
            let dict = NSMutableDictionary(dictionary: arrCartData[indexPath.row] as! Dictionary)
            
            if dict["Cart_Item_Type"] as? NSNumber == 2
            {
                return 0
            }
            
            var strData : String = ""

            if UserDefaults.standard.bool(forKey: "arabic")
            {
                strData = dict["Item_Name"] is String ?  (dict["Item_Name"] as? String)! : ""
            }
            else
            {
                strData = (dict["Item_Name"] as? String)!
            }
            
            let constraint = CGSize(width: (self.view.frame.size.width / 3) - 50, height: 50000.0)
            let fontText: UIFont = (UIFont(name:GlobalConstants.kEnglishRegular, size:16))!
            let size: CGRect = strData.boundingRect(with: constraint, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: fontText], context: nil)
            var height1: CGFloat
            height1 = size.size.height
            return height1 + 30
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: self.tblItemDetail.frame.size.width, height: 40))
        view1.backgroundColor = UIColor(red: 230/255, green: 97/255, blue: 88/255, alpha: 1.0)
        
        view1.isOpaque = true
        
        var lbl1 : UILabel!
        var lbl2 : UILabel!
        var lbl3 : UILabel!
        var lbl4 : UILabel!


        let width = (view1.frame.size.width)/3
        
        lbl1 = UILabel(frame: CGRect(x: 15, y: 0, width: width  + 13, height: 40))
        lbl2 = UILabel(frame: CGRect(x: lbl1.frame.size.width, y: 0, width:0, height: 40)) // width - 39
        lbl3 = UILabel(frame: CGRect(x: lbl2.frame.origin.x +  lbl2.frame.size.width - 13, y: 0, width: width, height: 40))
        lbl4 = UILabel(frame: CGRect(x: lbl3.frame.origin.x +  lbl3.frame.size.width - 30, y: 0, width: width - 2, height: 40))
        
        var fontSize : CGFloat!
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            fontSize = 20
        }
        else
        {
            fontSize = 15
        }

        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            view1.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            lbl1.font =  UIFont(name: GlobalConstants.kArabicBold, size: fontSize)
            
            lbl2.font =  UIFont(name: GlobalConstants.kArabicBold, size: fontSize)
            
            lbl3.font =  UIFont(name: GlobalConstants.kArabicBold, size: fontSize)
            
            lbl4.font =  UIFont(name: GlobalConstants.kArabicBold, size: fontSize)
            
            lbl1.textAlignment = .right
            lbl2.textAlignment = .center
            lbl3.textAlignment = .center
            lbl4.textAlignment = .left

            
            lbl1.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl2.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl3.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl4.transform = CGAffineTransform(scaleX: -1, y: 1)

        }
        else
        {
           

            
            lbl1.font =  UIFont(name: GlobalConstants.kEnglishBold, size:fontSize)
            
            lbl2.font =  UIFont(name: GlobalConstants.kEnglishBold, size: fontSize)
            
            lbl3.font =  UIFont(name: GlobalConstants.kEnglishBold, size: fontSize)
            
            lbl4.font =  UIFont(name: GlobalConstants.kEnglishBold, size: fontSize)

            lbl2.textAlignment = .center
            lbl3.textAlignment = .center
            lbl4.textAlignment = .right
            
        }
        
        lbl1.text = kItemname
        lbl2.text =  "" //kQty
        lbl3.text = kChildName
        lbl4.text = kTotal

//        lbl1.text = "1"
//        lbl2.text = "2"
//        lbl3.text = "3"
//        lbl4.text = kTotal

       


        lbl1.textColor = UIColor.white
        lbl2.textColor = UIColor.white
        lbl3.textColor = UIColor.white
        lbl4.textColor = UIColor.white


        view1.addSubview(lbl1)
        view1.addSubview(lbl2)
        view1.addSubview(lbl3)
        view1.addSubview(lbl4)

        return view1
    }
    
    //MARK:- Button Click
    
    func openTermsPage()
    {
        txtDesc.setContentOffset(.zero, animated: false)
        
        self.view.bringSubviewToFront(viewTerms)
        
        var TopPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            TopPadding = (window?.safeAreaInsets.top)!
        }
        
        self.conviewTermsHeight.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conviewTermsHeight.constant = self.view.frame.size.height - TopPadding
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }

    
//    @IBAction func openTermsPage(_ sender: Any)
//    {
//        txtDesc.setContentOffset(.zero, animated: false)
//
//        self.view.bringSubview(toFront: viewTerms)
//
//        self.conviewTermsHeight.constant = 0
//        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
//                       animations: {
//                        self.conviewTermsHeight.constant = self.view.frame.size.height
//                        self.view.layoutIfNeeded()
//        }, completion: nil)
//    }
    
    @IBAction func btnzazooClick(_ sender: Any)
    {
        openTermsPage()
        
        let attritemutable = NSMutableAttributedString(attributedString: ZazooTerms.html2AttributedString!)
        let font = UIFont(name: GlobalConstants.kEnglishBold , size: CGFloat(17))
        attritemutable.setFontFace(font: font!, color: UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = UserDefaults.standard.bool(forKey: "arabic") ? .right : .left
        
        attritemutable.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attritemutable.length))
        
        self.txtDesc.attributedText = attritemutable
    }
    
    @IBAction func btnvendorClick(_ sender: Any)
    {
        openTermsPage()
        
        let attritemutable = NSMutableAttributedString(attributedString: vendorTerms.html2AttributedString!)
        let font = UIFont(name: GlobalConstants.kEnglishBold , size: CGFloat(17))
        attritemutable.setFontFace(font: font!, color: UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 71.0/255.0, alpha: 1))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = UserDefaults.standard.bool(forKey: "arabic") ? .right : .left
        
        attritemutable.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attritemutable.length))

        
        self.txtDesc.attributedText = attritemutable
    }
    
    @IBAction func btncheckClick(_ sender: Any)
    {
      let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        if btn.isSelected
        {
            btnPayNow.isEnabled = true
            btnPayNow.backgroundColor = GlobalConstants.themeClr
        }
        else
        {
            btnPayNow.isEnabled = false
            btnPayNow.backgroundColor = UIColor.lightGray
        }
    }
    
    @IBAction func btncloseTermsClick(_ sender: Any)
    {
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.conviewTermsHeight.constant = 0
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnCloseDetailViewClick(_ sender: Any)
    {
        viewPaymentDetail.isHidden = true
        
    }
    @IBAction func btnBackClick(_ sender: Any)
    {
        if ((self.navigationController?.viewControllers) != nil)
        {
            let array = self.navigationController?.viewControllers
                as! Array<Any>
            for k in (0 ..< array.count)
            {
                let controller: UIViewController? = (array[k] as! UIViewController)
                if (controller is CartVC)
                {
//                    let adress = controller as! AddressListVC
                    self.navigationController?.popToViewController(controller!, animated: true)
                    
                    break
                }
            }
        }
    }
    @IBAction func btnPayNowClick(_ sender: Any)
    {
       wscallforSaveOrder()
    }
    
    @IBAction func btnViewDetailClick(_ sender: Any)
    {
        self.view.endEditing(true)

        viewPaymentDetail.isHidden = false
        self.view.bringSubviewToFront(viewPaymentDetail)
    }
    
    @IBAction func btnCodClick(_ sender: Any)
    {
        strmethod = strCod
        btnPayNow.setTitle(kOrder, for: .normal)

       btnCod.isSelected = true
       btnKnet.isSelected = false
       btnCreditCard.isSelected = false

    }
    
    @IBAction func btnKnetClick(_ sender: Any)
    {
        strmethod = strKnet

        btnPayNow.setTitle(kPaynow, for: .normal)
        btnKnet.isSelected = true
        btnCod.isSelected = false
        btnCreditCard.isSelected = false

    }
    

    @IBAction func btnCCClick(_ sender: Any)
    {
        strmethod = strCredit
        btnPayNow.setTitle(kPaynow, for: .normal)
        
        btnCreditCard.isSelected = true
        btnCod.isSelected = false
        btnKnet.isSelected = false
    }
    @IBAction func btnChangeClick(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            let add =  self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
            add.strcomeFrom = "payment"
            self.navigationController?.pushViewController(add, animated: true)
        }
        else
        {
          self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnAplyNowClick(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if txtCoupanCode.text == ""
        {
            let alertController = UIAlertController(title:kWarningEn, message:kEnterCode, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            }
            
            alertController.addAction(okAction)
            alertController.show()
        }
        else
        {
            let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wValidPromoCode)
            
            let dictPara:NSDictionary!
            
//            var arrVendor = [[String:Any]]()
//
//            for i in 0..<arrCartData.count
//            {
//                let dict = arrCartData[i] as! [String : Any]
//
//                var strAmount = (dict["Original_Price"] as? NSNumber)!.floatValue
//
//                for j in 0..<arrVendor.count
//                {
//                    let dicVendor = arrVendor[j]
//                    if dict["Vendor_Id"] as? NSNumber == dicVendor["Vendor_Id"] as? NSNumber
//                    {
//                       strAmount = strAmount + (dicVendor["Vendor_Order_Amount"] as? NSNumber)!.floatValue
//                        arrVendor.remove(at: j)
//                        break
//                    }
//                }
//
//                let dict1 = ["Vendor_Id":dict["Vendor_Id"] , "Vendor_Order_Amount":strAmount]
//                arrVendor.append(dict1 as [String : Any])
//            }
//
            
            dictPara = ["Promo_Code":self.txtCoupanCode.text!]
            
            
            WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
                
                DispatchQueue.main.async
                    {
                        
                        print(result)
                        let strStatus = result["Message"] as? String
                        
                        if strStatus == "Success"
                        {
                            self.promotionId = (result["Promotion_Id"] as? NSNumber ?? 0)!
                            self.MinOrderAmount = (result["Min_Order_Amount"] as? Float ?? 0.0)!

                            
                            var isProductAvailable : Bool = false
                            var availableTotalAmountForCode : Float = 0.0
                            

                            
                            for k in 0..<self.arrCartData.count
                            {
                                var dicVendor = self.arrCartData[k] as! [String:Any]
                                
                                dicVendor["Promo_Code"] = ""
                                
                                if result["Vendor_Id"] as? NSNumber == dicVendor["Vendor_Id"] as? NSNumber
                                {
                                    let arrPromotionItems = result["PromotionItems"] as! [[String:Any]]
                                    
                                    for h in 0..<arrPromotionItems.count
                                    {
                                        let dicItems = arrPromotionItems[h]
                                        if dicItems["Item_Id"] as? NSNumber == dicVendor["Item_Id"] as? NSNumber && dicItems["Item_Type_Id"] as? NSNumber == dicVendor["Item_Type_Id"] as? NSNumber
                                        {
                                            let strOriginalAmount = (dicVendor["Original_Price"] as? NSNumber ?? 0)!.floatValue
                                            var discountedAmount : Float = 0.00
                                            var NetAmount : Float = 0.00
                                            
                                            availableTotalAmountForCode = availableTotalAmountForCode + strOriginalAmount


                                            if result["Discount_Percent"] as? NSNumber != 0
                                            {
                                               discountedAmount = (strOriginalAmount * (result["Discount_Percent"] as? NSNumber ?? 0).floatValue)/100
                                                
                                                NetAmount = strOriginalAmount - discountedAmount

                                            }
                                            else
                                            {
                                                discountedAmount =  (result["Discount_Amount"] as? NSNumber ?? 0).floatValue

//                                                discountedAmount = strOriginalAmount - (result["Discount_Amount"] as! NSNumber).floatValue
                                                
                                                NetAmount = strOriginalAmount - discountedAmount
                                            }
                                            
                                            dicVendor["Discount_Amount"] = discountedAmount
                                            
                                            dicVendor["Net_Amount"] = NetAmount
                                            dicVendor["Promotion_Id"] = result["Promotion_Id"] as? NSNumber
                                            dicVendor["Promo_Code"] = self.txtCoupanCode.text!

                                            isProductAvailable = true

                                        }

                                    }
                                    

                                }
                                
                                self.arrCartData.replaceObject(at: k, with: dicVendor)

                            }
                            
                            var Errormsg = kcodeAlert
                            
                            if availableTotalAmountForCode < self.MinOrderAmount && isProductAvailable
                            {
                               isProductAvailable = false
                                Errormsg = String(format: "%@ %.3f",kcodeAmountAlert, self.MinOrderAmount)
                                self.arrCartData = NSMutableArray(array: self.arrCartTemp)
//                                self.tblItemDetail.reloadData()

                            }
                            
                            self.TotalCalculationAfterPromoCode(isProductAvailable: isProductAvailable)

                            if isProductAvailable
                            {
                            self.txtCoupanCode.isUserInteractionEnabled = false
                                self.btnAplyNow.isEnabled = false
                                self.btnAplyNow.backgroundColor = UIColor.lightGray

                                
                                self.lblBottomPrice.tag = 0
                                self.lblDiscountPrice.tag = 30
                                self.lblNetTotalPrice.tag = 30
                                
                                Manager.sharedInstance.setFontFamily("", for: self.viewTotal, andSubViews: true)
                                
                                self.lblBottomPrice.font = UIFont(name: GlobalConstants.kEnglishRegular, size: 16)
                            }
                            else
                            {
                                let alertController = UIAlertController(title:kWarningEn, message:Errormsg, preferredStyle: .alert)
                                
                                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                                    UIAlertAction in
                                    NSLog("OK Pressed")
                                    
                                    self.promotionId = 0
                                    self.txtCoupanCode.text = ""
                                }
                                
                                alertController.addAction(okAction)
                                alertController.show()
                            }

                            
                            
                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                            
                            let okAction = UIAlertAction(title:kOkEn, style: .default) {
                                UIAlertAction in
                                NSLog("OK Pressed")
                            }
                            
                            alertController.addAction(okAction)
                            alertController.show()
                        }
                        
                }
            }
            
        }
        
    }
        
    
    
    
    //MARK: webservice

    func wscallforSaveOrder()
    {
        let arrOrder : NSMutableArray = []
        
        for i in 0..<arrParam.count
        {

            let dictorder : NSMutableDictionary = [:]
            
            let dict = arrParam[i] as! [String:Any]
            
            var ChildrenId :String = ""

            let arrChildren = NSMutableArray(array: (dict["Children"] as? Array)!)
            
            if  arrChildren.count > 0
            {
               ChildrenId = Manager.sharedInstance.getCommaSeparatedValuefromArray(arrChildren, forkey: "Children_Id")
                dictorder.setValue(ChildrenId.components(separatedBy: ","), forKey: "ChildrenIds")
            }
            else
            {
                dictorder.setValue([], forKey: "ChildrenIds")
            }
            
            dictorder.setValue(dict["Item_Id"], forKey: "Prod_Id")
            dictorder.setValue(dict["Qty"], forKey: "Qty")
            dictorder.setValue(dict["Rate"], forKey: "Rate")
            dictorder.setValue(dict["Amount"], forKey: "Amount")
            dictorder.setValue(dict["Custom_Note"], forKey: "Custom_Note")
            dictorder.setValue(dict["Size_Id"], forKey: "Size_Id")
            dictorder.setValue(dict["Deal_Id"], forKey: "Deal_Id")
            dictorder.setValue(dict["Price_Detail_Id"], forKey: "Price_Detail_Id")
            dictorder.setValue(dict["Item_Type_Id"], forKey: "Item_Type_Id")
            
            dictorder.setValue(dict["Discount_Amount"], forKey: "Discount_Amount")
            dictorder.setValue(dict["Net_Amount"], forKey: "Net_Amount")
            dictorder.setValue(dict["Promotion_Id"], forKey: "Promotion_Id")
            dictorder.setValue(dict["Promo_Code"], forKey: "Promo_Code")


            
            let dateArray = NSMutableArray(array: dict["SelectedDates"] as! Array)
            
            if dateArray.count > 0
            {
                let CalId = Manager.sharedInstance.getCommaSeparatedValuefromArray(dateArray, forkey: "Calender_Id")
                
                let arr1 = CalId.components(separatedBy: ",")
                dictorder.setValue(arr1, forKey: "CalenderIds")
            }
            else
            {
                let arr1 : NSMutableArray = []
                dictorder.setValue(arr1, forKey: "CalenderIds")
            }
            
            
            if dict["AddOns"] is [[String:Any]]
            {
                var AddOnArray = dict["AddOns"] as! [[String:Any]]
                for i in 0..<AddOnArray.count
                {
                    var dict = AddOnArray[i]
                    dict.removeValue(forKey: "Add_On_Name")
                    AddOnArray[i] = dict
                }
                
                dictorder.setValue(AddOnArray, forKey: "AddOns")
            }
            else
            {
                dictorder.setValue([], forKey: "AddOns")

            }
            
            if dict["Cart_Item_Type"] as? NSNumber != 3
            {
                arrOrder.add(dictorder)
            }
        }
        
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wSaveOrder)

        let dictPara:NSDictionary!
        
        var userId : String!
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
            
            dictPara = ["AreaId":"","UserId":userId,"FirstName":strName,"SecondName":"","LastName":"","Mobile":strMobile,"Email":strEmail,"Payment_Type":strmethod,"DeliveryCharges":0,"Total_Amount":strTotal,"UserAddressId":addressId,"Order_Type":"1","OrderDetails":arrOrder,"Block":"","Street":"","HouseNo":"","ExtraDirection":"","Avenue":"","Building":"","Floor":"","Appartment":"" , "Discount_Amount":strDiscount,"Net_Amount":strNet,"Promotion_Id":promotionId,"Promo_Code":self.txtCoupanCode.text!]
        }
        else
        {
           userId = "0"
            dictPara = ["UserId":userId,"Total_Amount":strTotal,"Payment_Type":strmethod,"DeliveryCharges":0,"AreaId":dictAdd["AreaId"] as Any,"FirstName":strName,"SecondName":"","LastName":"","Block":dictAdd["Block"] as Any,"Street":dictAdd["Street"] as Any,"HouseNo":dictAdd["HouseNo"] as Any,"ExtraDirection":dictAdd["ExtraDirection"] as Any,"Avenue":dictAdd["Avenue"] as Any,"Building":dictAdd["Building"] as Any,"Floor":dictAdd["Floor"] as Any,"Appartment":dictAdd["Apparment"] as Any,"Mobile":strMobile,"Email":strEmail,"Order_Type":"1","OrderDetails":arrOrder, "Discount_Amount":strDiscount,"Net_Amount":strNet,"Promotion_Id":0,"Promo_Code":""]

        }
        
        print(dictPara)
        
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    
                     print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        Manager.sharedInstance.wscallforRemoveCart()
                        if self.strmethod == "COD"
                        {
                            let PaymentReceipt = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouScreenVC") as! ThankYouScreenVC
                            PaymentReceipt.orderId = result["OrderId"] as? NSNumber
                            
                            PaymentReceipt.strComeFrom = "normal"

                            PaymentReceipt.strAmount = self.lblBottomPrice.text
                            
                            PaymentReceipt.strPayMode = kCash   //self.strmethod

                            self.navigationController?.pushViewController(PaymentReceipt, animated: true)

                        }
//                        {
//                            let strOrderSuccess = String(format: "%@ %@", aOrderPlaced,result["OrderId"] as! NSNumber)
//
//                            let alertController = UIAlertController(title:"", message:strOrderSuccess, preferredStyle: .alert)
//
//                            // Create the actions
//                            let okAction = UIAlertAction(title:kOkEn, style: .default) {
//                                UIAlertAction in
//                                NSLog("OK Pressed")
//
//                            Manager.sharedInstance.DeleteSavedDataFromTextFile(fileName: "order.txt")
//                            Manager.sharedInstance.DeleteSavedDataFromTextFile(fileName: "cart.txt")
//
//                                Manager.sharedInstance.CartCount = "0"
//
//                                let array = self.navigationController?.viewControllers
//                                    as! Array<Any>
//                                for k in (0 ..< array.count)
//                                {
//                                    let controller: UIViewController? = (array[k] as! UIViewController)
//                                    if (controller is HomeVC)
//                                    {
//                                    self.navigationController?.popToViewController(controller!, animated: true)
//
//                                        break
//                                    }
//                                }
//                            }
//
//                            // Add the actions
//                            alertController.addAction(okAction)
//
//                            // Present the controller
//                            alertController.show()
//                        }
                        else
                        {
                            let knet =  self.storyboard?.instantiateViewController(withIdentifier: "KnetVC") as! KnetVC
                            knet.orderId = result["OrderId"] as? NSNumber
                            
                            knet.strAmount = self.lblBottomPrice.text
                            knet.strpayMode = self.strmethod
                            
                            knet.strcomeFrom = "normal"

                            self.navigationController?.pushViewController(knet, animated: true)
                        }
                    }
                    else
                    {
                        let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
                        
                        // Create the actions
                        let okAction = UIAlertAction(title:kOkEn, style: .default) {
                            UIAlertAction in
                            NSLog("OK Pressed")
                        }
                        
                        // Add the actions
                        alertController.addAction(okAction)
                        
                        // Present the controller
                        alertController.show()
                    }
                    
            }
        }
        
    }
    
    func wscallforPaymentType()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetPaymentTypes,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let arr1 = NSMutableArray(array: (result["PaymentTypes"] as? Array)!)
                        
                        var on1 : Bool = false
                        var on2 : Bool = false
                        var on3 : Bool = false
                        
                        for i in 0..<arr1.count
                        {
                            let dict1 = NSMutableDictionary(dictionary: arr1[i] as! Dictionary)

                            if i == 0
                            {
                                self.strCod = dict1["Short_Code"] as? String
                                if dict1["Is_Enabled"] as? NSNumber == 1
                                {
                                   on2 = true
                                }
                            }
                            if i == 1
                            {
                                self.strKnet = dict1["Short_Code"] as? String

                                if dict1["Is_Enabled"] as? NSNumber == 1
                                {
                                    on1 = true
                                }
                            }
                            if i == 2
                            {
                                self.strCredit = dict1["Short_Code"] as? String

                                if dict1["Is_Enabled"] as? NSNumber == 1
                                {
                                    on3 = true
                                }
                            }
                        }
                        
                        self.setPaymentHeight(isOn1: on1, isOn2: on2, isOn3: on3)
                        

                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
    func wscallforCMS(strReq: String)
    {
        var userId : String!
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as? CVarArg ?? 0)
        }
        
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetTerms,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    self.txtDesc.isHidden = false
                    self.activity.isHidden = true

                    if strStatus == "Success"
                    {

                        self.ZazooTerms = (result["Zazoo_Terms"] as? String  ?? "")!
                        self.vendorTerms = (result["Vendor_Terms"] as? String ?? "")!
                        
                        if self.vendorTerms == ""
                        {
                            self.UpdateTerms(isvendor: false)
                        }
                        else
                        {
                            self.UpdateTerms(isvendor: true)
                        }
                        
                        self.viewZazooterms.isHidden = false

                    }
                    else
                    {
                        self.txtDesc.text = ""
                    }
            }
            
            
        }
        
    }

    
    
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
            
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
