//
//  ItemDetailVC.swift
//  Zazoo
//
//  Created by HTNaresh on 23/8/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

class ItemDetailVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionItem: UICollectionView!
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var btnNew: ButtonExtender!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var btnItemDesc: UIButton!
    @IBOutlet weak var btnItemDetail: UIButton!
    
    @IBOutlet weak var btnFavouriteItem: UIButton!
    @IBOutlet weak var viewDesc: UIView!
    @IBOutlet weak var viewItemDetail: UIView!
    
    @IBOutlet weak var conviewItemDesc: NSLayoutConstraint!
    @IBOutlet weak var conviewItemDetail: NSLayoutConstraint!
    
    @IBOutlet weak var txtItemDesc: UITextView!
    @IBOutlet weak var txtItemDetail: UITextView!
    @IBOutlet weak var btnAddtoCart: UIButton!
    @IBOutlet weak var btncustomize: UIButton!

    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblItemSize: UILabel!
    @IBOutlet weak var lblItemClr: UILabel!
    
    @IBOutlet weak var btnQty: ButtonExtender!
    @IBOutlet weak var btnItemSize: ButtonExtender!
    @IBOutlet weak var btnItemClr: ButtonExtender!
    @IBOutlet weak var viewItem: UIView!
    
    @IBOutlet weak var pageControl: UIPageControl!

    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var imgDown1: UIImageView!
    @IBOutlet weak var imgDown2: UIImageView!
    
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var conBtnNewHeight: NSLayoutConstraint!
    @IBOutlet weak var conViewDataHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewsold: UIView!
    
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var conViewCustomizeHeight: NSLayoutConstraint!
    @IBOutlet weak var txtCustomize: UITextView!
    
    var timer = Timer()
    var sizeId : NSNumber!
    var availqty : Int!
    var dealId : NSNumber!


    var arrMedia : NSMutableArray = []
    var dictDetail : NSMutableDictionary = [:]
    var arrQty  =  [[String:Any]]()
    var arrSize  =  [[String:Any]]()
    var CatId : NSNumber!
    var strTitle : String!
    var strfrom : String!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblItemSize.isHidden = true
        viewItem.isHidden = true
        sizeId = 0
        dealId = 0

        
//        let image = UIImage(named: "unCheckbox")?.withRenderingMode(.alwaysTemplate)
//        let image1 = UIImage(named: "checkbox")?.withRenderingMode(.alwaysTemplate)
//
//        btnCheck.setImage(image, for: .normal)
//        btnCheck.setImage(image1, for: .selected)
//
//        btnCheck.tintColor = UIColor(red: 30.0/255.0, green: 181.0/255.0, blue: 180.0/255.0, alpha: 1)
        
        txtCustomize.layer.cornerRadius = 5.0
        txtCustomize.layer.borderColor = UIColor.lightGray.cgColor
        txtCustomize.layer.borderWidth = 1.0
        
        
        btncustomize.setTitle(kCustomize, for: .normal)
        txtCustomize.tintColor = UIColor(red: 30.0/255.0, green: 181.0/255.0, blue: 180.0/255.0, alpha: 1)

        arrQty = [["name":"1"],["name":"2"],["name":"3"],["name":"4"],["name":"5"],["name":"6"],["name":"7"],["name":"8"],["name":"9"],["name":"10"]]
        
        btnItemDesc.isSelected = false
        btnItemDetail.isSelected = false
        viewsold.isHidden = true
        
        lblTitle.text = strTitle

        
        wscallforItemDetail()
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        imgDown1.image = UIImage(named: "down")
        imgDown2.image = UIImage(named: "down")
        
        lblQty.text = kQuantity
        lblItemSize.text = kItemSize
        btnItemDesc.setTitle(kItemDesc, for: .normal)
        btnItemDetail.setTitle(kItemDetail, for: .normal)
        
        btnAddtoCart.setTitle(kAddtocart, for: .normal)
        btnAddtoCart.setTitle(kGotoCart, for: .selected)

        conViewCustomizeHeight.constant = 0
        txtCustomize.isHidden = true


    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.addTabBar2(self, tab: "0")
        
       



    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    func displayData(dict:NSMutableDictionary)
    {
        viewsold.isHidden = true

        dictDetail = dict.mutableCopy() as! NSMutableDictionary
        
        availqty = dict["Available_Qty"] as! Int
        
        if dict["Is_Customisable"] as! Int == 1
        {
            conViewCustomizeHeight.constant = 40
            txtCustomize.isHidden = true
        }
        
        if availqty <= 0
        {
            viewsold.isHidden = false
            self.btnQty.setTitle(String(format: "%d",0), for: .normal)
            btnAddtoCart.isSelected = false
            btnAddtoCart.setBackgroundImage(UIImage(named:"submit_passive.png"), for: .normal)
            btnAddtoCart.isEnabled = false
            btnItemSize.isEnabled = false
            btnQty.isEnabled = false
        }
        
        conBtnNewHeight.constant = 0
        conViewDataHeight.constant = 90
        
//        conBtnNewHeight.constant = 30
//        conViewDataHeight.constant = 120
        
        arrSize = dict["Sizes"] as! [[String:Any]]
        if arrSize.count > 0
        {
            btnItemSize.setTitle(String(format: "%@", (arrSize[0]["Size_Name"] as? String ?? "")!), for: .normal)
            sizeId = (arrSize[0]["Size_Id"] as? NSNumber)
            availqty = (arrSize[0]["Qty"] as? Int)

            lblItemSize.isHidden = false
            viewItem.isHidden = false

        }
        
        
        self.lblItemName.text = dict["Item_Name_E"] as? String
        self.txtItemDesc.text = (dict["Item_Short_Desc_E"] as? String)!.html2String
        self.txtItemDetail.text = (dict["Item_Desc_E"] as? String)!.html2String
        self.lblTitle.text = dict["Item_Name_E"] as? String

        
        self.lblBrand.text = dict["Vendor_Name"] as? String
        
        self.btnFavouriteItem.isSelected = false
        if (dict["Is_Favorite"] as? NSNumber) == 1
        {
            self.btnFavouriteItem.isSelected = true
        }
        
        if let n = dict.value(forKey: "Special_Price") as? NSNumber
        {
            let f = n.floatValue
            self.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, f)
            
        }
        else
        {
            self.lblPrice.text! =  String(format: "%@ %.3f",kCurrency, (dict["Special_Price"] as? Float ?? 0.0)!)
        }
        
        if (dict["Dis_Percent"] as? NSNumber) == 0
        {
            self.lblDiscount.text = ""
            self.lblDiscountPrice.text = ""
            
        }
        else
        {
            self.lblDiscount.text = String(format: "%@ %@", (dict["Dis_Percent"] as? NSNumber ?? 0)! , "% OFF")
            
            let strike : String!
            
            if let n = dict.value(forKey: "Primary_Price") as? NSNumber
            {
                let f = n.floatValue
                strike = String(format: "%@ %.3f",kCurrency, f)
                
                
            }
            else
            {
                strike = String(format: "%@ %.3f",kCurrency, (dict["Primary_Price"] as? Float ?? 0.0)!)
                
            }
            
            self.lblDiscountPrice.attributedText = Manager.sharedInstance.StrikeThroughString(str: strike)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cellReuseIdentifier = "HomeCollectionCell"
        let cell:HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! HomeCollectionCell
        
        let dict = arrMedia[indexPath.row] as! NSDictionary
        
        var Stringimg = String()
        Stringimg = String(format: "%@%@", GlobalConstants.kBaseURL,dict["Thumbnail_URL"] as! String)
        
        let urlString = Stringimg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: urlString!)
        cell.imgAdv.setImageWith(url, usingActivityIndicatorStyle: .gray)
        
        //            var type : Int!
        //
        //            type = objectData["Media_Type"] as! Int
        //
        //            if type == 2
        //            {
        //                cell.imgVideoIcon.isHidden = false
        //            }
        //            else
        //            {
        //                cell.imgVideoIcon.isHidden = true
        //            }
        //
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: Double(Manager.sharedInstance.Slidetime), target: self, selector: #selector(timerAction), userInfo: nil, repeats:true)
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let image = storyboard?.instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
        image.arrrimages = arrMedia.mutableCopy() as! NSMutableArray
        image.index = indexPath
        image.imagePath = GlobalConstants.kBaseURL
        navigationController?.pushViewController(image, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            let cellSize = CGSize(width:self.view.frame.size.width , height:200)
            
            return cellSize
    }
    @objc func timerAction()
    {
        
        if (collectionItem.visibleCells.count>0)
        {
            let cell:UICollectionViewCell = (collectionItem.visibleCells[0])
            
            let index:IndexPath = (collectionItem!.indexPath(for: cell))!
            
            if (index.row+1 < arrMedia.count) {
                
                collectionItem.scrollToItem(at: IndexPath(item:index.row+1, section: 0), at:.centeredHorizontally, animated: true)
                
            }
            else
            {
                collectionItem.scrollToItem(at: IndexPath(item:0, section: 0), at:.centeredHorizontally, animated: false)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        
        let pageWidth = collectionItem.frame.size.width
        pageControl.currentPage = Int(floor((collectionItem.contentOffset.x / pageWidth)))
        
    }
    //MARK:- Button Click
    @IBAction func btnSideClick(_ sender: Any)
    {
        Manager.sharedInstance.addSideBar(controller: self)
    }

    @IBAction func btnBackClick(_ sender: Any)
    {
        
//        if strfrom == "url"
//        {
//            let storyboard1 : UIStoryboard!
//
//            if UserDefaults.standard.bool(forKey: "arabic")
//            {
//                UIView.appearance().semanticContentAttribute = .forceRightToLeft
//                storyboard1 = UIStoryboard(name: "Main_ar", bundle: nil)
//            }
//            else
//            {
//                UIView.appearance().semanticContentAttribute = .forceLeftToRight
//                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
//            }
//
//            let home = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//
//            home.strComeFrom = "home"
//            home.wscallforCategory()
//            home.wscallforAdvertisement()
//
//            let navigationController = UINavigationController()
//            navigationController.isNavigationBarHidden = true
//            navigationController.setViewControllers([home], animated: false)
//            let window = UIApplication.shared.delegate!.window!!
//            window.rootViewController = navigationController
//        }
//        else
//        {
            self.navigationController?.popViewController(animated: true)

//        }

    }
    
    @IBAction func btnCartClick(_ sender: Any)
    {
        let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        
        self.navigationController?.pushViewController(subCatvc, animated: true)
    }
    
//    @IBAction func btnFavClick(_ sender: Any)
//    {
//        let favvc = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteVC") as! FavouriteVC
//
//        self.navigationController?.pushViewController(favvc, animated: true)
//    }
    @IBAction func btnShareClick(_ sender: Any)
    {
        let strItemName = self.lblItemName.text!
        
        let strShareLink = String(format: "https://www.pantoneme.com/itemdetail.aspx?Id=%@", CatId)
        
        let strName = "Pantone App -"
        
        let strAppUrl  = "https://itunes.apple.com/us/app/pantone/id1435900273?ls=1&mt=8"

        let controller = UIActivityViewController(activityItems: [strShareLink,strItemName,strName,strAppUrl], applicationActivities: nil)
        present(controller, animated: true) {() -> Void in }
    }
    
    @IBAction func btnSearchClick(_ sender: Any)
    {
        
    }
    
    @IBAction func btnNewClick(_ sender: Any) {
    }
    
    @IBAction func btnAddtoCartClick(_ sender: Any)
    {
        if btnAddtoCart.isSelected
        {
            let subCatvc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(subCatvc, animated: false)
            return
        }
        
        if btnCheck.isSelected && txtCustomize.text == ""
        {
            let alert = UIAlertController(title:kWarningEn, message:kPleaseentercustomize, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:kOkEn, style: .default, handler: nil))
            alert.show()
            return

        }
        
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wAddToCart)
        
        
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as! CVarArg)
        }
        
        let strAmount = self.lblPrice.text!.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890.").inverted)

//        let strAmount =  self.lblPrice.text!.components(separatedBy:CharacterSet.decimalDigits.inverted).joined(separator: "")
        
        var strRate : String = self.lblDiscountPrice.text!
        
        if self.lblDiscountPrice.text == ""
        {
            strRate = strAmount
        }
        
        let dictTemp : NSDictionary = ["Qty":btnQty.titleLabel?.text as Any,"Custom_Note":self.txtCustomize.text,"Item_Id":String(format: "%@", (dictDetail["Item_Id"] as? NSNumber ?? 0)!),"App_User_Id":userId,"Cart_Id":"0","Item_Type_Id":"1","Price_Detail_Id":"","Children_Id":"","CalenderIds":[],"Amount":strAmount,"Rate":strRate,"Size_Id":sizeId,"Deal_Id":dealId]
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictTemp as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        Manager.sharedInstance.showSmallAlert("Item added sucessfully")
                        self.navigationController?.popViewController(animated: true)
                        Manager.sharedInstance.wscallforgetCart()

                    }
            }
        }
        

    }
    
    
    @IBAction func btnItemDescClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        
        if btn.isSelected
        {
           let sizeThatFitsTextView1 = txtItemDesc.sizeThatFits(CGSize(width: txtItemDesc.frame.size.width, height: CGFloat(MAXFLOAT)))
            conviewItemDesc.constant = sizeThatFitsTextView1.height + 46
            
            imgDown1.image = UIImage(named: "up")

        }
        else
        {
            conviewItemDesc.constant = 36
            imgDown1.image = UIImage(named: "down")

        }
    }
    
    @IBAction func btnItemDetailClick(_ sender: Any)
    {
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        
        if btn.isSelected
        {
            let sizeThatFitsTextView1 = txtItemDetail.sizeThatFits(CGSize(width: txtItemDetail.frame.size.width, height: CGFloat(MAXFLOAT)))
            conviewItemDetail.constant = sizeThatFitsTextView1.height + 46
            
            imgDown2.image = UIImage(named: "up")

            
        }
        else
        {
            conviewItemDetail.constant = 36
            
            imgDown2.image = UIImage(named: "down")

            
        }
        
    }
    
    @IBAction func btnFavouriteItemClick(_ sender: Any)
    {
       let btn = sender as! UIButton
    
        if btn.isSelected
        {
            self.wscallforRemoveFav()
        }
        else
        {
            self.wscallforAddFav()
        }
        
       
    }
    
    @IBAction func btnItemClrClick(_ sender: Any) {
    }
    
    @IBAction func btnItemSizeClick(_ sender: Any)
    {
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            print(dictSelect)
            
            self.sizeId = dictSelect["Size_Id"] as! NSNumber
            self.btnItemSize.setTitle(dictSelect["Size_Name"] as? String, for: .normal)
            
            self.availqty = dictSelect["Qty"] as! Int
            
            var strMessage : String = ""
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                let str1 = "الكمية المختارة"
                let str2 = "لا يمكن أن يكون أكبر من الكمية المتوفرة"
                strMessage = String(format: "%@ : %@ %@ : %d" ,str1, (self.btnQty.titleLabel?.text)!,str2, self.availqty)
            }
            else
            {
                strMessage = String(format: "Selected quantity : %@ cannot be greater than Available quantity : %d" , (self.btnQty.titleLabel?.text)!, self.availqty)
            }
            
            if Int((self.btnQty.titleLabel?.text)!)! > self.availqty
            {
                let alertController = UIAlertController(title:kWarningEn, message:strMessage, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    
                    self.btnQty.setTitle(String(format: "%d",self.availqty), for: .normal)

                }
                
                alertController.addAction(okAction)
                alertController.show()
            }

        }, arrData: NSMutableArray(array: arrSize as Array), isLanguage: false, isNationality: false, isCategories: false, isProfession: true, isother: false,self)
    }

    @IBAction func btnQtyClick(_ sender: Any)
    {
        var arrQtyTemp  =  [[String:Any]]()

        if availqty < 10
        {
            for i in 0..<arrQty.count
            {
                let dict = arrQty[i]
                if i == availqty
                {
                    break
                }
                else
                {
                    arrQtyTemp.append(dict)
                }
            }
            
        }
        else
        {
            arrQtyTemp = arrQty
        }
        
        Manager.sharedInstance.addCustomPopup({ (dictSelect) in
            
            print(dictSelect)
            
            var strMessage : String = ""
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                let str1 = "الكمية المختارة"
                let str2 = "لا يمكن أن يكون أكبر من الكمية المتوفرة"
                strMessage = String(format: "%@ : %@ %@ : %d" ,str1, (dictSelect["name"] as! String),str2, self.availqty)
            }
            else
            {
                strMessage = String(format: "Selected quantity : %@ cannot be greater than Available quantity : %d" , dictSelect["name"] as! String, self.availqty)
            }
            
            if Int(dictSelect["name"] as! String)! > self.availqty
            {
                let alertController = UIAlertController(title:kWarningEn, message:strMessage, preferredStyle: .alert)
                
                let okAction = UIAlertAction(title:kOkEn, style: .default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                    
                    self.btnQty.setTitle(String(format: "%d",self.availqty), for: .normal)
                    
                }
                
                alertController.addAction(okAction)
                alertController.show()
            }
            else
            {
                self.btnQty.setTitle(dictSelect["name"] as? String, for: .normal)

            }

            
            
        }, arrData: NSMutableArray(array: arrQtyTemp as Array), isLanguage: false, isNationality: false, isCategories: false, isProfession: false, isother: true,self)
    }
    
    @IBAction func btnCheckClick(_ sender: Any)
    {
        self.view.endEditing(true)
        let btn = sender as! UIButton
        btn.isSelected = !btn.isSelected
        if btn.isSelected
        {
            btnCheck.isSelected = true
            conViewCustomizeHeight.constant = 135
            txtCustomize.isHidden = false
        }
        else
        {
            btnCheck.isSelected = false
            conViewCustomizeHeight.constant = 40
            txtCustomize.isHidden = true
            txtCustomize.text = ""
        }
    }

    //MARK:- Webservice
    
    func wscallforItemDetail()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetItemDetail,CatId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let dictData = NSMutableDictionary(dictionary: (result as? Dictionary)!)
                        
                        self.arrMedia = NSMutableArray(array: (result["Media"] as? Array)!)
                        
                        self.pageControl.numberOfPages = self.arrMedia.count
                        
                        self.collectionItem.reloadData()
                        
                        self.displayData(dict: dictData)
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    func wscallforsaleItemDetail()
    {
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetSaleItemDetail,CatId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let dictData = NSMutableDictionary(dictionary: (result as? Dictionary)!)
                        
                        self.arrMedia = NSMutableArray(array: (result["Media"] as? Array)!)
                        
                        self.pageControl.numberOfPages = self.arrMedia.count
                        
                        self.collectionItem.reloadData()
                        
                        self.displayData(dict: dictData)
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    
    
    func wscallforAddFav()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wAddFavoriteItem,CatId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.btnFavouriteItem.isSelected = true
                        self.refreshListView()

                        
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    func wscallforRemoveFav()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveFavoriteItem,CatId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {
                    print(result)
                    
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        self.btnFavouriteItem.isSelected = false
                        self.refreshListView()

                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    
    func refreshListView()
    {
        let array = self.navigationController?.viewControllers
            as! Array<Any>
        for k in (0 ..< array.count)
        {
            let controller: UIViewController? = (array[k] as! UIViewController)

//            if strfrom == "fav"
//            {
//                if (controller is FavouriteVC)
//                {
//                    let item = controller as! FavouriteVC
//                    item.wscallforfavoritelist()
//
//                    break
//                }
//            }
//            else
//            {
                if (controller is ItemListVC)
                {
                    let item = controller as! ItemListVC
                    item.wscallforItemlist()

                    break
                }
//            }


        }
    }
    
   
}

