//
//  sidecell.swift
//  ViteBird
//
//  Created by HTNaresh on 4/7/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit

protocol CellDelegate: class
{
    func updateDatainArray(ar1:NSMutableArray,index:NSInteger,type:String)
    
    func redirecttoCMS(index:NSInteger,dict:NSMutableDictionary,pagename:String)
    
    func redirecttoList(index:NSInteger,dict:NSMutableDictionary,Id:NSNumber)


    
}


class sidecell: UITableViewCell,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    

    weak var delegate: CellDelegate?

    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var btnExpand: UIButton!
    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    @IBOutlet weak var collectionList: UICollectionView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var viewBg: UIView!

//    @IBOutlet weak var conTblListHeight: NSLayoutConstraint!
    
    var arrTblList : NSMutableArray!
    var arrList : NSMutableArray!
    var indexRow : NSInteger!

    var strType : String!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- tableview delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  arrTblList.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cellReuseIdentifier = "SideTableCell"
        let cell:SideTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SideTableCell
        
        let dict = NSMutableDictionary(dictionary: (arrTblList[indexPath.row] as? Dictionary)!)
        
        if strType == "list"
        {
            cell.lblTitle.text = dict["Name"] as? String

        }
        else
        {
            if dict["Content_Id"] == nil
            {
                cell.lblTitle.text = dict["Category_Name_E"] as? String
            }
            else
            {
                cell.lblTitle.text = dict["Title"] as? String
            }
        }
       
        if dict["isSelect"] as? String == "0"
        {
            cell.btnCheck.isSelected = false
        }
        else
        {
            cell.btnCheck.isSelected = true
            
        }

        
        cell.btnCheck.tag  = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(btnCheckTableClick(_:))
            , for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if strType != "list"
        {
            let dict = NSMutableDictionary(dictionary: (arrTblList[indexPath.row] as? Dictionary)!)
            
            if dict["Content_Id"] != nil
            {
                delegate?.redirecttoCMS(index: indexRow, dict: dict, pagename: dict["PageName"] as! String)
            }
            else
            {
                delegate?.redirecttoList(index: indexRow, dict: dict, Id: 0)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if strType == "list"
        {
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                return 60
            }
            else
            {
               return 30
            }
            
        }
        else
        {
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                return 50
            }
            else
            {
                return 25
            }
        }

            
    }
    
    //MARK:- collectionView delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        var cell = SideCollectionCell()
        
        let dict = NSMutableDictionary(dictionary: (arrList[indexPath.row] as? Dictionary)!)

        if strType == "List"
        {
            let cellReuseIdentifier = "cell1"
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! SideCollectionCell
            
            if dict["isSelect"] as? String == "0"
            {
                cell.lblTitle.textColor = UIColor.darkGray
                cell.lblTitle.backgroundColor  = UIColor.white
            }
            else
            {
                cell.lblTitle.textColor = UIColor.white
                cell.lblTitle.backgroundColor  = GlobalConstants.brownClr
            }
            
        }
        else if strType == "check"
        {
            let cellReuseIdentifier = "cell2"
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! SideCollectionCell
            
            if dict["isSelect"] as? String == "0"
            {
                cell.btnCheck.isSelected = false
            }
            else
            {
                cell.btnCheck.isSelected = true
                
            }
            
            cell.btnCheck.tag  = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(btnCheckClick(_:))
                , for: .touchUpInside)

        }
        
        cell.lblTitle.text = dict["Name"] as? String

        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if strType == "List"
        {
//            let cell:SideCollectionCell = collectionList.cellForItem(at: IndexPath(row: indexPath.row, section: 0)) as! SideCollectionCell
            
            let dict = NSMutableDictionary(dictionary: (arrList[indexPath.row] as? Dictionary)!)
            if dict["isSelect"] as? String == "0"
            {
                dict.setObject("1", forKey: "isSelect" as NSCopying)
            }
            else
            {
                dict.setObject("0", forKey: "isSelect" as NSCopying)
            }
            arrList.replaceObject(at: indexPath.row, with: dict)
            collectionList.reloadData()
            
            
            let arr1 = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: arrList)
            
            let strComma : String = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Id")
            
            delegate?.updateDatainArray(ar1: arrList, index: indexRow, type: strComma)

        }
        
    
    }
    
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
//    {
//        if strType == "List"
//        {
//            let cell:SideCollectionCell = collectionList.cellForItem(at: IndexPath(row: indexPath.row, section: 0)) as! SideCollectionCell
//
//            cell.lblTitle.textColor = UIColor.darkGray
//            cell.lblTitle.backgroundColor  = UIColor.white
//
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if strType == "List"
        {
            let cellSize = CGSize(width: self.collectionList.frame.size.width/3 - 2 , height: self.collectionList.frame.size.height)
            return cellSize

        }
        else if strType == "check"
        {
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                let cellSize = CGSize(width: self.collectionList.frame.size.width / 2 , height: 50)
                return cellSize
            }
            else
            {
                let cellSize = CGSize(width: self.collectionList.frame.size.width / 2 , height: 30)
                return cellSize
            }


        }
        else
        {
            if (UIDevice.current.userInterfaceIdiom == .pad)
            {
                let cellSize = CGSize(width: self.collectionList.frame.size.width / 2 , height: 60)
                return cellSize
            }
            else
            {
                let cellSize = CGSize(width: self.collectionList.frame.size.width / 2 , height: 30)
                return cellSize
            }
            
        }
        
    }
    
    @objc func btnCheckClick(_ sender:Any)
    {
        let btn = sender as! UIButton
//        btn.isSelected = !btn.isSelected
        
        let dict = NSMutableDictionary(dictionary: (arrList[btn.tag] as? Dictionary)!)
        if dict["isSelect"] as? String == "0"
        {
            dict.setObject("1", forKey: "isSelect" as NSCopying)
        }
        else
        {
            dict.setObject("0", forKey: "isSelect" as NSCopying)
        }
        arrList.replaceObject(at: btn.tag, with: dict)
        
        let arr1 = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: arrList)
        
        let strComma : String = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Id")
        
        delegate?.updateDatainArray(ar1: arrList, index: indexRow, type: strComma)
        
        collectionList.reloadData()
    }
    
    @objc func btnCheckTableClick(_ sender:Any)
    {
        let btn = sender as! UIButton
//        btn.isSelected = !btn.isSelected
        
        let dict = NSMutableDictionary(dictionary: (arrTblList[btn.tag] as? Dictionary)!)
        if dict["isSelect"] as? String == "0"
        {
            dict.setObject("1", forKey: "isSelect" as NSCopying)
        }
        else
        {
            dict.setObject("0", forKey: "isSelect" as NSCopying)
        }
        arrTblList.replaceObject(at: btn.tag, with: dict)
        
        let arr1 = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: arrTblList)
        
        let strComma : String = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Id")
        
        delegate?.updateDatainArray(ar1: arrTblList, index: indexRow, type: strComma)
        
        tblList.reloadData()
    }
    
   
    
    


}
