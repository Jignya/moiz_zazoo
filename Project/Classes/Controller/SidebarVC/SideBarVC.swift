//
//  SideBarVC.swift
//  ViteBird
//
//  Created by HTNaresh on 4/7/18.
//  Copyright © 2018 HightecIT. All rights reserved.
//

import UIKit
import Alamofire

class SideBarVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CellDelegate,RangeSeekSliderDelegate
{
   
    
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var conLogOutHeight: NSLayoutConstraint!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var lblLogout: UILabel!
    @IBOutlet weak var viewTable: UIView!
    
    @IBOutlet weak var tblFilter: UITableView!
    @IBOutlet weak var btnGo: UIButton!
    @IBOutlet weak var btnClearFilter: UIButton!
    
    @IBOutlet weak var contblFilterHeight: NSLayoutConstraint!
    @IBOutlet weak var contblListHeight: NSLayoutConstraint!

    var strcomeFrom : String!
    var arrFilter : NSMutableArray = []
    var arrlist : NSMutableArray = []
    var dictFilter : NSMutableDictionary = [:]

    let app : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var strMaxTime : String!
    var strMinTime : String!
    var strItemId : String!
    var strAgeId : String!
    var strCategoryId : String!
    var strVendorId : String!
    



    override func viewDidLoad()
    {
        super.viewDidLoad()

        strcomeFrom = "home"

        let SingleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureClick(_:)))
        tapView.addGestureRecognizer(SingleFingerTap)
        
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        btnGo.setTitle(kGo, for: .normal)
        btnClearFilter.setTitle(kClearFilter, for: .normal)
        
        setupArray()
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat)
    {
        Manager.sharedInstance.Mintime = minValue
        Manager.sharedInstance.Maxtime = maxValue
        
        let strMin = String(format: "%.2f" , minValue)
        let strMax = String(format: "%.2f" , maxValue)
        
        let str1 = strMin.replacingOccurrences(of: ".", with: ":")
        let str2 = strMax.replacingOccurrences(of: ".", with: ":")
        
        let format1  = DateFormatter()
        format1.dateFormat = "HH:mm"
        let date1 = format1.date(from: str1)
        let date2 = format1.date(from: str2)

        let format  = DateFormatter()
        format.dateFormat = "hh:mm a"
        
        strMinTime = format.string(from: date1!)
        strMaxTime = format.string(from: date2!)

    }
    
    func setupArray()
    {
        if Manager.sharedInstance.arrFilter.count > 0
        {
            arrFilter = Manager.sharedInstance.arrFilter.mutableCopy() as! NSMutableArray
            
            for i in 0..<arrFilter.count
            {
                let dict = NSMutableDictionary(dictionary: arrFilter[i] as! [AnyHashable:Any])
                let dict1 : NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                dict1.setObject("0", forKey: "isSelected" as NSCopying)
                
                if dict["Type"] as? String != "Slider"
                {
                    let aa2 = NSMutableArray(array: dict["commonDTO"] as! Array)
                    for j in 0..<aa2.count
                    {
                        let dic = NSMutableDictionary(dictionary: aa2[j] as! [AnyHashable:Any])
                        let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
                        
                        if dict["Name"] as? String == "BOOK ACTIVITIES"
                        {
                            let arr1 = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: aa2)
                            strItemId  = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Id")
                        }
                        else if dict["Name"] as? String == "AGE"
                        {
                            let arr1 = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: aa2)
                            strAgeId  = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Id")
                            
                        }
                        else if dict["Name"] as? String == "CATEGORY"
                        {
                            let arr1 = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: aa2)
                            strCategoryId  = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Id")
                        }
                        else if dict["Name"] as? String == "PROVIDER"
                        {
                            let arr1 = Manager.sharedInstance.arrFilterDataforstring("1", withPredicateKey: "isSelect", fromArray: aa2)
                            strVendorId  = Manager.sharedInstance.getCommaSeparatedValuefromArray(arr1, forkey: "Id")
                        }
                    }
                }
            }
            
        }
        else
        {
            strMinTime = ""
            strMaxTime = ""
            strItemId = ""
            strAgeId = ""
            strCategoryId = ""
            strVendorId = ""
            
            dictFilter.setObject("", forKey: "Min_Time" as NSCopying)
            dictFilter.setObject("", forKey: "Max_Time" as NSCopying)
            dictFilter.setObject("", forKey: "Item_Type_Id" as NSCopying)
            dictFilter.setObject("", forKey: "Ages" as NSCopying)
            dictFilter.setObject("", forKey: "Categories" as NSCopying)
            dictFilter.setObject("", forKey: "Vendors" as NSCopying)
            
            let arr1 = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "filter")
            
            for i in 0..<arr1.count
            {
                let dict = NSMutableDictionary(dictionary: arr1[i] as! [AnyHashable:Any])
                let dict1 : NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
                dict1.setObject("0", forKey: "isSelected" as NSCopying)
                
                if dict["Type"] as? String != "Slider"
                {
                    let aa2 = NSMutableArray(array: dict["commonDTO"] as! Array)
                    for j in 0..<aa2.count
                    {
                        let dic = NSMutableDictionary(dictionary: aa2[j] as! [AnyHashable:Any])
                        let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
                        
                        if dict["Type"] as? String == "List" && j == 0
                        {
                            dic1.setObject("1", forKey: "isSelect" as NSCopying)
                            strItemId = String(format: "%@", dic1["Id"] as! NSNumber)
                            
                            print(strItemId)
                            
                        }
                        else
                        {
                            dic1.setObject("0", forKey: "isSelect" as NSCopying)
                        }
                        
                        aa2.replaceObject(at: j, with: dic1)
                    }
                    
                    dict1.setObject(aa2, forKey: "commonDTO" as NSCopying)
                    
                }
                arr1.replaceObject(at: i, with: dict1)
            }
            
            arrFilter = arr1
        }
        
//        let dic = NSMutableDictionary(dictionary: arrFilter.object(at: 3) as! Dictionary)
//        let temp = arrFilter.removeObject(at: 3)
//        arrFilter.insert(dic, at: 4)
//
//        print(arrFilter)

        
        
        let arr2 = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "second")
        
        for i in 0..<arr2.count
        {
            let dict = NSMutableDictionary(dictionary: arr2[i] as! [AnyHashable:Any])
            let dict1 : NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
            dict1.setObject("0", forKey: "isSelected" as NSCopying)
            arr2.replaceObject(at: i, with: dict1)
        }
        
        arrlist = arr2
    

//        arrlist = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "second")
        
        DispatchQueue.main.async
        {
            self.tblList.reloadData()
            self.tblFilter.reloadData()
            self.tblList.updateConstraints()
            self.tblFilter.updateConstraints()
            self.tblList.layoutIfNeeded()
            self.tblFilter.layoutIfNeeded()

            self.contblListHeight.constant = self.tblList.contentSize.height

            self.contblFilterHeight.constant = self.tblFilter.contentSize.height

        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblFilter
        {
            return  arrFilter.count
        }
        else
        {
            return arrlist.count
            
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblFilter
        {
            var cell = sidecell()
            let dict = NSMutableDictionary(dictionary: (arrFilter[indexPath.row] as? Dictionary)!)
            

            if dict["Type"] as? String == "List"
            {
                let cellReuseIdentifier = "sidecell"
                cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! sidecell

                cell.strType = "List"
                cell.arrList = NSMutableArray(array: dict["commonDTO"] as! Array)
                cell.collectionList.reloadData()
                
                cell.btnExpand.isSelected = true
                
            }
            else if dict["Type"] as? String == "CheckBox"
            {
                let cellReuseIdentifier = "sidecellCheck"
                cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! sidecell

                cell.strType = "check"
                cell.arrList = NSMutableArray(array: dict["commonDTO"] as! Array)
                cell.collectionList.reloadData()

            }
        
            else if dict["Type"] as? String == "Slider"
            {
                let cellReuseIdentifier = "sidecellSlide"
                cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! sidecell
                
                cell.rangeSlider.delegate = self
                
                
                cell.rangeSlider.minValue = CGFloat((dict["Min_Time"] as! NSString).floatValue)
                cell.rangeSlider.maxValue = CGFloat((dict["Max_Time"] as! NSString).floatValue)
                
                if Manager.sharedInstance.Mintime != 0
                {
                    cell.rangeSlider.selectedMinValue = Manager.sharedInstance.Mintime
                    cell.rangeSlider.selectedMaxValue = Manager.sharedInstance.Maxtime

                }
                else
                {
                    cell.rangeSlider.selectedMinValue = CGFloat((dict["Min_Time"] as! NSString).floatValue)
                    cell.rangeSlider.selectedMaxValue = CGFloat((dict["Max_Time"] as! NSString).floatValue)

                }

                

                cell.rangeSlider.enableStep = true
                cell.rangeSlider.step = 1
                cell.rangeSlider.minDistance = 1
                
            }
            else if dict["Type"] as? String == "CheckBoxList"
            {
                let cellReuseIdentifier = "sidecellList"
                cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! sidecell

                cell.strType = "list"
                cell.arrTblList = NSMutableArray(array: dict["commonDTO"] as! Array)
                cell.tblList.reloadData()

            }
            
            cell.lbltitle.text = (dict["Name"] as? String ?? "")!.uppercased()
            if dict["isSelected"] as? String == "0"
            {
                cell.btnExpand.isSelected = false
            }
            else
            {
                cell.btnExpand.isSelected = true

            }
            
            cell.delegate = self
            cell.indexRow = indexPath.row
            
            cell.btnExpand.tag  = indexPath.row
            cell.btnExpand.addTarget(self, action: #selector(btnExpandClick(_:))
                , for: .touchUpInside)
            
            tblFilter.updateConstraints()
            tblFilter.superview?.updateConstraints()
            tblFilter.layoutIfNeeded()
            tblFilter.superview?.layoutIfNeeded()
            contblFilterHeight.constant = tblFilter.contentSize.height

            return cell
        }
        else
        {
            let cellReuseIdentifier = "sidecellList1"
            let cell : sidecell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! sidecell
            
            let dict = NSMutableDictionary(dictionary: (arrlist[indexPath.row] as? Dictionary)!)

            
            cell.strType = "list1"
            if dict["Pages"] != nil
            {
                cell.arrTblList = NSMutableArray(array: dict["Pages"] as! Array)
//                cell.btnExpand.isSelected = true
//                cell.btnExpand.isEnabled = false
//                cell.viewBg.isHidden = true
//                cell.lbltitle.textColor = UIColor.darkGray
                
                if cell.arrTblList.count == 0
                {
                    cell.btnExpand.isHidden = true
                }
                
                cell.btnExpand.isEnabled = true
                cell.lbltitle.textColor = UIColor.white
                cell.viewBg.isHidden = false
                
                if dict["isSelected"] as? String == "0"
                {
                    cell.btnExpand.isSelected = false
                    
                    let origImage = UIImage(named: "down.png");
                    let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                    cell.btnExpand.setImage(tintedImage, for: .normal)
                    cell.btnExpand.tintColor = UIColor.white
                    
                }
                else
                {
                    cell.btnExpand.isSelected = true
                    
                    let origImage = UIImage(named: "up.png");
                    let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                    cell.btnExpand.setImage(tintedImage, for: .selected)
                    cell.btnExpand.tintColor = UIColor.white
                    
                    
                }
                
                
                
                cell.btnExpand.tag  = indexPath.row
                cell.btnExpand.addTarget(self, action: #selector(btnExpandListClick(_:))
                    , for: .touchUpInside)

            }
            else
            {
                cell.arrTblList = NSMutableArray(array: dict["Categories"] as! Array)
                
                if cell.arrTblList.count == 0
                {
                    cell.btnExpand.isHidden = true
                }
                
                cell.btnExpand.isEnabled = true
                cell.lbltitle.textColor = UIColor.white
                cell.viewBg.isHidden = false
                
                if dict["isSelected"] as? String == "0"
                {
                    cell.btnExpand.isSelected = false
                    
                    let origImage = UIImage(named: "down.png");
                    let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                    cell.btnExpand.setImage(tintedImage, for: .normal)
                    cell.btnExpand.tintColor = UIColor.white

                }
                else
                {
                    cell.btnExpand.isSelected = true
                    
                    let origImage = UIImage(named: "up.png");
                    let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                    cell.btnExpand.setImage(tintedImage, for: .selected)
                    cell.btnExpand.tintColor = UIColor.white

                    
                }
                
                
                
                cell.btnExpand.tag  = indexPath.row
                cell.btnExpand.addTarget(self, action: #selector(btnExpandListClick(_:))
                    , for: .touchUpInside)
            }
            
           
            
            
            cell.delegate = self
            cell.indexRow = indexPath.row
            
            cell.lbltitle.text = (dict["Name"] as? String ?? "").uppercased()
            cell.tblList.reloadData()
            
            contblListHeight.constant = tblList.contentSize.height

            tblList.updateConstraints()
            tblList.superview?.updateConstraints()
            tblList.layoutIfNeeded()
            tblList.superview?.layoutIfNeeded()

            contblListHeight.constant = tblList.contentSize.height


            return cell

        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblList
        {
            let dict = NSMutableDictionary(dictionary: (arrlist[indexPath.row] as? Dictionary)!)
            
            
            if dict["Pages"] == nil
            {
                let arrCat = NSMutableArray(array: dict["Categories"] as! Array)
                
                if arrCat.count == 0
                {
                    let storyboard1 : UIStoryboard!
                    if UserDefaults.standard.bool(forKey: "arabic")
                    {
                        storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
                    }
                    else
                    {
                        storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
                    }
                    let home = storyboard1.instantiateViewController(withIdentifier: "AllVendorListVC") as! AllVendorListVC
                    home.strTitle = dict["Name"] as? String
                    
                    let navigationController = UINavigationController()
                    navigationController.isNavigationBarHidden = true
                    
                    navigationController.setViewControllers([home], animated: false)
                    let window = UIApplication.shared.delegate!.window!!
                    window.rootViewController = navigationController
                    
                    
                    Hidesidemenu()
                }
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblFilter
        {
            let dict = NSMutableDictionary(dictionary: (arrFilter[indexPath.row] as? Dictionary)!)
            
            if dict["Type"] as? String == "Slider"
            {
                if (UIDevice.current.userInterfaceIdiom == .pad)
                {
                    return 150

                }
                else
                {
                    return 100

                }
            }
            else if dict["Type"] as? String == "List"
            {
                if (UIDevice.current.userInterfaceIdiom == .pad)
                {
                    return 130
                }
                else
                {
                    return 80
                }
            }
            else if dict["Type"] as? String == "CheckBoxList"
            {
                let ar1 = NSMutableArray(array: dict["commonDTO"] as! Array)

                if dict["isSelected"] as? String == "0"
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return 50
                    }
                    else
                    {
                        return 40
                    }
                }
                else
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return (CGFloat(50 + (65 * ar1.count)))
                    }
                    else
                    {
                        return (CGFloat(40 + (35 * ar1.count)))
                    }
                }
                
            }
            else
            {
                let ar1 = NSMutableArray(array: dict["commonDTO"] as! Array)

                if dict["isSelected"] as? String == "0"
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return 50
                    }
                    else
                    {
                        return 40
                    }
                }
                else
                {
                    var count = ar1.count % 2
                
                    if count != 0
                    {
                        count = (ar1.count / 2) + 1
                    }
                    else
                    {
                       count = (ar1.count / 2)
                    }
                    
                    
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return (CGFloat(50 + (60 * count)))
                    }
                    else
                    {
                        return (CGFloat(40 + (35 * count)))
                    }
                }
            }
           
        }
        else
        {
            let dict = NSMutableDictionary(dictionary: (arrlist[indexPath.row] as? Dictionary)!)
            
            if dict["Pages"] != nil
            {
                let ar1 = NSMutableArray(array: dict["Pages"] as! Array)
                if dict["isSelected"] as? String == "0"
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return 50
                    }
                    else
                    {
                        return 40
                    }
                }
                else
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return (CGFloat(50 + (50 * ar1.count)))
                    }
                    else
                    {
                        return (CGFloat(40 + (25 * ar1.count)))
                    }
                    
                }
                
//                if (UIDevice.current.userInterfaceIdiom == .pad)
//                {
//                    return (CGFloat(50 + (50 * ar1.count)))
//                }
//                else
//                {
//                    return (CGFloat(40 + (25 * ar1.count)))
//                }

                
            }
            else
            {
                let ar1 = NSMutableArray(array: dict["Categories"] as! Array)
                
                if dict["isSelected"] as? String == "0"
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return 50
                    }
                    else
                    {
                        return 40
                    }
                }
                else
                {
                    if (UIDevice.current.userInterfaceIdiom == .pad)
                    {
                        return (CGFloat(50 + (50 * ar1.count)))
                    }
                    else
                    {
                        return (CGFloat(40 + (25 * ar1.count)))
                    }
                    
                }
                
            }

        }
        
    }
    
    @objc func btnExpandClick(_ sender:Any)
    {
        let btn = sender as! UIButton
        let dict = NSMutableDictionary(dictionary: (arrFilter[btn.tag] as? Dictionary)!)

        if dict["isSelected"] as? String == "0"
        {
            dict.setObject("1", forKey: "isSelected" as NSCopying)
        }
        else
        {
            dict.setObject("0", forKey: "isSelected" as NSCopying)
        }
        
        arrFilter.replaceObject(at: btn.tag, with: dict)
        
        
        tblFilter.reloadData()
        tblFilter.updateConstraints()
        tblFilter.layoutIfNeeded()
        contblFilterHeight.constant = tblFilter.contentSize.height

        
    }
    
    
    @objc func btnExpandListClick(_ sender:Any)
    {
        let btn = sender as! UIButton
        let dict = NSMutableDictionary(dictionary: (arrlist[btn.tag] as? Dictionary)!)
        
        if dict["isSelected"] as? String == "0"
        {
            dict.setObject("1", forKey: "isSelected" as NSCopying)
        }
        else
        {
            dict.setObject("0", forKey: "isSelected" as NSCopying)
        }
        
        arrlist.replaceObject(at: btn.tag, with: dict)
        
        tblList.reloadData()
        tblList.updateConstraints()
        tblList.layoutIfNeeded()
        contblListHeight.constant = tblList.contentSize.height
    }


    @objc func tapGestureClick(_ tapGesture: UITapGestureRecognizer?)
    {
        Hidesidemenu()
    }
    
    @IBAction func btnGoClick(_ sender: Any)
    {
        dictFilter.setObject(strMinTime, forKey: "Min_Time" as NSCopying)
        dictFilter.setObject(strMaxTime, forKey: "Max_Time" as NSCopying)
        
        dictFilter.setObject(strItemId.replacingOccurrences(of: " ", with: ""), forKey: "Item_Type_Id" as NSCopying)
        dictFilter.setObject(strAgeId.replacingOccurrences(of: " ", with: ""), forKey: "Ages" as NSCopying)
        dictFilter.setObject(strCategoryId.replacingOccurrences(of: " ", with: ""), forKey: "Categories" as NSCopying)
        dictFilter.setObject(strVendorId.replacingOccurrences(of: " ", with: ""), forKey: "Vendors" as NSCopying)
        
        
        let calendar = UIApplication.topViewController() as! UIViewController
        if (calendar is CalendarVC)
        {
            let calendar2 = calendar as? CalendarVC
            calendar2?.dictFilterPara = dictFilter.mutableCopy() as! NSMutableDictionary
            calendar2?.collectionType.reloadData()
            calendar2?.wscallforClasses()
        }
        else
        {
            let storyboard1 : UIStoryboard!
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            }

            let home = storyboard1.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            home.dictFilterPara = dictFilter.mutableCopy() as! NSMutableDictionary
            home.strcomeFrom = ""
            
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
        }
        
        Hidesidemenu()
        
        Manager.sharedInstance.arrFilter = arrFilter.mutableCopy() as! NSMutableArray


    }

    @IBAction func btnClearFilterClick(_ sender: Any)
    {
        Manager.sharedInstance.Mintime = 0
        Manager.sharedInstance.Maxtime = 0
        Manager.sharedInstance.arrFilter.removeAllObjects()
        
        strMinTime = ""
        strMaxTime = ""
        strItemId = "2"
        strAgeId = ""
        strCategoryId = ""
        strVendorId = ""
        
        dictFilter.setObject("", forKey: "Min_Time" as NSCopying)
        dictFilter.setObject("", forKey: "Max_Time" as NSCopying)
        dictFilter.setObject("", forKey: "Ages" as NSCopying)
        dictFilter.setObject("", forKey: "Categories" as NSCopying)
        dictFilter.setObject("", forKey: "Vendors" as NSCopying)
        
        let arr1 = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "filter")
        
        for i in 0..<arr1.count
        {
            let dict = NSMutableDictionary(dictionary: arr1[i] as! [AnyHashable:Any])
            let dict1 : NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
            dict1.setObject("0", forKey: "isSelected" as NSCopying)
            
            if dict["Type"] as? String != "Slider"
            {
                let aa2 = NSMutableArray(array: dict["commonDTO"] as! Array)
                for j in 0..<aa2.count
                {
                    let dic = NSMutableDictionary(dictionary: aa2[j] as! [AnyHashable:Any])
                    let dic1 : NSMutableDictionary = dic.mutableCopy() as! NSMutableDictionary
                    
                    if dict["Type"] as? String == "List" && j == 0
                    {
                        dic1.setObject("1", forKey: "isSelect" as NSCopying)
                        strItemId = String(format: "%@", dic1["Id"] as! NSNumber)
                        dictFilter.setObject(strItemId, forKey: "Item_Type_Id" as NSCopying)

                        
                        print(strItemId)
                        
                    }
                    else
                    {
                        dic1.setObject("0", forKey: "isSelect" as NSCopying)
                    }
                    
                    aa2.replaceObject(at: j, with: dic1)
                }
                
                dict1.setObject(aa2, forKey: "commonDTO" as NSCopying)
                
            }
            arr1.replaceObject(at: i, with: dict1)
        }
        
        arrFilter = arr1
        
        let arr2 = Manager.sharedInstance.getSavedDataFromTextFile(fileName: "second")
        
        for i in 0..<arr2.count
        {
            let dict = NSMutableDictionary(dictionary: arr2[i] as! [AnyHashable:Any])
            let dict1 : NSMutableDictionary = dict.mutableCopy() as! NSMutableDictionary
            dict1.setObject("0", forKey: "isSelected" as NSCopying)
            arr2.replaceObject(at: i, with: dict1)
        }
        
        arrlist = arr2
        
        DispatchQueue.main.async
            {
                self.tblList.reloadData()
                self.tblFilter.reloadData()
        }
        
        
        let calendar = UIApplication.topViewController() as! UIViewController
        if (calendar is CalendarVC)
        {
            let calendar2 = calendar as? CalendarVC
            calendar2?.dictFilterPara = dictFilter.mutableCopy() as! NSMutableDictionary
            calendar2?.collectionType.reloadData()
            calendar2?.wscallforClasses()
        }
        
        Hidesidemenu()
        
    }
    
    //MARK: CellDelegates
    
    func updateDatainArray(ar1: NSMutableArray, index: NSInteger, type: String)
    {
        let dict = NSMutableDictionary(dictionary: (arrFilter[index] as? Dictionary)!)
        dict.setObject(ar1, forKey: "commonDTO" as NSCopying)
        arrFilter.replaceObject(at: index, with: dict)
        
        if dict["Name"] as? String == "BOOK ACTIVITIES"
        {
            strItemId = type
        }
        else if dict["Name"] as? String == "AGE"
        {
            strAgeId = type
        }
        else if dict["Name"] as? String == "CATEGORY"
        {
            strCategoryId = type
        }
        else if dict["Name"] as? String == "PROVIDER"
        {
            strVendorId = type
        }

    }
    
    
    func redirecttoCMS(index: NSInteger, dict: NSMutableDictionary, pagename: String)
    {
        if strcomeFrom == "cms"
        {
            let calendar = UIApplication.topViewController() as! UIViewController
            if (calendar is CMSVC)
            {
                let cms = calendar as? CMSVC
                cms?.strTitle = dict["Title"] as? String
                cms?.strRequest = pagename
                cms?.wscallforCMS()
                Hidesidemenu()

                return
            }
        }
        
        let storyboard1 : UIStoryboard!
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }
        let home = storyboard1.instantiateViewController(withIdentifier: "CMSVC") as! CMSVC
        home.strTitle = dict["Title"] as? String
        home.strRequest = pagename
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
        

        Hidesidemenu()
        
    }
    
    
    func redirecttoList(index: NSInteger, dict: NSMutableDictionary, Id: NSNumber)
    {
        if strcomeFrom == "item"
        {
            let controller = UIApplication.topViewController() as! UIViewController
            if (controller is ItemListVC)
            {
                let cms = controller as? ItemListVC
                cms?.strTitle = dict["Title"] as? String
                Hidesidemenu()
                return
            }
        }
        else if strcomeFrom == "venue"
        {
            let controller = UIApplication.topViewController() as! UIViewController
            if (controller is VenueListVC)
            {
                let venue = controller as? VenueListVC
                let catId = dict["Cat_Id"] as? NSNumber
                venue?.strTitle = dict["Category_Name_E"] as? String

                if catId == 1
                {
                    venue?.strcomeFrom = "venue"
                    venue?.Id = 0
                    venue?.wscallforVenue()
                }
                else
                {
                    venue?.strcomeFrom = "vendor"
                    venue?.wscallforVendor()
                }
                Hidesidemenu()
                return
            }
        }
        else if strcomeFrom == "category"
        {
            let controller = UIApplication.topViewController() as! UIViewController
            if (controller is CategoryVC)
            {
                let classdetail = controller as! CategoryVC
                classdetail.catId = String(format: "%@", (dict["Cat_Id"] as? NSNumber ?? 0)!)
                classdetail.wscallforSubCategory()
                Hidesidemenu()
                return
            }
        }
        
        let catId = dict["Cat_Id"] as? NSNumber

        if catId == 1
        {
            let storyboard1 : UIStoryboard!
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            }
            
            let home = storyboard1.instantiateViewController(withIdentifier: "VenueListVC") as! VenueListVC
            home.strTitle = dict["Category_Name_E"] as? String
            home.strcomeFrom = "venue"
            home.Id = 0
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
            
        }
        else if catId == 2
        {
            let storyboard1 : UIStoryboard!
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            }
            let home = storyboard1.instantiateViewController(withIdentifier: "VenueListVC") as! VenueListVC
            home.strTitle = dict["Category_Name_E"] as? String
            home.strcomeFrom = "vendor"
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
        }
        else
        {
            let storyboard1 : UIStoryboard!
            if UserDefaults.standard.bool(forKey: "arabic")
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            }
            let classdetail = storyboard1.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
            classdetail.strTitle = dict["Category_Name_E"] as? String
            classdetail.catId = String(format: "%@", (dict["Cat_Id"] as? NSNumber ?? 0)!)
            classdetail.wscallforSubCategory()
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            
            navigationController.setViewControllers([classdetail], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
            

            
        }
        
        Hidesidemenu()
    }

    func Hidesidemenu()
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in

            if UserDefaults.standard.bool(forKey: "arabic") == false
            {
                self.viewTable.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: self.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width/2 - 10, height: self.viewTable.frame.size.height)
            }
            else
            {
                self.viewTable.frame=CGRect(x: UIScreen.main.bounds.size.width, y: self.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width/2 - 10, height: self.viewTable.frame.size.height)
            }

            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }

    
}


