//
//  SideCollectionCell.swift
//  Project
//
//  Created by HTNaresh on 5/4/19.
//  Copyright © 2019 HightecIT. All rights reserved.
//

import UIKit

class SideCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!

    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnQty: UIButton!

    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!

    
}
