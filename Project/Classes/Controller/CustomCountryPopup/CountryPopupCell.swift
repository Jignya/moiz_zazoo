//
//  CountryPopupCell.swift
//  Maki
//
//  Created by Jigar Joshi on 8/30/16.
//  Copyright © 2016 olivermaki. All rights reserved.
//

import UIKit

class CountryPopupCell: UITableViewCell {

    @IBOutlet var lblBackground: UILabel!
    @IBOutlet var imgFlag: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var ConLblLeading: NSLayoutConstraint! // 7
    @IBOutlet var ConImgWidth: NSLayoutConstraint!  // 40
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Manager.sharedInstance.setFontFamily("", for: self.contentView, andSubViews: true)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
