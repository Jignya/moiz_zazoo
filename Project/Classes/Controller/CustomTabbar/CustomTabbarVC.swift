//
//  CustomTabbarVC.swift
//  ATV
//
//  Created by HTNaresh on 3/19/18.
//  Copyright © 2018 HTNaresh. All rights reserved.
//

import UIKit

class CustomTabbarVC: UIViewController
{
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var btnAccount: UIButton!
    @IBOutlet weak var btnWishlist: UIButton!
    @IBOutlet weak var btnCart: UIButton!

    
    @IBOutlet weak var btnHome1: UIButton!
    @IBOutlet weak var btnCalendar1: UIButton!
    @IBOutlet weak var btnAccount1: UIButton!
    @IBOutlet weak var btnWishlist1: UIButton!
    @IBOutlet weak var btnCart1: UIButton!
    @IBOutlet weak var lblCount: UILabel!

    
    
    var storyboard1 : UIStoryboard!


    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnHome1.setTitle(kHome, for: .normal)
        btnCalendar1.setTitle(kcalendar, for: .normal)
        btnAccount1.setTitle(kAccount, for: .normal)
        btnWishlist1.setTitle(kWishlist, for: .normal)
        btnCart1.setTitle(kBag, for: .normal)

       
        btnHome.isSelected = true
        btnCalendar.isSelected = false
        btnAccount.isSelected = false
        btnWishlist.isSelected = false
        
        lblCount.layer.cornerRadius = 10.0
        lblCount.clipsToBounds = true
        

        setLabel()
        
//        if UserDefaults.standard.bool(forKey: "arabic")
//        {
//            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
//        }

//        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
        
        

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        Manager.sharedInstance.setFontFamily("", for: self.view, andSubViews: true)
    }
    
    func setLabel()
    {
//        lblProfile.text = kMyProfile
//        lblMagazine.text = kMagazine
//        lblSources.text = kMySources
//        lbllatest.text = kLatestNews
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnCartClick(_ sender: Any)
    {
        btnHome.isSelected =  false
        btnCalendar.isSelected = false
        btnAccount.isSelected = false
        btnWishlist.isSelected = false
        btnCart.isSelected = true
        
        btnHome1.isSelected = false
        btnCalendar1.isSelected = false
        btnAccount1.isSelected = false
        btnWishlist1.isSelected = false
        btnCart1.isSelected = true


        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true

        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
    }
    
    @IBAction func btnWishlistClick(_ sender: Any)
    {
        btnHome.isSelected = false
        btnCalendar.isSelected = false
        btnAccount.isSelected = false
        btnWishlist.isSelected = true
        btnCart.isSelected = false
        
        btnHome1.isSelected = false
        btnCalendar1.isSelected = false
        btnAccount1.isSelected = false
        btnWishlist1.isSelected = true
        btnCart1.isSelected = false


        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "WishlistVC") as! WishlistVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true

//        home.strComeFrom = ""

        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController
    }
    
    @IBAction func btnAccountClick(_ sender: Any)
    {
      
        
        if((UserDefaults.standard.object(forKey: GlobalConstants.kisLogin)) != nil)
        {
            if UserDefaults.standard.bool(forKey: GlobalConstants.kisLogin)
            {
                btnHome.isSelected = false
                btnCalendar.isSelected = false
                btnAccount.isSelected = true
                btnWishlist.isSelected = false
                btnCart.isSelected = false
        
        btnHome1.isSelected = false
        btnCalendar1.isSelected = false
        btnAccount1.isSelected = true
        btnWishlist1.isSelected = false
        btnCart1.isSelected = false

                
                
                if UserDefaults.standard.bool(forKey: "arabic")
                {
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
                }
                else
                {
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    
                    storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
                }

                let home = storyboard1.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
                let navigationController = UINavigationController()
                navigationController.isNavigationBarHidden = true

                navigationController.setViewControllers([home], animated: false)
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = navigationController
            }
            else
            {
                loginAlert()
            }
        }
        else
        {
            loginAlert()
        }

    }
    
    func loginAlert()
    {
//        let alertController = UIAlertController(title:CommanConstants.kWarningAr, message:"Please login first", preferredStyle: .alert)
//
//        // Create the actions
//        let okAction = UIAlertAction(title:kOkEn, style: .default) {
//            UIAlertAction in
//            NSLog("OK Pressed")
        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }

            let home = self.storyboard1.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            home.strComeFrom = "account"
            
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            
            navigationController.setViewControllers([home], animated: false)
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = navigationController
//        }
//
//        let CancelAction = UIAlertAction(title:kCancel, style: .default) {
//            UIAlertAction in
//            NSLog("Cancel Pressed")
//        }
//        // Add the actions
//        alertController.addAction(okAction)
//        alertController.addAction(CancelAction)
//
//        // Present the controller
//        alertController.show()
    }
    
    @IBAction func btnHomeClick(_ sender: Any)
    {
        btnHome.isSelected = true
        btnCalendar.isSelected = false
        btnAccount.isSelected = false
        btnWishlist.isSelected = false
        btnCart.isSelected = false
        
        btnHome1.isSelected = true
        btnCalendar1.isSelected = false
        btnAccount1.isSelected = false
        btnWishlist1.isSelected = false
        btnCart1.isSelected = false


        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true
        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController

    }
    
    @IBAction func btnCalendarClick(_ sender: Any)
    {
        btnHome.isSelected = false
        btnCalendar.isSelected = true
        btnAccount.isSelected = false
        btnWishlist.isSelected = false
        btnCart.isSelected = false
        
        btnHome1.isSelected = false
        btnCalendar1.isSelected = true
        btnAccount1.isSelected = false
        btnWishlist1.isSelected = false
        btnCart1.isSelected = false


        
        if UserDefaults.standard.bool(forKey: "arabic")
        {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            storyboard1 = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
            storyboard1 = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
        }

        let home = storyboard1.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        home.strcomeFrom = ""

        let navigationController = UINavigationController()
        navigationController.isNavigationBarHidden = true

        navigationController.setViewControllers([home], animated: false)
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = navigationController

    }

}
