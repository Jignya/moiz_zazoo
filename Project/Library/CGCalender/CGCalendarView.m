//
//  CGCalendarView.m
//  CapitalGene
//
//  Created by Chen Liang on 9/11/13.
//  Copyright (c) 2013 Chen Liang. All rights reserved.
//  See the LICENSE file distributed with this work.

#import "CGCalendarView.h"
#import "CGCalendarCell.h"

@interface CGCalendarView () <UITableViewDataSource, UITableViewDelegate >



@end

@implementation CGCalendarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    [self _CGCalendarView_commonInit];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder;
{
    self = [super initWithCoder:aDecoder];
    if (!self) {
        return nil;
    }
    [self _CGCalendarView_commonInit];
    return self;
}

- (void)_CGCalendarView_commonInit
{
    _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.tableView.allowsMultipleSelection = NO;
    [self addSubview:_tableView];
    CGAffineTransform rotateTable = CGAffineTransformMakeRotation(-M_PI_2);
    _tableView.transform = rotateTable;
    [_tableView setFrame:self.bounds];
    //_tableView.frame = CGRectMake(0, 500,_tableView.frame.size.width, _tableView.frame.size.height);
}

- (void)dealloc;
{
    _tableView.dataSource = nil;
    _tableView.delegate = nil;
}

- (NSCalendar *)calendar;
{
    if (!_calendar) {
        self.calendar = [NSCalendar currentCalendar];
    }
    
    return _calendar;
}

- (Class)rowCellClass;
{
    if (!_rowCellClass) {
        self.rowCellClass = [CGCalendarCell class];
    }
    return _rowCellClass;
}

- (Class)cellClassForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return [self rowCellClass];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor;
{
    [super setBackgroundColor:backgroundColor];
    [self.tableView setBackgroundColor:backgroundColor];
}

- (void)setFirstDate:(NSDate *)firstDate;
{
    // clamp to the beginning of its month
    _firstDate = firstDate;
}


- (void)setLastDate:(NSDate *)lastDate;
{
    _lastDate = lastDate;
}

- (void)setInitialSelectedDate:(NSDate *)date
{
    _selectedDate = date;
}

- (void)setSelectedDate:(NSDate *)newSelectedDate;
{
    if ([self.delegate respondsToSelector:@selector(calendarView:shouldSelectDate:)] && ![self.delegate calendarView:self shouldSelectDate:newSelectedDate]) {
        return;
    }

    _selectedDate = newSelectedDate;

    if ([self.delegate respondsToSelector:@selector(calendarView:didSelectDate:)]) {
        [self.delegate calendarView:self didSelectDate:newSelectedDate];
    }
}


- (void)scrollToDate:(NSDate *)date animated:(BOOL)animated
{
    [self.tableView scrollToRowAtIndexPath:[self indexPathForRowAtDate:date] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

#pragma mark Calendar calculations

- (CGCalendarCell *)cellForRowAtDate:(NSDate *)date;
{
    return (CGCalendarCell *)[self.tableView cellForRowAtIndexPath:[self indexPathForRowAtDate:date]];
}

- (NSInteger)sectionForDate:(NSDate *)date;
{
    return 0;
}

- (NSDate *)dateForCellAtIndexPath:(NSIndexPath*)indexPath
{
    if (_isonlymonth)
    {
        NSDateComponents *components=[[NSDateComponents alloc] init];
        components.month=indexPath.row;
        NSDate *targetDate =[self.calendar dateByAddingComponents:components toDate:self.firstDate options: 0];
        return targetDate;
    }
    else
    {
        NSDateComponents *components=[[NSDateComponents alloc] init];
        components.day=indexPath.row;
        NSDate *targetDate =[self.calendar dateByAddingComponents:components toDate:self.firstDate options: 0];
        return targetDate;
    }
   
}

- (NSIndexPath *)indexPathForRowAtDate:(NSDate *)date;
{
    if (!date) {
        return nil;
    }

    NSInteger section = [self sectionForDate:date];

    int days = [self daysWithinEraFromDate:self.firstDate toDate:date];
    return [NSIndexPath indexPathForRow:days inSection:section];
}

//https://developer.apple.com/library/ios/documentation/cocoa/Conceptual/DatesAndTimes/Articles/dtCalendricalCalculations.html
-(NSInteger)daysWithinEraFromDate:(NSDate *) startDate toDate:(NSDate *) endDate
{
    if (_isonlymonth)
    {
        NSInteger startDay=[self.calendar ordinalityOfUnit:NSMonthCalendarUnit inUnit:NSEraCalendarUnit forDate:startDate];
        NSInteger endDay=[self.calendar ordinalityOfUnit:NSMonthCalendarUnit inUnit:NSEraCalendarUnit forDate:endDate];
        return endDay-startDay;
    }
    else
    {
        NSInteger startDay=[self.calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:startDate];
        NSInteger endDay=[self.calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:endDate];
        return endDay-startDay;
    }
    
}

- (void)refresh
{
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:[self indexPathForRowAtDate:self.selectedDate] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
}

#pragma mark UIView

- (void)layoutSubviews;
{
    self.tableView.frame = self.bounds;
    if (self.selectedDate == Nil) {
        self.selectedDate = [NSDate date];
        [self.tableView selectRowAtIndexPath:[self indexPathForRowAtDate:self.selectedDate] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    }else{
        [self.tableView selectRowAtIndexPath:[self indexPathForRowAtDate:self.selectedDate] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    }
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [self daysWithinEraFromDate:self.firstDate toDate:self.lastDate];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
        static NSString *identifier = @"row";
        CGCalendarCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

        if (!cell) {
            cell = [[[self rowCellClass] alloc] initWithCalendar:self.calendar reuseIdentifier:identifier];
            cell.calendarView = self;
            
            cell.transform = CGAffineTransformMakeRotation(M_PI/2);
        }
  
        cell.isonlymonth = _isonlymonth;
    
    
//    [cell.monthLabel  sizeToFit];
//    [cell.dayLabel  sizeToFit];
//    [cell.weekdayLabel  sizeToFit];
        return cell;
}

#pragma mark UITableViewDelegate

// Call `rowCellClass` `setDate`
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDate *targetDate = [self dateForCellAtIndexPath:indexPath];
    [(CGCalendarCell*)cell setDate:targetDate];
//    UIView *v1 = [(CGCalendarCell*)cell todayDot];
    
    UILabel *l1 = [(CGCalendarCell*)cell dayLabel];
    UILabel *l2 = [(CGCalendarCell*)cell weekdayLabel];
    UILabel *l3 = [(CGCalendarCell*)cell monthLabel];
    UIImageView *i1 = [(CGCalendarCell*)cell selectedImageView];



    NSIndexPath *SelectedindexPath = [self indexPathForRowAtDate:self.selectedDate];
    
    if (indexPath.row  == SelectedindexPath.row)
    {
        l1.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
        l2.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
        l3.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
        i1.hidden = NO;
    }
    else
    {
        l1.textColor = [UIColor darkGrayColor];
        l2.textColor = [UIColor darkGrayColor];
        l3.textColor = [UIColor darkGrayColor];
        i1.hidden = YES;

    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"arabic"])
    {
        l1.transform = CGAffineTransformMakeScale(-1, 1);
        l2.transform = CGAffineTransformMakeScale(-1, 1);
        l3.transform = CGAffineTransformMakeScale(-1, 1);
    }
    
    
    
    

//    if (indexPath.row  == SelectedindexPath.row)
//    {
//        v1.backgroundColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
//    }
//    else
//    {
//        v1.backgroundColor = [UIColor clearColor];
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (_isonlymonth)
    {
        return [[self cellClassForRowAtIndexPath:indexPath] cellHeight1];
    }
    else
    {
        return [[self cellClassForRowAtIndexPath:indexPath] cellHeight];

    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isonlymonth)
    {
        CGCalendarCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
        
        NSDateComponents *components = [self.calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[NSDate date]];
        NSInteger getMonth = [components month];
        NSInteger getYear = [components year];
        int GetCurrentMonth = (int) getMonth;
        int GetCurrentyear = (int) getYear;

        
        NSDate *aDate = [cell.monthFormatter1  dateFromString:cell.monthLabel.text];
        NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:aDate];
        
        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:[[NSDate date] dateByAddingTimeInterval:-12 * 60 * 60 * 24 * 30]];
        
        NSLog(@"Month: %li", (long)[components1 month]); /* => 7 */
        NSInteger GetSelectedMonth = [components1 month];
        int convertGetMonth = (int) GetSelectedMonth;
        
        
        NSInteger GetSelectedMonth2 = [components2 month];
        int convertGetMonth2 = (int) GetSelectedMonth2;

        
//        NSInteger GetSelectedYear = [components2 year];
        int convertGetyear = [cell.dayLabel.text intValue];
        
        if(convertGetMonth > GetCurrentMonth && convertGetyear >= GetCurrentyear)
        {
            [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] animated:NO];
            [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row - 1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
            return;
        }
        else if(convertGetMonth < convertGetMonth2 && convertGetyear < GetCurrentyear)
        {
            [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] animated:NO];
            [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
            return;
        }
        else
        {
            self.selectedDate = [self dateForCellAtIndexPath:indexPath];
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
        }
    }
    else
    {
        CGCalendarCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
//
//
//        NSDate *aDate = [NSDate date];
//
//        NSDateComponents *components2 = [[NSCalendar currentCalendar] components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[[NSDate date] dateByAddingTimeInterval:-60 * 60 * 24 * 33]];
//        NSDate *otherDate = [self.calendar dateFromComponents:components2];

        
        
        
        self.selectedDate = [self dateForCellAtIndexPath:indexPath];
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
       
//        if (cell.isSelected)
//        {
//            cell.dayLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
//            cell.weekdayLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
//            cell.monthLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
//        }
//        else
//        {
//            cell.dayLabel.textColor = [UIColor darkGrayColor];
//            cell.weekdayLabel.textColor = [UIColor darkGrayColor];
//            cell.monthLabel.textColor = [UIColor darkGrayColor];
//        }

//        [self.tableView reloadData];
        
    }
    
   
    
    //        NSDateFormatter *dateformat1 = [[NSDateFormatter alloc] init];
    //        [dateformat1 setDateFormat:@"dd MM YYYY"];
    //
    //        NSString *strToday = [dateformat1 stringFromDate:[NSDate date]];
    //        NSString *strselected = [dateformat1 stringFromDate:self.selectedDate];
    //
    //
    //        NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    //        [dateformat setDateFormat:@"MMM YYYY"];
    //
    //        NSDate *d1 = [dateformat dateFromString:strToday];
    //        NSDate *d2 = [dateformat dateFromString:strselected];

    
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;
{

    if (velocity.y > 0.38f) {
        velocity.y = 0.38f;
    }

    scrollView.decelerationRate = velocity.y;

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //if (scrollView.dragging && !scrollView.decelerating) {
        //[self detectScrollViewSelectedDate:scrollView];
    //}
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self detectScrollViewSelectedDate:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{

    if (!decelerate)
    {
        [self detectScrollViewSelectedDate:scrollView];
    }

}



- (void)detectScrollViewSelectedDate:(UIScrollView *)scrollView
{
    CGPoint centerPoint = CGPointMake(self.tableView.frame.origin.x + self.tableView.frame.size.width/2, self.tableView.frame.origin.y + self.tableView.frame.size.height/2);

    CGPoint sPoint = [self convertPoint:centerPoint toView:scrollView];

    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:sPoint];
    NSIndexPath *SelectedindexPath = [self indexPathForRowAtDate:self.selectedDate];

    CGRect rec = [self.tableView rectForRowAtIndexPath:SelectedindexPath];

    if (!CGRectContainsPoint(rec, sPoint))
    {
        
        self.selectedDate = [self dateForCellAtIndexPath:indexPath];
         [self.tableView selectRowAtIndexPath:[self indexPathForRowAtDate:self.selectedDate] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        
    }

}

@end
