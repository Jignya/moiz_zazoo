//
//  CGCalendarCell.m
//  CapitalGene
//
//  Created by Chen Liang on 9/11/13.
//  Copyright (c) 2013 Chen Liang. All rights reserved.
//  See the LICENSE file distributed with this work.

#import "CGCalendarCell.h"
#import "UIImage+Additions.h"
#import "UIView+ViewHelpers.h"
@interface CGCalendarCell()

//@property (nonatomic, strong) UILabel *dayLabel;
//@property (nonatomic, strong) UILabel *weekdayLabel;
//@property (nonatomic, strong) UILabel *monthLabel;

//@property (nonatomic, strong) UIImageView *selectedImageView;
@property (nonatomic, strong) NSDateFormatter *dayFormatter;
@property (nonatomic, strong) NSDateFormatter *weekdayFormatter;
@property (nonatomic, strong) NSDateFormatter *monthFormatter;
//@property (nonatomic, strong) NSDateFormatter *yearFormatter;

@property (nonatomic, strong) NSDateComponents *todayDateComponents;
@property (nonatomic) BOOL isToday;
@property (nonatomic) BOOL isGreater;


@end

CGFloat height;
CGFloat height1;

@implementation CGCalendarCell

- (id)initWithCalendar:(NSCalendar *)calendar reuseIdentifier:(NSString *)reuseIdentifier;
{
    self = [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
        
    }
    _calendar = calendar;
    
    CGFloat onePixel = 1.0f / [UIScreen mainScreen].scale;
    
    static CGSize shadowOffset;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shadowOffset = CGSizeMake(0.0f, onePixel);
    });
    self.shadowOffset = shadowOffset;
    self.columnSpacing = onePixel;
    self.textColor = [UIColor colorWithRed:0.47f green:0.5f blue:0.53f alpha:1.0f];
    self.backgroundColor = [UIColor clearColor];
    
    static dispatch_once_t imgToken;
    static UIImage *img;
    dispatch_once(&imgToken, ^{
        img = [UIImage imageWithColor:[UIColor whiteColor] size:CGSizeMake([[self class]cellHeight]-4, [[self class]cellHeight]-4) andRoundSize:8.0];
    });
    //    self.selectedImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arr.png"]];
    
    return self;
    
    
}
+ (CGFloat)cellHeight;
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 90;
        
    }
    else
    {
        return 70;
        
    }
}

+ (CGFloat)cellHeight1;
{
    
    return UIScreen.mainScreen.bounds.size.width / 3;
}

- (void)createDayLabel
{
    if (_isonlymonth)
    {
        self.monthLabel = [UILabel new];
        [self.monthLabel sizeToFit];
        self.monthLabel.font = [UIFont fontWithName:@"Dax-Light" size:10];
        self.monthLabel.backgroundColor = [UIColor clearColor];
        self.monthLabel.textColor = [UIColor whiteColor];
        [self.contentView insertSubview:self.monthLabel atIndex:0];
        
        self.dayLabel = [UILabel new];
        [self.dayLabel  sizeToFit];
        
        self.dayLabel.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:10];
        self.dayLabel.textColor = [UIColor whiteColor];
        self.dayLabel.backgroundColor = [UIColor clearColor];
        
        [self.contentView insertSubview:self.dayLabel atIndex:1];
        
        self.todayDot = [UIView new];
        self.todayDot.backgroundColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
        [self.contentView insertSubview:self.todayDot atIndex:3];
    }
    else
    {
        self.monthLabel = [UILabel new];
        [self.monthLabel sizeToFit];
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            self.monthLabel.font = [UIFont fontWithName:@"Dax-Light" size:17];
            
        }
        else
        {
            self.monthLabel.font = [UIFont fontWithName:@"Dax-Light" size:13];
            
        }
        
        self.monthLabel.backgroundColor = [UIColor clearColor];
        self.monthLabel.textColor = [UIColor whiteColor];
        [self.contentView insertSubview:self.monthLabel atIndex:0];
        
        self.dayLabel = [UILabel new];
        [self.dayLabel sizeToFit];
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            self.dayLabel.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:15];
            
        }
        else
        {
            self.dayLabel.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:8];
            
        }
        self.dayLabel.textColor = [UIColor whiteColor];
        self.dayLabel.backgroundColor = [UIColor clearColor];
        
        [self.contentView insertSubview:self.dayLabel atIndex:1];
        
        self.selectedImageView = [UIImageView new];
        [self.contentView insertSubview:self.selectedImageView atIndex:2];
        
        
        //        self.weekdayLabel = [UILabel new];
        //        [self.weekdayLabel sizeToFit];
        //
        //        self.weekdayLabel.font = [UIFont fontWithName:@"Dax-Light" size:10];
        //        self.weekdayLabel.backgroundColor = [UIColor clearColor];
        //        self.weekdayLabel.textColor = [UIColor whiteColor];
        //        [self.contentView insertSubview:self.weekdayLabel atIndex:2];
        //        self.todayDot = [UIView new];
        //        self.todayDot.backgroundColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
        //        [self.contentView insertSubview:self.todayDot atIndex:3];
        
    }
    
}

- (UITableViewCellSelectionStyle)selectionStyle;
{
    return UITableViewCellSelectionStyleNone;
}

- (void)setIsToday:(BOOL)isToday
{
    if (isToday) {
        [self.todayDot setHidden:NO];
    }
    else
    {
        //[self.todayDot setHidden:YES];
        self.todayDot.backgroundColor = [UIColor clearColor];
        
    }
    _isToday = isToday;
}

- (void)setDate: (NSDate*)date
{
    _date = date;
    
    if (!self.dayLabel)
    {
        [self createDayLabel];
    }
    if (_isonlymonth)
    {
        NSDateComponents *components = [self.calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth) fromDate:[NSDate date]];
        NSDate *today = [self.calendar dateFromComponents:components];
        NSDateComponents *components1  = [[NSCalendar currentCalendar] components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth) fromDate:[[NSDate date] dateByAddingTimeInterval:-12 * 60 * 60 * 24 * 30]];
        NSDate *previousDate = [self.calendar dateFromComponents:components1];
        components = [self.calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth) fromDate:date];
        NSDate *otherDate = [self.calendar dateFromComponents:components];
        if([today isEqualToDate:otherDate])
        {
            self.isToday = YES;
        }
        else
        {
            self.isToday = NO;
        }
        
        if ([today compare:otherDate] == NSOrderedAscending)
        {
            self.dayLabel.textColor = [UIColor lightGrayColor];
            self.weekdayLabel.textColor = [UIColor lightGrayColor];
            self.monthLabel.textColor = [UIColor lightGrayColor];
        }
        else if ([otherDate compare:previousDate] == NSOrderedAscending)
        {
            self.dayLabel.textColor = [UIColor lightGrayColor];
            self.weekdayLabel.textColor = [UIColor lightGrayColor];
            self.monthLabel.textColor = [UIColor lightGrayColor];
        }
        
        else
        {
            self.dayLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
            self.weekdayLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
            self.monthLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
        }
        
        self.monthLabel.text = [self.monthFormatter stringFromDate:date];
        self.dayLabel.text = [self.yearFormatter stringFromDate:date];
        
        self.monthLabel.textAlignment = NSTextAlignmentCenter;
        self.dayLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    else
    {
        NSDateComponents *components = [self.calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitDay|NSCalendarUnitMonth) fromDate:[NSDate date]];
        NSDate *today = [self.calendar dateFromComponents:components];
        NSDateComponents *components11  = [[NSCalendar currentCalendar] components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitDay|NSCalendarUnitMonth) fromDate:[[NSDate date] dateByAddingTimeInterval:-60 * 60 * 24 * 30]];
        NSDate *previousDate1 = [self.calendar dateFromComponents:components11];
        components = [self.calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitDay|NSCalendarUnitMonth) fromDate:date];
        NSDate *otherDate = [self.calendar dateFromComponents:components];
        
        if([today isEqualToDate:otherDate])
        {
            self.isToday = YES;
            
        }else{
            self.isToday = NO;
        }
        
        
        
        if ([today compare:otherDate] == NSOrderedAscending)
        {
            self.dayLabel.textColor = [UIColor blackColor];
            self.weekdayLabel.textColor = [UIColor blackColor];
            self.monthLabel.textColor = [UIColor blackColor];
            
        }
        
        else if ([otherDate compare:previousDate1] == NSOrderedAscending)
        {
            self.dayLabel.textColor = [UIColor blackColor];
            self.weekdayLabel.textColor = [UIColor blackColor];
            self.monthLabel.textColor = [UIColor blackColor];
            
        }
        
        else
        {
            self.dayLabel.textColor = [UIColor colorWithRed:247.0 / 255.0 green:166.0 / 255.0 blue:148.0 / 255.0 alpha:1];
            self.weekdayLabel.textColor = [UIColor colorWithRed:247.0 / 255.0 green:166.0 / 255.0 blue:148.0 / 255.0 alpha:1];
            self.monthLabel.textColor = [UIColor colorWithRed:247.0 / 255.0 green:166.0 / 255.0 blue:148.0 / 255.0 alpha:1];
            
        }
        [self.dayLabel  sizeToFit];
        
        // jignya
        
        
        if (_isToday)
        {
            self.monthLabel.text = [[NSUserDefaults standardUserDefaults] boolForKey:@"arabic"] ? @"اليوم" : @"TODAY";
            
        }
        else
        {
            self.monthLabel.text = [NSString stringWithFormat:@"%@",[[self.weekdayFormatter stringFromDate:date] uppercaseString]];
            
        }
        
        self.dayLabel.text = [NSString stringWithFormat:@"%@ %@",[[self.monthFormatter stringFromDate:date] uppercaseString],[self.dayFormatter stringFromDate:date]];
        
        
        
        
        //        self.dayLabel.text = [self.dayFormatter stringFromDate:date];
        //        self.weekdayLabel.text = [[self.weekdayFormatter stringFromDate:date] uppercaseString];
        //        self.monthLabel.text = [self.monthFormatter stringFromDate:date];
        
        
        
    }
    
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    //if(self.selected == NO){
    /*
     if(highlighted){
     self.contentView.backgroundColor = [UIColor redColor];
     }else{
     self.contentView.backgroundColor = [UIColor clouds];
     }
     */
    //}else{
    //    self.contentView.backgroundColor = [UIColor orange];
    // }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated;
{
    if (_isonlymonth)
    {
        //        NSDateComponents *components = [self.calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[NSDate date]];
        //        NSInteger getMonth = [components month];
        //        int GetCurrentMonth = (int) getMonth;
        //
        //        NSString *getStrFromLabel;
        
        if (selected)
        {
            self.todayDot.backgroundColor =  [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];;
            [self.contentView.layer setBorderWidth:0.0f];
            NSLog(@"%@",self.contentView.subviews);
            //            if(self.contentView.subviews.count > 0 )
            //            {
            //                if([self.contentView.subviews[0] isKindOfClass:[UILabel class]])
            //                {
            //                    UILabel *getLabel = (UILabel *)self.contentView.subviews[0];
            //                    getStrFromLabel = getLabel.text;
            //                    NSLog(@"%@",getStrFromLabel);
            //
            //                }
            //            }
            
            //            NSDate *aDate = [self.monthFormatter  dateFromString:getStrFromLabel];
            //            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:aDate];
            //            NSLog(@"Month: %li", (long)[components month]); /* => 7 */
            //            NSInteger GetSelectedMonth = [components month];
            //            int convertGetMonth = (int) GetSelectedMonth;
            //
            //            if(convertGetMonth > GetCurrentMonth)
            //            {
            //                self.todayDot.backgroundColor = [UIColor clearColor];
            //                return;
            //            }
            //            else
            {
                self.todayDot.backgroundColor =  [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];;
                [self.contentView.layer setBorderWidth:0.0f];
                
            }
            //
        }
        else
        {
            self.todayDot.backgroundColor = [UIColor redColor];
            [self.contentView.layer setBorderWidth:0.0f];
        }
        
    }
    else
    {
        if (selected)
        {
            self.dayLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
            self.weekdayLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
            self.monthLabel.textColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
            
            self.selectedImageView.hidden = NO;
        }
        else
        {
            self.dayLabel.textColor = [UIColor darkGrayColor];
            self.weekdayLabel.textColor = [UIColor darkGrayColor];
            self.monthLabel.textColor = [UIColor darkGrayColor];
            
            self.selectedImageView.hidden = YES;
            
        }
        
    }
}
/*
 - (void)layoutViewsForColumnAtIndex:(NSUInteger)index inRect:(CGRect)rect;
 {
 // for subclass to implement
 }
 */

- (void)layoutSubviews;
{
    [super layoutSubviews];
    
    self.contentView.frame = self.bounds;
    
    //    height = self.contentView.frame.size.width / 7;
    //    height1 = self.contentView.frame.size.width / 3;
    
    if (_isonlymonth)
    {
        [self.monthLabel sizeToFit];
        self.monthLabel.font = [UIFont systemFontOfSize:12];
        self.monthLabel.frame  = CGRectMake(self.contentView.center.x, self.contentView.center.y - 10, 40 , 15);
        self.monthLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y - 5);
        
        [self.dayLabel sizeToFit];
        self.dayLabel.font = [UIFont systemFontOfSize:12];
        self.dayLabel.frame  = CGRectMake(self.contentView.center.x, self.contentView.center.y + 10, 40 , 15);
        
        self.dayLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y + 5 );
        
        //self.todayDot.center = CGPointMake(self.contentView.center.x, self.contentView.center.y + 20);
        
        self.todayDot.frame = CGRectMake(self.contentView.center.x - 55, 47, (UIScreen.mainScreen.bounds.size.width / 3) - 15, 5);
        
        self.selectedImageView.center = self.contentView.center;
        [self.contentView sendSubviewToBack:self.selectedImageView];
    }
    else
    {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            [self.monthLabel sizeToFit];
            self.monthLabel.font = [UIFont systemFontOfSize:17];
            self.monthLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y - 10);
            
            [self.dayLabel sizeToFit];
            self.dayLabel.font = [UIFont systemFontOfSize:15];
            self.dayLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y + 15);
            
            self.dayLabel.textAlignment = NSTextAlignmentCenter;
            self.monthLabel.textAlignment = NSTextAlignmentCenter;

        }
        else
        {
            [self.monthLabel sizeToFit];
            self.monthLabel.font = [UIFont systemFontOfSize:13];
            self.monthLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y - 10);
            
            [self.dayLabel sizeToFit];
            self.dayLabel.font = [UIFont systemFontOfSize:11];
            self.dayLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y + 5);
            
            self.dayLabel.textAlignment = NSTextAlignmentCenter;
            self.monthLabel.textAlignment = NSTextAlignmentCenter;

        }
        
        
        
        //        [self.weekdayLabel sizeToFit];
        //        self.weekdayLabel.font = [UIFont systemFontOfSize:12];
        //        self.weekdayLabel.center = CGPointMake(self.contentView.center.x, self.contentView.center.y + 20);
        //        self.todayDot.backgroundColor = [UIColor colorWithRed:240.0 / 255.0 green:120.0 / 255.0 blue:23.0 / 255.0 alpha:1];
        
        self.selectedImageView.frame  = CGRectMake(self.contentView.center.x, self.contentView.center.y + 26, 50 , 50);
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        {
            self.selectedImageView.center = CGPointMake(self.contentView.center.x, self.contentView.center.y + 36 );
            
        }
        else
        {
            self.selectedImageView.center = CGPointMake(self.contentView.center.x, self.contentView.center.y + 26 );
            
        }
        
        self.selectedImageView.image = [UIImage imageNamed:@"arr.png"];
        self.selectedImageView.image = [self.selectedImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.selectedImageView setTintColor:[UIColor whiteColor]];
        
        self.selectedImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        
        
        
        //        self.selectedImageView.center = self.contentView.center;
        
        //        [self.contentView sendSubviewToBack:self.selectedImageView];
        //[self.contentView bringSubviewToFront:self.contentView];
    }
    
    
    
}

- (NSDateFormatter *)dayFormatter
{
    if (!_dayFormatter) {
        _dayFormatter = [NSDateFormatter new];
        _dayFormatter.calendar = self.calendar;
        _dayFormatter.dateFormat = @"d";
    }
    return _dayFormatter;
}

- (NSDateFormatter *)weekdayFormatter
{
    if (!_weekdayFormatter) {
        _weekdayFormatter = [NSDateFormatter new];
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"arabic"])
        {
            [_weekdayFormatter setLocale: [NSLocale localeWithLocaleIdentifier:@"ar_SA"]];
        }
        else
        {
            [_weekdayFormatter setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US"]];
        }
        _weekdayFormatter.calendar = self.calendar;
        _weekdayFormatter.dateFormat = @"E";
    }
    
    return _weekdayFormatter;
}

- (NSDateFormatter *)monthFormatter
{
    if (!_monthFormatter) {
        _monthFormatter = [NSDateFormatter new];
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"arabic"])
        {
            [_monthFormatter setLocale: [NSLocale localeWithLocaleIdentifier:@"ar_SA"]];
        }
        else
        {
            [_monthFormatter setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US"]];
        }
        _monthFormatter.calendar = self.calendar;
        _monthFormatter.dateFormat = @"MMM";
    }
    
    return _monthFormatter;
}
- (NSDateFormatter *)monthFormatter1
{
    if (!_monthFormatter1) {
        _monthFormatter1 = [NSDateFormatter new];
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"arabic"])
        {
            [_monthFormatter1 setLocale: [NSLocale localeWithLocaleIdentifier:@"ar_SA"]];
        }
        else
        {
            [_monthFormatter1 setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US"]];
        }
        _monthFormatter1.calendar = self.calendar;
        _monthFormatter1.dateFormat = @"MMM";
    }
    
    return _monthFormatter;
}

- (NSDateFormatter *)yearFormatter
{
    if (!_yearFormatter) {
        _yearFormatter = [NSDateFormatter new];
        _yearFormatter.calendar = self.calendar;
        _yearFormatter.dateFormat = @"YYYY";
    }
    
    return _yearFormatter;
}
- (NSDateComponents *)todayDateComponents;
{
    if (!_todayDateComponents) {
        self.todayDateComponents = [self.calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    }
    return _todayDateComponents;
}

- (void)prepareForReuse
{
    self.contentView.backgroundColor = [UIColor clearColor];
}
@end
