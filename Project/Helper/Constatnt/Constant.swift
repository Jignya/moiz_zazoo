//
//  Constrant.swift
//  Cosmos
//
//  Created by MAC on 1/28/18.
//  Copyright © 2018 cosmoskw. All rights reserved.
//

import Foundation


struct GlobalConstants
{
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

//    static let kBaseURL = "http://13.90.151.26:803/"
//    static let kShareUrl = "http://13.90.151.26:803/ItemDetail.aspx?Id="

    static let kBaseURL = "http://admin.myzazoo.com/"
    static let kShareUrl = "http://admin.myzazoo.com/ItemDetail.aspx?Id="

    static let kisArabic = UserDefaults.standard.bool(forKey: "arabic")

    static let englishStoryboard =  UIDevice.current.userInterfaceIdiom == .pad ? "Main_ipad" : "Main"
    
    static let arabicStoryboard =  UIDevice.current.userInterfaceIdiom == .pad ? "Main_ipad_Ar" : "Main_Ar"

    static let brownClr = UIColor(red: 231.0/255.0, green: 97.0/255.0, blue: 88.0/255.0, alpha: 1)
    
    static let highLightClr = UIColor.red


//    static let kFavData = "fav.txt"
//    static let kOrderData = "order.txt"
//    static let kOrderDetail = "orderDetail.txt"
    
    static let selectedClr = UIColor.white.cgColor
    static let themeClr = UIColor(red: 231.0/255.0, green: 123.0/255.0, blue: 79.0/255.0, alpha: 1)

    static let kEnglishRegular = "Calibri"
    static let kEnglishBold = "Calibri-Bold"
    
    static let kArabicLight = "Calibri"
    static let kArabicRegular = "Calibri"
    static let kArabicBold = "Calibri-Bold"

    static let kStoreKey = "X-Zaazoo-SecurityToken"
    static let kHeaderKey = "X-Zaazoo-SecurityToken"

    static let kisFirstTime = "XIsATVFirstTime"
    static let kisLogin = "isLogin"


    static let wRegisterDevice = "RegisterDevice"
    static let wLogin = "Login"
    static let wRegister = "Register"
    static let wChangePassword = "ChangePassword"
    static let wGetHomePage = "GetHomePage"
    static let wGetSideMenu = "GetSideMenu"
    static let wGetClaases = "GetClasses"
    static let wRemoveChildren = "RemoveChildren"
    static let wAddChildren = "AddChildren"
    static let wGetClassDetail = "GetClassDetail"
    static let wBookClass = "BookClass"
    static let wGetPartyVendors = "GetPartyVendors"
    static let wGetParties = "GetParties"
    static let wAddToCart = "AddToCart"
    static let wRemoveCartItem = "RemoveCartItem"
    static let wRemoveCart = "RemoveCart"
    static let wGetCart = "GetCart"
    static let wGetNotifications = "GetNotifications"
    static let wGetNotificationDetail = "GetNotificationDetail"

    static let wReadNotification = "ReadNotification"
    static let wDeleteNotification = "DeleteNotification"
    static let wSwitchNotification = "SwitchNotification"
    static let wGetFavoriteItems = "GetFavoriteItems"
    static let wAddFavoriteItem = "AddFavoriteItem"
    static let wRemoveFavoriteItem = "RemoveFavoriteItem"
    static let wLogout = "Logout"
    
    static let wGetSubCategories = "GetSubCategories"
    static let wGetItems = "GetItems"
    static let wGetItemDetail = "GetItemDetail"
    static let wGetSaleItemDetail = "GetSaleItemDetail"
    
    

    static let wSaveOrder = "SaveOrder"
    static let wGetAdvertisements = "GetAdvertisements"
    static let wGetUserAddress = "GetUserAddress"
    static let wUpdateUserAddress = "UpdateUserAddress"
    static let wDeleteUserAddress = "DeleteUserAddress"
    static let wGetAreas = "GetAreas"
    static let wGetOrderPaymentDetail = "GetOrderPaymentDetail"
    static let wGetUserProfile = "GetUserProfile"
    static let wCancelOrder = "CancelOrder"
    static let wGetDeviceOrderHistory = "GetDeviceOrderHistory"
    static let wGetOrderDetail = "GetOrderDetail"
    static let wGetUserOrderHistory = "GetUserOrderHistory"
    static let wUpdateProfile = "UpdateProfile"
    static let wGetCMSContent = "GetCMSContent"
    static let wForgotPassword = "ForgotPassword"
    static let wGetPaymentTypes =  "GetPaymentTypes"
    static let wGetTerms =  "GetTerms"
    static let wGetChildren =  "GetChildren"
    static let wValidPromoCode =  "ValidPromoCode"

    static let wRefundRequest =  "RefundRequest"

    static let wGetVendors = "GetVendors"
    static let wGetVendorItems = "GetVendorItems"
    static let wGetNewHomePage = "GetNewHomePage"
    static let wGetClassesVendors = "GetClassesVendors"
    static let wGetVendorClassDetail = "GetVendorClassDetail"
    static let wUpdateLanguage = "UpdateLanguage"
    static let wGetBundleClassDetail = "GetBundleClassDetail"

    
    //   static let kKNETUrl = "http://13.90.151.26:803/PantoneKnet.aspx?orderId="
    //   static let kPaymentSuccessUrl1 = "http://13.90.151.26:803/Receipt.aspx"

    
    static let kKNETUrl = "http://admin.myzazoo.com/PantoneKnet.aspx?orderId="
    static let kPaymentSuccessUrl1 = "http://admin.myzazoo.com/Receipt.aspx"
    static let kPaymentSuccessUrl = "Receipt.aspx"
    static let kPaymentFailureUrl  = "Error.aspx"

}




