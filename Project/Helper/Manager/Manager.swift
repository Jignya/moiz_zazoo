//
//  Manager.swift
//  Maki
//   Change check
//  Created by Jigar Joshi on 7/27/16.
//  Copyright © 2016 olivermaki. All rights reserved.
//

import UIKit
import Foundation

class Manager: NSObject
{
    static let sharedInstance = Manager()
    var sidebar = SideBarVC()
    var CountryPopup = CustomCountryPopup()
    var tabbarController = CustomTabbarVC()



    var arrArea : NSMutableArray = []
    var arrFilter : NSMutableArray = []
    var arrcart : NSMutableArray = []
    var arrcartOrder : NSMutableArray = []

    var dictprofile : NSMutableDictionary = [:]
    var dictHomePage : NSMutableDictionary = [:]

    var CartCount : Int!
    var Slidetime : Int!
    var strItemId : String!

    var Mintime : CGFloat = 0
    var Maxtime : CGFloat = 0
    
    var isRemoveCart : NSNumber = 0
    var RemindNotificationTime : Int! = 15
//    var refundHourClass : Int = 72
//    var refundHourSemester : Int = 72
//    var refundHourVenue : Int = 72

    
    //MARK: get comma separeted string
    func getCommaSeparatedValuefromArray(_ array: NSMutableArray, forkey strKey: String) -> String
    {
        let array = array
        var strSepratedKey = ""
        
        for i in 0..<(array.count)
        {
            var dict = array[i] as? [AnyHashable : Any]
            strSepratedKey = strSepratedKey + (", \(dict?[strKey ] ?? "")")
        }
        if strSepratedKey.hasPrefix(",") && strSepratedKey.count > 1
        {
            strSepratedKey = (strSepratedKey as? NSString)!.substring(from: 1)
        }
        if strSepratedKey.hasPrefix(" ") && strSepratedKey.count > 1
        {
            strSepratedKey = (strSepratedKey as? NSString)!.substring(from: 1)
        }
        
        return strSepratedKey
    }

    func arrFilterDataforInt(_ str1: Int?, withPredicateKey str2: String?, fromArray arrMain: NSMutableArray) -> NSMutableArray
    {
        let arrMain = arrMain
        let predicate = NSPredicate(format: "self.%@ = %d", str2 ?? "", str1 ?? 0)
        var arr1: [AnyHashable]? = nil
        if let filtered = (arrMain as NSArray?)?.filtered(using: predicate) {
            arr1 = filtered as? [AnyHashable]
        }
        
        return NSMutableArray(array: (arr1 as? Array)!)
    }
    
    func arrFilterDataforstring(_ str1: String?, withPredicateKey str2: String?, fromArray arrMain: NSMutableArray) -> NSMutableArray
    {
        let arrMain = arrMain
        let predicate = NSPredicate(format: "self.%@ LIKE[c] %@", str2 ?? "", str1 ?? "")
        var arr1: [AnyHashable]? = nil
        if let filtered = (arrMain as NSArray?)?.filtered(using: predicate) {
            arr1 = filtered as? [AnyHashable]
        }
        
        return NSMutableArray(array: (arr1 as? Array)!)
    }
    
    func arrFilterDataforKey(_ str1: String?, withPredicateKey str2: String?, fromArray arrMain: NSMutableArray) -> NSMutableArray
    {
        let arrMain = arrMain
        let predicate = NSPredicate(format: "self.%@ = 1", str2 ?? "")
        var arr1: [AnyHashable]? = nil
        if let filtered = (arrMain as NSArray?)?.filtered(using: predicate) {
            arr1 = filtered as? [AnyHashable]
        }
        
        return NSMutableArray(array: (arr1 as? Array)!)
    }


    
    //MARK: set In text file
    func SaveDatainTextFile(fileName:String , arrData:NSMutableArray)
    {
        var jsonData: NSData!
        do
        {
            jsonData = try JSONSerialization.data(withJSONObject: arrData, options: JSONSerialization.WritingOptions()) as NSData
        }
        catch let error as NSError
        {
            print("Array to JSON conversion failed: \(error.localizedDescription)")
        }
        let file = fileName
        let writePath = (self.documentsDirectory() as NSString).appendingPathComponent(file)
        jsonData!.write(toFile: writePath, atomically:true)
    }
    
    
    func getSavedDataFromTextFile(fileName:String) -> NSMutableArray
    {
        let file = fileName
        
        var dataarray : NSMutableArray = []
        
        if let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first {
            let path = NSURL(fileURLWithPath: dir).appendingPathComponent(file)
            do {
                let text2 = try String(contentsOf: path!, encoding: .utf8)
                
                let data = text2.data(using: String.Encoding.utf8)
                
               let jsonArray = try JSONSerialization.jsonObject(with:data!, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                
//                print("get Array \(jsonArray)")

                dataarray = NSMutableArray(array: jsonArray)

            }
            catch let error as NSError
            {
                print(error.localizedDescription)
            }
        }
        
        return dataarray
    }
    
    func DeleteSavedDataFromTextFile(fileName:String)
    {
        let file = fileName
        
        var filePath = ""
        
        
        let dirs : [String] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
        
        if dirs.count > 0
        {
            let dir = dirs[0] //documents directory
            filePath = dir.appendingFormat("/" + file)
            print("Local path = \(filePath)")
            
        } else {
            print("Could not find local directory to store file")
            return
        }
        
        
        do {
            
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: filePath)
            {
                // Delete file
                try fileManager.removeItem(atPath: filePath)
            }
            else
            {
                print("File does not exist")
            }
            
        }
        catch let error as NSError
        {
            print(error.localizedDescription)
        }
        
    }
    
    func documentsDirectory() -> String
    {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        return documentsFolderPath
    }
    
    func StrikeThroughString(str:String) -> NSMutableAttributedString
    {
        let attributedString = NSMutableAttributedString(string: str)
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributedString.length))
        
        return attributedString

    }
    
    func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK:- add tabbar
    
    func addTabBar2(_ controller: UIViewController?,tab:String)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!

        var bottomPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }
        //        if view == nil
        //        {
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
            tabbarController = CustomTabbarVC(nibName: "CustomTabbarVC1", bundle: nil)
        }
        else
        {
            tabbarController = CustomTabbarVC(nibName: "CustomTabbarVC", bundle: nil)
        }
        
        tabbarController.view.tag = 111
        controller?.view.addSubview(tabbarController.view)
        if let aWidth = window?.frame.size.width
        {
            tabbarController.view.frame = CGRect(x: 0, y: (controller!.view.frame.size.height) - tabbarController.view.frame.size.height - bottomPadding, width: aWidth, height: (UIDevice.current.userInterfaceIdiom == .pad) ? 80 : 60)
        }
        
        
        tabbarController.view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.RawValue(UInt8(UIView.AutoresizingMask.flexibleWidth.rawValue) | UInt8(UIView.AutoresizingMask.flexibleTopMargin.rawValue)))
        
        
        tabbarController.btnHome.isSelected = false
        tabbarController.btnCalendar.isSelected = false
        tabbarController.btnAccount.isSelected = false
        tabbarController.btnWishlist.isSelected = false
        tabbarController.btnCart.isSelected = false
        
        tabbarController.btnHome1.isSelected = false
        tabbarController.btnCalendar1.isSelected = false
        tabbarController.btnAccount1.isSelected = false
        tabbarController.btnWishlist1.isSelected = false
        tabbarController.btnCart1.isSelected = false
        
        if tab == "0"
        {
            tabbarController.btnHome.isSelected = true
            tabbarController.btnHome1.isSelected = true
        }
        else if tab == "1"
        {
            tabbarController.btnCalendar.isSelected = true
            tabbarController.btnCalendar1.isSelected = true
        }
        else if tab == "2"
        {
            tabbarController.btnAccount.isSelected = true
            tabbarController.btnAccount1.isSelected = true
        }
        else if tab == "3"
        {
            tabbarController.btnWishlist.isSelected = true
            tabbarController.btnWishlist1.isSelected = true
        }
        else if tab == "4"
        {
            tabbarController.btnCart.isSelected = true
            tabbarController.btnCart1.isSelected = true
        }
        
        
        if self.arrcart.count > 0
        {
            let strCount =  String(format: "%d", CartCount)
            if strCount == "0"
            {
                self.tabbarController.lblCount.isHidden = true
            }
            else
            {
                self.tabbarController.lblCount.isHidden = false
                self.tabbarController.lblCount.text = strCount
            }
        }
        else
        {
            self.tabbarController.lblCount.isHidden = true
        }
        
        
        
    }
    
    func addTabBar1(_ controller: UIViewController?,tab:String)
    {
        return
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
        let view: UIView? = window?.viewWithTag(111)
        
        var bottomPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }

        
//        if view == nil
//        {
            tabbarController = CustomTabbarVC(nibName: "CustomTabbarVC", bundle: nil)
            tabbarController.view.tag = 111
            controller?.view.addSubview(tabbarController.view)
            if let aWidth = window?.frame.size.width
            {
                tabbarController.view.frame = CGRect(x: 0, y: (window?.frame.size.height)! - tabbarController.view.frame.size.height - bottomPadding, width: aWidth, height: tabbarController.view.frame.size.height)
            }
//        }
        
        tabbarController.btnHome.isSelected = false
        tabbarController.btnCalendar.isSelected = false
        tabbarController.btnAccount.isSelected = false
        tabbarController.btnWishlist.isSelected = false
        tabbarController.btnCart.isSelected = false
        
        tabbarController.btnHome1.isSelected = false
        tabbarController.btnCalendar1.isSelected = false
        tabbarController.btnAccount1.isSelected = false
        tabbarController.btnWishlist1.isSelected = false
        tabbarController.btnCart1.isSelected = false

        if tab == "0"
        {
            tabbarController.btnHome.isSelected = true
            tabbarController.btnHome1.isSelected = true
        }
        else if tab == "1"
        {
            tabbarController.btnCalendar.isSelected = true
            tabbarController.btnCalendar1.isSelected = true
        }
        else if tab == "2"
        {
            tabbarController.btnAccount.isSelected = true
            tabbarController.btnAccount1.isSelected = true
        }
        else if tab == "3"
        {
            tabbarController.btnWishlist.isSelected = true
            tabbarController.btnWishlist1.isSelected = true
        }
        else if tab == "4"
        {
            tabbarController.btnCart.isSelected = true
            tabbarController.btnCart1.isSelected = true
        }
        
   
        
    }
    
    func addTabBar(_ controller: UIViewController?,tab:String)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
//        let view: UIView? = window?.viewWithTag(101)
        
        var bottomPadding : CGFloat = 0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }
        
        
//        if view == nil
//        {
            tabbarController = CustomTabbarVC(nibName: "CustomTabbarVC", bundle: nil)
            tabbarController.view.tag = 111
            window!.addSubview(tabbarController.view)
            if let aWidth = window?.frame.size.width
            {
                tabbarController.view.frame = CGRect(x: 0, y: (window?.frame.size.height)! - tabbarController.view.frame.size.height - bottomPadding, width: aWidth, height: tabbarController.view.frame.size.height)
            }
//        }
        
        
        
        tabbarController.btnHome.isSelected = false
        tabbarController.btnCalendar.isSelected = false
        tabbarController.btnAccount.isSelected = false
        tabbarController.btnWishlist.isSelected = false
        tabbarController.btnCart.isSelected = false
        
        tabbarController.btnHome1.isSelected = false
        tabbarController.btnCalendar1.isSelected = false
        tabbarController.btnAccount1.isSelected = false
        tabbarController.btnWishlist1.isSelected = false
        tabbarController.btnCart1.isSelected = false
        
        if tab == "0"
        {
            tabbarController.btnHome.isSelected = true
            tabbarController.btnHome1.isSelected = true
        }
        else if tab == "1"
        {
            tabbarController.btnCalendar.isSelected = true
            tabbarController.btnCalendar1.isSelected = true
        }
        else if tab == "2"
        {
            tabbarController.btnAccount.isSelected = true
            tabbarController.btnAccount1.isSelected = true
        }
        else if tab == "3"
        {
            tabbarController.btnWishlist.isSelected = true
            tabbarController.btnWishlist1.isSelected = true
        }
        else if tab == "4"
        {
            tabbarController.btnCart.isSelected = true
            tabbarController.btnCart1.isSelected = true
        }
        
        if self.arrcart.count > 0
        {
            let strCount =  String(format: "%d", CartCount)
            if strCount == "0"
            {
                self.tabbarController.lblCount.isHidden = true
            }
            else
            {
                self.tabbarController.lblCount.isHidden = false
                self.tabbarController.lblCount.text = strCount
            }
        }
        else
        {
            self.tabbarController.lblCount.isHidden = true
        }
        
        
        
    }
    
    
    func removeTabbar()
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!

        if ((window?.viewWithTag(111)) != nil)
        {
            let vc = window?.viewWithTag(111)
            vc?.removeFromSuperview()
        }
        
        tabbarController.view.removeFromSuperview()

    }
    
    func hideTabBar()
    {
        tabbarController.view.isHidden = true
    }
    
    func showTabBar() {
        tabbarController.view.isHidden = false
    }
    
    func bringTabbarToFront()
    {
        tabbarController.view.superview?.bringSubviewToFront(tabbarController.view)
    }
    
    //MARK:- add popup
    
    func addCustomPopup(_ completion: @escaping (_ dictSelect: NSDictionary) -> Void ,arrData:NSMutableArray,isLanguage:Bool ,isNationality:Bool , isCategories:Bool ,isProfession:Bool ,isother:Bool,_ controller: UIViewController?)
    {        
        if arrData.count > 0
        {
            var Storyboard = UIStoryboard()
            if UserDefaults.standard.bool(forKey: "arabic") == true
            {
                Storyboard = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
            }
            else
            {
                Storyboard = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
                
            }
            
            CountryPopup = Storyboard.instantiateViewController(withIdentifier: "CustomCountryPopup") as! CustomCountryPopup
            
            
            controller?.view.addSubview(CountryPopup.view)
            
            
            CountryPopup.openView({ (dictSelect) in
                completion(dictSelect)
                
            }, arrData: arrData, isLanguage: isLanguage, isNationality: isNationality, isCategories: isCategories, isProfession: isProfession, isother: isother)
        }
    }
    
    //MARK:- add sidebar
    
    func addSideBar(controller : UIViewController)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!

        var Storyboard = UIStoryboard()
        
        if UserDefaults.standard.bool(forKey: "arabic") == true
        {
            Storyboard = UIStoryboard(name: GlobalConstants.arabicStoryboard, bundle: nil)
        }
        else
        {
            Storyboard = UIStoryboard(name: GlobalConstants.englishStoryboard, bundle: nil)
            
        }
        sidebar = Storyboard.instantiateViewController(withIdentifier: "SideBarVC") as! SideBarVC
        
        sidebar.view.tag = 2226
        
        if controller is CMSVC
        {
           sidebar.strcomeFrom = "cms"
            
        }
        else if controller is ItemListVC
        {
            sidebar.strcomeFrom = "item"
            
        }
        else if controller is VenueListVC
        {
            sidebar.strcomeFrom = "venue"
            
        }
        else if controller is CategoryVC
        {
            sidebar.strcomeFrom = "category"
            
        }
        else
        {
            sidebar.strcomeFrom = ""
        }
        
//        controller.view.addSubview(sidebar.view)
        window!.addSubview(sidebar.view)
//        controller.addChildViewController(sidebar)
        sidebar.view.layoutIfNeeded()
//        controller.view.bringSubview(toFront: sidebar.view)
        
        var viewwidth : CGFloat = 0
        
        if (UIDevice.current.userInterfaceIdiom == .pad)
        {
           viewwidth = 350
        }
        else
        {
            viewwidth = 150

        }

        
        if UserDefaults.standard.bool(forKey: "arabic") == false
        {
            sidebar.viewTable.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width - viewwidth, height: self.sidebar.viewTable.frame.size.height)

        }
        else
        {
            sidebar.viewTable.frame=CGRect(x: UIScreen.main.bounds.size.width, y: sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width - viewwidth, height: self.sidebar.viewTable.frame.size.height);

        }

        UIView.animate(withDuration: 0.3, animations: { () -> Void in

            if UserDefaults.standard.bool(forKey: "arabic") == false
            {
                self.sidebar.viewTable.frame=CGRect(x: 0, y: self.sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width - viewwidth, height: self.sidebar.viewTable.frame.size.height);
            }
            else
            {
                self.sidebar.viewTable.frame=CGRect(x: viewwidth  , y: self.sidebar.viewTable.frame.origin.y, width: UIScreen.main.bounds.size.width - viewwidth, height: self.sidebar.viewTable.frame.size.height);

            }
        

            
        }  , completion:nil)
        
    }
    
    
    func removesidemenu()
    {
//        sidebar.Hidesidemenu()
    }
    
    
    
    
    //MARK: function Set font
    func setFontFamily(_ fontFamily: String, for view: UIView, andSubViews isSubViews: Bool)
    {
        if UserDefaults.standard.bool(forKey: "arabic") == false
        {
            if (view is UILabel)
            {
                let lbl = view as? UILabel
                if lbl?.tag == 25
                {
                    if let aSize = UIFont(name: GlobalConstants.kEnglishBold, size: (lbl?.font.pointSize)!) {
                        lbl?.font = aSize
                    }
                }
                else if lbl?.tag == 30
                {
                    if let aSize = UIFont(name: GlobalConstants.kEnglishRegular, size: 18) {
                        lbl?.font = aSize
                    }
                }
                else
                {
                    if let aSize = UIFont(name: GlobalConstants.kEnglishRegular, size: ((lbl?.font.pointSize)! + 2)) {
                        lbl?.font = aSize
                    }
                }
            }
            if (view is UITextView)
            {
                let txt = view as? UITextView
                
                if let aSize = txt?.font?.pointSize
                {
                    txt?.font = UIFont(name: GlobalConstants.kEnglishRegular, size: aSize)
                }
                
            }
            
            if (view is UITextField)
            {
                let txt1 = view as? UITextField
                
                if let aSize = txt1?.font?.pointSize
                {
                    txt1?.font = UIFont(name: GlobalConstants.kEnglishRegular, size: aSize)
                }
            }
            if (view is UIButton)
            {
                
            }
            
            if (view is UIImageView)
            {
                let imgview = view as? UIImageView
                
                if imgview?.tag == 15
                {
                    imgview?.image = imgview?.image!.withRenderingMode(.alwaysTemplate)
                    imgview?.tintColor = UIColor.darkGray

                }
                
            }
           
            if isSubViews
            {
                for sview: UIView in view.subviews
                {
                    setFontFamily(fontFamily, for: sview, andSubViews: true)
                }
            }
        }
        else
        {
            if (view is UILabel)
            {
                let lbl = view as? UILabel
                if lbl?.tag == 25
                {
                    if let aSize = UIFont(name: GlobalConstants.kArabicBold, size: (lbl?.font.pointSize)!)
                    {
                        lbl?.font = aSize
                    }
                }
                else if lbl?.tag == 30
                {
                    if let aSize = UIFont(name: GlobalConstants.kArabicRegular, size: 18) {
                        lbl?.font = aSize
                    }
                }
                else
                {
                    if let aSize = UIFont(name: GlobalConstants.kArabicRegular, size: ((lbl?.font.pointSize)!))
                    {
                        lbl?.font = aSize
                    }
                }
            }
            if (view is UITextView) {
                let txt = view as? UITextView

                if let aSize = txt?.font?.pointSize
                {
                    txt?.font = UIFont(name:GlobalConstants.kArabicRegular, size: aSize)
                }
            }
            if (view is UITextField)
            {
                let txt1 = view as? UITextField
                if let aSize = txt1?.font?.pointSize
                {
                    txt1?.font = UIFont(name: GlobalConstants.kArabicRegular, size: aSize)
                }
            }
            if (view is UIImageView)
            {
                let imgview = view as? UIImageView
                
                if imgview?.tag == 15
                {
                    imgview?.image = imgview?.image!.withRenderingMode(.alwaysTemplate)
                    imgview?.tintColor = UIColor.darkGray
                    
                }
                
            }
            if isSubViews
            {
                for sview: UIView in view.subviews {
                    setFontFamily(fontFamily, for: sview, andSubViews: true)
                }
            }
            
        }
        
    }
    
    //MARK:- logout api
    func callWsLogout()
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            var strToken = appDelegate.UUIDValue
            if strToken == ""
            {
                strToken = "1234"
            }
    
            let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wLogout,strToken)
    
            let dictPara = NSDictionary()
            WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
    
                DispatchQueue.main.async
                    {
    
                        print(result)
                        let strStatus = result["Message"] as? String
    
                        if strStatus == "Success"
                        {
                            UserDefaults.standard.set(false, forKey: GlobalConstants.kisLogin)
                          UserDefaults.standard.removeObject(forKey: "userid")
                            UserDefaults.standard.synchronize()
    
                        }
//                        else
//                        {
//                            let alertController = UIAlertController(title:kWarningEn, message:result["Message"] as? String, preferredStyle: .alert)
//
//                            // Create the actions
//                            let okAction = UIAlertAction(title:kOkEn, style: .default) {
//                                UIAlertAction in
//                                NSLog("OK Pressed")
//                            }
//
//                            // Add the actions
//                            alertController.addAction(okAction)
//
//                            // Present the controller
//                            alertController.show()
//                        }
    
                }
            }
    
    
    
    
        }
    
    //MARK:- Version check
    func newVersionPresent() -> Bool
    {
        let infoDict = Bundle.main.infoDictionary
        let appID = infoDict?["CFBundleIdentifier"] as? String
        let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(appID ?? "")")
        var data: Data? = nil
        
        if let anUrl = url
        {
            do {
                try data = Data(contentsOf: anUrl)
            }
            catch _ {
                // Error handling
            }
            
        }
        var itunesVersionInfo : NSDictionary!
        if let aData = data
        {
            itunesVersionInfo = try! JSONSerialization.jsonObject(with: aData, options: []) as! NSDictionary
        }
        if itunesVersionInfo != nil
        {
            let getArr = itunesVersionInfo["results"] as! NSArray
            
            if itunesVersionInfo["resultCount"] as? Int == 1
            {
                let arrAppStore = getArr.object(at: 0) as! Dictionary<AnyHashable, Any>
                let appStoreVersion =  String(format: "%@", arrAppStore["version"] as! CVarArg)
                let currentVersion = String(format: "%@", infoDict?["CFBundleShortVersionString"] as! CVarArg)
                if appStoreVersion.compare(currentVersion, options: .numeric, range: nil, locale: .current) == .orderedDescending
                {
                    return true
                }
            }
        }
        
        
        return false
    }
    
    //MARK:- area webservice
    
    func wscallforHomePageAlldata()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetNewHomePage,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        
                        self.dictHomePage = NSMutableDictionary(dictionary: result)

                    }
                    else
                    {
                        
                    }
                    
            }
            
            
        }
        
    }
    
    func wscallforSideMenu()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetSideMenu,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: true, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    //                    print(result)
                    let strStatus = result["Message"] as? String
                    
                    if strStatus == "Success"
                    {
                        let filterArray = NSMutableArray(array: (result["Filters"] as? Array)!)
                        
                        let secondArray : NSMutableArray = []
                        
                        if result["Parties"] != nil
                        {
                            let parties = NSMutableDictionary(dictionary: result["Parties"] as! [AnyHashable : Any])
                            secondArray.add(parties)
                        }
                        
                        if result["Vendors"] != nil
                        {
                            let parties = NSMutableDictionary(dictionary: result["Vendors"] as! [AnyHashable : Any])
                            secondArray.add(parties)
                        }
                        
                        if result["Gifts"] != nil
                        {
                            let Gift = NSMutableDictionary(dictionary: result["Gifts"] as! [AnyHashable : Any])
                            secondArray.add(Gift)
                            
                            
                            let giftAr = NSMutableArray(array: (result.value(forKeyPath: "Gifts.Categories") as! Array))
                            Manager.sharedInstance.SaveDatainTextFile(fileName: "gift", arrData: giftAr)
                            
                        }
                        
                        if result["Contents"] != nil
                        {
                            let content = NSMutableDictionary(dictionary: result["Contents"] as! [AnyHashable : Any])
                            secondArray.add(content)
                        }
                        
                        
                        for i in 0..<filterArray.count
                        {
                            let dict = NSMutableDictionary(dictionary: filterArray[i] as! [AnyHashable:Any])
                            
                            if dict["Type"] as? String == "List"
                            {
                                let aa2 = NSMutableArray(array: dict["commonDTO"] as! Array)
                                
                                Manager.sharedInstance.SaveDatainTextFile(fileName: "calenderType", arrData: aa2)
                                
                            }
                        }
                        
                        Manager.sharedInstance.SaveDatainTextFile(fileName: "filter", arrData: filterArray)
                        
                        Manager.sharedInstance.SaveDatainTextFile(fileName: "second", arrData: secondArray)
                        
                    }
                    else
                    {
                        
                    }
            }
        }
    }
    
    func wscallforArea()
    {
        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetAreas,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
            {
                self.arrArea = NSMutableArray(array: result["Areas"] as! Array)
            }
            
            
        }
        
    }
    func wscallforUserDetail()
    {
        let userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as! CVarArg)

        let strUrl = String(format: "%@%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetUserProfile,userId)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    print(result)
                    self.dictprofile = NSMutableDictionary(dictionary: result as! Dictionary)
                    let account = UIApplication.topViewController()
                    if (account is AccountVC)
                    {
                        let account1 = account as? AccountVC
                        account1?.viewWillLayoutSubviews()
                    }

            }
            
            
        }
        
    }
    
    func wscallforgetCart()
    {
        var userId = "0"
        if UserDefaults.standard.value(forKey: "userid") != nil
        {
            userId = String(format: "%@", UserDefaults.standard.value(forKey: "userid") as! CVarArg)
        }
        
        let strUrl = String(format: "%@%@/%@/%@", GlobalConstants.kBaseURL,GlobalConstants.wGetCart,userId,UserDefaults.standard.bool(forKey: "arabic") ? "A" : "E")
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.getWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in
            
            DispatchQueue.main.async
                {
                    self.CartCount = 0
                    self.arrcartOrder.removeAllObjects()
                    let dict = NSMutableDictionary(dictionary: result as! Dictionary)
                    self.arrcart = NSMutableArray(array: dict["CartItems"] as! Array)
                    
                    let arrReminder : NSMutableArray = []
                    for i in 0..<self.arrcart.count
                    {
                        let dict = self.arrcart[i] as! [String:Any]
                        
                        if dict["Cart_Item_Type"] as? NSNumber != 2
                        {
                            self.CartCount = self.CartCount + 1
                        }
                        
                        if dict["Cart_Item_Type"] as? NSNumber != 3
                        {
                            self.arrcartOrder.add(dict)
                        }
                        

                        if dict["Item_Type_Id"] as! NSNumber != 5 && dict["Item_Type_Id"] as! NSNumber != 1
                        {
                            let arrDate = dict["SelectedDates"] as! [[String:Any]]
                            for j in 0..<arrDate.count
                            {
                                let dictdate = arrDate[j]
                                let dict1 : NSDictionary = ["date":dictdate["Calender_Date"] as! String,"time":dict["Start_Time"] as! String ,"name":dict["Item_Name"] as! String]
                                
                                arrReminder.add(dict1)
                            }
                            
                        }
                        
                    }
                    
                    self.SaveDatainTextFile(fileName: "cart.txt", arrData: arrReminder)
                    
                    if self.arrcart.count > 0
                    {
                        let strCount =  String(format: "%d", self.CartCount)
                        if strCount == "0"
                        {
                            self.tabbarController.lblCount.isHidden = true
                        }
                        else
                        {
                            self.tabbarController.lblCount.isHidden = false
                            self.tabbarController.lblCount.text = strCount
                        }
                    }
                    else
                    {
                        self.tabbarController.lblCount.isHidden = true
                    }
                    
                   
            }
            
            
        }
        
    }
    
    func wscallforRemoveCart()
    {
        let strUrl = String(format: "%@%@", GlobalConstants.kBaseURL,GlobalConstants.wRemoveCart)
        
        let dictPara = NSDictionary()
        WebServiceManager.sharedInstance.postWebServiceWithAlamofire(strUrl, parameter: dictPara as! Dictionary<String, AnyObject>, isshowLoading: false, LoadingText: "") { (result) in

            DispatchQueue.main.async
                {
                    print(result)
                    self.wscallforgetCart()
            }
            
            
        }
        
    }
    
    
    
    
    func displayAddress(_ dict: NSMutableDictionary) -> String
    {
        let dict1 = dict.mutableCopy() as! NSMutableDictionary
        var strdata = ""
        
        let array = dict.allKeys(for: "")
        for i in 0..<(array.count ?? 0)
        {
            dict1.removeObject(forKey: array[i])
        }
        if dict1["AddressName"] != nil
        {
            if let aKey = dict1["AddressName"] {
                strdata = strdata + ("\(aKey)") + "\n"
            }
        }
        if dict1["AREA_NAME"] != nil {
            if let aKey = dict1["AREA_NAME"] {
                strdata = strdata + kArea + " : " + ("\(aKey)")
            }
        }
        if dict1["Block"] != nil {
            if let aKey = dict1["Block"] {
                strdata = strdata + " ," + kblock + " : " + ("\(aKey)")
            }
        }
        if dict1["Street"] != nil {
            if let aKey = dict1["Street"] {
                strdata = strdata + " ," + kStreet + " : " + ("\(aKey)")
            }
        }
       
        if dict1["HouseNo"] != nil {
            if let aKey = dict1["HouseNo"] {
                strdata = strdata + " ," + kHousNo + " : " + ("\(aKey)")
            }
        }
        if dict1["Avenue"] != nil {
            if let aKey = dict1["Avenue"] {
                strdata = strdata + " ," + kAvenue + " : " + ("\(aKey)")
            }
        }
        if dict1["Building"] != nil {
            if let aKey = dict1["Building"] {
                strdata = strdata + " ," + kBuilding + " : " + ("\(aKey)")
            }
        }
        if dict1["Floor"] != nil {
            if let aKey = dict1["Floor"] {
                strdata = strdata + " ," + kFloor + " : " + ("\(aKey)")
            }
        }
        if dict1["Apparment"] != nil {
            if let aKey = dict1["Apparment"] {
                strdata = strdata + " ," + kAppartment + " : " + ("\(aKey)")
            }
        }
//        if dict1["mobile_number"] != nil {
//            if let aKey = dict1["mobile_number"] {
//                strdata = strdata + ("\(aKey)")
//            }
//        }
        if dict1["ExtraDirection"] != nil {
            if let aKey = dict1["ExtraDirection"] {
                strdata = strdata + " ," + kExtraDirection + " : " + ("\(aKey)")
            }
        }

        return strdata
    }
    
    
    func displayAddress11(_ dict: NSMutableDictionary) -> String
    {
        let dict1 = dict.mutableCopy() as! NSMutableDictionary
        var strdata = ""
        
        let array = dict.allKeys(for: "")
        for i in 0..<(array.count ?? 0)
        {
            dict1.removeObject(forKey: array[i])
        }
        if dict1["AREA_NAME"] != nil {
            if let aKey = dict1["AREA_NAME"] {
                strdata = strdata + kArea + " : " + ("\(aKey)\n")
            }
        }
        if dict1["Block"] != nil {
            if let aKey = dict1["Block"] {
                strdata = strdata + " ," + kblock + " : " + ("\(aKey)")
            }
        }
        if dict1["Street"] != nil {
            if let aKey = dict1["Street"] {
                strdata = strdata + " ," + kStreet + " : " + ("\(aKey)")
            }
        }
        
        if dict1["HouseNo"] != nil {
            if let aKey = dict1["HouseNo"] {
                strdata = strdata + " ," + kHousNo + " : " + ("\(aKey)")
            }
        }
        if dict1["Avenue"] != nil {
            if let aKey = dict1["Avenue"] {
                strdata = strdata + " ," + kAvenue + " : " + ("\(aKey)")
            }
        }
        if dict1["Building"] != nil {
            if let aKey = dict1["Building"] {
                strdata = strdata + " ," + kBuilding + " : " + ("\(aKey)")
            }
        }
        if dict1["Floor"] != nil {
            if let aKey = dict1["Floor"] {
                strdata = strdata + " ," + kFloor + " : " + ("\(aKey)")
            }
        }
        if dict1["Apparment"] != nil {
            if let aKey = dict1["Apparment"] {
                strdata = strdata + " ," + kAppartment + " : " + ("\(aKey)")
            }
        }
        if dict1["ExtraDirection"] != nil {
            if let aKey = dict1["ExtraDirection"] {
                strdata = strdata + " ," + kExtraDirection + " : " + ("\(aKey)")
            }
        }
        if dict1["Mobile"] != nil {
            if let aKey = dict1["Mobile"] {
                strdata = strdata + "\n" + kMobNum + " : " + ("\(aKey)")
            }
        }

        
        return strdata
    }
    
    //MARK: Small Alert
    var viewAlert:UIView!
    
    func showSmallAlert(_ strText: String?)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
        let intWidth: Int = ((strText?.count ?? 0) > 17) ? ((strText?.count ?? 0) > 30) ? 280 : 220 : 150
        if window?.viewWithTag(101) != nil
        {
            self.hideViewandCompletion({ complete in
            })
            
        }
        
        viewAlert = UIView(frame: CGRect(x: ((window?.frame.size.width ?? 0.0) - CGFloat(intWidth)) / 2, y: (window?.frame.size.height ?? 0.0) - 130, width: CGFloat(intWidth), height: 40))
        viewAlert.backgroundColor = UIColor.black
        viewAlert.layer.cornerRadius = 10
        viewAlert.clipsToBounds = true
        viewAlert.tag = 101
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: viewAlert.frame.size.width, height: viewAlert.frame.size.height))
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.numberOfLines = 2
        lbl.text = strText
        lbl.font = UIFont(name: "Calibri", size: 16)
        viewAlert.addSubview(lbl)
        window?.addSubview(viewAlert)
        //        view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        viewAlert.alpha = 0.0
        UIView.animate(withDuration: 0.2, animations: {
            self.viewAlert.alpha = 1.0
            //            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }) { finished in
            //            UIView.animate(withDuration: 0.1, animations: {
            //                self.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            //            }) { finished in
            //                UIView.animate(withDuration: 0.1, animations: {
            //                    self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            //                }) { finished in
            //                    RunLoop.current.run(until: Date(timeIntervalSinceNow: 1.0))
            sleep(UInt32(2.0))
            self.hideViewandCompletion({ complete in
            })
            //                }
            //            }
        }
    }
    
    func hideViewandCompletion(_ complete: @escaping (_ complete: Bool) -> Void)
    {
        let window: UIWindow? = (UIApplication.shared.delegate?.window)!
        
        if ((window?.viewWithTag(101)) != nil)
        {
            
            viewAlert.alpha = 0.0
            viewAlert.removeFromSuperview()
            
        }
        complete(true)
        
        
        //        UIView.animate(withDuration: 0.1, animations: {
        //            self.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        //        }) { finished in
        //            UIView.animate(withDuration: 0.2, animations: {
        //                self.view.alpha = 0.0
        //                self.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        //                 self.view.removeFromSuperview()
        
        //            }) { finished in
        //                self.view.removeFromSuperview()
        //                complete(true)
        //            }
        
        //        }
    }
}

extension Collection where Iterator.Element == [String:Any] {
    func toJSONStringFilter(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:Any]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}


