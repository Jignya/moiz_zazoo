//
//  WebServiceManager.swift
//  Benihana
//
//  Created by Monali on 7/11/16.
//  Copyright © 2016 Monali. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WebServiceManager: NSObject
{
    class var sharedInstance: WebServiceManager {
        struct Static {
            static let instance: WebServiceManager = WebServiceManager()
        }
        return Static.instance
    }
    
    // MARK:- Alamofire
    
    func postWebServiceWithoutHeaderAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let urlString = strurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let myUrl = URL(string: urlString)
        
        AF.request(myUrl!, method: .post, parameters: parameter, encoding: JSONEncoding.default)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                //                print("Progress: \(progress.fractionCompleted)")
            }
            .validate(statusCode: 200..<300)
            
            .responseJSON { response in
                debugPrint(response)
                if (response.error) == nil
                {
                    let swiftyJsonVar = JSON(response.value!)
                    print(swiftyJsonVar)
                    completion(swiftyJsonVar.dictionaryObject! as NSDictionary)
                }
                else
                {
                    debugPrint(response.error)
                    
                }
                
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
        }
    }
    
    func postWebServiceWithAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
        {
            if isshowLoading
            {
                Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
            }
        }

        let urlString = strurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let myUrl = URL(string: urlString)
        
        let Key = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String
        
        let headers: HTTPHeaders = [
            GlobalConstants.kHeaderKey: Key,
            "Content-Type": "application/json"
        ]
        
        //  ///application/x-www-form-urlencoded
        AF.request(myUrl!, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
            }
            .validate(statusCode: 200..<300)

            .responseJSON { response in
                if (response.error) == nil
                {
                    
                    if var swiftyJsonVar = try? JSON(response.value!) as? JSON
                    {

                        completion(self.nullToNil(value:  swiftyJsonVar.dictionaryObject as AnyObject) as! NSDictionary)
                    }
                    else
                    {

                        let strmsg = String(format: "(%@) api response is not working", strurl)
                        SwiftLoggor.fileContent = SwiftLoggor.log(message:strmsg,event:ErroTypes.e)

                    }
                    
                    
                }
                else
                {
                    debugPrint(response.error)
                    SwiftLoggor.fileContent = SwiftLoggor.log(message:response.error.debugDescription,event:ErroTypes.e)


                }
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
                

        }
    }
    
    func postWebServiceForFilter(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let urlString = strurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let myUrl = URL(string: urlString)
        
        let Key = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String
        
        let headers: HTTPHeaders = [
            GlobalConstants.kHeaderKey: Key,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        //  ///application/x-www-form-urlencoded
        AF.request(myUrl!, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
            }
            .validate(statusCode: 200..<300)

            .responseJSON { response in
                if (response.error) == nil
                {
                    
                    if var swiftyJsonVar = try? JSON(response.value!) as? JSON
                    {
                        
                        completion(self.nullToNil(value:  swiftyJsonVar.dictionaryObject as AnyObject) as! NSDictionary)
                    }
                    else
                    {
                        
                        let strmsg = String(format: "(%@) api response is not working", strurl)
                        SwiftLoggor.fileContent = SwiftLoggor.log(message:strmsg,event:ErroTypes.e)
                        
                    }
                    
                    
                }
                else
                {
                    debugPrint(response.error)
                    SwiftLoggor.fileContent = SwiftLoggor.log(message:response.error.debugDescription,event:ErroTypes.e)
                    
                    
                }
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
                
                
        }
    }
    
    func getWebServiceWithAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let Key = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String

        let urlString = strurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let myUrl = URL(string: urlString)

        let headers: HTTPHeaders = [
            GlobalConstants.kHeaderKey: Key,
            "Content-Type": "application/json"
        ]
        
        AF.request(myUrl!, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                //                print("Progress: \(progress.fractionCompleted)")
            }
            .validate(statusCode: 200..<300)
            .responseJSON { response in
//                debugPrint(response)
                if (response.error) == nil
                {
                    if var swiftyJsonVar = try? JSON(response.value!) as? JSON
                    {

                        // Do something with non-optional value "result"
//                        swiftyJsonVar = JSON(response.result.value!)
                        completion(self.nullToNil(value:  swiftyJsonVar.dictionaryObject as AnyObject) as! NSDictionary)
                    }
                    else
                    {

                        let strmsg = String(format: "(%@) api response is not working", strurl)
                        SwiftLoggor.fileContent = SwiftLoggor.log(message:strmsg,event:ErroTypes.e)

                    }

                    
                }
                else
                {
                    debugPrint(response.error)
                    SwiftLoggor.fileContent = SwiftLoggor.log(message:response.error.debugDescription,event:ErroTypes.e)

                }
                
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
        }
    }
    
    func getWebServiceWithAlamofirewithPara(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let Key = UserDefaults.standard.object(forKey: GlobalConstants.kStoreKey) as! String
        
        let urlString = strurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let myUrl = URL(string: urlString)

        let headers: HTTPHeaders = [
            GlobalConstants.kHeaderKey: Key,
            "Content-Type": "application/json"
        ]
        
        AF.request(myUrl!, method: .get, parameters: parameter, encoding: JSONEncoding.default, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                //                print("Progress: \(progress.fractionCompleted)")
            }
            
            .validate(statusCode: 200..<300)
            
            .responseString { response in
                //                debugPrint(response)
                if (response.error) == nil
                {
                    if var swiftyJsonVar = try? JSON(response.value!) as? JSON
                    {
                        // Do something with non-optional value "result"
                        //                        swiftyJsonVar = JSON(response.result.value!)
                        completion(self.nullToNil(value:  swiftyJsonVar.dictionaryObject as AnyObject) as! NSDictionary)
                    }
                    else
                    {
                        
                        let strmsg = String(format: "(%@) api response is not working", strurl)
                        SwiftLoggor.fileContent = SwiftLoggor.log(message:strmsg,event:ErroTypes.e)
                        
                    }
                    
                    
                }
                else
                {
                    debugPrint(response.error)
                    
                }
                
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
        }
    }
    
    func nullToNil(value : AnyObject?) -> AnyObject?
    {
        if value is NSNull
        {
            return nil
        }
        else
        {
            return value
        }
    }
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
        
    
    
    // MARK:--------------------

   
    
    fileprivate func post(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request, method: "POST", completion: completion)
    }
    
    fileprivate func put(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request, method: "PUT", completion: completion)
    }
    
    fileprivate func get(_ request: NSMutableURLRequest, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        dataTask(request, method: "GET", completion: completion)
    }
    
    fileprivate func dataTask(_ request: NSMutableURLRequest, method: String, completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    completion(true, json as AnyObject)
                } else {
                    completion(false, json as AnyObject)
                }
            }
        }) .resume()
    }
    
    
    fileprivate func clientURLRequest(_ path: String, params: Dictionary<String, AnyObject>? = nil) -> NSMutableURLRequest {
        //        let request = NSMutableURLRequest(URL: NSURL(string: "http://api.example.com/"+path)!)
        let request = NSMutableURLRequest(url: URL(string: String(format:"%@%@",GlobalConstants.kBaseURL,path))!)
        
        if let params = params {
            var paramString = ""
            for (key, value) in params {
                let escapedKey = key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let escapedValue = value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                paramString += "\(escapedKey)=\(escapedValue)&"
            }
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = paramString.data(using: String.Encoding.utf8)
        }
        
        return request
    }
    
    
    func postWebServiceForOcrImageParseWithAlamofire(_ strurl:String,parameter:Dictionary<String, AnyObject>,isshowLoading:Bool,LoadingText:String,completion: @escaping (_ result: NSDictionary) -> Void)
    {
        DispatchQueue.main.async
            {
                if isshowLoading
                {
                    Helper.sharedInstance.ShowProgressBarWithText(LoadingText)
                }
        }
        
        let urlString = strurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let myUrl = URL(string: urlString)
        
        let headers: HTTPHeaders = [
            "apikey": "7fdfe9c8ed88957",
        ]
//        "Content-Type": "application/json"

        
        AF.request(myUrl!, method: .post, parameters: parameter, encoding: JSONEncoding.prettyPrinted, headers: headers)
            .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                //                print("Progress: \(progress.fractionCompleted)")
            }
            .validate(statusCode: 200..<300)

            .responseJSON { response in
                //                debugPrint(response)
                if (response.error) == nil
                {
                    let swiftyJsonVar = JSON(response.value!)
                    print(swiftyJsonVar)
                    completion(swiftyJsonVar.dictionaryObject as! NSDictionary)
                    
                }
                else
                {
                    debugPrint(response.error)
                    
                    
                }
                if isshowLoading
                {
                    Helper.sharedInstance.DismissProgressBar()
                }
                
                
        }
    }
    
    
}

