//
//  ConstantLabel.swift
//  IHallab
//
//  Created by Monali on 12/6/16.
//  Copyright © 2016 Monali. All rights reserved.
//

import Foundation

struct CommanConstants {
    // Constant define here.
    
//    static let kWarningEn = "Warning"
    static let kWarningAr = "تحذير"
    static let kYesEn = "Yes"
    static let kYesAr = "نعم"
    static let kNoEn = "No"
    static let kNoAr = "لا"
    static let kSuccessEn = "Successfully"
    static let kSuccessAr = "بنجاح"
  
    
    static let kUId = "UserId"
    static let kUserEmail = "UserEmail"
    static let kUserFirstName = "UserFName"
    
    static var kSelectedLanguage = String(format: "%@",UserDefaults.standard.object(forKey: "SelectedLanguage") as! String)
    
}
var kLoading:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "جاري التحميل"
    }
    else
    {
        return "Loading"
    }
}
var kYes:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نعم"
    }
    else
    {
        return "Yes"
        
    }
}
var kNo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لا"
    }
    else
    {
        return "NO"
        
    }
}
var kWarningEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تحذير"
    }
    else
    {
        return "Warning"
        
    }
}

var kDeleteEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حذف"
    }
    else
    {
        return "Delete"
        
    }
}

var kAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تنبيه"
    }
    else
    {
        return "Alert"
        
    }
}


var kEnglish:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "انجليزي"
    }
    else
    {
        return "English"
        
    }
}
var kArab:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عربي"
    }
    else
    {
        return "عربي"
        
    }
}

var kOkEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نعم"
    }
    else
    {
        return "OK"
        
    }
}
var kShareFeedback:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مشاركه ملاحظاتك"
    }
    else
    {
        return "Share your feedback"
        
    }
}

var kCategory:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الفئة"
    }
    else
    {
        return "Category"
        
    }
}

var kFavorite:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المفضلات"
    }
    else
    {
        return "Favorites"
        
    }
}
var kMobNum:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الهاتف المحمول"
    }
    else
    {
        return "Mobile"
        
    }
}
var kEmail:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "البريد الالكتروني"
    }
    else
    {
        return "Email"
        
    }
}
var kFullname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسم الكامل"
    }
    else
    {
        return "Full Name"
        
    }
}
var kNoItem:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لا يوجد سلعة"
    }
    else
    {
        return "No Items available"
    }
}

var kPleaseenterFullnameEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال الاسم الكامل"
    }
    else
    {
        return "Please enter fullname"
        
    }
}
var kPleaseenteryouremailaddressEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال البريد الالكتروني"
    }
    else
    {
        return "Please enter your email"
        
    }
}


var kNameValidationEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يمكنك إدخال الحد الأدنى 3 حرف والحد الأقصى 30 حرف للاسم"
    }
    else
    {
        return "You can enter min 3 character and max 30 character for name"
        
    }
}
var kPleaseentervalidMobnum:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال رقم الهاتف المحمول صحيح"
    }
    else
    {
        return "Please enter valid mobile number"
        
    }
}
var kEmailValidationEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى إدخال البريد الإلكتروني الصحيح"
    }
    else
    {
        return "Please enter a valid email"
        
    }
}
var kfeedbackValidationEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال ملاحظاتك"
    }
    else
    {
        return "Please enter your feedback"
        
    }
}
var kfeedback:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ردود الفعل"
    }
    else
    {
        return "Feedback"
        
    }
}
var kfeedbackSuccess:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ردود الفعل المقدمة بنجاح"
    }
    else
    {
        return "Feedback submitted succesfully"
        
    }
}
var kremoveAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ستتم إزالة جميع العناصر المفضلة إذا قمت بالضغط على حسنا"
    }
    else
    {
        return "All favorite items will be removed if you press ok"
        
    }
}
var kCancel:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الغاء"
    }
    else
    {
        return "Cancel"
        
    }
}
var kConfirmOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تأكيد الطلب"
    }
    else
    {
        return "Confirm Order"
        
    }
}
var kaddtoOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اضافة للأمر"
    }
    else
    {
        return "Add to Order"
        
    }
}
var kPreview:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "معاينة"
    }
    else
    {
        return "Preview"
        
    }
}
var kTotal:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المجموع"
    }
    else
    {
        return "Total"
        
    }
}
var kTotalAmount:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اجمالي المبلغ"
    }
    else
    {
        return "Total Amount"
        
    }
}

var kCurrency:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "د.ك"
    }
    else
    {
        return "KD"
        
    }
}
var kQty:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الكمية"
    }
    else
    {
        return "Qty"
        
    }
}
var kPrice:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "السعر"
    }
    else
    {
        return "Price"
    }
        
}

var aplaceOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return " هل انت متأكد من إتمام الطلب"
        
    }
    else
    {
        return "Are you sure you want to place order?"
    }
    
}
var kThankyou:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شكرا!"
    }
    else
    {
        return "Thank you!"
    }
    
}
var aConfirmOrderSuccess:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "طلبك مؤكد!"
    }
    else
    {
        return "Your Order Confirmed!"
    }
    
}
var aEnterPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال كلمة السر"
    }
    else
    {
        return "Please enter your password"
    }
    
}

var kNorecord:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لا توجد بيانات"
    }
    else
    {
        return "No data found"
    }
    
}
var kOrderMsg:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أجل معرف الخاص بك هو"
    }
    else
    {
        return "Your Order id is"
    }
    
}
var kreset:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إعادة تعيين"
    }
    else
    {
        return "RESET"
    }
    
}
var kNext:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التالي"
    }
    else
    {
        return "NEXT"
    }
    
}
var kentertocontinue:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أدخل كلمة المرور للمتابعة"
    }
    else
    {
        return "ENTER PASSWORD TO CONTINUE"
    }
    
}


var kselectQty:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى تحديد الكمية"
    }
    else
    {
        return "Select your quantity"
    }
    
}
var kAddOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أضف طلب"
    }
    else
    {
        return "Add Order"
    }
    
}

var kPleaseenteryourmobileEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال رقم الهاتف المحمول"
    }
    else
    {
        return "Please enter your mobile number"
        
    }
}
var kPleaseenteryourpasswordEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال كلمة السر"
    }
    else
    {
        return "Please enter your password"
        
    }
}

var kPasswordValidEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يجب أن تحتوي كلمة المرور على 8 أحرف على الأقل"
    }
    else
    {
        return "Password should contain atleast 8 characters"
        
    }
}
var kPleaseenteryourConfirmpasswordEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال كلمة السر مرة اخرى"
    }
    else
    {
        return "Please re-enter  password"
        
    }
}
var kpasswordmismatchEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلمة السر غير مطابقة"
    }
    else
    {
        return "Password does not match"
        
    }
}
var kYouhavesuccessfullyregistered:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم التسجيل بنجاح"
    }
    else
    {
        return "You have successfully registered"
        
    }
}
var kPleaseenteryourNameEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال الاسم"
    }
    else
    {
        return "Please enter name"
        
    }
}
var kPleaseenterAddresssname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال اسم للعنوان"
    }
    else
    {
        return "Please enter address name"
        
    }
}
var kPleaseSelectArea:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال المنطقة"
    }
    else
    {
        return "Please select area"
        
    }
}
var kPleaseSelectblock:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال القطعة"
    }
    else
    {
        return "Please enter block"
    }
}
var kPleaseenterStreet: String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال الشارع"
    }
    else
    {
        return "Please enter street name"
    }
}
var kPleaseenterhouse: String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال المنزل"
    }
    else
    {
        return "Please enter house name"
    }
}
var kPleaseenteraprtmnt: String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى ادخال رقم الشقة"
    }
    else
    {
        return "Please enter appartment"
    }
}
var kviewDetails:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شاهد التفاصيل"
    }
    else
    {
        return "View Detail"
        
    }
}
var kDeleteadd:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل انت متأكد من حذف هذا العنوان؟"
    }
    else
    {
        return "Are you sure you want to delete address?"
    }
        
}
var aOrderPlaced:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لقد تم الطلب بنجاح.رقم الطلب"
    }
    else
    {
        return "Your order has been placed successfully.Your order Id is"
    }
    
}

var kTerms:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الشروط والأحكام"
    }
    else
    {
        return "Terms & Condition"
    }
    
}
var kAbout:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عن زازو"
    }
    else
    {
        return "About us"
    }
    
}
var kDelPolicy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سياسة التسليم"
    }
    else
    {
        return "Delivery Policy"
    }
    
}
var kreturnPolicy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سياسة الاسترجاع"
    }
    else
    {
        return "Return Policy"
    }
    
}
var kPaynow:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ادفع الان"
    }
    else
    {
        return "Pay Now"
    }
    
}
var kOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "طلب"
    }
    else
    {
        return "Order"
    }
    
}
var kPassword:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلمة السر"
    }
    else
    {
        return "Password"
    }
    
}
var kLogin:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل الدخول"
    }
    else
    {
        return "Login"
    }
}
var kForgotPW:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return " هل نسيت كلمة المرور؟"
    }
    else
    {
        return "Forgot Password?"
    }
}
var kLoginwith:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل الدخول مع"
    }
    else
    {
        return "LOGIN WITH"
    }
}
var kDontAccount:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ليس لديك حساب؟"
    }
    else
    {
        return "Don't have an account?"
    }
}
var kregister:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل"
    }
    else
    {
        return "Register"
    }
}
var kIhaveAcnt:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لدي حساب"
    }
    else
    {
        return "I have an account"
    }
}

var ksignupwith:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سجل مع"
    }
    else
    {
        return "SIGN UP WITH"
    }
}
var kName:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسم"
    }
    else
    {
        return "Name"
    }
}
var kSubmit:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إرسال"
    }
    else
    {
        return "Submit"
    }
}
var kForgotTitle:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نسيت كلمة المرور هل"
    }
    else
    {
        return "FORGOT PASSWORD"
    }
}
var kForgotDesc:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ادخل بريدك الالكتروني المسجل سوف نقوم بأرسال رابط كلمة السر"
    }
    else
    {
        return "Enter your registered email address.we will send you reset password link or new password."
    }
}
var kSelcat:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اختر الفئات"
    }
    else
    {
        return "Select Categories"
    }
}
var kSearch:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بحث"
    }
    else
    {
        return "Search"
    }
}
var kApplynow:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "قدم الآن"
    }
    else
    {
        return "Apply Now"
    }
}
var kBuyNow:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اشتر الان"
    }
    else
    {
        return "Buy Now"
    }
}
var kQuantity:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الكمية"
    }
    else
    {
        return "Quantity"
    }
}
var kItemSize:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مقاس السلعة"
    }
    else
    {
        return "Item Size"
    }
}
var kItemname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اسم السلعة"
    }
    else
    {
        return "Item Name"
    }
}
var kItemDetail:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تفاصيل السلعة"
    }
    else
    {
        return "Item Detail"
    }
}
var kItemDesc:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "وصف السلعة"
    }
    else
    {
        return "Item Description"
    }
}
var kAddtocart:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اضف الى السلة"
    }
    else
    {
        return "Add to cart"
    }
}
var kGotoCart:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الذهاب الى السلة"
    }
    else
    {
        return "Go to cart"
    }
}
var kHome:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الصفحة الرئيسية"
    }
    else
    {
        return "Home"
    }
}
var kMyOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "طلباتي"
    }
    else
    {
        return "My Order"
    }
}

var kMycart:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سلتي"
    }
    else
    {
        return "My Cart"
    }
}
var kwish:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "قائمة الاماني"
    }
    else
    {
        return "Wishlist"
    }
}
var kNotification:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاشعارات"
    }
    else
    {
        return "Notification"
    }
}
var ksetting:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاعدادات"
    }
    else
    {
        return "Setting"
    }
}

var kAppInside:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التطبيق في الداخل"
    }
    else
    {
        return "App Inside"
    }
}
var kChangePW:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تغيير كلمة السر"
    }
    else
    {
        return "Change Password"
    }
}
var kSavedAdd:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "العناوين المسجلة"
    }
    else
    {
        return "Saved Address"
    }
}
var kprofile:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تفصيل الحساب"
    }
    else
    {
        return "Profile Details"
    }
}
var klang:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اللغة"
    }
    else
    {
        return "Language"
    }
}
var kExchange:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسترجاع والتبديل"
    }
    else
    {
        return "Exchange & Return"
    }
}
var kprivacy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سياسة الخصوصية"
    }
    else
    {
        return "Privacy Policy"
    }
}
var kShipping:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "معلومات التوصيل"
    }
    else
    {
        return "Shipping Information"
    }
}
var ksell:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بيع معنا"
    }
    else
    {
        return "Sell with us"
    }
}
var kcontact:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اتصل بنا"
    }
    else
    {
        return "Contact us"
    }
}
var kOrders:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الطلبات"
    }
    else
    {
        return "Orders"
    }
}
var kNocategory:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لم يتم العثور على القائمة"
    }
    else
    {
        return "No Categories found"
    }
}
var kmostRecent:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الطلبات الحديثة"
    }
    else
    {
        return "Most recent order"
    }
}
var kTotalPaid:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اجمالي المبلغ المدفوع"
    }
    else
    {
        return "Total Paid"
    }
}
var kOrderSummary:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ملخص الطلب"
    }
    else
    {
        return "Order Summary"
    }
}
var kDate:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التاريخ"
    }
    else
    {
        return "Date"
    }
}
var kTime:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الوقت"
    }
    else
    {
        return "Time"
    }
}

var kOrderNumber:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم الطلب"
    }
    else
    {
        return "Order Number"
    }
}
var kPaidBy:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الدفع عن طريق"
    }
    else
    {
        return "Paid By"
    }
}

var kDelCharges:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رسوم التوصيل"
    }
    else
    {
        return "Delivery Charges"
    }
}
var kShoppingBag:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سلة التسوق"
    }
    else
    {
        return "Shopping Bag"
    }
}
var kBagEmpty:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "السلة فارغة"
    }
    else
    {
        return "Your bag is empty"
    }
}

var kPlaceOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اتمام طلب"
    }
    else
    {
        return "Place Order"
    }
}
var ksize:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المقاس"
    }
    else
    {
        return "Size"
    }
}
var kaddress:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "العنوان"
    }
    else
    {
        return "Address"
    }
}
var kaddressname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اسم العنوان"
    }
    else
    {
        return "Address Name"
    }
}
var kArea:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المنطقة"
    }
    else
    {
        return "Area"
    }
}
var kblock:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "القطعة"
    }
    else
    {
        return "Block"
    }
}
var kStreet:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الشارع"
    }
    else
    {
        return "Street"
    }
}
var kStreetname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اسم / رقم الشارع"
    }
    else
    {
        return "Street Name/Number"
    }
}
var kHousNo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم المنزل"
    }
    else
    {
        return "House No"
    }
}
var kHousNoOptional:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم المنزل (اختياري)"
    }
    else
    {
        return "House No(Optional)"
    }
}
var kAvenue:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الجادة"
    }
    else
    {
        return "Avenue"
    }
}
var kAvenueoptional:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الجادة (اختياري)"
    }
    else
    {
        return "Avenue(Optional)"
    }
}
var kBuilding:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المبنى"
    }
    else
    {
        return "Building"
    }
}
var kBuildingOptioanl:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المبنى (اختياري)"
    }
    else
    {
        return "Building(Optional)"
    }
}
var kFloor:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الدور"
    }
    else
    {
        return "Floor"
    }
}
var kFloorOptional:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الدور (اختياري)"
    }
    else
    {
        return "Floor(Optional)"
    }
}
var kAppartment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الشقة"
    }
    else
    {
        return "Appartment"
    }
}
var kExtraDir:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إرشادات أخرى (اختياري)"
    }
    else
    {
        return "Extra direction(Optional)"
    }
}
var kSaveChanges:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حفظ التغييرات"
    }
    else
    {
        return "Save Changes"
    }
}
var kNewPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلمة سر جديدة"
    }
    else
    {
        return "New password"
    }
}
var kConfirmPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تأكيد كلمة السر"
    }
    else
    {
        return "Confirm password"
    }
}
var kcod:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نقدا"
    }
    else
    {
        return "Cash On Delivery"
    }
}
var kKnetPayment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كي - نت"
    }
    else
    {
        return "Knet Payment"
    }
}
var kCredit:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بطاقة الائتمان"
    }
    else
    {
        return "Credit Card"
    }
}
var kItems:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "منتج"
    }
    else
    {
        return "Items"
    }
}
var kTotalPayable:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اجمالي المدفوع"
    }
    else
    {
        return "Total Payable"
    }
}
var kChange:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تغير"
    }
    else
    {
        return "Change"
    }
}
var kPaymentoption:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "طريقة الدفع"
    }
    else
    {
        return "Payment Options"
    }
}
var kDeliverTo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "توصيل الى"
    }
    else
    {
        return "Deliver To"
    }
}
var kPayment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الدفع"
    }
    else
    {
        return "Payment"
    }
}
var kTransaction:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "رقم العملية"
    }
    else
    {
        return "Transaction Number"
    }
}
var kOrderStatus:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حالة الطلب"
    }
    else
    {
        return "Order Status"
    }
}
var kSave:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "حفظ"
    }
    else
    {
        return "Save"
    }
}
var kPaymentreceipt:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الفاتورة"
    }
    else
    {
        return "Payment Receipt"
    }
}
var kDone:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم"
    }
    else
    {
        return "Done"
    }
}
var knotLoggedin:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "غير مسجل حاليا انت"
    }
    else
    {
        return "You are not currently logged in"
    }
}
var kGuest:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "زائر"
    }
    else
    {
        return "Guest"
    }
}
var kSignIn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التسجيل"
    }
    else
    {
        return "Sign In"
    }
}
var kItemAdded:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم إضافة المنتج بنجاح"
    }
    else
    {
        return "Item added to cart successfully"
    }
}
var kRemoveItem:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "متأكد من حذف المنتج؟"
    }
    else
    {
        return "Are you sure want to remove this item from cart?"
    }
}
var kUnread:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "غير مقروء"
    }
    else
    {
        return "Unread"
    }
}
var kcancelorder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إذا غادرت الصفحة سيتم حذف طلبك. اضغط نعم للاستمرار"
    }
    else
    {
        return "If you leave this page,Your order will be cancelled.Click yes to continue."
    }
}
var kReceiveOrder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سوف تتلقي الطلب وفقا لعمليه التسليم لدينا"
    }
    else
    {
        return "you will receive order as per our standard delivery process"
    }
}
var kForpayment:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "للدفع"
    }
    else
    {
        return "For the payment"
    }
}

var kThanku:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شكرا"
    }
    else
    {
        return "THANK YOU"
    }
}

var kViewHistory:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عرض سجل الدفع"
    }
    else
    {
        return "View your payment history"
    }
}

var kError:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هناك خطأ بالدفع، يرجى المحاولة مرة أخرى مع طريقة دفع مختلفة."
    }
    else
    {
        return "There was an error with payment, please try again with different payment method."
    }
}

var kRedirecttoHome:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "سيتم إعادة توجيهك إلى شاشة الدفع مرة أخرى بعد 5 ثوان..."
    }
    else
    {
        return "You will be redirect to payment screen again after 5 seconds..."
    }
}

var kConnecttoInternet:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاتصال بإنترنت يرجى"
    }
    else
    {
        return "Connect to the Internet"
    }
}
var kCheckConnection:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أنت غير متصل. التحقق من الاتصال"
    }
    else
    {
        return "You're offline. Check your connection"
    }
}
var kretry:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اعاده المحاوله"
    }
    else
    {
        return "Retry"
    }
}
var kExtraDirection:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إرشادات اخرى"
    }
    else
    {
        return "Extra Direction"
    }
}
var kwelcome:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مرحبا بكم"
    }
    else
    {
        return "Welcome"
    }
}
var kStandard:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التوصيل المحلي"
    }
    else
    {
        return "Standard Delivery"
    }
}
var kproductAmt:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "قيمة السلعة"
    }
    else
    {
        return "Product Amount"
    }
}
var kLogoutalert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل انت متأكد من تسجيل الخروج؟"
    }
    else
    {
        return "Are you sure want to logout?"
    }
}
var kfororder:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لطلبك"
    }
    else
    {
        return "For your order"
    }
}
var kFav:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المفضلات"
    }
    else
    {
        return "Favourites"
    }
}
var kVersion:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الإصدار"
    }
    else
    {
        return "Version"
    }
}
var kCustomize:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يعدل أو يكيف"
    }
    else
    {
        return "Customize"
    }
}
var kPleaseentercustomize:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى إدخال التخصيص"
    }
    else
    {
        return "Please enter customization"
        
    }
}
var kSuccessSocial:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تسجيل الدخول بنجاح مع تم"
    }
    else
    {
        return "You have successfully login with"
    }
}
var kTapToRegister:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اضغط على التسجيل للتواصل معنا"
    }
    else
    {
        return "Tap Register to connect with us"
    }
}

var kPleaseEnterdata:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال البيانات أولاً"
    }
    else
    {
        return "Please insert data first"
    }
}
var kPleaseSelectgender:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء اختيار نوع الجنس"
    }
    else
    {
        return "Please select gender"
        
    }
}
var kPleaseSelectdob:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى تحديد تاريخ الميلاد"
    }
    else
    {
        return "Please select date of birth"
        
    }
}
var kPleaseenterChildname:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الطفل الرجاء إدخال اسم"
    }
    else
    {
        return "Please enter child name"
        
    }
}
var kPleaseenterChildInfo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال اسم الطفل"
    }
    else
    {
        return "Please enter child info"
        
    }
}
var klogout:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الخروج"
    }
    else
    {
        return "Logout"
        
    }
}
var kMyAddress:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "عناويني"
    }
    else
    {
        return "My Addresses"
    }
}
var kpersonal:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المعلومات الشخصية"
    }
    else
    {
        return "Personal Information"
    }
}
var kChildinfo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "معلومات الطفل"
    }
    else
    {
        return "Child Information"
    }
}
var kPleaseenterlastNameEn:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال الاسم الاخير"
    }
    else
    {
        return "Please enter last name"
        
    }
}
var kAgeGrp:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الفئة العمرية"
    }
    else
    {
        return "Age Group"
        
    }
}

var kDeleteItem:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل متاكد من حذف السلعة؟"
    }
    else
    {
        return "Are you sure you want to remove item?"
    }
    
}

var kAppartmentOptional:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "شقة (اختيارية)"
    }
    else
    {
        return "Appartment(Optional)"
    }
}
var kCash:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نقدا"
    }
    else
    {
        return "Cash"
    }
}
var kremoveFav:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل متأكد من حذف السلعة من قائمة الاماني ؟"
    }
    else
    {
        return "Are you sure you want to remove item from wishlist?"
    }
    
}
var kotherClass:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "صفوف أخرى من قبل"
    }
    else
    {
        return "Other classes by"
    }
    
}
var kpopular:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الأكثر طلبا"
    }
    else
    {
        return "Popular right now"
    }
    
}
var kBookadventure:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "احجز مغامرة أطفالك القادمة"
    }
    else
    {
        return "Book your kids next adventure"
    }
    
}
var kDetails:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تفاصيل"
    }
    else
    {
        return "Details"
    }
    
}
var kinfo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "معلومات"
    }
    else
    {
        return "Info"
    }
    
}
var kattending:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "من الذي سيحضر؟"
    }
    else
    {
        return "Who is attending?"
    }
    
}
var kselectchild:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اختر الطفل"
    }
    else
    {
        return "Select child"
    }
    
}
var klocation:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الموقع"
    }
    else
    {
        return "Location"
    }
    
}
var kaboutProvider:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "معلومات عن المزود"
    }
    else
    {
        return "About the Provider"
    }
    
}
var kSeldates:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اختر التواريخ"
    }
    else
    {
        return "Select dates"
    }
    
}

var ksignup:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التسجيل"
    }
    else
    {
        return "SIGN UP"
    }
    
}
var kasGuest:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "زائر في زازو"
    }
    else
    {
        return "Zazoo as a guest"
    }
    
}
var kaddChildren:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اضف طفل"
    }
    else
    {
        return "Add children"
    }
    
}
var kDOB:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تاريخ الميلاد"
    }
    else
    {
        return "Date Of Birth"
    }
    
}
var kAge:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "العمر"
    }
    else
    {
        return "Age"
    }
    
}
var klastName:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الاسم الاخير"
    }
    else
    {
        return "Last name"
    }
    
}
var kfPWalert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال بريدك الإلكتروني المسجل"
    }
    else
    {
        return "Please enter your registered email"
    }
    
}


var kGo:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الذهاب"
    }
    else
    {
        return "Go"
    }
    
}
var kClearFilter:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مسح الفلتر"
    }
    else
    {
        return "Clear Filters"
    }
    
}
var kdatesselected:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التواريخ المحددة"
    }
    else
    {
        return "dates selected"
    }
    
}
var kcleardates:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مسح كافة التواريخ"
    }
    else
    {
        return "Clear all dates"
    }
    
}
var kbook:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "احجز"
    }
    else
    {
        return "BOOK"
    }
    
}

var kenterNewPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء إدخال كلمة السر الجديدة"
    }
    else
    {
        return "Please enter new password"
    }
    
}
var kenterConfirmPw:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء تأكيد كلمة السر"
    }
    else
    {
        return "Please enter confirm password"
    }
    
}

var kPleaseSelDates:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء تحديد التواريخ"
    }
    else
    {
        return "Please select dates"
    }
    
}

var kPleaseSelChild:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجاء اختيار الطفل"
    }
    else
    {
        return "Please select child"
    }
    
}
var kcalendar:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "التقويم"
    }
    else
    {
        return "Calendar"
    }
    
}
var kAccount:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الحساب"
    }
    else
    {
        return "Account"
    }
    
}
var kWishlist:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "قائمة الاماني"
    }
    else
    {
        return "Wishlist"
    }
    
}
var kBag:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "السلة"
    }
    else
    {
        return "Bag"
    }
    
}
var kcelebrate:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "دعونا نحتفل"
    }
    else
    {
        return "let's celebrate"
    }
    
}
var kClear :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "ازالة"
    }
    else
    {
        return "Clear"
    }
    
}

var kagree :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أوافق على"
    }
    else
    {
        return "I agree to"
    }
    
}

var kZazooandvendor :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "زازو ومزودي الخدمات"
    }
    else
    {
        return "Zazoo & Vendor's"
    }
    
}

var kZazoo :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "زازو"
    }
    else
    {
        return "Zazoo's"
    }
    
}
var kVendor :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مزودي الخدمات"
    }
    else
    {
        return "Vendor's"
    }
    
}
var kand :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "و"
    }
    else
    {
        return "and"
    }
    
}
var kback :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرجوع"
    }
    else
    {
        return "Back"
    }
    
}
var kClassHistory :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تاريخ النشاطات"
    }
    else
    {
        return "Class History"
    }
    
}
var kGender :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الجنس"
    }
    else
    {
        return "Gender"
    }
    
}
var kDropIn :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلاس"
    }
    else
    {
        return "Drop In"
    }
}
var kSemester :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "فصل"
    }
    else
    {
        return "Semester"
    }
    
}
var kVenue :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مكان"
    }
    else
    {
        return "Venue"
    }
    
}
var kCamp :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مخيم"
    }
    else
    {
        return "Camp"
    }
    
}
var kClass :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كلاس"
    }
    else
    {
        return "Class"
    }
    
}
var kChildName :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "اسم الطفل"
    }
    else
    {
        return "Child Name"
    }
    
}

var kAddEventCalendarAlert :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل انت متأكد من إضافة النشاطات في تقويم الهاتف؟"
    }
    else
    {
        return "Are you sure you want to add multiple dates event in device default calendar ?"
    }
    
}
var kDiscountAmount :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مبلغ الخصم"
    }
    else
    {
        return "Discount Amount"
    }
    
}
var kCoupanCode :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "كود القسيمة"
    }
    else
    {
        return "Coupan Code"
    }
    
}
var kAgealert :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "العمر غير صالح لهذه الفئة."
    }
    else
    {
        return "age is not valid for this class."
    }
    
}
var kcodeAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الرمز الترويجي غير صالح لهذه المنتجات."
    }
    else
    {
        return "Promo code is not valid for these activities."
    }
    
}
var kcodeAmountAlert:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "لتطبيق الكود المطلوب هو الحد الأدنى لمبلغ الطلب"
    }
    else
    {
        return "For applying code required min order amount is"
    }
    
}
var kVendorTerms:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "قراءة الشروط والأحكام"
    }
    else
    {
        return "Read our terms and condition"
    }
    
}

var kOpenGoogleMap:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل تريد فتح خريطة جوجل؟"
    }
    else
    {
        return "Do you want to open google map ?"
    }
    
}
var kGenderalert :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هذه الفئة متاحة ل"
    }
    else
    {
        return "This class is available for"
    }
    
}
var kVendorLbl :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بائع"
    }
    else
    {
        return "Vendor"
    }
    
}
var kActivity :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "نشاط"
    }
    else
    {
        return "Activity"
    }
    
}
var kStatus :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "الحالة"
    }
    else
    {
        return "Status"
    }
    
}
var kSeatAvailable :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المقاعد غير متوفرة لهذا العنصر"
    }
    else
    {
        return "Seats are not available for this item"
    }
    
}
var kConfirm :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تؤكد"
    }
    else
    {
        return "Confirm"
    }
    
}
var kRefund :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "إعادة مال"
    }
    else
    {
        return "Refund"
    }
    
}
var kRefundAlert :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يرجى اختيار نشاط واحد على الأقل"
    }
    else
    {
        return "Please select atleast one activity"
    }
    
}
var kConfirmAlert :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل تريد بالتأكيد استرداد الأنشطة المحددة؟"
    }
    else
    {
        return "Are you sure want to refund selected activities?"
    }
    
}
var krefundSuccess :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "تم وضع طلب استرداد الأموال الخاص بك بنجاح ، وسوف نتصل بك قريبًا."
    }
    else
    {
        return "Your refund request has been placed successfully, we will contact you soon."

    }
    
}
var kSoldOut :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "بيعت كلها"
    }
    else
    {
        return "Sold Out"
        
    }
    
}
var kSeatLeft :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "مقاعد"
    }
    else
    {
        return "seats are left"
        
    }
    
}
var kOnly :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "فقط"
    }
    else
    {
        return "Only"
        
    }
    
}
var kAlreadyAdded :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "أضيف بالفعل في عربة"
    }
    else
    {
        return "Already added in cart"
        
    }
    
}
var kVenueNotAvailable :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "المكان غير متاح لهذا التاريخ"
    }
    else
    {
        return "Venue is not available for this date"
        
    }
    
}
var kEnterCode :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "من فضلك ادخل الكود"
    }
    else
    {
        return "Please enter code"
        
    }
    
}
var kDeleteNot :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "هل تريد بالتأكيد حذف الإشعار؟"
    }
    else
    {
        return "Are you sure want to delete notification?"
        
    }
    
}
var kRemoveItemFromBag :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "يبدو أن بعض العناصر في حقيبتك غير متوفرة ، يرجى إزالتها للمتابعة"
    }
    else
    {
        return "It seems like some item in your bag is not available, Please remove it to continue"
        
    }
    
}
var kSession :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "جلسة"
    }
    else
    {
        return "Session"
        
    }
    
}
var kTo :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "to"
    }
    else
    {
        return "to"
    }
    
}
var kavailable:String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return "متاح"
    }
    else
    {
        return "available"
    }
    
}
var k :String
{
    if UserDefaults.standard.bool(forKey: "arabic")
    {
        return ""
    }
    else
    {
        return ""
        
    }
    
}

